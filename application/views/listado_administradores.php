<h3 class="page-title">Administradores <small>Listado</small></h3>
      <section class="row"style="text-align: center;margin-left: 150px">
        <?=$cuadros;?>
      </section>
      <section class="row">
        <div id="listado">
          <div id="list-cab">
            <i class="fa fa-list-alt"></i><span><?php echo $titulo; ?></span>
          </div>
          <div id="filtros">
              <form method="post" action="<?php echo base_url()?>index.php?c=administradores">
            <div>
              <span>Usuario</span>
              <input type="text" name="busq_administrador" value="<?=$busq_administrador;?>">
            </div>
            <div>
              <span>Correo</span>
              <input type="text" name="busq_correo" value="<?=$busq_correo;?>">
            </div>
            <div>
              <span>Departamento</span>
              <select name="id_departamento">
              </select>
            </div>
            <div>
              <span>Estado</span>
              <select name="estado">
                <option <?php if ($estado=="2") {echo "selected";}?> value="2">Todos</option>
                <option <?php if ($estado=="1") {echo "selected";}?> value="1">Habilitados</option>
                <option <?php if ($estado=="0") {echo "selected";}?> value="0">Desahabilitados</option>
              </select>
            </div>
            <div>
              <span>cantidad</span>
              <select name="limite">
                <option <?php if ($limite=="5") {echo "selected";}?> value="5">5</option>
                <option <?php if ($limite=="10") {echo "selected";}?> value="10">10</option>
                <option <?php if ($limite=="50") {echo "selected";}?> value="50">50</option>
              </select>
            </div>
            <div><input type="submit" value="buscar" name="buscar"></div>
            </form>
          </div>
          <div id="list-cuerpo">
            <table class="listado">
              <tr>
                <th><input type="checkbox"></th>
                <th>Foto</th>
                <th>Usuario</th>
                <th>Correo</th>
                <th>Departamento</th>
                <th>Estado</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
              <?php
             
                foreach ($listado as $key) {
              ?>
              <tr>
                <td><input type="checkbox"></td>
                <td id="fot-list">
                  <?php
                  if ($key[6]!="") {echo '<img src="'.$base_url.'img/usuarios/'.$key[6].'">';}
                  else{echo '<img src="'.$base_url.'img/usuarios/usu.jpg">';}
                ?>
                </td>
                <td><?=$key[1];?></td>
                <td><?=$key[2];?></td>
               <?php if($key[5]==""){?>
                <td><?=$key[4];?></td>
               <?php }  else {               
                ?>
                 <td><?=$key[5];?></td>
              <?php
               }
              if ($key[3]==1) {
              ?>
                <td><a><i class="fa fa-check green"></i></a></td>
              <?php
              }else{
              ?>
                <td><a><i class="fa fa-times red"></i></a></td>
              <?php
              }
              ?>
                <td><a href="<?=$base_url?>index.php?c=administradores&m=admin_config&id=<?=$key[0]?>"><i class="fa fa-pencil-square-o"></a></i></td>
                <td><a href="<?=$base_url?>index.php?c=administradores&m=delete&id=<?=$key[0]?>"><i class="fa fa-trash-o"></i></a></i></td>
              </tr>
              <?php
                }
              ?>
            </table>
            <div id="list-pie">
              <?=$paginador?>
            </div>  
          </div>
        </div>
      </section>
      <script type="text/javascript">
        $(document).ready(function(){
          function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
          }
       
          <?php if (isset($id_departamento)) { ?>
            departamento(<?=$id_departamento?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          
        })
      </script>
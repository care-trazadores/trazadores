<h3 class="page-title"><?=$titulo?></h3>
      <section class="row">
        <div id="listado">
          <div id="list-cab">
            <i class="fa fa-list-alt"></i><span><?php echo $titulo_filtro; ?></span>
          </div>
          <div id="filtros">
              <form method="post" action="<?php echo base_url()?>index.php?c=log&m=encuestas">
            <div>
              <span>Usuario</span>
              <input type="text" name="busq_usuario" value="<?=$busq_usuario;?>">
            </div>
            <div>
              <span>Departamento</span>
              <select name="id_departamento">
              </select>
            </div>
            <div>
              <span>Provincia</span>
              <select name="id_provincia">
              </select>
            </div>
            <div>
              <span>Distrito</span>
              <select name="id_distrito">
              </select>
            </div>
            <div>
              <span>cantidad</span>
              <select name="limite">
                <option <?php if ($limite=="5") {echo "selected";}?> value="5">5</option>
                <option <?php if ($limite=="10") {echo "selected";}?> value="10">10</option>
                <option <?php if ($limite=="50") {echo "selected";}?> value="50">50</option>
              </select>
            </div>
            <div>
              <span>Localidad</span>
              <input type="text" name="busq_localidad" value="<?=$busq_localidad;?>">
            </div>
            <div>
              <span>Accion</span>
              <select name="busq_accion">
                <option value="">Todo</option>
                <option <?php if ($busq_accion=="Creacion") {echo "selected";}?> value="Creacion">Creacion</option>
                <option <?php if ($busq_accion=="Actualizacion") {echo "selected";}?> value="Actualizacion">Actualizacion</option>
                <option <?php if ($busq_accion=="Eliminacion") {echo "selected";}?> value="Eliminacion">Eliminacion</option>
              </select>
            </div>
            <div><input type="submit" value="buscar" name="buscar"></div>
            </form>
          </div>
          <div id="list-cuerpo">
            <table class="listado">
              <tr>
                <th>ID log</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Acciòn</th>
                <th>ID encuesta</th>
                <th>Localidad</th>
                <th>ID usuario</th>
                <th>usuario</th>
              </tr>
              <?php
                foreach ($registros as $key) {
                  echo '<tr>
                <td>'.$key->id_log_encuesta.'</td>
                <td>'.$key->fecha.'</td>
                <td>'.$key->hora.'</td>
                <td>'.$key->accion.'</td>
                <td>'.$key->id_encuesta.'</td>
                <td>'.$key->localidad.'</td>
                <td>'.$key->id_usuario.'</td>
                <td>'.$key->usuario.'</td></tr>';
                }
              ?>
            </table>
            <div id="list-pie">
              <?=$paginador;?>
            </div>  
          </div>
        </div>
      </section>

<?php
  foreach ($footer_js as $js) {
    echo '<script src="'.$base_url.'js/'.$js.'"></script>';
  }
?>
<script type="text/javascript">
$(document).ready(function(){
  <?php if (isset($id_departamento)) { ?>
            departamento(<?=$id_departamento?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if (isset($id_departamento) and isset($id_provincia) and $id_departamento!="" and $id_provincia!="") { ?>
            provincia(<?=$id_departamento?>,<?=$id_provincia?>);
          <?php }elseif(isset($id_departamento) and $id_departamento!=""){ ?>
            provincia(<?=$id_departamento?>,null);
          <?php };?>
          <?php if (isset($id_provincia) and isset($id_distrito) and $id_provincia!="" and $id_distrito!="") { ?>
            distrito(<?=$id_provincia?>,<?=$id_distrito?>);
          <?php }elseif(isset($id_provincia) and $id_provincia!=""){ ?>
            distrito(<?=$id_provincia?>,null);
          <?php };?>
})  
</script>

      
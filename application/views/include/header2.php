<!DOCTYPE html>
<html lang="es">
<head>
<title><?=$title?></title>
<meta charset="utf-8" />
<?php
	foreach ($css as $cs) {
		echo "<link href='".$base_url."css/".$cs."' rel='stylesheet' type='text/css'/>";
	}
?>
<script type="text/javascript">
	var base_url="<?=$base_url;?>"
</script>
<?php
	foreach ($js as $j) {
		echo '<script src="'.$base_url.'js/'.$j.'"></script>';
	}
?>
</head>
<body>
	<header>
		<div class="head-con">
			<div class="nav-top">
				<ul>
					<li>
						<a href="<?=$base_url?>" <?php if ($controlador=="home") {echo "class='selecte'";}?>>Home</a>
					</li>
					<li>
						<a href="<?=$base_url?>index.php?c=presentacion" <?php if ($controlador=="presentacion") {echo "class='selecte'";}?>>Presentación</a>
					</li>
					<li>
						<a href="<?=$base_url?>index.php?c=presentacion&m=trazador"  <?php if ($controlador=="trazador") {echo "class='selecte'";}?>>Indicadores Trazadores</a>
					</li>
				</ul>
			</div>
			<div class="sesion-top">
				<form method="post" action="<?=$base_url?>index.php?c=login&m=sesion">
					<span>
						<label>Usuario:</label>
						<input type="text" name="username"></input>
					</span>
					<span>
						<label>Contraseña:</label>
						<input type="password" name="password">
					</span>
					<div class="iniciar">
						<input class="btn boton" type="submit" name="iniciar" value="iniciar">
						<a class="btn boton" href="<?=$base_url?>index.php?c=presentacion&m=registro">Registrate</a>
					</div>
				</form>
			</div>
		</div>
	</header>
	<script type="text/javascript">
		
		function departamento(id_departamento){
		  $.ajax({
		    type: "POST",
		    url: "<?=$base_url?>index.php?c=distritos&m=departamento",
		    data: {id_departamento:id_departamento}
		  }).done(function( data ) {
		    $("select[name=id_departamento]").html(data);
		  }); 
		}
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
                $(document).ready(function(){
          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });
        })
	</script>
<!DOCTYPE html>
<html lang="es">
<head>
    <title><?= $title;?></title>
    <meta charset="utf-8" />
    <?php foreach ($css as $a) { ?>
      <link rel="stylesheet" href="<?=$base_url?>css/<?=$a?>" />
    <?php } ?>
    <?php foreach ($js as $b) { ?>
      <script src="<?=$base_url?>js/<?=$b?>"></script>
    <?php } ?>
    <script type="text/javascript">
        var base_url="<?=$base_url?>"
    </script>
</head>
<body>
  <section class="page-header navbar navbar-fixed-top">
    <?=$nav;?>
  </section>
   <section id="rigth" class="page-container">
    <nav id="menu" class="page-sidebar" style="position: relative;">
        <ul>
          <?=$menus;?>
        </ul>
        <div style=" position: absolute;width: 90%;bottom: 3px;left: 1%;"><img src="<?=$base_url?>img/cosude.png"></div>
    </nav>
    <section id="container" class="page-content">
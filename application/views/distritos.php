
<script type="text/javascript">
    var distrito;
    
    function  EditDistrito(id_distrito) {

        $.ajax({
            url: "<?= $base_url ?>index.php?c=Iniciodist&m=edit_distrito",
            data: {id_distrito: id_distrito},
            type: 'post',
            dataType: 'json',
            success: function (r) {
                distrito = r.idProv;
                if (r.estado) {
                    var p = r.data;
                    $('#txtId_distrito').val(r.idDist);
                    $('#txtDistrito').val(r.distrito);
                    $('#id_departamentos').val(r.idDepa);
                    depa();
//                    $('#id_provincias').val(r.idProv);

                } else {
                    alert("Sin datos--");
                    mensaje('success', r.mensaje);
                }

            }

        });

    }
    function DeleteDistrito($this, id_distrito) {
        var $tr = $this.closest('tr');

        if (!confirm('Desea Eliminar el Registro ')) {
            return false;
        }

        $.ajax({
            url: '<?= $base_url ?>index.php?c=Iniciodist&m=delete_distrito',
            data: {id_distrito: id_distrito},
            type: 'post',
            dataType: 'json',
            success: function (r) {
                $tr.hide();
                mensaje('success', r.mensaje);




            }

        });
    }
    function mensaje(msn, a) {
        var titulo, html, clase;
        switch (a) {
            case 1:
                titulo = 'OK.!';
                clase = 'msn_success';
                break;
            case 2:
                titulo = 'ERROR.!';
                clase = 'msn_error';
                break;
            case 3:
                titulo = 'ALERTA.!';
                clase = 'msn_warn';
                break;
            case 4:
                titulo = 'INFO.!';
                clase = 'msn_info';
                break;
            default:
                titulo = 'AVISO.!';
                clase = 'msn_warn';
                break;
        }
        html = "<b>" + titulo + "</b><br><label>" + msn + "</label>";
        $("#div_msn").attr('class', '').addClass(clase);
        $("#div_msn").html(html).fadeIn('fast').delay(3000).fadeOut('slow');
    }
</script>
<h3 class="page-title"><small></small></h3>
<section class="row"style="text-align: center;margin-left: 150px">


</section>

<section class="row">
    <div id="listado">
        <div id="list-cab">
            <i class="fa fa-list-alt"></i><span><?php echo $titulo2; ?></span>
        </div>

        <div id="filtros2">
            <form method="post" action="<?php echo base_url() ?>index.php?c=iniciodist&m=insertar_distrito">

                <div class="cod" style="display: none">
                    <span>Id Distrito</span>
                    <input type="text" name="txtId_distrito" id="txtId_distrito"value="<?php
                    foreach ($maxdistrito as $p) {
                        echo $p->max + 1;
                    }
                    ?>" >
                </div> 


                <div>
                    <span>Region</span>
                    <select name="id_departamentos" id="id_departamentos">                       

                    </select>
                </div>
                <div>
                    <span>Provincia</span>
                    <select name="id_provincias" id="id_provincias" required="required">                       

                    </select>
                </div>
                <div>
                    <span>Distrito</span>
                    <input type="text" name="txtDistrito"id="txtDistrito" required="required" value="" placeholder="Edit Distrito"/>
                </div> 
                <div><input type="submit" value="Guardar" name="Guardar" class="btn btn-warning"></div>
            </form>
        </div>
        <div id="list-cuerpo">
            <div id="filtros">
                 <h3 style="background-color: #7fd17f;color: #ffffff;padding: 5px 20px;margin: 0 10px;text-align: center;font-size: 18px">Buscar Distrito</h3>
                <form method="post" action="<?php echo base_url() ?>index.php?c=iniciodist">
                    
                    <div>
                        <span>Region</span>
                        <select name="id_departamentodis" id="id_departamentodis">

                        </select>
                    </div>
                    <div>
                        <span>Provincia</span>
                        <select name="id_provinciadis" id="id_provinciadis">

                        </select>
                    </div>
                    <div>
                        <span>Distrito</span>
                        <input type="text" name="distrito" placeholder="Buscar Distrito" value="<?php echo $distrito; ?>">
                    </div>
                    <div><input type="submit" value="Buscar Distrito" name="buscar" class="btn btn-info"></div>
                </form>
            </div>
            <table class="listado">
                <tr>
                    <th>Item</th>
                    <th>Region</th>
                    <th>Provincia</th>
                    <th>Distrito</th>                   
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
                <?php
                $i = 1;
                foreach ($listado as $key) {
                    ?>
                    <tr>

                        <td><?= $i; ?></td>
                        <td><?= $key->departamento; ?></td>
                        <td><?= $key->provincia; ?></td>
                        <td><?= $key->distrito; ?></td>                       
                        <td><a onclick="EditDistrito('<?php echo $key->idDist; ?>')"><i class="fa fa-pencil-square-o"></a></i></td>
                        <td><a onclick="DeleteDistrito($(this), '<?php echo $key->idDist; ?>')"><i class="fa fa-trash-o"></i></a></i></td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
            </table>
            <div id="list-pie">
                <?= $paginador ?>
            </div>  
        </div>
    </div>
</section>




<script type="text/javascript">
    $(document).ready(function () {
        function departamento(id_departamento) {
            $.ajax({
                type: "POST",
                url: "<?= $base_url ?>index.php?c=distritos&m=departamento",
                data: {id_departamento: id_departamento}
            }).done(function (data) {
                $("select[id=id_departamentos]").html(data);
//                $("select[name=id_departamento]").html(data);
            });
        }
        function departamentodis(id_departamentodis) {
            $.ajax({
                type: "POST",
                url: "<?= $base_url ?>index.php?c=distritos&m=departamento",
                data: {id_departamento: id_departamentodis}
            }).done(function (data) {
                $("select[id=id_departamentodis]").html(data);
            });
        }
        function provinciadis(id_departamento, id_provincia) {
            $.ajax({
                type: "POST",
                url: "<?= $base_url ?>index.php?c=distritos&m=provincia",
                data: {id_departamento: id_departamento, id_provincia: id_provincia}
            }).done(function (data) {
                $("select[name=id_provinciadis]").html(data);
            });
        }
//        function provincia(id_departamento, id_provincia) {
//            $.ajax({
//                type: "POST",
//                url: "<?= $base_url ?>index.php?c=distritos&m=provincia",
//                data: {id_departamento: id_departamento, id_provincia: id_provincia}
//            }).done(function (data) {
//                $("select[name=id_provincia]").html(data);
//            });
//        }
        function provincias(id_departamentos, id_provincias) {
            $.ajax({
                type: "POST",
                url: "<?= $base_url ?>index.php?c=distritos&m=provincia",
                data: {id_departamento: id_departamentos, id_provincia: id_provincias}
            }).done(function (data) {
                $("select[id=id_provincias]").html(data);
            });
        }
//        function distrito(id_provincia, id_distrito) {
//            $.ajax({
//                type: "POST",
//                url: "<?= $base_url ?>index.php?c=distritos&m=distrito",
//                data: {id_provincia: id_provincia, id_distrito: id_distrito}
//            }).done(function (data) {
//                $("select[name=id_distrito]").html(data);
//            });
//        }
<?php if (isset($id_departamento)) { ?>
            departamento(<?= $id_departamento ?>);
<?php } else { ?>
            departamento();
<?php }; ?>
<?php if (isset($id_departamentodis)) { ?>
            departamentodis(<?= $id_departamentodis ?>);
<?php } else { ?>
            departamentodis();
<?php }; ?>
   


<?php if (isset($id_departamentodis) and isset($id_provinciadis)) { ?>
            provinciadis(<?= $id_departamentodis ?>,<?= $id_provinciadis ?>);
<?php } elseif (isset($id_departamentodis)) { ?>
            provinciadis(<?= $id_departamentodis ?>, null);
<?php }; ?>
    


        $("select[name=id_departamento]").change(function () {
            var departamento = $(this).val();
            provincia(departamento, null);
        });
        $("select[name=id_departamentos]").change(function () {
            var departamento = $(this).val();
            provincias(departamento, null);
        });
        $("select[name=id_departamentodis]").change(function () {
            var departamento = $(this).val();
            provinciadis(departamento, null);
        });
        $("select[name=id_provincia]").change(function () {
            var provincia = $(this).val();
            distrito(provincia, null);
        });

    });
    function depa() {
        $(document).ready(function () {
            // Así accedemos al Valor de la opción seleccionada
            var id_departamentos = $("#id_departamentos").val();
//    alert(id_departamentos);
            $.ajax({
                type: "POST",
                url: "<?= $base_url ?>index.php?c=distritos&m=provincia",
                data: {id_departamento: id_departamentos, id_provincia: ""}
            }).done(function (data) {

                $("select[id=id_provincias]").html(data);
                $('#id_provincias').val(distrito);
            });

            // Si seleccionamos la opción "Texto 1"
            // nos mostrará por pantalla "1"
        });


    }
</script>
<?php
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
    return $rgb; //returns an array with the rgb values
}
	if ($archivo=="pdf") {
	/*require("application/libraries/fpdf17/fpdf.php");
        class PDF extends FPDF{
            // Cabecera de página
            function cabecera_re(){
                $this->Cell(null,5,utf8_decode(""),0,0,'C');
                $this->SetY(5);
                $this->SetX(5);
                $this->SetFont('Arial','B',12);
                $this->MultiCell(90,10,utf8_decode("Sistema trazadores SIAS"),0,'L',false);
                $this->SetY(5);
                $this->SetX(95);
                $this->SetFont('Arial','B',18);
                $this->MultiCell(100,10,utf8_decode("Reporte general"),0,'C',false);
                $this->SetY(5);
                $this->SetX(195);
                $this->SetFont('Arial','B',12);
                $this->MultiCell(90,10,utf8_decode("Fecha de impresion: ".date("d-m-Y")),0,'R',false);
            }
            function titulos($tablas){
                $this->Cell(null,5,utf8_decode(""),0,0,'C');
                $posy=20;
                $posx=5;
                $a=0;
                $this->SetFont('Arial','B',9);
                // Movernos a la derecha
                $this->Cell(30,10,utf8_decode("asd"),0,0,'C');
                // Salto de línea
                $this->Ln(20);
                $color=hex2rgb(58,157,202);
                $this->SetFillColor(58,157,202);
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(22,30,utf8_decode("Localizacion"),1,'C',true);
                $posx=$posx+22;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(22,30,utf8_decode("Localidad"),1,'C',true);
                $posx=$posx+22;
                foreach ($tablas[0]["result"] as $ke) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    
                    $total_texto_pregunta=strlen($tablas[0]["result"][$a]["titulo"]);

                    if ($a==0 || $a==1 || $a==2) {
                        $ancho_cuadro=30;
                    }elseif ($a==6) {
                        $ancho_cuadro=15;
                    }elseif ($a==7 || $a==9 || $a==10 ) {
                        $ancho_cuadro=10;
                    }elseif ($a==8|| $a==5) {
                        $ancho_cuadro=7.5;
                    }elseif ($a==5 || $a==3 || $a==4) {
                        $ancho_cuadro=6;
                    }else{
                        $ancho_cuadro=5;
                    }
                     
                    $this->MultiCell(22,$ancho_cuadro,utf8_decode($tablas[0]["result"][$a]["titulo"]),1,'C',true);
                    $posx=$posx+22;
                    $a++;
                }
            }
            function tamrespu($texto){
                $total_texto_pregunta=strlen($texto);
                $ancho=$total_texto_pregunta/13;
                if ($ancho>=0 and $ancho<=1) {
                    $ancho_cuadro=45;
                }elseif ($ancho>1 and $ancho<=2) {
                    $ancho_cuadro=22.5;
                }elseif ($ancho>2 && $ancho<=3) {
                    $ancho_cuadro=15;
                }elseif ($ancho>3 && $ancho<=4) {
                    $ancho_cuadro=11.25;
                }elseif ($ancho>4 && $ancho<=5) {
                    $ancho_cuadro=9;
                }elseif ($ancho>5 && $ancho<=6) {
                    $ancho_cuadro=7.5;
                }elseif ($ancho>6 && $ancho<=7) {
                    $ancho_cuadro=6.42;
                }elseif ($ancho>7 && $ancho<=8) {
                    $ancho_cuadro=5.6;
                }/*elseif ($ancho>8 && $ancho<=9) {
                    $ancho_cuadro=5.5;
                }else{
                    $ancho_cuadro=5;
                }
                return $ancho_cuadro;
            }
            function respuestas($tablas){
                $this->SetFont('Arial','B',7);
                $posy=50;
                $posx=5;
                $inicio_encuesta=0;
                $posicion=0;
                foreach ($tablas as $key) {
                    if ($posicion==3) {
                        $this->AddPage("L","A4");
                        $posicion=1;
                        $posy=30;
                        $posx=5;
                    }else{
                        $posicion=$posicion+1;
                    }
                    $e=0;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(22,$this->tamrespu(strtolower($tablas[$inicio_encuesta]["Localizacion"])),utf8_decode(strtolower($tablas[$inicio_encuesta]["Localizacion"])),1,'C',false);
                    $posx=$posx+22;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(22,$this->tamrespu(strtolower($tablas[$inicio_encuesta]["comunidad"])),utf8_decode(strtolower($tablas[$inicio_encuesta]["comunidad"])),1,'C',false);
                    $posx=$posx+22;
                    foreach ($tablas[$inicio_encuesta]["result"] as $ke) {
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(22,$this->tamrespu(strtolower($tablas[$inicio_encuesta]["result"][$e]["respuesta"])),utf8_decode(strtolower($tablas[$inicio_encuesta]["result"][$e]["respuesta"])),1,'C',false);
                        $posx=$posx+22;
                        $e++;
                    }
                    $posy=$posy+45;
                    $posx=5;
                    $inicio_encuesta++;         
                }
                
                
            }
            function Footer(){
                // Posición: a 1,5 cm del final
                $this->SetY(-15);
                // Arial italic 8
                $this->SetFont('Arial','I',8);
                // Número de página
                $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
            }
        }    
        $pdf=new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage("L","A4");
        $pdf->cabecera_re();
        $pdf->titulos($tablas);
        $pdf->respuestas($tablas);
        $pdf->Output();*/

        require("application/libraries/dompdf/dompdf_config.inc.php");
        $codigoHTML='
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Lista</title>
        </head>
        <body>
        <div align="center">
            <table style="margin-bottom:20px;width:95%;">';
            $codigoHTML.='<tr>';
                $codigoHTML.='<td>Sistemas Sias Trazadores</td>';
                $codigoHTML.='<td style="text-align:center;font-size:18px;">Reporte General</td>';
                $codigoHTML.='<td style="text-align:right">Fecha de impresion:'.date('d-m-Y').'</td>';
            $codigoHTML.='</tr>';
        $codigoHTML.='</table>';
        $codigoHTML.='<table cellpadding="0" cellspacing="0" style="width:95%;white-space: normal;">';
           $codigoHTML.='<tr style="font-size:14px;">';
                $codigoHTML.='<th style="border:1px solid #000;padding:10px 3px;color: white;background-color: #4594B9;">'.utf8_encode("Localización").'</th>';
                $codigoHTML.='<th style="border:1px solid #000;padding:10px 3px;color: white;background-color: #4594B9;">Localidad</th>';
                $a=0;
                foreach ($tablas[0]["result"] as $ke) {
                    $codigoHTML.='<th style="border:1px solid #000;padding:10px 3px;color: white;background-color: #4594B9;">'.utf8_encode($tablas[0]["result"][$a]["titulo"]).'</th>';
                    $a++;
                }
            $codigoHTML.='</tr>';
            
                $inicio_encuesta=0;
                $posicion=0;
                foreach ($tablas as $key) {
                    $codigoHTML.='<tr style="font-size:12px;">';
                    $e=0;
                    $codigoHTML.='<td style="border:1px solid #000;padding:2px">'.utf8_encode($tablas[$inicio_encuesta]["Localizacion"]).'</td>';
                    $codigoHTML.='<td style="border:1px solid #000;padding:2px">'.$tablas[$inicio_encuesta]["comunidad"].'</td>';
                    foreach ($tablas[$inicio_encuesta]["result"] as $ke) {
                        $codigoHTML.='<td style="text-align:center;border:1px solid #000;padding:2px">'.utf8_encode($tablas[$inicio_encuesta]["result"][$e]["respuesta"]).'</td>';
                        $e++;
                    }
                    $inicio_encuesta++;   
                    $codigoHTML.='</tr>';      
                }
           
        $codigoHTML.='</table></div></body>';

        $codigoHTML=utf8_decode($codigoHTML);
        $dompdf=new DOMPDF();
        $dompdf->set_paper("letter","landscape");
        $dompdf->load_html($codigoHTML);
        ini_set("memory_limit","128M");
        $dompdf->render();
        $dompdf->stream("ListadoEmpleado.pdf", array("Attachment" => 0));
	}else{
		$posicion = array("A","B","C","D","E","F","G","H","I","J",
            "K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        require "application/libraries/phpexcel/PHPExcel.php";
        $objPHPExcel=new PHPExcel();
        $archivo="general.xls";
        $borders= array(
            'borders'=> array(
                'allborders'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('argb'=>'FF000000'
                )
            ),
            'outline'     => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN,
              'color' => array(
                 'argb' => 'FF000000'
              )
            )
          )
        );
        $objPHPExcel->getProperties()->setCreator("weblocalhost")
            ->setLastModifiedBy("weblocalhost")
            ->setTitle("Reporte XLS")
            ->setSubject("Réporte")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial Narrow');
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->mergeCells('B3:'.$posicion[count($tablas[0]["result"])+2].'3');
        $objPHPExcel->getActiveSheet()->getStyle('B3:'.$posicion[count($tablas[0]["result"])+2].'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B3:'.$posicion[count($tablas[0]["result"])+2].'3')->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->mergeCells('B4:I4');
        $objPHPExcel->getActiveSheet()->getStyle('B4:I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B4:I4')->getFont()->setSize(16);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B2', "Fecha de exportacion: ".date("d-m-Y"))
            ->setCellValue('C2', "Hora de exportacion: ".date("H:i"))
            ->setCellValue('B3', "Sistema trazadores");
        $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
        $pos=3;
		foreach ($tablas[0]["result"] as $key) {
		    $objPHPExcel->getActiveSheet()->getColumnDimension($posicion[$pos])->setAutoSize(true);
		    $pos++;
		}        
		$i=6;
        $objPHPExcel->getActiveSheet()
            ->getStyle('B'.$i.':'.$posicion[count($tablas[0]["result"])+2].$i)
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FF00CCFF');
        $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':'.$posicion[count($tablas[0]["result"])+2].$i)->applyFromArray($borders);
        $objPHPExcel->setActiveSheetIndex(0)
           	->setCellValue('B'.$i, "Localización")
           	->setCellValue('C'.$i, "Localidad");
       	$pos=3;
       	$ini=0;
       	foreach ($tablas[0]["result"] as $key) {
           	$objPHPExcel->setActiveSheetIndex(0)
           	->setCellValue($posicion[$pos].$i, $tablas[0]["result"][$ini]["titulo"]);
           	$pos++;
           	$ini++;
       	}
       	$i++;
        
            $inicio_encuesta=0;
                foreach ($tablas as $key) {
                    $pos=1;
                    $objPHPExcel->getActiveSheet()->getStyle($posicion[$pos].$i)->applyFromArray($borders);
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($posicion[$pos].$i,$tablas[$inicio_encuesta]["Localizacion"]);
                    $pos++;
                    $objPHPExcel->getActiveSheet()->getStyle($posicion[$pos].$i)->applyFromArray($borders);
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($posicion[$pos].$i,$tablas[$inicio_encuesta]["comunidad"]);
                    $pos++;
                    $posicion_res=0;
                    foreach ($tablas[$inicio_encuesta]["result"] as $ke) {
                        $objPHPExcel->getActiveSheet()->getStyle($posicion[$pos].$i)->applyFromArray($borders);
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($posicion[$pos].$i, $tablas[$inicio_encuesta]["result"][$posicion_res]["respuesta"]);
                        $posicion_res++;
                        $pos++;
                    }
                    $inicio_encuesta++;
                    $i++;
                }  

		header('Content-Type: application/vmd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$archivo.'"');
        header('Cache-Control: max-age-0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
        $objWriter->save('php://output');                
	}
?>
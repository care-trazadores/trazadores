<?php
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
    return $rgb; //returns an array with the rgb values
}
    if ($archivo=="pdf") {
        require("application/libraries/fpdf17/fpdf.php");
        $pdf=new FPDF();
        $pdf->AddPage("L","A4");
        $a=0;
        $u=1;
        foreach ($tablas as $key) {
            $pdf->SetFont("Arial", "", 15);
            $pdf->SetTextColor(106,106,114);
            $pdf->Cell(80,10,"Sistema Trazadores SIAS",0,0,"C");
            $pdf->Cell(120,10,"Reporte Globales Detallados",0,0,"C");
            $pdf->Cell(60,10,"Fecha de impresion:".date('d-m-Y')."",0,0,"C");
            $pdf->Ln(15);
            $pdf->SetFont("Arial", "", 12);
            $pdf->Cell(20,15,"",0,0,"");
            $pdf->Cell(80,10,$tablas[$a]["localizacion"],0,0,"");
            $pdf->Ln(5);
            $pdf->Cell(20,15,"",0,0,"");
            $pdf->Cell(200,10,"Localidad:".$tablas[$a]["comunidad"],0,0,"");
            $pdf->Cell(40,10,"Fecha: ".$tablas[$a]["fecha"],0,0,"");

            $pdf->Ln(10);
            $color=hex2rgb("#7fd17f");
            $pdf->SetFillColor($color[0],$color[1],$color[2]); 
            $pdf->SetFont("Arial", "", 10);
            $pdf->SetTextColor(255,255,255);
            $pdf->Cell(20,15,"",0,0,"");
            $pdf->Cell(7,15,"",1,0,"", True);
            $pdf->Cell(80,15,"Trazadores",1,0,"", True);
            $pdf->Cell(26,15,"Valor evaluado",1,0,"C", True);
            $pdf->Cell(50,15,utf8_decode("Calificación"),1,0,"C", True);
            $pdf->Cell(25,15,utf8_decode("Ponderación"),1,0,"C", True);
            $pdf->Cell(45,15,utf8_decode("Valoración Ponderada"),1,0,"C", True);
            $e=1;
            foreach ($tablas[$a]["result"] as $ke) {
                $pdf->Ln(15);
                $pdf->SetFillColor(201,224,255); 
                $pdf->SetTextColor(0,0,0);
                $pdf->Cell(20,15,"",0,0,"");
                $pdf->Cell(7,15,$e,1,0,"C", True);
                $pdf->Cell(80,15,utf8_decode($tablas[$a]["result"][$e]["titulo"]),1,0,"", True);
                $pdf->Cell(26,15,$tablas[$a]["result"][$e]["promedio"],1,0,"C", True);
                $color=hex2rgb($tablas[$a]["result"][$e]["calificacion_cuadro"]['background']);
                $pdf->SetFillColor($color[0],$color[1],$color[2]); 
                $pdf->Cell(50,15,utf8_decode($tablas[$a]["result"][$e]["calificacion_cuadro"]['calificacion']),1,0,"C", True);
                $pdf->SetFillColor(201,224,255); 
                $pdf->Cell(25,15,$tablas[$a]["result"][$e]["ponderacion"],1,0,"C", True);
                $pdf->Cell(45,15,$tablas[$a]["result"][$e]["valoracion_ponderada"],1,0,"C", True);
                $e++;
            }
            foreach ($tablas[$a]["final"] as $ke) {
                $pdf->Ln(15);
                $pdf->SetFillColor(255,255,255); 
                $pdf->Cell(20,15,"",0,0,"");
                $pdf->Cell(7,15,"",0,0,"");
                $pdf->Cell(80,15,"Sumatoria",1,0,"", True);
                $pdf->Cell(26,15,$tablas[$a]["final"][$e]["sumatoria_evaluado"],1,0,"C", True);
                $pdf->Cell(50,15,"",1,0,"C", True);
                $pdf->Cell(25,15,$tablas[$a]["final"][$e]["ponderacion"],1,0,"C", True);
                $pdf->Cell(45,15,$tablas[$a]["final"][$e]["sumatoria_ponderacion"],1,0,"C", True);

                $pdf->Ln(15);
                $pdf->SetFillColor(17,92,139); 
                $pdf->SetTextColor(255,255,255);
                $pdf->Cell(20,15,"",0,0,"");
                $pdf->Cell(7,15,"",0,0,"");
                $pdf->Cell(80,15,"Promedio global",1,0,"", True);
                $pdf->Cell(26,15,"",1,0,"C", True);
                $pdf->Cell(50,15,"Promedio:",1,0,"C", True);
                $pdf->Cell(25,15,$tablas[$a]["final"][$e]["promedio_aponderacion"],1,0,"C", True);
                $color=hex2rgb($tablas[$a]["final"][$e]["calificacion_cuadro"]['background']);
                $pdf->SetFillColor($color[0],$color[1],$color[2]);
                $pdf->SetTextColor(0,0,0);
                $pdf->Cell(45,15,utf8_decode($tablas[$a]["final"][$e]["calificacion_cuadro"]['calificacion']),1,0,"C", True);
            }
            $pdf->Ln(18);
            $pdf->Cell(135,10,"",0,0,"");
            $pdf->Cell(7,10,$u,0,0,"");
            $pdf->Ln(250);
            $u++;
            $a++;
        }
        $pdf->Output();
    
        /*require("application/libraries/dompdf/dompdf_config.inc.php");
        $codigoHTML='
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Lista</title>
            <style>
                .cabezera{margin-bottom:20px;width:95%;color:rgb(106,106,114)}
                .cabezera .center{font-size:24px}
                .datos{width:95%;color:rgb(106,106,114);margin:20px;0px}
                .info{border-collapse:collapse;border: none;width:95%;}
                .info th{background-color:#7fd17f;border:1px solid #000;}
                .info td{border:1px solid #000;padding 5px;}
                .left{text-align:left;}
                .center{text-align:center;}
                .right{text-align:right;}
                .blue{background-color:#4594B9;}
            </style>
        </head>
        <body>';
            $a=0;
            $u=1;
            $codigoHTML.='<div style="display:none">Localización</div>';
            foreach ($tablas as $key) {
                $codigoHTML.='<div style="heigth:100%;"><table class="cabezera">';
                $codigoHTML.='<tr>';
                    $codigoHTML.='<th>Sitema Sias Trazadores</th>';
                    $codigoHTML.='<th class="center">Reporte General</th>';
                    $codigoHTML.='<th class="right">Fecha de impresion:'.date('d-m-Y').'</th>';
                $codigoHTML.='</tr>';
                $codigoHTML.='</table>';
                $codigoHTML.='<table class="datos">';
                    $codigoHTML.='<tr>';
                        $codigoHTML.='<td>Localización: '.$tablas[$a]["localizacion"].'</td>';
                    $codigoHTML.='</tr>';
                    $codigoHTML.='<tr>';
                        $codigoHTML.='<td>Localidad: '.utf8_encode($tablas[$a]["comunidad"]).'</td>';
                        $codigoHTML.='<td class="right">Fecha: '.utf8_encode($tablas[$a]["fecha"]).'</td>';
                    $codigoHTML.='</tr>';
                    
                $codigoHTML.='</table>';

                $codigoHTML.='<table class="info">';
                    $codigoHTML.='<tr>';
                        $codigoHTML.='<th colspan="2">Trazadores</td>';
                        $codigoHTML.='<th>Valor evaluado</th>';
                        $codigoHTML.='<th>Calificación</th>';
                        $codigoHTML.='<th>Ponderación</th>';
                        $codigoHTML.='<th>Valoración Ponderada</th>';
                    $codigoHTML.='</tr>';
                    $e=1;
                    foreach ($tablas[$a]["result"] as $ke) {
                        $codigoHTML.='<tr>';
                            $codigoHTML.='<td class="center">'.$e.'</td>';
                            $codigoHTML.='<td>'.$tablas[$a]["result"][$e]["titulo"].'</td>';
                            $codigoHTML.='<td class="center">'.$tablas[$a]["result"][$e]["promedio"].'</td>';
                            $codigoHTML.='<td class="center" style="background-color:'.$tablas[$a]["result"][$e]["calificacion_cuadro"]['background'].'">'.$tablas[$a]["result"][$e]["calificacion_cuadro"]['calificacion'].'</td>';
                            $codigoHTML.='<td class="center">'.$tablas[$a]["result"][$e]["ponderacion"].'</td>';
                            $codigoHTML.='<td class="center">'.$tablas[$a]["result"][$e]["valoracion_ponderada"].'</td>';
                        $codigoHTML.='</tr>';
                     $e++;
                    }
               
                
                    foreach ($tablas[$a]["final"] as $ke) {
                        $codigoHTML.='<tr class="blue">';
                            $codigoHTML.='<td colspan="2">Sumatoria</td>';
                            $codigoHTML.='<td class="center">'.$tablas[$a]["final"][$e]["sumatoria_evaluado"].'</td>';
                            $codigoHTML.='<td></td>';
                            $codigoHTML.='<td class="center">'.$tablas[$a]["final"][$e]["ponderacion"].'</td>';
                            $codigoHTML.='<td class="center">'.$tablas[$a]["final"][$e]["sumatoria_ponderacion"].'</td>';
                        $codigoHTML.='</tr>';
                        $codigoHTML.='<tr class="blue">';
                            $codigoHTML.='<td colspan="2">Promedio global</td>';
                            $codigoHTML.='<td></td>';
                            $codigoHTML.='<td></td>';
                            $codigoHTML.='<td style="background-color:'.$tablas[$a]["final"][$e]["calificacion_cuadro"]['background'].'">Promedio: '.$tablas[$a]["final"][$e]["promedio_aponderacion"].'</td>';
                            $codigoHTML.='<td style="background-color:'.$tablas[$a]["final"][$e]["calificacion_cuadro"]['background'].'">'.$tablas[$a]["final"][$e]["calificacion_cuadro"]['calificacion'].'</td>';

                        $codigoHTML.='</tr>';
                    }
                    $u++;
                    $a++;
                $codigoHTML.='</table>';
                $codigoHTML.='<br><table style="page-break-after:always;"></br></table><br>';
            }
        $codigoHTML.='</div></body>';


        $codigoHTML=utf8_decode($codigoHTML);
        $dompdf=new DOMPDF();
        $dompdf->set_paper("letter","landscape");
        $dompdf->load_html($codigoHTML);
        ini_set("memory_limit","128M");
        $dompdf->render();
        $dompdf->stream("ListadoEmpleado.pdf", array("Attachment" => 0));
        */
    }elseif ($archivo=="excel") {
        global $objPHPExcel;
        require "application/libraries/phpexcel/PHPExcel.php";
        $objPHPExcel=new PHPExcel();
        $archivo= "globales.xls";
        $borders= array(
            'borders'=> array(
                'allborders'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('argb'=>'FF0000FF'
                )
            ),
            'outline'     => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN,
              'color' => array(
                 'argb' => 'FF0000FF'
              )
            )
          )
        );
        function cellColor($cells,$color){
            global $objPHPExcel;
            $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                     'rgb' => $color
                )
            ));
        }
        $objPHPExcel->getProperties()->setCreator("weblocalhost")
            ->setLastModifiedBy("weblocalhost")
            ->setTitle("Reporte XLS")
            ->setSubject("Réporte")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial Narrow');
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
        $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
        $objPHPExcel->getActiveSheet()->getStyle('A2:G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //$objPHPExcel->getActiveSheet()->mergeCells('A3:G3');
        //$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A2:G2')->getFont()->setSize(16);
        //$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFont()->setSize(16);
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('C1', "Fecha de exportacion:".date("d-m-Y"))
        ->setCellValue('D1', "Hora:".date("H:i"))
        ->setCellValue('A2', "Sistema trazadores");
        
            $objPHPExcel->getActiveSheet()->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $i=3;
    $inicio_encuesta=0;
        foreach ($tablas as $key) {
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('g')->setAutoSize(true);

            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B'.$i, "Localizacion:".$tablas[$inicio_encuesta]["localizacion"]);
            $i++;
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':E'.$i);
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B'.$i, $tablas[$inicio_encuesta]["comunidad"])
            ->setCellValue('G'.$i, $tablas[$inicio_encuesta]["fecha"]);
            
            $i++;

            $objPHPExcel->getActiveSheet()
                ->getStyle('B'.$i.':G'.$i)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FF00CCFF');
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':G'.$i)->applyFromArray($borders);


            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B'.$i, " ")
                ->setCellValue('C'.$i, "trazadores")
                ->setCellValue('D'.$i, "Valor evaluado")
                ->setCellValue('E'.$i, "Calificación")
                ->setCellValue('F'.$i, "Ponderación")
                ->setCellValue('G'.$i, "Valoración Ponderada");
                $i++;
            $inicio_cuadros=1;
            foreach ($tablas[$inicio_encuesta]["result"] as $ke) {
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B'.$i.':G'.$i)
                    ->getFill()
                    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFCCFFFF');
                $col = str_replace("#","",$tablas[$inicio_encuesta]["result"][$inicio_cuadros]["calificacion_cuadro"]["background"]);
                cellColor('E'.$i, $col);
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$i, $inicio_cuadros)
                    ->setCellValue('C'.$i, $tablas[$inicio_encuesta]["result"][$inicio_cuadros]["titulo"])
                    ->setCellValue('D'.$i, $tablas[$inicio_encuesta]["result"][$inicio_cuadros]["promedio"])
                    ->setCellValue('E'.$i, $tablas[$inicio_encuesta]["result"][$inicio_cuadros]["calificacion_cuadro"]["calificacion"])
                    ->setCellValue('F'.$i, $tablas[$inicio_encuesta]["result"][$inicio_cuadros]["ponderacion"])
                    ->setCellValue('G'.$i, $tablas[$inicio_encuesta]["result"][$inicio_cuadros]["valoracion_ponderada"]);
                     $inicio_cuadros++;
                $i++;
            }
            foreach ($tablas[$inicio_encuesta]["final"] as $ke) {
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B'.$i, " ")
                ->setCellValue('C'.$i, "Sumatoria")
                ->setCellValue('D'.$i, $tablas[$inicio_encuesta]["final"][$inicio_cuadros]["sumatoria_evaluado"])
                ->setCellValue('E'.$i, " ")
                ->setCellValue('F'.$i, $tablas[$inicio_encuesta]["final"][$inicio_cuadros]["ponderacion"])
                ->setCellValue('G'.$i, $tablas[$inicio_encuesta]["final"][$inicio_cuadros]["sumatoria_ponderacion"]);
                $i++;
                $col = str_replace("#","",$tablas[$inicio_encuesta]["final"][$inicio_cuadros]["calificacion_cuadro"]["background"]);
                cellColor('F'.$i, $col);
                cellColor('G'.$i, $col);
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B'.$i, " ")
                ->setCellValue('C'.$i, "Promedio global")
                ->setCellValue('D'.$i, " ")
                ->setCellValue('E'.$i, "Promedio:")
                ->setCellValue('F'.$i, $tablas[$inicio_encuesta]["final"][$inicio_cuadros]["promedio_aponderacion"])
                ->setCellValue('G'.$i, $tablas[$inicio_encuesta]["final"][$inicio_cuadros]["calificacion_cuadro"]["calificacion"]);
            }
            $inicio_encuesta++;
            $i++;
            $i++;
            $i++;
        }
    header('Content-Type: application/vmd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$archivo.'"');
    header('Cache-Control: max-age-0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
    $objWriter->save('php://output');
    }

	
?>
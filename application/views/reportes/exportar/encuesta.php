<?php
	function hex2rgb($hex) {
       $hex = str_replace("#", "", $hex);

       if(strlen($hex) == 3) {
          $r = hexdec(substr($hex,0,1).substr($hex,0,1));
          $g = hexdec(substr($hex,1,1).substr($hex,1,1));
          $b = hexdec(substr($hex,2,1).substr($hex,2,1));
       } else {
          $r = hexdec(substr($hex,0,2));
          $g = hexdec(substr($hex,2,2));
          $b = hexdec(substr($hex,4,2));
       }
       $rgb = array($r, $g, $b);
       //return implode(",", $rgb); // returns the rgb values separated by commas
        return $rgb; //returns an array with the rgb values
    }
    require("application/helpers/operaciones_helper.php");
    if ($archivo=="pdf") {
        require("application/libraries/fpdf17/fpdf.php");
        class PDF extends FPDF{
            // Cabecera de página
            function cabecera($datos_llenado){
                // Logo
                $this->Image('img/logo_pb.png',10,8,33);
                // Arial bold 15
                $this->SetFont('Arial','B',13);
                // Movernos a la derecha
                $this->Cell(40);
                // Título
                $this->Cell(30,5,'Sistemas Trazadores',0,0,'L');
                $this->Cell(120);
                $this->Cell(30,5,'Fecha de llenado: '.$datos_llenado['fecha'],0,0,'L');
                $this->Ln(5);
                $this->SetFont('Arial','B',10);
                $this->Cell(40);
                $this->Cell(150,5,'Localidad: '.utf8_decode($datos_llenado['comunidad']),0,0,'L');
                if ($datos_llenado['usuario']=='') {$usuario='Usuario eliminado';}
                else{ $usuario=$datos_llenado['usuario'];}
                $this->Cell(120,5,'Usuario: '.$usuario,0,0,'L');
                $this->Ln(5);
                $this->Cell(40);
                $this->Cell(138,5,'Localizacion: '.$datos_llenado['departamento'].'--'.$datos_llenado['provincia'].'--'.$datos_llenado['distrito'],0,0,'L');
                $this->Ln(10);
            }

            // Pie de página
            function Footer(){
                // Posición: a 1,5 cm del final
                $this->SetY(-15);
                // Arial italic 8
                $this->SetFont('Arial','I',8);
                // Número de página
                $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
            }
            function cuadros($id_cuadro=null,$tipo=null,$titulo=null,
                $calificacion=null,$criterios=null,$criterios_detalle=null,
                $notas=null,$tablas=null,$preguntas=null,$alternativas=null,
                $respuestas=null,$aponderacion=null,$calificaciones=null){
                if ($tipo==0) {
                    $this->cuadro_tipo_0($id_cuadro,$titulo,$tablas,$preguntas,$alternativas,$respuestas);
                }elseif ($tipo==1) {
                    $this->cuadro_tipo_1($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas);
                }elseif ($tipo==2) {
                    $this->cuadro_tipo_2($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas);
                }elseif ($tipo==3) {
                    $this->cuadro_tipo_3($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas);
                }elseif ($tipo==4) {
                    $this->cuadro_tipo_4($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$aponderacion,$calificaciones);
                }elseif ($tipo==5) {
                    $this->cuadro_tipo_5($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas);
                }
            }
            function cuadro_tipo_0($id_cuadro,$titulo,$tablas,$preguntas,$alternativas,$respuestas){
                $this->SetFont('Arial','B',15);
                // Movernos a la derecha
                $this->Cell(null,10,utf8_decode($titulo),0,0,'C');
                // Salto de línea
                $this->Ln(20);
                $this->SetFont('Times','',10);
                foreach ($tablas as $key) {
                    if ($key->tipo==1) {
                        $posx=10;
                        $posy=50;
                        $this->SetFillColor(127,209,127);
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(120,5,utf8_decode("Georeferenciación del centro poblado"),1,'C',true);
                        $posy=$posy+5;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(80,5,utf8_decode("Coordenadas UTM"),1,'C',true);
                        $posx=$posx+80;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(40,10,"Altitud msnm",1,'C',true);
                        $posy=$posy+5;
                        $posx=10;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(40,5,"Norte",1,'C',true);
                        $posx=$posx+40;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(40,5,"Este",1,'C',true);

                        $posy=$posy+5;
                        $posx=10;
                        foreach ($preguntas as $ke) {
                            $respuesta="---";
                            if ($ke->id_tabla==$key->id_tabla) {
                                if ($respuestas[$ke->id_pregunta]!=null) {
                                    foreach ($respuestas[$ke->id_pregunta] as $res) {
                                        if ($res->respuesta!="") {
                                            $respuesta=$res->respuesta;
                                        }
                                    }
                                }
                                $this->SetY($posy);
                                $this->SetX($posx);
                                $this->MultiCell(40,5,$respuesta,1,'C',false);
                                $posx=$posx+40;
                            }
                        }
                        $posy=$posy+5;
                        $posy=$posy+5;
                    }elseif ($key->tipo==2) {
                        $id_pregunta=[];
                        $i=0;
                        foreach ($preguntas as $ke) {
                            if ($ke->id_tabla==$key->id_tabla) {
                                //echo $ke->id_pregunta;
                                $id_pregunta[$i]=$ke->id_pregunta;
                                $i++;
                            }
                        }
                        $posx=10;
                        $posy=$posy;
                        $this->SetFillColor(127,209,127);
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(80,5,utf8_decode("Información básica"),1,'C',true);
                        $posx=$posx+80;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(40,5,utf8_decode("Llenar"),1,'C',true);
                        $posy=$posy+5;
                        $posx=10;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(80,5,utf8_decode("Número de habitantes totales en el sistema"),1,'L',false);
                        $posx=$posx+80;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        if ($respuestas!=null) {
                            foreach ($respuestas[$id_pregunta[0]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        $this->MultiCell(40,5,$respuesta,1,'C',false);
                        
                        $posy=$posy+5;
                        $posx=10;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(80,5,utf8_decode("Caudal de producción de las fuentes de agua (lt/seg)"),1,'L',false);
                        $posx=$posx+80;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        if ($respuestas!=null) {
                            foreach ($respuestas[$id_pregunta[1]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        $this->MultiCell(40,5,$respuesta,1,'C',false);  

                        $posy=$posy+5;
                        $posx=10;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(80,5,utf8_decode("Monto promedio de la cuota mensual S/"),1,'L',false);
                        $posx=$posx+80;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        if ($respuestas!=null) {
                            foreach ($respuestas[$id_pregunta[2]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        $this->MultiCell(40,5,$respuesta,1,'C',false);

                        $posy=$posy+5;
                        $posx=10;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(120,5,utf8_decode("Dotación de agua"),1,'C',true);

                        $posy=$posy+5;
                        $posx=10;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(40,5,utf8_decode("Zona geográfica"),1,'C',true);
                        $posy=$posy+5;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        if ($respuestas!=null) {
                            foreach ($respuestas[$id_pregunta[3]] as $res) {
                                if ($res->respuesta!="") {
                                    if ($res->respuesta==0) {$respuesta="Costa";}
                                    elseif ($res->respuesta==1) {$respuesta="Sierra";}
                                    elseif ($res->respuesta==2) {$respuesta="Selva";} 
                                }
                            }
                        }
                        $this->MultiCell(40,5,$respuesta,1,'C',false); 

                        $posy=$posy-5;
                        $posx=$posx+40;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(40,5,utf8_decode("Sistema de baño"),1,'C',true);
                        $posy=$posy+5;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        if ($respuestas!=null) {
                            foreach ($respuestas[$id_pregunta[4]] as $res) {
                                if ($res->respuesta!="") {
                                    if ($res->respuesta==0) {$respuesta="Sin arrastre";}
                                    elseif ($res->respuesta==1) {$respuesta="Con arrastre";}
                                }
                            }
                        }
                        $this->MultiCell(40,5,$respuesta,1,'C',false);

                        $posy=$posy-5;
                        $posx=$posx+40;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(40,5,utf8_decode("Lt/hab/día"),1,'C',true);
                        $posy=$posy+5;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        if ($respuestas!=null) {
                            foreach ($respuestas[$id_pregunta[5]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        $this->MultiCell(40,5,$respuesta,1,'C',false);      
                    }elseif ($key->tipo==3) {
                        $respuesta="";
                        if ($key->id_tabla==15) {
                            $posy=$posy+10;
                            $posx=10;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(100,5,utf8_decode($key->titulo),1,'C',true);
                            $posx=$posx+100;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(20,5,utf8_decode("Marcar"),1,'C',true);
                            foreach ($preguntas as $ke) {
                                if ($ke->id_tabla==$key->id_tabla) {
                                    foreach ($alternativas as $k) {
                                        if ($k->id_pregunta==$ke->id_pregunta) {
                                            $posx=10;
                                            $posy=$posy+5;
                                            $this->SetY($posy);
                                            $this->SetX($posx);
                                            $this->MultiCell(100,5,utf8_decode($k->texto),1,'L',false);

                                            if ($respuestas!=null) {
                                                foreach ($respuestas[$ke->id_pregunta] as $res) {
                                                    $valores=explode("/", $res->respuesta);
                                                    $respuesta="";
                                                    foreach ($valores as $var) {
                                                        if ($var==$k->valor) {
                                                            $respuesta="X";
                                                        }
                                                    }
                                                }
                                            }
                                            $posx=$posx+100;
                                            $this->SetY($posy);
                                            $this->SetX($posx);
                                            $this->MultiCell(20,5,$respuesta,1,'C',false);

                                        }
                                    }
                                }
                            }
                        }elseif ($key->id_tabla==17) {
                            $posy=130;
                            $respuesta="";
                            $posx=140;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(100,5,utf8_decode($key->titulo),1,'C',true);
                            $posx=$posx+100;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(20,5,utf8_decode("Marcar"),1,'C',true);
                            foreach ($preguntas as $ke) {
                                if ($ke->id_tabla==$key->id_tabla) {
                                    foreach ($alternativas as $k) {
                                        if ($k->id_pregunta==$ke->id_pregunta) {
                                            $posx=140;
                                            $posy=$posy+5;
                                            $this->SetY($posy);
                                            $this->SetX($posx);
                                            $this->MultiCell(100,5,utf8_decode($k->texto),1,'L',false);

                                            if ($respuestas!=null) {
                                                foreach ($respuestas[$ke->id_pregunta] as $res) {
                                                    $valores=explode("/", $res->respuesta);
                                                    $respuesta="";
                                                    foreach ($valores as $var) {
                                                        if ($var==$k->valor) {
                                                            $respuesta="X";
                                                        }
                                                    }
                                                }
                                            }
                                            $posx=$posx+100;
                                            $this->SetY($posy);
                                            $this->SetX($posx);
                                            $this->MultiCell(20,5,$respuesta,1,'C',false);
                                        }
                                    }
                                    
                                }
                            }
                            $posy=$posy+5;
                        }elseif ($key->id_tabla==16) {
                            $posy=90;
                            $respuesta="";
                            $posx=140;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(100,5,utf8_decode($key->titulo),1,'C',true);
                            $posx=$posx+100;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(20,5,utf8_decode("Marcar"),1,'C',true);
                            foreach ($preguntas as $ke) {
                                if ($ke->id_tabla==$key->id_tabla) {
                                    foreach ($alternativas as $k) {
                                        if ($k->id_pregunta==$ke->id_pregunta) {
                                            $posx=140;
                                            $posy=$posy+5;
                                            $this->SetY($posy);
                                            $this->SetX($posx);
                                            $this->MultiCell(100,5,utf8_decode($k->texto),1,'L',false);

                                            if ($respuestas!=null) {
                                                foreach ($respuestas[$ke->id_pregunta] as $res) {
                                                    $valores=explode("/", $res->respuesta);
                                                    $respuesta="";
                                                    foreach ($valores as $var) {
                                                        if ($var==$k->valor) {
                                                            $respuesta="X";
                                                        }
                                                    }
                                                }
                                            }
                                            $posx=$posx+100;
                                            $this->SetY($posy);
                                            $this->SetX($posx);
                                            $this->MultiCell(20,5,$respuesta,1,'C',false);
                                        }
                                    }
                                    
                                }
                            }
                        }elseif ($key->id_tabla==18) {
                            $posy=50;
                            $respuesta="";
                            $posx=140;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(100,5,utf8_decode($key->titulo),1,'C',true);
                            $posx=$posx+100;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(20,5,utf8_decode("Marcar"),1,'C',true);
                            foreach ($preguntas as $ke) {
                                if ($ke->id_tabla==$key->id_tabla) {
                                    foreach ($alternativas as $k) {
                                        if ($k->id_pregunta==$ke->id_pregunta) {
                                            $posx=140;
                                            $posy=$posy+5;
                                            $this->SetY($posy);
                                            $this->SetX($posx);
                                            $this->MultiCell(100,5,utf8_decode($k->texto),1,'L',false);
                                            if ($respuestas!=null) {
                                                foreach ($respuestas[$ke->id_pregunta] as $res) {
                                                    $valores=explode("/", $res->respuesta);
                                                    $respuesta="";
                                                    foreach ($valores as $var) {
                                                        if ($var==$k->valor) {
                                                            $respuesta="X";
                                                        }
                                                    }
                                                }
                                            }
                                            $posx=$posx+100;
                                            $this->SetY($posy);
                                            $this->SetX($posx);
                                            $this->MultiCell(20,5,$respuesta,1,'C',false);
                                        }
                                    }
                                    
                                }
                            }
                            $posy=$posy+15;
                        }
                    }
                }
            }
            function cuadro_tipo_1($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas){
                $this->SetFont('Arial','B',16);
                // Movernos a la derecha
                $this->Cell(null,5,utf8_decode($titulo),0,0,'C');
                // Salto de línea
                $this->SetFont('Times','',10);
                // Imprimir texto en una columna de 6 cm de ancho
                $this->SetFillColor(127,209,127);
                $this->SetY(40);
                $this->SetX(10);
                $this->MultiCell(80,10,"TRAZADOR",1,'C',true);
                $this->SetY(40);
                $this->SetX(90);
                $this->MultiCell(90,10,utf8_decode("CRITERIOS DE VALORACIÓN"),1,'C',true);
                $this->SetY(40); 
                $this->SetX(180);
                $this->MultiCell(30,10,utf8_decode("CALIFICACIÓN"),1,'C',true);
                $this->SetY(40); 
                $this->SetX(210);
                $this->MultiCell(80,10,utf8_decode("OBSERVACIONES"),1,'C',true);
                $posx=10;
                $posy=50;
                foreach ($tablas as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,10,$key->titulo,1,'C');
                    $posx=$posx+80;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(90,10,"",1,'C');
                    $posx=$posx+90;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(30,10,"Valor",1,'C');
                    $posx=$posx+30;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,10,"",1,'C');
                    
                    $posy=60;
                    $a=1;
                    $this->SetFont('Times','',8);
                    $total_sumatoria="";
                    $total_division="";
                  
                    $color=hex2rgb("#A2D5F9");
                    $this->SetFillColor($color[0],$color[1],$color[2]);
                    foreach ($preguntas as $ke) {
                        $posx=10;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(5,15,$a,1,'C',true);
                        $posx=$posx+5;
                        $this->SetY($posy);
                        $this->SetX($posx);

                        $total_texto_pregunta=strlen($ke->pregunta);
                        $ancho=$total_texto_pregunta/55;
                        if ($ancho>=0 && $ancho<=1) {
                            $ancho_cuadro=15;
                        }elseif ($ancho>1 && $ancho<=2) {
                            $ancho_cuadro=7.5;
                        }else{
                            $ancho_cuadro=5;
                        }
                        $this->MultiCell(75,$ancho_cuadro,utf8_decode($ke->pregunta),1,'L',true);
                        $poscy=$posy;
                        $poscx=$posx+75;
                        $posx=$posx+75;
                        foreach ($alternativas as $k) {
                            if ($k->id_pregunta==$ke->id_pregunta) {
                                $this->SetY($poscy);
                                $this->SetX($poscx);
                                $this->MultiCell(90,5,utf8_decode("  ".$k->valor." : ".$k->texto),1,'L',true);
                                $poscy=$poscy+5;
                            }
                        }
                        $respuesta="--";
                        $observacion="";
                        if (array_key_exists($ke->id_pregunta,$respuestas)) {
                            foreach ($respuestas[$ke->id_pregunta] as $k) {
                                $respuesta=$k->respuesta;
                                $observacion=$k->observacion;
                                $total_sumatoria=$total_sumatoria+$respuesta;
                                $total_division=$total_division+1;
                            }
                        }
                        $posx=$posx+90;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(30,15,$respuesta,1,'C',true);
                        $posx=$posx+30;
                        $this->SetY($posy);
                        $this->SetX($posx);

                        $total_texto_pregunta=strlen($observacion);
                        $ancho=$total_texto_pregunta/70;
                        if ($ancho>=-1 && $ancho<=1) {
                            $ancho_cuadro=15;
                        }elseif ($ancho>1 && $ancho<=2) {
                            $ancho_cuadro=7.5;
                        }else{
                            $ancho_cuadro=5;
                        }
                        $this->MultiCell(80,$ancho_cuadro,utf8_decode($observacion),1,'C',true);
                        
                        $poscy=$posy;
                        $a++;
                        $posy=$posy+17;
                    }
                }
                $posx=10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,"Sumatoria",1,'L',false);
                $posx=$posx+80;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(90,10,'',1,'C',false);
                $posx=$posx+90;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(30,10,$total_sumatoria,1,'C',false);
                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,'',1,'C',false);

                $calificaciones_final="";
                $promedio=number_format($total_sumatoria/$total_division, 2, '.', ' ');
                $cali=calificaciones_final($calificacion,$promedio);
                $color=hex2rgb($cali["background"]);
                $posx=10;
                $posy=$posy+10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,"Promedio ".utf8_decode($titulo),1,'L',false);
                $posx=$posx+80;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(90,10,'',1,'C',false);
                $posx=$posx+90;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->SetFillColor($color[0],$color[1],$color[2]);
                $this->MultiCell(30,10,$promedio,1,'C',true);

                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,utf8_decode($cali["calificacion"]),1,'C',true);

                //saltamos de pagina
                $this->AddPage("L","A4");
                //codigo para imprimir las calificaciones
                $posx=10;
                $posy=10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->SetFillColor(127,209,127);
                $this->MultiCell(60,10,utf8_decode("Calificación"),1,'C',true);
                $posx=$posx+60;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(60,10,"Rango",1,'C',true);
                $posx=10;
                $posy=$posy+10;
                foreach ($calificacion as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(60,10,utf8_decode($key->calificacion),1,'C',false);
                    $posx=$posx+60;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(60,10,$key->texto_rango,1,'C',false);
                    $posx=10;
                    $posy=$posy+10;
                }
                if (count($criterios)!=null) {
                    //codigo para imprimir los criterios
                    $posy=$posy+10;
                    $posx=$posx+60;
                    $posx=150;
                    $posy=10;
                    foreach ($criterios as $key) {
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(5,10,"",0,'C',false);
                        $posx=$posx+5;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(120,10,utf8_decode($key->titulo),1,'C',true);
                        $posy=$posy+10;
                        $e=1;
                        foreach ($criterios_detalle as $ke) {
                            $posx=150;
                            if ($key->id_criterio==$ke->id_criterio) {
                                if ($ke->valor!='') {
                                    $this->SetY($posy);
                                    $this->SetX($posx);
                                    $this->MultiCell(5,10,$ke->valor,1,'C',false);
                                }
                                $posx=$posx+5;
                                $this->SetY($posy);
                                $this->SetX($posx);
                                $this->MultiCell(120,10,utf8_decode($ke->texto),1,'L',false);
                                $posy=$posy+10;
                                $e++;
                            }
                        }
                        $posy=$posy+10;
                    }
                }
                if (count($notas)!=null) {
                    $posx=10;
                    if (count($criterios)!=null) {
                       $posy=$posy+40; 
                   }else{$posy=$posy+10;}
                    
                    foreach ($notas as $key) {
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $vowels = array('<strong style="font-weight:700">','</strong>');
                        $this->MultiCell('',10,utf8_decode(str_replace($vowels, "", $key->texto)),0,'L',false);
                        $posy=$posy+10;
                    }
                }           
            }
            function cuadro_tipo_2($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas){
                $this->SetFont('Arial','B',16);
                // Movernos a la derecha
                $this->Cell(null,5,utf8_decode($titulo),0,0,'C');
                // Salto de línea
                $this->SetFont('Times','',10);
                // Imprimir texto en una columna de 6 cm de ancho
                $this->SetFillColor(127,209,127);
                $this->SetY(40);
                $this->SetX(10);
                $this->MultiCell(80,15,"TRAZADOR",1,'C',true);
                $this->SetY(55);
                $this->SetX(10);
                $this->MultiCell(80,5,"COMPONENTES DEL SISTEMA",1,'C',true);

                $this->SetY(40);
                $this->SetX(90);
                $this->MultiCell(30,5,"Existencia de componentes",1,'L',true);
                $this->SetY(50);
                $this->SetX(90);
                $this->MultiCell(15,5,"SI",1,'C',true);
                $this->SetY(50);
                $this->SetX(105);
                $this->MultiCell(15,5,"NO",1,'C',true);
                $this->SetY(55);
                $this->SetX(90);
                $this->MultiCell(30,5,"Marcar con X",1,'L',true);



                $this->SetY(40);
                $this->SetX(120);
                $this->MultiCell(60,5,utf8_decode("Criterio Nº1"),1,'C',true);
                $this->SetY(45);
                $this->SetX(120);
                $this->MultiCell(60,5,utf8_decode("CALIFICACIÓN"),1,'C',true);
                $this->SetY(50);
                $this->SetX(120);
                $this->SetFont('Times','',7);
                $this->MultiCell(20,5,"DETERIORADO",1,'C',true);
                $this->SetY(50);
                $this->SetX(140);
                $this->MultiCell(20,5,"LIMITADO",1,'C',true);
                $this->SetY(50);
                $this->SetX(160);
                $this->MultiCell(20,5,"ADECUADO",1,'C',true);
                $this->SetFont('Times','',10);
                $this->SetY(55);
                $this->SetX(120);
                $this->MultiCell(20,5,"2",1,'C',true);
                $this->SetY(55);
                $this->SetX(140);
                $this->MultiCell(20,5,"1",1,'C',true);
                $this->SetY(55);
                $this->SetX(160);
                $this->MultiCell(20,5,"0",1,'C',true);

                $this->SetY(40);
                $this->SetX(180);
                $this->MultiCell(30,10,"Estado del componente",1,'C',true);

                $this->SetY(40);
                $this->SetX(210);
                $this->MultiCell(80,20,"Observaciones",1,'C',true);

                $posx=10;
                $posy=60;
                $total_no=0;
                $total_si=0;
                foreach ($tablas as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,5,$key->titulo,1,'L');
                    $posx=$posx+80;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(90,5,"",1,'C');
                    $posx=$posx+90;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(30,5,"Valor",1,'C');
                    $posx=$posx+30;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,5,"",1,'C');
                    
                    $posy=65;
                    $a=1;
                    $this->SetFont('Times','',8);
                    $total_sumatoria="";
                    $total_division="";
                  
                    $color=hex2rgb("#A2D5F9");
                    $this->SetFillColor($color[0],$color[1],$color[2]);
                    foreach ($preguntas as $ke) {
                        $posx=10;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(5,10,$a,1,'C',true);
                        $posx=$posx+5;
                        $this->SetY($posy);
                        $this->SetX($posx);

                        $total_texto_pregunta=strlen($ke->pregunta);
                        $ancho=$total_texto_pregunta/70;
                        if ($ancho>=0 && $ancho<=1) {
                            $ancho_cuadro=10;
                        }elseif ($ancho>1 && $ancho<=2) {
                            $ancho_cuadro=5;
                        }
                        $this->MultiCell(75,$ancho_cuadro,utf8_decode($ke->pregunta),1,'L',true);
                        /*$poscy=$posy;
                        $poscx=$posx+75;
                        $posx=$posx+75;

                        foreach ($alternativas as $k) {
                            if ($k->id_pregunta==$ke->id_pregunta) {
                                $this->SetY($poscy);
                                $this->SetX($poscx);
                                $this->MultiCell(90,5,utf8_decode("  ".$k->valor." : ".$k->texto),1,'L',true);
                                $poscy=$poscy+5;
                            }
                        }*/
                        $respuesta="--";
                        $observacion="";
                        if (array_key_exists($ke->id_pregunta,$respuestas)) {
                            foreach ($respuestas[$ke->id_pregunta] as $k) {
                                $respuesta=$k->respuesta;
                                $observacion=$k->observacion;
                                $total_sumatoria=$total_sumatoria+$respuesta;
                                $total_division=$total_division+1;
                            }
                        }
                        if ($respuesta=="--" || $respuesta=="") {
                            $total_no=$total_no+1;
                            $posx=$posx+75;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(15,10,"",1,'C',true);
                            $posx=$posx+15;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(15,10,"X",1,'C',true);
                        }else{
                            $total_si=$total_si+1;
                            $posx=$posx+75;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(15,10,"X",1,'C',true);
                            $posx=$posx+15;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(15,10,"",1,'C',true);
                        }
                        $posx=$posx+15;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        if ($respuesta=="") {
                            $this->MultiCell(60,10,"---",1,'C',true);
                        }else{
                            $this->MultiCell(60,10,$respuesta,1,'C',true);
                        }
                        $posx=$posx+60;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        if ($respuesta==2) {
                           $this->MultiCell(30,10,"DETERIORADO",1,'C',true);
                        }elseif ($respuesta==1) {
                            $this->MultiCell(30,10,"LIMITADO",1,'C',true);
                        }elseif ($respuesta=="0") {
                           $this->MultiCell(30,10,"ADECUADO",1,'C',true);
                        }else{
                            $this->MultiCell(30,10,"---",1,'C',true);
                        }
                        

                        $posx=$posx+30;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $total_texto_pregunta=strlen($observacion);
                        $ancho=$total_texto_pregunta/70;
                        if ($ancho>=-1 && $ancho<=1) {
                            $ancho_cuadro=10;
                        }elseif ($ancho>1 && $ancho<=2) {
                            $ancho_cuadro=7.5;
                        }else{
                            $ancho_cuadro=5;
                        }
                        $this->MultiCell(80,$ancho_cuadro,utf8_decode($observacion),1,'C',true);
                        
                        //$poscy=$posy;
                        $a++;
                        $posy=$posy+10;
                    }
                }
                $posx=10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,5,"Sumatoria",1,'L',false);
                $posx=$posx+80;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(15,5,$total_si,1,'C',false);
                $posx=$posx+15;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(15,5,$total_no,1,'C',false);
                $posx=$posx+15;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(60,5,$total_sumatoria,1,'C',false);
                $posx=$posx+60;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(30,5,'GLOBAL',1,'C',false);
                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,5,'',1,'C',false);

                $calificaciones_final="";
                if ($total_sumatoria!=0) {
                   $promedio=number_format($total_sumatoria/$total_division, 2, '.', ' ');
                }else{
                    $promedio=$total_sumatoria;
                }
                
                $cali=calificaciones_final($calificacion,$promedio);
                $color=hex2rgb($cali["background"]);
                $posx=10;
                $posy=$posy+5;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,5,"Promedio ".utf8_decode($titulo),1,'L',false);
                $posx=$posx+80;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(90,5,'',1,'C',false);
                $posx=$posx+90;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->SetFillColor($color[0],$color[1],$color[2]);
                $this->MultiCell(30,5,$promedio,1,'C',true);
                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,5,utf8_decode($cali["calificacion"]),1,'C',true);


                //saltamos de pagina
                $this->AddPage("L","A4");
                //codigo para imprimir las calificaciones
                $posx=10;
                $posy=10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->SetFillColor(127,209,127);
                $this->MultiCell(50,10,utf8_decode("Calificación"),1,'C',true);
                $posx=$posx+50;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(50,10,"Rango",1,'C',true);
                $posx=10;
                $posy=$posy+10;
                foreach ($calificacion as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(50,10,utf8_decode($key->calificacion),1,'C',false);
                    $posx=$posx+50;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(50,10,$key->texto_rango,1,'C',false);
                    $posx=10;
                    $posy=$posy+10;
                }
                $posy=$posy+30;
                if (count($criterios)!=null) {
                    //codigo para imprimir los criterios
                    $posx=120;
                    $posy=10;
                    foreach ($criterios as $key) {

                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(5,10,"",0,'C',false);
                        $posx=$posx+5;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(160,10,utf8_decode($key->titulo),1,'C',true);
                        $posy=$posy+10;
                        $e=1;
                        foreach ($criterios_detalle as $ke) {
                            $posx=120;
                            if ($key->id_criterio==$ke->id_criterio) {
                                if ($ke->valor!='') {
                                    $this->SetY($posy);
                                    $this->SetX($posx);
                                    $this->MultiCell(5,10,$ke->valor,1,'C',false);
                                }
                                $posx=$posx+5;
                                $this->SetY($posy);
                                $this->SetX($posx);
                                $this->MultiCell(160,10,utf8_decode($ke->texto),1,'L',false);
                                $posy=$posy+10;
                                $e++;
                            }
                        }
                        $posy=$posy+10;
                    }
                }
                if (count($notas)!=null) {
                    $posx=10;
                    $posy=$posy;
                    foreach ($notas as $key) {
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $vowels = array('<strong style="font-weight:700">','</strong>');
                        $this->MultiCell('',10,utf8_decode(str_replace($vowels, "", $key->texto)),0,'L',false);
                        $posy=$posy+10;
                    }
                }
            }
            function cuadro_tipo_3($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas){
                $this->SetFont('Arial','B',16);
                // Movernos a la derecha
                $this->Cell(null,5,utf8_decode($titulo),0,0,'C');
                // Salto de línea
                $this->SetFont('Times','',10);
                // Imprimir texto en una columna de 6 cm de ancho
                $this->SetFillColor(127,209,127);
                $this->SetY(40);
                $this->SetX(10);
                $this->MultiCell(80,10,"TRAZADOR",1,'C',true);
                $this->SetY(40);
                $this->SetX(90);
                $this->MultiCell(90,10,utf8_decode("CRITERIOS DE VALORACIÓN"),1,'C',true);
                $this->SetY(40); 
                $this->SetX(180);
                $this->MultiCell(30,10,utf8_decode("CALIFICACIÓN"),1,'C',true);
                $this->SetY(40); 
                $this->SetX(210);
                $this->MultiCell(80,10,utf8_decode("OBSERVACIONES"),1,'C',true);
                $posx=10;
                $posy=50;
                foreach ($tablas as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,10,$key->titulo,1,'C');
                    $posx=$posx+80;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(90,10,"",1,'C');
                    $posx=$posx+90;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(30,10,"Porcentaje",1,'C');
                    $posx=$posx+30;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,10,"",1,'C');
                    
                    $posy=60;
                    $a=1;
                    $this->SetFont('Times','',8);
                    $total_sumatoria="";
                    $total_division="";
                  
                    $color=hex2rgb("#A2D5F9");
                    $this->SetFillColor($color[0],$color[1],$color[2]);
                    foreach ($preguntas as $ke) {
                        $posx=10;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(5,15,$a,1,'C',true);
                        $posx=$posx+5;
                        $this->SetY($posy);
                        $this->SetX($posx);

                        $total_texto_pregunta=strlen($ke->pregunta);
                        $ancho=$total_texto_pregunta/55;
                        if ($ancho>=0 && $ancho<=1) {
                            $ancho_cuadro=15;
                        }elseif ($ancho>1 && $ancho<=2) {
                            $ancho_cuadro=7.5;
                        }else{
                            $ancho_cuadro=5;
                        }
                        $this->MultiCell(75,$ancho_cuadro,utf8_decode($ke->pregunta),1,'L',true);
                        $poscy=$posy;
                        $poscx=$posx+75;
                        $posx=$posx+75;
                        foreach ($alternativas as $k) {
                            if ($k->id_pregunta==$ke->id_pregunta) {
                                $this->SetY($poscy);
                                $this->SetX($poscx);
                                $this->MultiCell(90,5,utf8_decode("  ".$k->valor." : ".$k->texto),1,'L',true);
                                $poscy=$poscy+5;
                            }
                        }
                        $respuesta="--";
                        $observacion="";
                        if (array_key_exists($ke->id_pregunta,$respuestas)) {
                            foreach ($respuestas[$ke->id_pregunta] as $k) {
                                $respuesta=$k->respuesta;
                                $observacion=$k->observacion;
                                $total_sumatoria=$total_sumatoria+$respuesta;
                                $total_division=$total_division+1;
                            }
                        }
                        $posx=$posx+90;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(30,15,$respuesta,1,'C',true);
                        $posx=$posx+30;
                        $this->SetY($posy);
                        $this->SetX($posx);

                        $total_texto_pregunta=strlen($observacion);
                        $ancho=$total_texto_pregunta/70;
                        if ($ancho>=-1 && $ancho<=1) {
                            $ancho_cuadro=15;
                        }elseif ($ancho>1 && $ancho<=2) {
                            $ancho_cuadro=7.5;
                        }else{
                            $ancho_cuadro=5;
                        }
                        $this->MultiCell(80,$ancho_cuadro,utf8_decode($observacion),1,'C',true);
                        
                        $poscy=$posy;
                        $a++;
                        $posy=$posy+17;
                    }
                }
                $posx=10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,"Sumatoria",1,'L',false);
                $posx=$posx+80;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(90,10,'',1,'C',false);
                $posx=$posx+90;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(30,10,$total_sumatoria,1,'C',false);
                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,'',1,'C',false);

                $calificaciones_final="";
                $promedio=$total_sumatoria;
                $cali=calificaciones_final($calificacion,$promedio);
                $color=hex2rgb($cali["background"]);
                $posx=10;
                $posy=$posy+10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,"Promedio ".utf8_decode($titulo),1,'L',false);
                $posx=$posx+80;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(90,10,'',1,'C',false);
                $posx=$posx+90;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->SetFillColor($color[0],$color[1],$color[2]);
                $this->MultiCell(30,10,$promedio,1,'C',true);

                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,utf8_decode($cali["calificacion"]),1,'C',true);

                //saltamos de pagina
                $this->AddPage("L","A4");
                //codigo para imprimir las calificaciones
                $posx=10;
                $posy=10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->SetFillColor(127,209,127);
                $this->MultiCell(50,10,utf8_decode("Calificación"),1,'C',true);
                $posx=$posx+50;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(50,10,"Rango",1,'C',true);
                $posx=10;
                $posy=$posy+10;
                foreach ($calificacion as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(50,10,utf8_decode($key->calificacion),1,'C',false);
                    $posx=$posx+50;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(50,10,$key->texto_rango,1,'C',false);
                    $posx=10;
                    $posy=$posy+10;
                }
                $posy=$posy+30;
                if (count($criterios)!=null) {
                    //codigo para imprimir los criterios
                    $posx=120;
                    $posy=10;
                    foreach ($criterios as $key) {

                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(5,10,"",0,'C',false);
                        $posx=$posx+5;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(160,10,utf8_decode($key->titulo),1,'C',true);
                        $posy=$posy+10;
                        $e=1;
                        foreach ($criterios_detalle as $ke) {
                            $posx=120;
                            if ($key->id_criterio==$ke->id_criterio) {
                                if ($ke->valor!='') {
                                    $this->SetY($posy);
                                    $this->SetX($posx);
                                    $this->MultiCell(5,10,$ke->valor,1,'C',false);
                                }
                                $posx=$posx+5;
                                $this->SetY($posy);
                                $this->SetX($posx);
                                $this->MultiCell(160,10,utf8_decode($ke->texto),1,'L',false);
                                $posy=$posy+10;
                                $e++;
                            }
                        }
                        $posy=$posy+10;
                    }
                }
                if (count($notas)!=null) {
                    $posx=10;
                    $posy=$posy;
                    foreach ($notas as $key) {
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $vowels = array('<strong style="font-weight:700">','</strong>');
                        $this->MultiCell('',10,utf8_decode(str_replace($vowels, "", $key->texto)),0,'L',false);
                        $posy=$posy+10;
                    }
                }
            }
            function cuadro_tipo_4($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$aponderacion,$calificaciones){
                $this->SetFont('Arial','B',16);
                // Movernos a la derecha
                $this->Cell(null,5,utf8_decode($titulo),0,0,'C');
                // Salto de línea
                $this->SetFont('Times','',10);
                // Imprimir texto en una columna de 6 cm de ancho
                $this->SetFillColor(127,209,127);
                $this->SetY(40);
                $this->SetX(10);
                $this->MultiCell(90,10,"TRAZADORES",1,'C',true);
                $this->SetY(40);
                $this->SetX(100);
                $this->MultiCell(40,10,utf8_decode("VALOR EVALUADO"),1,'C',true);
                $this->SetY(40); 
                $this->SetX(140);
                $this->MultiCell(60,10,utf8_decode("CALIFICACIÓN"),1,'C',true);
                $this->SetY(40); 
                $this->SetX(200);
                $this->MultiCell(30,10,utf8_decode("PONDERACION"),1,'C',true);
                $this->SetY(40); 
                $this->SetX(230);
                $this->MultiCell(55,10,utf8_decode("VALOCARION PONDERADA"),1,'C',true);

                $valor_evaluado="";
                $cali["background"]="";
                $cali["calificacion"]="";
                $valoracion_ponderada="";
                $sumatoria="";
                $ponderacion_total="";
                $total_valoracion_ponderada="";
                $posx=10;
                $posy=50;
                $a=1;
                $color=hex2rgb("#A2D5F9");
                $this->SetFillColor($color[0],$color[1],$color[2]);
                foreach ($aponderacion as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(5,10,$a,1,'C',true);
                    $posx=$posx+5;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(85,10,utf8_decode($key->titulo),1,'L',true);
                    
                    if ($respuestas!=null) {
                        $valor_evaluado=sumatoria($respuestas[$key->id_cuadro]);
                        $cali=calificaciones_final($calificaciones[$key->id_cuadro],$valor_evaluado);
                        $valoracion_ponderada=$valor_evaluado*$key->ponderacion;
                        $sumatoria=$sumatoria+$valor_evaluado;
                        $total_valoracion_ponderada=$total_valoracion_ponderada+$valoracion_ponderada;
                    }
                    $posx=$posx+85;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(40,10,$valor_evaluado,1,'C',true);
                    $posx=$posx+40;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $color=hex2rgb($cali['background']);
                    $this->SetFillColor($color[0],$color[1],$color[2]);
                    $this->MultiCell(60,10,$cali['calificacion'],1,'C',true);
                    $posx=$posx+60;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $color=hex2rgb("#A2D5F9");
                    $this->SetFillColor($color[0],$color[1],$color[2]);
                    $this->MultiCell(30,10,$key->ponderacion,1,'C',true);
                    $posx=$posx+30;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(55,10,$valoracion_ponderada,1,'C',true);

                    $ponderacion_total=$ponderacion_total+$key->ponderacion;
                    $a++;
                    $posx=10;
                    $posy=$posy+10;
                }

                if ($respuestas!=null) {
                    $promedio=number_format($total_valoracion_ponderada/$ponderacion_total, 2, '.', ' ');
                    $califi_fi=calificaciones_final($calificacion,$promedio);
                }
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(90,10,"Sumatoria",1,'C',false);
                $posx=$posx+90;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(40,10,$sumatoria,1,'C',false);
                $posx=$posx+40;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(60,10,"",1,'C',false);
                $posx=$posx+60;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(30,10,$ponderacion_total,1,'C',false);
                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(55,10,'',1,'C',false);

                $posx=10;
                $posy=$posy+10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(90,10,"Promedio Global",1,'C',false);
                $posx=$posx+90;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(40,10,"",1,'C',false);
                $posx=$posx+40;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(60,10,"",1,'C',false);
                $posx=$posx+60;
                $color=hex2rgb($califi_fi["background"]);
                $this->SetFillColor($color[0],$color[1],$color[2]);
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(30,10,$promedio,1,'C',true);
                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(55,10,$califi_fi["calificacion"],1,'C',true);

                //saltamos de pagina
                $this->AddPage("L","A4");
                //codigo para imprimir las calificaciones
                $posx=10;
                $posy=10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->SetFillColor(127,209,127);
                $this->MultiCell(50,10,utf8_decode("Calificación"),1,'C',true);
                $posx=$posx+50;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(50,10,"Rango",1,'C',true);
                $posx=10;
                $posy=$posy+10;
                foreach ($calificacion as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(50,10,utf8_decode($key->calificacion),1,'C',false);
                    $posx=$posx+50;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(50,10,$key->texto_rango,1,'C',false);
                    $posx=10;
                    $posy=$posy+10;
                }
                $posy=$posy+30;
                if (count($criterios)!=null) {
                    //codigo para imprimir los criterios
                    $posx=120;
                    $posy=10;
                    foreach ($criterios as $key) {
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(5,10,"",0,'C',false);
                        $posx=$posx+5;
                        $this->SetY($posy);
                        $this->SetX($posx);
                        $this->MultiCell(160,10,utf8_decode($key->titulo),1,'C',true);
                        $posy=$posy+10;
                        $e=1;
                        foreach ($criterios_detalle as $ke) {
                            $posx=120;
                            if ($key->id_criterio==$ke->id_criterio) {
                                if ($ke->valor!='') {
                                    $this->SetY($posy);
                                    $this->SetX($posx);
                                    $this->MultiCell(5,10,$ke->valor,1,'C',false);
                                }
                                $posx=$posx+5;
                                $this->SetY($posy);
                                $this->SetX($posx);
                                $this->MultiCell(160,10,utf8_decode($ke->texto),1,'L',false);
                                $posy=$posy+10;
                                $e++;
                            }
                        }
                        $posy=$posy+10;
                    }
                }
                if (count($notas)!=null) {
                    $posx=10;
                    $posy=$posy;
                    foreach ($notas as $key) {
                        $this->SetY($posy);
                        $this->SetX($posx);
                         $vowels = array('<strong style="font-weight:700">','</strong>');
                        $this->MultiCell('',10,utf8_decode(str_replace($vowels, "", $key->texto)),0,'L',false);
                        $posy=$posy+10;
                    }
                }

            }
            function cuadro_tipo_5($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas){
                $this->SetFont('Arial','B',16);
                // Movernos a la derecha
                $this->Cell(null,5,utf8_decode($titulo),0,0,'C');
                // Salto de línea
                $this->SetFont('Times','',10);
                // Imprimir texto en una columna de 6 cm de ancho
                $this->SetFillColor(127,209,127);
                $this->SetY(40);
                $this->SetX(10);
                $this->MultiCell(80,10,"TRAZADOR",1,'C',true);
                $this->SetY(40);
                $this->SetX(90);
                $this->MultiCell(90,10,utf8_decode("CRITERIOS DE VALORACIÓN"),1,'C',true);
                $this->SetY(40); 
                $this->SetX(180);
                $this->MultiCell(30,10,utf8_decode("CALIFICACIÓN"),1,'C',true);
                $this->SetY(40); 
                $this->SetX(210);
                $this->MultiCell(80,10,utf8_decode("OBSERVACIONES"),1,'C',true);
                $posx=10;
                $posy=50;
                $total_sumatoria="";
                $total_division="";
                foreach ($tablas as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,5,$key->titulo,1,'C');
                    $posx=$posx+80;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(90,5,"",1,'C');
                    $posx=$posx+90;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(30,5,"Porcentaje",1,'C');
                    $posx=$posx+30;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,5,"",1,'C');
                    
                    $posy=$posy+5;
                    $a=1;
                    $this->SetFont('Times','',8);
                    
                  
                    $color=hex2rgb("#A2D5F9");
                    $this->SetFillColor($color[0],$color[1],$color[2]);
                    foreach ($preguntas as $ke) {
                        if ($key->id_tabla==$ke->id_tabla) {
                            $posx=10;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(5,15,$a,1,'C',true);
                            $posx=$posx+5;
                            $this->SetY($posy);
                            $this->SetX($posx);

                            $total_texto_pregunta=strlen($ke->pregunta);
                            $ancho=$total_texto_pregunta/55;
                            if ($ancho>=0 && $ancho<=1) {
                                $ancho_cuadro=15;
                            }elseif ($ancho>1 && $ancho<=2) {
                                $ancho_cuadro=7.5;
                            }else{
                                $ancho_cuadro=5;
                            }
                            $this->MultiCell(75,$ancho_cuadro,utf8_decode($ke->pregunta),1,'L',true);
                            $poscy=$posy;
                            $poscx=$posx+75;
                            $posx=$posx+75;
                            foreach ($alternativas as $k) {
                                if ($k->id_pregunta==$ke->id_pregunta) {
                                    $this->SetY($poscy);
                                    $this->SetX($poscx);
                                    $this->MultiCell(90,5,utf8_decode("  ".$k->valor." : ".$k->texto),1,'L',true);
                                    $poscy=$poscy+5;
                                }
                            }
                            $respuesta="--";
                            $observacion="";
                            if (array_key_exists($ke->id_pregunta,$respuestas)) {
                                foreach ($respuestas[$ke->id_pregunta] as $k) {
                                    $respuesta=$k->respuesta;
                                    $observacion=$k->observacion;
                                    $total_sumatoria=$total_sumatoria+$respuesta;
                                    $total_division=$total_division+1;
                                }
                            }
                            $posx=$posx+90;
                            $this->SetY($posy);
                            $this->SetX($posx);
                            $this->MultiCell(30,15,$respuesta,1,'C',true);
                            $posx=$posx+30;
                            $this->SetY($posy);
                            $this->SetX($posx);

                            $total_texto_pregunta=strlen($observacion);
                            $ancho=$total_texto_pregunta/70;
                            if ($ancho>=-1 && $ancho<=1) {
                                $ancho_cuadro=15;
                            }elseif ($ancho>1 && $ancho<=2) {
                                $ancho_cuadro=7.5;
                            }else{
                                $ancho_cuadro=5;
                            }
                            $this->MultiCell(80,$ancho_cuadro,utf8_decode($observacion),1,'C',true);
                            
                            $poscy=$posy;
                            $a++;
                            $posy=$posy+15;
                        }
                    }
                    $posx=10;
                }
                $posx=10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,"Sumatoria",1,'L',false);
                $posx=$posx+80;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(90,10,'',1,'C',false);
                $posx=$posx+90;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(30,10,$total_sumatoria,1,'C',false);
                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,'',1,'C',false);

                $calificaciones_final="";
                $promedio=$total_sumatoria;
                $cali=calificaciones_final($calificacion,$promedio);
                $color=hex2rgb($cali["background"]);
                $posx=10;
                $posy=$posy+10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,"Promedio ".utf8_decode($titulo),1,'L',false);
                $posx=$posx+80;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(90,10,'',1,'C',false);
                $posx=$posx+90;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->SetFillColor($color[0],$color[1],$color[2]);
                $this->MultiCell(30,10,$promedio,1,'C',true);

                $posx=$posx+30;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,utf8_decode($cali["calificacion"]),1,'C',true);

                //saltamos de pagina
                $this->AddPage("L","A4");
                //codigo para imprimir las calificaciones
                $posx=10;
                $posy=10;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->SetFillColor(127,209,127);
                $this->MultiCell(80,10,utf8_decode("Calificación"),1,'C',true);
                $posx=$posx+80;
                $this->SetY($posy);
                $this->SetX($posx);
                $this->MultiCell(80,10,"Rango",1,'C',true);
                $posx=10;
                $posy=$posy+10;
                foreach ($calificacion as $key) {
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,10,utf8_decode($key->calificacion),1,'C',false);
                    $posx=$posx+80;
                    $this->SetY($posy);
                    $this->SetX($posx);
                    $this->MultiCell(80,10,$key->texto_rango,1,'C',false);
                    $posx=10;
                    $posy=$posy+10;
                }
                //codigo para imprimir los criterios
            }
        }
        $pdf=new PDF();
        $pdf->AliasNbPages();
        foreach ($cuadros as $key) {
            $pdf->AddPage("L","A4");
            $pdf->cabecera($datos_llenado);
            $pdf->cuadros($info[$key->id_cuadro]["id_cuadro"],$info[$key->id_cuadro]["tipo"],$info[$key->id_cuadro]["titulo"],$info[$key->id_cuadro]["calificacion"],$info[$key->id_cuadro]["criterios"]
                ,$info[$key->id_cuadro]["criterios_detalle"],$info[$key->id_cuadro]["notas"],$info[$key->id_cuadro]["tablas"]
                ,$info[$key->id_cuadro]["preguntas"],$info[$key->id_cuadro]["alternativas"],$info[$key->id_cuadro]["respuestas"]
                ,$info[$key->id_cuadro]["aponderacion"],$info[$key->id_cuadro]["calificaciones"]);
        }
        $pdf->Output();
    }elseif ($archivo=="excel") {
        $posicion = array("A","B","C","D","E","F","G","H","I","J",
            "K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        require "application/libraries/phpexcel/PHPExcel.php";
        require_once 'application/libraries/phpexcel/PHPExcel/Writer/Excel5.php';     // Para otros bajo la versión xls 
        require_once 'application/libraries/phpexcel/PHPExcel/Writer/Excel2007.php'; // Formato para la excel-2007 
        global $objPHPExcel;
        $objPHPExcel=new PHPExcel();
        $archivo="encuesta.xls";
        $e=0;
        
        function cellColor($cells,$color){
            global $objPHPExcel;
            $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                     'rgb' => $color
                )
            ));
        }
        $textblue = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '17375D'),
                'size'  => 10,
                'name'  => 'Arial'
            )
        );
        $textblueborder = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '17375D'),
                'size'  => 10,
                'name'  => 'Arial'
            ),
            'borders'=> array(
                'allborders'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('rgb' => '000000')
                ) 
            ) 
        );
        $border = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 10,
                'name'  => 'Arial'
            ),
            'borders'=> array(
                'top'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('rgb' => '000000')
                ),
                'left'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('rgb' => '000000')
                ),
                'right'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('rgb' => '000000')
                ),
                'bottom'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('rgb' => '000000')
                )
                
            ) 
        );
        $textblanco = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'FFFFFF'),
                'size'  => 10,
                'name'  => 'Arial'
        ));
        $detalleplomo= array(
            'borders'=> array(
                'top'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('rgb' => '808080')
                ),
                'left'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('rgb' => '808080')
                ),
                'right'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('rgb' => 'FFFFFF')
                ),
                'bottom'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('rgb' => 'FFFFFF')
                )
                
            ) 
        );
        foreach ($cuadros as $keys) {
            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex($e)->setTitle($info[$keys->id_cuadro]["tab"]);
            /*
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $logo = 'img/logo_pb.png'; // Provide path to your logo file
            $objDrawing->setPath($logo);  //setOffsetY has no effect
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(110); // logo height
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/

            $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
            $objPHPExcel->getProperties()->setCreator("weblocalhost")
                ->setLastModifiedBy("weblocalhost")
                ->setTitle("Reporte XLS")
                ->setSubject("Réporte")
                ->setDescription("")
                ->setKeywords("")
                ->setCategory("");
            $objPHPExcel->getActiveSheet()
                ->mergeCells('A1:M1');
            $objPHPExcel->getActiveSheet()->mergeCells('A2:M2')
                ->getStyle('A2')
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->setActiveSheetIndex($e)
                ->setCellValue('A2',$info[$keys->id_cuadro]["titulo"]);
            $objPHPExcel->getActiveSheet()->mergeCells('A3:M3');

            if ($info[$keys->id_cuadro]["tipo"]==0) {
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(23);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(19); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);  
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(26); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(7); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(4); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(7); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(4);

                cellColor('A4:M9', 'BFBFBF');
                $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A5',"Nombre del poblado/localidad:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A5')->applyFromArray($textblue);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G5',"Evaluador:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G5')->applyFromArray($textblue);
                $objPHPExcel->getActiveSheet()->mergeCells('A6:E6'); 
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A6:E6')->applyFromArray($detalleplomo);   
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A6',$datos_llenado["comunidad"]);
                    if ($datos_llenado["usuario"]=="") {
                        $datos_llenado["usuario"]="usuario eliminado";
                    }
                 $objPHPExcel->getActiveSheet()
                    ->getStyle('G6:L6')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G6',$datos_llenado["usuario"]);

                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A7',"Localizacion:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A7')->applyFromArray($textblue);            
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G7',"Fecha:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G7')->applyFromArray($textblue);

                $objPHPExcel->getActiveSheet()->mergeCells('A8:E8');
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A8:E8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A8',$datos_llenado["departamento"]."--".$datos_llenado["provincia"]."--".$datos_llenado["distrito"]);
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G8:L8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G8',$datos_llenado["fecha"]);
                $objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(5);

                $a=11;
                $posder=9;
                foreach ($info[$keys->id_cuadro]["tablas"] as $key) {
                    if ($key->tipo==1) {
                        cellColor('A'.$a.':C'.$a, '17375D');
                        $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':C'.$a)
                            ->getStyle('A'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a)->applyFromArray($textblanco);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"Georeferenciación del centro poblado");

                        $a++;
                        cellColor('A'.$a.':B'.$a, 'FFFF99');
                        $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a)
                            ->getStyle('A'.$a.':B'.$a)
                            ->applyFromArray($textblueborder);
                        
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"Coordenas  UTM")
                            ->getStyle('A'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                        $a++;
                        cellColor('A'.$a, 'FFFF99');
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a)
                            ->applyFromArray($textblueborder);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"Norte")
                            ->getStyle('A'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   
                        cellColor('B'.$a, 'FFFF99');
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('B'.$a)
                            ->applyFromArray($textblueborder);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('B'.$a,"Este")
                            ->getStyle('B'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        cellColor('C'.($a-1).':C'.$a, 'FFFF99');
                        $objPHPExcel->getActiveSheet()->mergeCells('C'.($a-1).':C'.$a)
                            ->getStyle('C'.($a-1).':C'.$a)
                            ->applyFromArray($textblueborder);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.($a-1),"Altitud msnm")
                            ->getStyle('C'.($a-1).':C'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  

                        $a++;
                        $i=0;
                        foreach ($info[$keys->id_cuadro]["preguntas"] as $ke) {
                            if ($ke->id_tabla==$key->id_tabla) {
                                $respuesta="---";
                                if ($info[$keys->id_cuadro]["respuestas"][$ke->id_pregunta]!=null) {
                                    foreach ($info[$keys->id_cuadro]["respuestas"][$ke->id_pregunta] as $res) {
                                        if ($res->respuesta!="") {
                                            $respuesta=$res->respuesta;
                                        }
                                    }
                                }
                                $objPHPExcel->getActiveSheet()
                                    ->getStyle($posicion[$i].$a)
                                    ->applyFromArray($border);
                                $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue($posicion[$i].$a,$respuesta)
                                        ->getStyle($posicion[$i].$a)
                                        ->getAlignment()
                                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $i++;
                            }
                        } 
                    }elseif ($key->tipo==2) {
                        $a++;$a++;
                        cellColor('A'.$a.':B'.$a, '17375D');
                        $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a)
                            ->getStyle('A'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a)->applyFromArray($textblanco);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"Información básica");
                        
                        cellColor('C'.$a, '17375D');
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)
                            ->applyFromArray($textblanco);
                        
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.$a,"Llenar")
                            ->getStyle('C'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                
                        $id_pregunta=[];
                        $i=0;
                        foreach ($info[$keys->id_cuadro]["preguntas"] as $ke) {
                            if ($ke->id_tabla==$key->id_tabla) {
                                //echo $ke->id_pregunta;
                                $id_pregunta[$i]=$ke->id_pregunta;
                                $i++;
                            }
                        }

                        $a++;
                        //cellColor('A'.$a.':B'.$a, '17375D');
                        $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a.':B'.$a)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"Número de habitantes totales en el sistema ");
                        $respuesta="---";
                        if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                            foreach ($info[$keys->id_cuadro]["respuestas"][$id_pregunta[0]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.$a,$respuesta);

                        $a++;
                        $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a.':B'.$a)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"Caudal de producción de las fuentes de agua (lt/seg)");
                        $respuesta="---";
                        if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                            foreach ($info[$keys->id_cuadro]["respuestas"][$id_pregunta[1]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.$a,$respuesta);

                        $a++;
                        $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a.':B'.$a)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"Monto promedio de la cuota mensual S/");
                        $respuesta="---";
                        if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                            foreach ($info[$keys->id_cuadro]["respuestas"][$id_pregunta[2]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.$a,$respuesta);


                        $a++;
                        cellColor('A'.$a.':C'.$a, 'FFFF99');
                        $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':C'.$a)
                            ->getStyle('A'.$a.':C'.$a)
                            ->applyFromArray($textblueborder);
                        
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"Dotación de agua")
                            ->getStyle('A'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                        $a++;
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"Zona geográfica");
                        $respuesta="---";
                        if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                            foreach ($info[$keys->id_cuadro]["respuestas"][$id_pregunta[3]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        if ($respuesta==2) {
                            $respuesta="Selva";
                        }elseif ($respuesta==1) {
                            $respuesta="Sierra";
                        }elseif ($respuesta==="0") {
                            $respuesta="Costa";
                        }else{
                            $respuesta="---";
                        }
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.($a+1))->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.($a+1),$respuesta);

                        $objPHPExcel->getActiveSheet()
                            ->getStyle('B'.$a)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('B'.$a,"Zona geográfica");
                        $respuesta="---";
                        if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                            foreach ($info[$keys->id_cuadro]["respuestas"][$id_pregunta[4]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        if ($respuesta==0) {
                            $respuesta="Con arrastre";
                        }elseif ($respuesta==1) {
                            $respuesta="Sin arrastre";
                        }else{
                            $respuesta="---";
                        }
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('B'.($a+1))->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('B'.($a+1),$respuesta);


                        $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.$a,"Lt/hab/día");
                        $respuesta="---";
                        if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                            foreach ($info[$keys->id_cuadro]["respuestas"][$id_pregunta[5]] as $res) {
                                if ($res->respuesta!="") {
                                    $respuesta=$res->respuesta;
                                }
                            }
                        }
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.($a+1))->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.($a+1),$respuesta);
                    }elseif ($key->tipo==3) {
                        if ($key->id_tabla==15) {
                            $a++;$a++;$a++;
                            cellColor('A'.$a.':C'.$a, '17375D');
                            $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a);
                            $objPHPExcel->getActiveSheet()
                                ->getStyle('A'.$a.':B'.$a)->applyFromArray($textblanco);
                            $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('A'.$a,$key->titulo);
                            
                          
                            $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a)->applyFromArray($textblanco);
                            $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('C'.$a,"Marcar");

                            foreach ($info[$keys->id_cuadro]["preguntas"] as $ke) {
                                if ($ke->id_tabla==$key->id_tabla) {
                                    foreach ($info[$keys->id_cuadro]["alternativas"] as $k) {
                                        if ($k->id_pregunta==$ke->id_pregunta) {
                                            $a++;
                                            $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a);
                                            $objPHPExcel->getActiveSheet()
                                                ->getStyle('A'.$a.':B'.$a)->applyFromArray($border);
                                            $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('A'.$a,$k->texto);
                                            if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                                                foreach ($info[$keys->id_cuadro]["respuestas"][$ke->id_pregunta] as $res) {
                                                    $valores=explode("/", $res->respuesta);
                                                    $respuesta="";
                                                    foreach ($valores as $var) {
                                                        if ($var==$k->valor) {
                                                            $respuesta="X";
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            $objPHPExcel->getActiveSheet()
                                                ->getStyle('C'.$a)->applyFromArray($border);
                                            $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('C'.$a,$respuesta);
                                        }
                                    }
                                }
                            }
                        }else{
                            $posder++;$posder++;
                            cellColor('E'.$posder.':G'.$posder, '17375D');
                            $objPHPExcel->getActiveSheet()->mergeCells('E'.$posder.':F'.$posder);
                            $objPHPExcel->getActiveSheet()
                                ->getStyle('E'.$posder.':F'.$posder)->applyFromArray($textblanco);
                            $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('E'.$posder,$key->titulo);
                            
                            
                            $objPHPExcel->getActiveSheet()
                                ->getStyle('G'.$posder)->applyFromArray($textblanco);
                            $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('G'.$posder,"Marcar");

                            foreach ($info[$keys->id_cuadro]["preguntas"] as $ke) {
                                if ($ke->id_tabla==$key->id_tabla) {
                                    foreach ($info[$keys->id_cuadro]["alternativas"] as $k) {
                                        if ($k->id_pregunta==$ke->id_pregunta) {
                                            $posder++;
                                            $objPHPExcel->getActiveSheet()->mergeCells('E'.$posder.':F'.$posder);
                                            $objPHPExcel->getActiveSheet()
                                                ->getStyle('E'.$posder.':F'.$posder)->applyFromArray($border);
                                            $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('E'.$posder,$k->texto);
                                            if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                                                foreach ($info[$keys->id_cuadro]["respuestas"][$ke->id_pregunta] as $res) {
                                                    $valores=explode("/", $res->respuesta);
                                                    $respuesta="";
                                                    foreach ($valores as $var) {
                                                        if ($var==$k->valor) {
                                                            $respuesta="X";
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            $objPHPExcel->getActiveSheet()
                                                ->getStyle('G'.$posder)->applyFromArray($border);
                                            $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('G'.$posder,$respuesta);
                                        }
                                    }
                                }
                            }
                        }
                    }                 
                }

            }elseif ($info[$keys->id_cuadro]["tipo"]==1) {
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(42); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(2);  
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(44); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(7); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(22); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(2);

                cellColor('A4:R9', 'BFBFBF');
                $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A5',"Nombre del poblado/localidad:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A5')->applyFromArray($textblue);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G5',"Evaluador:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G5')->applyFromArray($textblue);
                $objPHPExcel->getActiveSheet()->mergeCells('B6:E6'); 
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B6:E6')->applyFromArray($detalleplomo);   
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('B6',$datos_llenado["comunidad"]);
                    if ($datos_llenado["usuario"]=="") {
                        $datos_llenado["usuario"]="usuario eliminado";
                    }
                 $objPHPExcel->getActiveSheet()
                    ->getStyle('G6:L6')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G6',$datos_llenado["usuario"]);

                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A7',"Localizacion:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A7')->applyFromArray($textblue);            
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G7',"Fecha:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G7')->applyFromArray($textblue);

                $objPHPExcel->getActiveSheet()->mergeCells('B8:E8');
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B8:E8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('B8',$datos_llenado["departamento"]."--".$datos_llenado["provincia"]."--".$datos_llenado["distrito"]);
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G8:L8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G8',$datos_llenado["fecha"]);
                $objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(5);
                $a=11;
                $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(33);
                cellColor('A'.$a.':R'.$a, '17375D');
                $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a)
                            ->getStyle('A'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"TRAZADOR");

                $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                            ->getStyle('C'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.$a,"CRITERIOS DE VALORACIÓN");

                $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                            ->getStyle('F'.$a.':I'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('F'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('F'.$a,"CALIFICACIÓN");

                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a)
                            ->getStyle('J'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('J'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('J'.$a,"OBSERVACIONES");
                $total_sumatoria="";
                $total_division="";
                foreach ($info[$keys->id_cuadro]["tablas"] as $key) {
                    $a++;
                    cellColor('A'.$a.':R'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a)
                            ->getStyle('A'.$a.':B'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('A'.$a.':B'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('A'.$a,$key->titulo);

                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($textblueborder);

                    $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                            ->getStyle('F'.$a.':I'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('F'.$a,"Valor");

                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':R'.$a)->applyFromArray($textblueborder);
                                
                    $nu=1;
                    foreach ($info[$keys->id_cuadro]["preguntas"] as $ke) {
                        if ($key->id_tabla==$ke->id_tabla) {
                            $a++;
                            cellColor('A'.$a.':R'.$a, 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(5);
                            $a++;
                            cellColor('A'.$a.':A'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':A'.($a+2))
                                    ->getStyle('A'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('A'.$a.':A'.($a+2))->applyFromArray($border);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('A'.$a,$nu);

                            cellColor('B'.$a.':B'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('B'.$a.':B'.($a+2))
                                    ->getStyle('B'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('B'.$a.':B'.($a+2))->applyFromArray($border);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('B'.$a,$ke->pregunta);
                            $poalter=0;            
                            foreach ($info[$keys->id_cuadro]["alternativas"] as $k) {
                                if ($k->id_pregunta==$ke->id_pregunta) {
                                    cellColor('C'.($a+$poalter).':E'.($a+$poalter), 'C0C0C0');
                                    $objPHPExcel->getActiveSheet()->mergeCells('C'.($a+$poalter).':E'.($a+$poalter))
                                            ->getStyle('C'.($a+$poalter).':E'.($a+$poalter))
                                            ->getAlignment()
                                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                ->getStyle('C'.($a+$poalter).':E'.($a+$poalter))->applyFromArray($border);
                                    $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('C'.($a+$poalter),$k->valor." : ".$k->texto);
                                    $poalter++;                  
                                }
                            }
                            

                            $respuesta="--";
                            $observacion="";
                            if (array_key_exists($ke->id_pregunta,$info[$keys->id_cuadro]["respuestas"])) {
                                foreach ($info[$keys->id_cuadro]["respuestas"][$ke->id_pregunta] as $k) {
                                    $respuesta=$k->respuesta;
                                    $observacion=$k->observacion;
                                    $total_sumatoria=$total_sumatoria+$respuesta;
                                    $total_division=$total_division+1;
                                }
                            }
                            cellColor('F'.$a.':F'.($a+2), 'C0C0C0');
                            cellColor('G'.$a.':H'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('G'.$a.':H'.($a+2))
                                    ->getStyle('G'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('G'.$a.':H'.($a+2))->applyFromArray($detalleplomo);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('G'.$a,$respuesta);
                            cellColor('I'.$a.':I'.($a+2), 'C0C0C0');
                            cellColor('J'.$a.':J'.($a+2), 'C0C0C0');            
                            cellColor('K'.$a.':Q'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('K'.$a.':Q'.($a+2))
                                    ->getStyle('K'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('K'.$a.':Q'.($a+2))->applyFromArray($detalleplomo);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('K'.$a,$observacion);
                            cellColor('R'.$a.':R'.($a+2), 'C0C0C0');            
                            $a++;
                            $a++;
                            $nu++;
                        }
                    }  
                    $a++;
                    cellColor('A'.$a.':R'.$a, 'C0C0C0');
                    $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(5);

                    
                }
                $a++;
                    cellColor('B'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Sumatoria");

                    cellColor('C'.$a.':E'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);

                    $calificaciones_final="";
                    $promedio=number_format($total_sumatoria/$total_division, 2, '.', ' ');
                    $cali=calificaciones_final($info[$keys->id_cuadro]["calificacion"],$promedio);

                    $col = str_replace("#","",$cali["background"]);
                    cellColor('F'.$a.':I'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                                ->getStyle('F'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('F'.$a,$total_sumatoria);

                    cellColor('J'.$a.':R'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a)
                                ->getStyle('J'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':R'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('J'.$a,$keys->titulo);

                    $a++;
                    cellColor('B'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Promedio");

                    cellColor('C'.$a.':E'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);

                    cellColor('F'.$a.':I'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                                ->getStyle('F'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('F'.$a,$promedio);

                    cellColor('J'.$a.':R'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a)
                                ->getStyle('J'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':R'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('J'.$a,$cali["calificacion"]);
                    $a++;$a++;

                    cellColor('B'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Calificación");

                    cellColor('C'.$a.':E'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                                ->getStyle('C'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('C'.$a,"Rango");
                    $incri=$a;
                foreach ($info[$keys->id_cuadro]["calificacion"] as $ke) {
                    $a++;
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('B'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                         ->getStyle('B'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('B'.$a,$ke->calificacion);

                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                        ->getStyle('C'.$a.':E'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('C'.$a,$ke->texto_rango);
                } 
                if (count($info[$keys->id_cuadro]["criterios"])!=null) {
                    foreach ($info[$keys->id_cuadro]["criterios"] as $key) {

                        //$this->MultiCell(5,10,"",0,'C',false);

                        $objPHPExcel->getActiveSheet()->mergeCells('G'.$incri.':R'.$incri)
                            ->getStyle('G'.$incri.':R'.$incri)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('G'.$incri.':R'.$incri)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('G'.$incri,$key->titulo);

                        foreach ($info[$keys->id_cuadro]["criterios_detalle"] as $ke) {
                            if ($key->id_criterio==$ke->id_criterio) {
                                $incri++;
                                $objPHPExcel->getActiveSheet()->mergeCells('G'.$incri.':R'.$incri)
                                    ->getStyle('G'.$incri.':R'.$incri)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                $objPHPExcel->getActiveSheet()
                                    ->getStyle('G'.$incri.':R'.$incri)->applyFromArray($border);
                                $objPHPExcel->setActiveSheetIndex($e)
                                    ->setCellValue('G'.$incri,$ke->texto);
                            }
                        }
                        $incri++;
                        $incri++;
                    }
                }
                $a++;
                $a++;
                $a++;
                 $a++;
                  $a++;
                $a++;
                if (count($info[$keys->id_cuadro]["notas"])!=null) {
                    foreach ($info[$keys->id_cuadro]["notas"] as $key) {
                        $objPHPExcel->getActiveSheet()->mergeCells('B'.$a.':Q'.$a)
                            ->getStyle('B'.$a.':Q'.$a);
                        $vowels = array('<strong style="font-weight:700">','</strong>');
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('B'.$a,str_replace($vowels, "", $key->texto));
                        $a++;
                    }
                }
            }elseif ($info[$keys->id_cuadro]["tipo"]==2) {
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(3); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(38);  
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(1); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(1); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(1); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(1); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(1);
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(1);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(6);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(1);
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(1);
                $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(6);
                $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(1);
                $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(1);
                $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(6);
                $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(1);
                $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(1);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(34);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(1);

                $a=4;
                cellColor('A'.$a.':Z'.($a+3), '17375D');
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a.':Z'.($a+3))->applyFromArray($textblanco);
                $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':C'.($a+2))
                            ->getStyle('A'.$a.':C'.($a+2))
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a.':C'.($a+2))->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"TRAZADOR");

                $objPHPExcel->getActiveSheet()->mergeCells('D'.$a.':I'.($a+1))
                            ->getStyle('D'.$a.':I'.($a+1))
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('D'.$a.':I'.($a+1))->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('D'.$a,"Existencia de componentes en el proyecto");

                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':N'.$a)
                            ->getStyle('J'.$a.':N'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('J'.$a.':N'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('J'.$a,"Criterio Nº 1");

                $objPHPExcel->getActiveSheet()->mergeCells('O'.$a.':W'.($a+3))
                            ->getStyle('O'.$a.':W'.($a+3))
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('O'.$a.':W'.($a+3))->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('O'.$a,"Estado del componente");

                $objPHPExcel->getActiveSheet()->mergeCells('X'.$a.':Z'.($a+3))
                            ->getStyle('X'.$a.':Z'.($a+3))
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('X'.$a.':Z'.($a+3))->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('X'.$a,"Observaciones");

                $a++;
                $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(30);
                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':N'.$a)
                            ->getStyle('J'.$a.':N'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('J'.$a.':N'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('J'.$a,"CALIFICACIÓN");
                
                $a++;
                $objPHPExcel->getActiveSheet()->mergeCells('D'.$a.':F'.$a)
                            ->getStyle('D'.$a.':F'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('D'.$a.':F'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('D'.$a,"SI");

                $objPHPExcel->getActiveSheet()->mergeCells('G'.$a.':I'.$a)
                            ->getStyle('G'.$a.':I'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('G'.$a.':I'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('G'.$a,"NO");

                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':K'.$a)
                            ->getStyle('J'.$a.':K'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('J'.$a,"DETERIORADO");

                $objPHPExcel->getActiveSheet()
                            ->getStyle('L'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('L'.$a,"LIMITADO");

                $objPHPExcel->getActiveSheet()->mergeCells('M'.$a.':N'.$a)
                            ->getStyle('M'.$a.':N'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('M'.$a,"ADECUADO");


                $a++;
                $objPHPExcel->getActiveSheet()->mergeCells('D'.$a.':I'.$a)
                            ->getStyle('D'.$a.':I'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('D'.$a,"Marcar con 'X'");

                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':K'.$a)
                            ->getStyle('J'.$a.':K'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('J'.$a,"2");

                $objPHPExcel->getActiveSheet()
                            ->getStyle('L'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('L'.$a,"1");

                $objPHPExcel->getActiveSheet()->mergeCells('M'.$a.':N'.$a)
                            ->getStyle('M'.$a.':N'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('M'.$a,"0");

                foreach ($info[$keys->id_cuadro]["tablas"] as $key) {
                    $a++;
                    cellColor('A'.$a.':Z'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':C'.$a)
                        ->getStyle('A'.$a.':C'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('A'.$a,$key->titulo);

                    $objPHPExcel->getActiveSheet()->mergeCells('D'.$a.':I'.$a)
                        ->getStyle('D'.$a.':I'.$a)->applyFromArray($textblueborder);

                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':N'.$a)
                        ->getStyle('J'.$a.':N'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->getActiveSheet()
                            ->getStyle('J'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);   
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('J'.$a,"Seleccionar valor");

                    $objPHPExcel->getActiveSheet()->mergeCells('O'.$a.':W'.$a)
                        ->getStyle('O'.$a.':W'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->getActiveSheet()->mergeCells('X'.$a.':Z'.$a)
                        ->getStyle('X'.$a.':Z'.$a)->applyFromArray($textblueborder);

                    $npre=1;    
                    $total_si="";
                    $total_no="";
                    $total_sumatoria="";
                    $total_division="";
                    $estado="";
                    foreach ($info[$keys->id_cuadro]["preguntas"] as $ke) {
                        if ($ke->id_tabla==$key->id_tabla) {
                            $a++;
                                cellColor('A'.$a.':Z'.$a, 'BFBFBF');
                                $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(6);

                            $a++;
                                cellColor('A'.$a.':Z'.$a, 'BFBFBF');
                                $objPHPExcel->getActiveSheet()
                                            ->getStyle('A'.$a)
                                            ->getAlignment()
                                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                $objPHPExcel->setActiveSheetIndex($e)
                                            ->setCellValue('A'.$a,$npre);
                            
                                $objPHPExcel->getActiveSheet()->mergeCells('B'.$a.':C'.$a)
                                    ->getStyle('B'.$a.':C'.$a);
                                $objPHPExcel->getActiveSheet()
                                        ->getStyle('B'.$a)
                                        ->getAlignment()
                                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);   
                                $objPHPExcel->setActiveSheetIndex($e)
                                            ->setCellValue('B'.$a,$ke->pregunta);

                                $respuesta="--";
                                $observacion="";
                                if (array_key_exists($ke->id_pregunta,$info[$keys->id_cuadro]["respuestas"])) {
                                    foreach ($info[$keys->id_cuadro]["respuestas"][$ke->id_pregunta] as $k) {
                                        $respuesta=$k->respuesta;
                                        $observacion=$k->observacion;
                                        $total_sumatoria=$total_sumatoria+$respuesta;
                                        $total_division=$total_division+1;
                                    }
                                }
                                if ($respuesta=="--" || $respuesta=="") {
                                    $total_no=$total_no+1;
                                    $objPHPExcel->getActiveSheet()
                                                ->getStyle('E'.$a)
                                                ->getAlignment()
                                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                            ->getStyle('E'.$a)
                                            ->applyFromArray($detalleplomo);
                                    $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('E'.$a,"");

                                    $objPHPExcel->getActiveSheet()
                                                ->getStyle('H'.$a)
                                                ->getAlignment()
                                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                            ->getStyle('H'.$a)
                                            ->applyFromArray($detalleplomo);
                                    $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('H'.$a,"X");
                                }else{
                                    $total_si=$total_si+1;                           
                                    $objPHPExcel->getActiveSheet()
                                                ->getStyle('E'.$a)
                                                ->getAlignment()
                                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                            ->getStyle('E'.$a)
                                            ->applyFromArray($detalleplomo);
                                    $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('E'.$a,"X");

                                    $objPHPExcel->getActiveSheet()
                                                ->getStyle('H'.$a)
                                                ->getAlignment()
                                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                            ->getStyle('H'.$a)
                                            ->applyFromArray($detalleplomo);
                                    $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('H'.$a,"");
                                }

                                $objPHPExcel->getActiveSheet()->mergeCells('K'.$a.':M'.$a)
                                    ->getStyle('K'.$a.':M'.$a)->applyFromArray($detalleplomo);
                                $objPHPExcel->getActiveSheet()
                                        ->getStyle('K'.$a)
                                        ->getAlignment()
                                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);   
                                $objPHPExcel->setActiveSheetIndex($e)
                                            ->setCellValue('K'.$a,$respuesta);

                                if ($respuesta==2) {
                                   $estado="DETERIORADO";
                                }elseif ($respuesta==1) {
                                    $estado="LIMITADO";
                                }elseif ($respuesta==="0") {
                                   $estado="ADECUADO";
                                }else{
                                    $estado="---";
                                }  
                                $objPHPExcel->getActiveSheet()->mergeCells('P'.$a.':V'.$a)
                                    ->getStyle('P'.$a.':V'.$a)->applyFromArray($detalleplomo);
                                $objPHPExcel->getActiveSheet()
                                        ->getStyle('P'.$a)
                                        ->getAlignment()
                                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);   
                                $objPHPExcel->setActiveSheetIndex($e)
                                            ->setCellValue('P'.$a,$estado);
                                
                                $objPHPExcel->getActiveSheet()
                                    ->getStyle('Y'.$a)->applyFromArray($detalleplomo);
                                $objPHPExcel->getActiveSheet()
                                        ->getStyle('Y'.$a)
                                        ->getAlignment()
                                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);   
                                $objPHPExcel->setActiveSheetIndex($e)
                                            ->setCellValue('Y'.$a,$observacion);

                            
                            $a++;
                                cellColor('A'.$a.':Z'.$a, 'BFBFBF');
                                $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(6);
                            $npre++;
                        }
                    }
                }
                $a++;
                cellColor('A'.$a.':C'.$a, 'FFFF99');
                $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':C'.$a)
                    ->getStyle('A'.$a.':C'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('A'.$a.':C'.$a)->applyFromArray($textblueborder);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('A'.$a,"Promedio de Sumatoria");

                cellColor('D'.$a.':F'.$a, 'FFFF99');
                $objPHPExcel->getActiveSheet()->mergeCells('D'.$a.':F'.$a)
                    ->getStyle('D'.$a.':F'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('D'.$a.':F'.$a)->applyFromArray($textblueborder);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('D'.$a,$total_si);

                cellColor('G'.$a.':I'.$a, 'FFFF99');
                $objPHPExcel->getActiveSheet()->mergeCells('G'.$a.':I'.$a)
                    ->getStyle('G'.$a.':I'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('G'.$a.':I'.$a)->applyFromArray($textblueborder);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('G'.$a,$total_no);

                cellColor('J'.$a.':N'.$a, 'FFFF99');
                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':N'.$a)
                    ->getStyle('J'.$a.':N'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':N'.$a)->applyFromArray($textblueborder);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('J'.$a,$total_sumatoria);

                $calificaciones_final="";
                $promedio=number_format($total_sumatoria/$total_division, 2, '.', ' ');
                $cali=calificaciones_final($info[$keys->id_cuadro]["calificacion"],$promedio);
                $col = str_replace("#","",$cali["background"]);

                cellColor('O'.$a.':W'.$a, $col);
                $objPHPExcel->getActiveSheet()->mergeCells('O'.$a.':W'.$a)
                    ->getStyle('O'.$a.':W'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('O'.$a.':W'.$a)->applyFromArray($border);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('O'.$a,"GLOBAL COMPONENTES");

                cellColor('X'.$a.':Z'.($a+1), $col);
                $objPHPExcel->getActiveSheet()->mergeCells('X'.$a.':Z'.($a+1))
                    ->getStyle('X'.$a.':Z'.($a+1))
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('X'.$a.':Z'.($a+1))->applyFromArray($border);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('X'.$a,$cali["calificacion"]);

                $a++;
                cellColor('A'.$a.':C'.$a, '17375D');
                $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':C'.$a)
                    ->getStyle('A'.$a.':C'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('A'.$a.':C'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('A'.$a,"TRAZADOR COMPONENTES");

                cellColor('D'.$a.':F'.$a, '17375D');
                $objPHPExcel->getActiveSheet()->mergeCells('D'.$a.':F'.$a)
                    ->getStyle('D'.$a.':F'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('D'.$a.':F'.$a)->applyFromArray($border);
                

                cellColor('G'.$a.':I'.$a, '17375D');
                $objPHPExcel->getActiveSheet()->mergeCells('G'.$a.':I'.$a)
                    ->getStyle('G'.$a.':I'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('G'.$a.':I'.$a)->applyFromArray($border);

                cellColor('J'.$a.':N'.$a, '17375D');
                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':N'.$a)
                    ->getStyle('J'.$a.':N'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':N'.$a)->applyFromArray($border);

                cellColor('O'.$a.':W'.$a, $col);
                $objPHPExcel->getActiveSheet()->mergeCells('O'.$a.':W'.$a)
                    ->getStyle('O'.$a.':W'.$a)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('O'.$a.':W'.$a)->applyFromArray($border);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('O'.$a,$promedio);

                $a++;$a++;
                cellColor('C'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('C'.$a,"Calificación");

                    cellColor('D'.$a.':J'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()->mergeCells('D'.$a.':J'.$a)
                                ->getStyle('D'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('D'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('D'.$a,"Rango");
                $incri=$a;
                foreach ($info[$keys->id_cuadro]["calificacion"] as $ke) {
                    $a++;
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('C'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                         ->getStyle('C'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('C'.$a,$ke->calificacion);

                    $objPHPExcel->getActiveSheet()->mergeCells('D'.$a.':J'.$a)
                        ->getStyle('D'.$a.':J'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('D'.$a.':J'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('D'.$a,$ke->texto_rango);
                } 
                if (count($info[$keys->id_cuadro]["criterios"])!=null) {
                    cellColor('L'.$incri.':Z'.$incri, '17375D');
                    foreach ($info[$keys->id_cuadro]["criterios"] as $key) {
                        //$this->MultiCell(5,10,"",0,'C',false);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('L'.$incri)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('L'.$incri)->applyFromArray($border);
                        

                        $objPHPExcel->getActiveSheet()->mergeCells('M'.$incri.':Z'.$incri)
                            ->getStyle('M'.$incri.':Z'.$incri)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('M'.$incri.':Z'.$incri)->applyFromArray($textblanco);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('M'.$incri,$key->titulo);

                        foreach ($info[$keys->id_cuadro]["criterios_detalle"] as $ke) {
                            if ($key->id_criterio==$ke->id_criterio) {
                                $incri++;

                                $objPHPExcel->getActiveSheet()
                                    ->getStyle('L'.$incri)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                $objPHPExcel->getActiveSheet()
                                    ->getStyle('L'.$incri)->applyFromArray($border);
                                $objPHPExcel->setActiveSheetIndex($e)
                                    ->setCellValue('L'.$incri,$ke->valor);


                                $objPHPExcel->getActiveSheet()->mergeCells('M'.$incri.':Z'.$incri)
                                    ->getStyle('M'.$incri.':Z'.$incri)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                $objPHPExcel->getActiveSheet()
                                    ->getStyle('M'.$incri.':Z'.$incri)->applyFromArray($border);
                                $objPHPExcel->setActiveSheetIndex($e)
                                    ->setCellValue('M'.$incri,$ke->texto);
                            }
                        }
                        $incri++;
                        $incri++;
                    }
                }
                $a++;
                $a++;
                $a++;
                $a++;
                if (count($info[$keys->id_cuadro]["notas"])!=null) {
                    foreach ($info[$keys->id_cuadro]["notas"] as $key) {
                        $objPHPExcel->getActiveSheet()->mergeCells('B'.$a.':Q'.$a)
                            ->getStyle('B'.$a.':Q'.$a);
                        $vowels = array('<strong style="font-weight:700">','</strong>');
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('B'.$a,str_replace($vowels, "", $key->texto));
                        $a++;
                    }
                }
            }elseif ($info[$keys->id_cuadro]["tipo"]==3) {
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(42); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(2);  
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(44); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(7); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(22); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(2);

                cellColor('A4:R9', 'BFBFBF');
                $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A5',"Nombre del poblado/localidad:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A5')->applyFromArray($textblue);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G5',"Evaluador:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G5')->applyFromArray($textblue);
                $objPHPExcel->getActiveSheet()->mergeCells('B6:E6'); 
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B6:E6')->applyFromArray($detalleplomo);   
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('B6',$datos_llenado["comunidad"]);
                    if ($datos_llenado["usuario"]=="") {
                        $datos_llenado["usuario"]="usuario eliminado";
                    }
                 $objPHPExcel->getActiveSheet()
                    ->getStyle('G6:L6')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G6',$datos_llenado["usuario"]);

                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A7',"Localizacion:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A7')->applyFromArray($textblue);            
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G7',"Fecha:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G7')->applyFromArray($textblue);

                $objPHPExcel->getActiveSheet()->mergeCells('B8:E8');
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B8:E8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('B8',$datos_llenado["departamento"]."--".$datos_llenado["provincia"]."--".$datos_llenado["distrito"]);
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G8:L8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G8',$datos_llenado["fecha"]);
                $objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(5);
                $a=11;
                $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(33);
                cellColor('A'.$a.':R'.$a, '17375D');
                $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a)
                            ->getStyle('A'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"TRAZADOR");

                $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                            ->getStyle('C'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.$a,"CRITERIOS DE VALORACIÓN");

                $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                            ->getStyle('F'.$a.':I'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('F'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('F'.$a,"CALIFICACIÓN");

                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a)
                            ->getStyle('J'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('J'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('J'.$a,"OBSERVACIONES");
                $total_sumatoria="";
                $total_division="";
                foreach ($info[$keys->id_cuadro]["tablas"] as $key) {
                    $a++;
                    cellColor('A'.$a.':R'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a)
                            ->getStyle('A'.$a.':B'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('A'.$a.':B'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('A'.$a,$key->titulo);

                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($textblueborder);

                    $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                            ->getStyle('F'.$a.':I'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('F'.$a,"Porcentaje");

                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':R'.$a)->applyFromArray($textblueborder);
                                
                    $nu=1;
                    foreach ($info[$keys->id_cuadro]["preguntas"] as $ke) {
                        if ($key->id_tabla==$ke->id_tabla) {
                            $a++;
                            cellColor('A'.$a.':R'.$a, 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(5);
                            $a++;
                            cellColor('A'.$a.':A'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':A'.($a+2))
                                    ->getStyle('A'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('A'.$a.':A'.($a+2))->applyFromArray($border);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('A'.$a,$nu);

                            cellColor('B'.$a.':B'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('B'.$a.':B'.($a+2))
                                    ->getStyle('B'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('B'.$a.':B'.($a+2))->applyFromArray($border);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('B'.$a,$ke->pregunta);
                            $poalter=0;            
                            foreach ($info[$keys->id_cuadro]["alternativas"] as $k) {
                                if ($k->id_pregunta==$ke->id_pregunta) {
                                    cellColor('C'.($a+$poalter).':E'.($a+$poalter), 'C0C0C0');
                                    $objPHPExcel->getActiveSheet()->mergeCells('C'.($a+$poalter).':E'.($a+$poalter))
                                            ->getStyle('C'.($a+$poalter).':E'.($a+$poalter))
                                            ->getAlignment()
                                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                ->getStyle('C'.($a+$poalter).':E'.($a+$poalter))->applyFromArray($border);
                                    $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('C'.($a+$poalter),$k->valor." : ".$k->texto);
                                    $poalter++;                  
                                }
                            }
                            

                            $respuesta="--";
                            $observacion="";
                            if (array_key_exists($ke->id_pregunta,$info[$keys->id_cuadro]["respuestas"])) {
                                foreach ($info[$keys->id_cuadro]["respuestas"][$ke->id_pregunta] as $k) {
                                    $respuesta=$k->respuesta;
                                    $observacion=$k->observacion;
                                    $total_sumatoria=$total_sumatoria+$respuesta;
                                    $total_division=$total_division+1;
                                }
                            }
                            cellColor('F'.$a.':F'.($a+2), 'C0C0C0');
                            cellColor('G'.$a.':H'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('G'.$a.':H'.($a+2))
                                    ->getStyle('G'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('G'.$a.':H'.($a+2))->applyFromArray($detalleplomo);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('G'.$a,$respuesta);
                            cellColor('I'.$a.':I'.($a+2), 'C0C0C0');
                            cellColor('J'.$a.':J'.($a+2), 'C0C0C0');            
                            cellColor('K'.$a.':Q'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('K'.$a.':Q'.($a+2))
                                    ->getStyle('K'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('K'.$a.':Q'.($a+2))->applyFromArray($detalleplomo);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('K'.$a,$observacion);
                            cellColor('R'.$a.':R'.($a+2), 'C0C0C0');            
                            $a++;
                            $a++;
                            $nu++;
                        }
                    }  
                    $a++;
                    cellColor('A'.$a.':R'.$a, 'C0C0C0');
                    $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(5);

                    
                }
                $a++;
                    cellColor('B'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Sumatoria");

                    cellColor('C'.$a.':E'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);

                    $calificaciones_final="";
                    $promedio=$total_sumatoria;
                    $cali=calificaciones_final($info[$keys->id_cuadro]["calificacion"],$promedio);

                    $col = str_replace("#","",$cali["background"]);
                    cellColor('F'.$a.':I'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                                ->getStyle('F'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('F'.$a,$total_sumatoria);

                    cellColor('J'.$a.':R'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a)
                                ->getStyle('J'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':R'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('J'.$a,$keys->titulo);

                    $a++;
                    cellColor('B'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Promedio");

                    cellColor('C'.$a.':E'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);

                    cellColor('F'.$a.':I'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                                ->getStyle('F'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('F'.$a,$promedio);

                    cellColor('J'.$a.':R'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a)
                                ->getStyle('J'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':R'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('J'.$a,$cali["calificacion"]);
                    $a++;$a++;

                    cellColor('B'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Calificación");

                    cellColor('C'.$a.':E'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                                ->getStyle('C'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('C'.$a,"Rango");
                    $incri=$a;
                foreach ($info[$keys->id_cuadro]["calificacion"] as $ke) {
                    $a++;
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('B'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                         ->getStyle('B'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('B'.$a,$ke->calificacion);

                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                        ->getStyle('C'.$a.':E'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('C'.$a,$ke->texto_rango);
                } 
                if (count($info[$keys->id_cuadro]["criterios"])!=null) {
                    foreach ($info[$keys->id_cuadro]["criterios"] as $key) {

                        //$this->MultiCell(5,10,"",0,'C',false);

                        $objPHPExcel->getActiveSheet()->mergeCells('G'.$incri.':R'.$incri)
                            ->getStyle('G'.$incri.':R'.$incri)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('G'.$incri.':R'.$incri)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('G'.$incri,$key->titulo);

                        foreach ($info[$keys->id_cuadro]["criterios_detalle"] as $ke) {
                            if ($key->id_criterio==$ke->id_criterio) {
                                $incri++;
                                $objPHPExcel->getActiveSheet()->mergeCells('G'.$incri.':R'.$incri)
                                    ->getStyle('G'.$incri.':R'.$incri)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                $objPHPExcel->getActiveSheet()
                                    ->getStyle('G'.$incri.':R'.$incri)->applyFromArray($border);
                                $objPHPExcel->setActiveSheetIndex($e)
                                    ->setCellValue('G'.$incri,$ke->texto);
                            }
                        }
                        $incri++;
                        $incri++;
                    }
                }
                $a++;
                $a++;
                $a++;
                $a++;
                if (count($info[$keys->id_cuadro]["notas"])!=null) {
                    foreach ($info[$keys->id_cuadro]["notas"] as $key) {
                        $objPHPExcel->getActiveSheet()->mergeCells('B'.$a.':Q'.$a)
                            ->getStyle('B'.$a.':Q'.$a);
                        $vowels = array('<strong style="font-weight:700">','</strong>');
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('B'.$a,str_replace($vowels, "", $key->texto));
                        $a++;
                    }
                }
            }elseif ($info[$keys->id_cuadro]["tipo"]==4) {
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(42); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(2);  
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(29); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(7); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(7); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(6);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(4);
                $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(2);

                cellColor('A4:S9', 'BFBFBF');
                $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A5',"Nombre del poblado/localidad:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A5')->applyFromArray($textblue);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G5',"Evaluador:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G5')->applyFromArray($textblue);
                $objPHPExcel->getActiveSheet()->mergeCells('B6:E6'); 
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B6:E6')->applyFromArray($detalleplomo);   
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('B6',$datos_llenado["comunidad"]);
                    if ($datos_llenado["usuario"]=="") {
                        $datos_llenado["usuario"]="usuario eliminado";
                    }
                 $objPHPExcel->getActiveSheet()
                    ->getStyle('G6:L6')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G6',$datos_llenado["usuario"]);

                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A7',"Localizacion:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A7')->applyFromArray($textblue);            
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G7',"Fecha:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G7')->applyFromArray($textblue);

                $objPHPExcel->getActiveSheet()->mergeCells('B8:E8');
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B8:E8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('B8',$datos_llenado["departamento"]."--".$datos_llenado["provincia"]."--".$datos_llenado["distrito"]);
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G8:L8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G8',$datos_llenado["fecha"]);
                $objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(5);
                $a=11;
                $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(33);
                cellColor('A'.$a.':R'.$a, '17375D');
                $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a)
                            ->getStyle('A'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"TRAZADOR");

                $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                            ->getStyle('C'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.$a,"CRITERIOS DE VALORACIÓN");

                $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                            ->getStyle('F'.$a.':I'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('F'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('F'.$a,"CALIFICACIÓN");

                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':M'.$a)
                            ->getStyle('J'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('J'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('J'.$a,"PONDERACION");

                $objPHPExcel->getActiveSheet()->mergeCells('N'.$a.':S'.$a)
                            ->getStyle('N'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('N'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('N'.$a,"OBSERVACIONES");

               
                $pos=0;
                $valor_evaluado="";
                $cali="";
                $valoracion_ponderada="";
                $sumatoria="";
                $total_valoracion_ponderada="";
                $ponderacion_total="";
                foreach ($info[$keys->id_cuadro]["aponderacion"] as $key) {
                    $a++;
                    cellColor('A'.$a.':S'.$a, 'C0C0C0');
                    $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(5);
                    $a++;
                    cellColor('A'.$a.':S'.$a, 'C0C0C0');
                    $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(15);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('A'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('A'.($a-1).':A'.($a+1))->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('A'.$a,$pos);
                
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.($a-1).':B'.($a+1))->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,$key->titulo);


                    if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                        $valor_evaluado=sumatoria($info[$keys->id_cuadro]["respuestas"][$key->id_cuadro]);
                        $cali=calificaciones_final($info[$keys->id_cuadro]["calificaciones"][$key->id_cuadro],$valor_evaluado);
                        $valoracion_ponderada=$valor_evaluado*$key->ponderacion;
                        $sumatoria=$sumatoria+$valor_evaluado;
                        $total_valoracion_ponderada=$total_valoracion_ponderada+$valoracion_ponderada;
                    }
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('D'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.($a-1).':E'.($a+1))->applyFromArray($border);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('D'.$a.':D'.$a)->applyFromArray($detalleplomo);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('D'.$a,$valor_evaluado);

                    $col = str_replace("#","",$cali["background"]);
                    $objPHPExcel->getActiveSheet()->mergeCells('G'.$a.':H'.$a)
                                ->getStyle('G'.$a.':H'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                 
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.($a-1).':I'.($a+1))->applyFromArray($border);
                    cellColor('G'.$a.':H'.$a, $col);             
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('G'.$a.':H'.$a)->applyFromArray($detalleplomo);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('G'.$a,$cali['calificacion']);
                                        

                    $objPHPExcel->getActiveSheet()->mergeCells('K'.$a.':L'.$a)
                                ->getStyle('K'.$a.':L'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.($a-1).':M'.($a+1))->applyFromArray($border);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('K'.$a.':L'.$a)->applyFromArray($detalleplomo);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('K'.$a,$key->ponderacion);


                    $objPHPExcel->getActiveSheet()->mergeCells('O'.$a.':R'.$a)
                                ->getStyle('O'.$a.':R'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('N'.($a-1).':S'.($a+1))->applyFromArray($border);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('O'.$a.':R'.$a)->applyFromArray($detalleplomo);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('O'.$a,$valoracion_ponderada);
                    $ponderacion_total=$ponderacion_total+$key->ponderacion;
                    $a++;
                    cellColor('A'.$a.':S'.$a, 'C0C0C0');
                    $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(5);
                    $pos++;
                }
                $a++;
                cellColor('B'.$a, 'FFFF99');
                $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblueborder);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Sumatoria");

                cellColor('C'.$a.':E'.$a, 'FFFF99');
                $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                                ->getStyle('C'.$a.':E'.$a.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($textblueborder);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('C'.$a,$sumatoria);

                cellColor('F'.$a.':I'.$a, 'FFFF99');
                $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($textblueborder);
                

                cellColor('J'.$a.':M'.$a, 'FFFF99');
                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':M'.$a)
                                ->getStyle('J'.$a.':M'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':M'.$a)->applyFromArray($textblueborder);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('J'.$a,$ponderacion_total);


                if ($info[$keys->id_cuadro]["respuestas"]!=null) {
                    $promedio=number_format($total_valoracion_ponderada/$ponderacion_total, 2, '.', ' ');
                    $califi_fi=calificaciones_final($info[$keys->id_cuadro]["calificacion"],$promedio);
                }
                $col = str_replace("#","",$califi_fi["background"]);              
                cellColor('N'.$a.':S'.($a+1), $col);
                $objPHPExcel->getActiveSheet()->mergeCells('N'.$a.':S'.($a+1))
                                ->getStyle('N'.$a.':S'.($a+1))
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('N'.$a.':S'.($a+1))->applyFromArray($textblueborder);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('N'.$a,$promedio);

                $a++;
                cellColor('B'.$a.':M'.$a, '17375D');
                $objPHPExcel->getActiveSheet()
                            ->getStyle('B'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('B'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('B'.$a,"PROMEDIO GLOBAL");


                $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                            ->getStyle('C'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)->applyFromArray($textblueborder);

                $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                            ->getStyle('F'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('F'.$a)->applyFromArray($textblueborder);

                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':M'.$a)
                            ->getStyle('J'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('J'.$a)->applyFromArray($textblueborder);


                $a++;$a++;
                cellColor('B'.$a, '17375D');
                $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Calificación");

                cellColor('C'.$a.':E'.$a, '17375D');
                $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                                ->getStyle('C'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('C'.$a,"Rango");
                $incri=$a;
                foreach ($info[$keys->id_cuadro]["calificacion"] as $ke) {
                    $a++;
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('B'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                         ->getStyle('B'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('B'.$a,$ke->calificacion);

                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                        ->getStyle('C'.$a.':E'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('C'.$a,$ke->texto_rango);
                } 


                cellColor('K'.$incri.':S'.$incri, $col);
                $objPHPExcel->getActiveSheet()->mergeCells('K'.$incri.':S'.$incri)
                        ->getStyle('K'.$incri.':S'.$incri)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                        ->getStyle('K'.$incri.':S'.($incri+3))->applyFromArray($border);
                $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('K'.$incri,"INDICE TRAZADOR FINAL");
                $incri++;
                cellColor('K'.$incri.':S'.($incri+2), $col);
                $objPHPExcel->getActiveSheet()->mergeCells('K'.$incri.':S'.($incri+2))
                        ->getStyle('K'.$incri.':S'.($incri+2))
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('K'.$incri,$califi_fi["calificacion"]);
            }elseif ($info[$keys->id_cuadro]["tipo"]==5) {
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(42); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(2);  
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(44); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(7); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(22); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(2); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(2);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(5);
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(2);

                cellColor('A4:R9', 'BFBFBF');
                $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A5',"Nombre del poblado/localidad:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A5')->applyFromArray($textblue);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G5',"Evaluador:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G5')->applyFromArray($textblue);
                $objPHPExcel->getActiveSheet()->mergeCells('B6:E6'); 
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B6:E6')->applyFromArray($detalleplomo);   
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('B6',$datos_llenado["comunidad"]);
                    if ($datos_llenado["usuario"]=="") {
                        $datos_llenado["usuario"]="usuario eliminado";
                    }
                 $objPHPExcel->getActiveSheet()
                    ->getStyle('G6:L6')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G6',$datos_llenado["usuario"]);

                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('A7',"Localizacion:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('A7')->applyFromArray($textblue);            
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G7',"Fecha:");
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G7')->applyFromArray($textblue);

                $objPHPExcel->getActiveSheet()->mergeCells('B8:E8');
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B8:E8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('B8',$datos_llenado["departamento"]."--".$datos_llenado["provincia"]."--".$datos_llenado["distrito"]);
                $objPHPExcel->getActiveSheet()
                    ->getStyle('G8:L8')->applyFromArray($detalleplomo);
                $objPHPExcel->setActiveSheetIndex($e)
                    ->setCellValue('G8',$datos_llenado["fecha"]);
                $objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(5);
                $a=11;
                $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(33);
                cellColor('A'.$a.':R'.$a, '17375D');
                $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a)
                            ->getStyle('A'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('A'.$a,"TRAZADOR");

                $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                            ->getStyle('C'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('C'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('C'.$a,"CRITERIOS DE VALORACIÓN");

                $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                            ->getStyle('F'.$a.':I'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('F'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('F'.$a,"CALIFICACIÓN");

                $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a)
                            ->getStyle('J'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()
                            ->getStyle('J'.$a)->applyFromArray($textblanco);
                $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('J'.$a,"OBSERVACIONES");
                $total_sumatoria="";
                $total_division="";
                foreach ($info[$keys->id_cuadro]["tablas"] as $key) {
                    $a++;
                    cellColor('A'.$a.':R'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':B'.$a)
                            ->getStyle('A'.$a.':B'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('A'.$a.':B'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('A'.$a,$key->titulo);

                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($textblueborder);

                    $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                            ->getStyle('F'.$a.':I'.$a)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('F'.$a,"Porcentaje");

                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':R'.$a)->applyFromArray($textblueborder);
                                
                    $nu=1;
                    foreach ($info[$keys->id_cuadro]["preguntas"] as $ke) {
                        if ($key->id_tabla==$ke->id_tabla) {
                            $a++;
                            cellColor('A'.$a.':R'.$a, 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(5);
                            $a++;
                            cellColor('A'.$a.':A'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('A'.$a.':A'.($a+2))
                                    ->getStyle('A'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('A'.$a.':A'.($a+2))->applyFromArray($border);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('A'.$a,$nu);

                            cellColor('B'.$a.':B'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('B'.$a.':B'.($a+2))
                                    ->getStyle('B'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('B'.$a.':B'.($a+2))->applyFromArray($border);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('B'.$a,$ke->pregunta);
                            $poalter=0;            
                            foreach ($info[$keys->id_cuadro]["alternativas"] as $k) {
                                if ($k->id_pregunta==$ke->id_pregunta) {
                                    cellColor('C'.($a+$poalter).':E'.($a+$poalter), 'C0C0C0');
                                    $objPHPExcel->getActiveSheet()->mergeCells('C'.($a+$poalter).':E'.($a+$poalter))
                                            ->getStyle('C'.($a+$poalter).':E'.($a+$poalter))
                                            ->getAlignment()
                                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                ->getStyle('C'.($a+$poalter).':E'.($a+$poalter))->applyFromArray($border);
                                    $objPHPExcel->setActiveSheetIndex($e)
                                                ->setCellValue('C'.($a+$poalter),$k->valor." : ".$k->texto);
                                    $poalter++;                  
                                }
                            }
                            

                            $respuesta="--";
                            $observacion="";
                            if (array_key_exists($ke->id_pregunta,$info[$keys->id_cuadro]["respuestas"])) {
                                foreach ($info[$keys->id_cuadro]["respuestas"][$ke->id_pregunta] as $k) {
                                    $respuesta=$k->respuesta;
                                    $observacion=$k->observacion;
                                    $total_sumatoria=$total_sumatoria+$respuesta;
                                    $total_division=$total_division+1;
                                }
                            }
                            cellColor('F'.$a.':F'.($a+2), 'C0C0C0');
                            cellColor('G'.$a.':H'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('G'.$a.':H'.($a+2))
                                    ->getStyle('G'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('G'.$a.':H'.($a+2))->applyFromArray($detalleplomo);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('G'.$a,$respuesta);
                            cellColor('I'.$a.':I'.($a+2), 'C0C0C0');
                            cellColor('J'.$a.':J'.($a+2), 'C0C0C0');            
                            cellColor('K'.$a.':Q'.($a+2), 'C0C0C0');
                            $objPHPExcel->getActiveSheet()->mergeCells('K'.$a.':Q'.($a+2))
                                    ->getStyle('K'.$a)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()
                                        ->getStyle('K'.$a.':Q'.($a+2))->applyFromArray($detalleplomo);
                            $objPHPExcel->setActiveSheetIndex($e)
                                        ->setCellValue('K'.$a,$observacion);
                            cellColor('R'.$a.':R'.($a+2), 'C0C0C0');            
                            $a++;
                            $a++;
                            $nu++;
                        }
                    }  
                    $a++;
                    cellColor('A'.$a.':R'.$a, 'C0C0C0');
                    $objPHPExcel->getActiveSheet()->getRowDimension($a)->setRowHeight(5);

                    
                }
                $a++;
                    cellColor('B'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblueborder);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Sumatoria");

                    cellColor('C'.$a.':E'.$a, 'FFFF99');
                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);

                    $calificaciones_final="";
                    $promedio=$total_sumatoria;
                    $cali=calificaciones_final($info[$keys->id_cuadro]["calificacion"],$promedio);

                    $col = str_replace("#","",$cali["background"]);
                    cellColor('F'.$a.':I'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                                ->getStyle('F'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('F'.$a,$total_sumatoria);

                    cellColor('J'.$a.':R'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a)
                                ->getStyle('J'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':R'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('J'.$a,$keys->titulo);

                    $a++;
                    cellColor('B'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Promedio");

                    cellColor('C'.$a.':E'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);

                    cellColor('F'.$a.':I'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('F'.$a.':I'.$a)
                                ->getStyle('F'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('F'.$a.':I'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('F'.$a,$promedio);

                    cellColor('J'.$a.':R'.$a, $col);
                    $objPHPExcel->getActiveSheet()->mergeCells('J'.$a.':R'.$a)
                                ->getStyle('J'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('J'.$a.':R'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('J'.$a,$cali["calificacion"]);
                    $a++;$a++;

                    cellColor('B'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('B'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('B'.$a,"Calificación");

                    cellColor('C'.$a.':E'.$a, '17375D');
                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                                ->getStyle('C'.$a)
                                ->getAlignment()
                                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                                ->getStyle('C'.$a)->applyFromArray($textblanco);
                    $objPHPExcel->setActiveSheetIndex($e)
                                ->setCellValue('C'.$a,"Rango");
                    $incri=$a;
                foreach ($info[$keys->id_cuadro]["calificacion"] as $ke) {
                    $a++;
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('B'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                         ->getStyle('B'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('B'.$a,$ke->calificacion);

                    $objPHPExcel->getActiveSheet()->mergeCells('C'.$a.':E'.$a)
                        ->getStyle('C'.$a.':E'.$a)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                        ->getStyle('C'.$a.':E'.$a)->applyFromArray($border);
                    $objPHPExcel->setActiveSheetIndex($e)
                        ->setCellValue('C'.$a,$ke->texto_rango);
                } 
                if (count($info[$keys->id_cuadro]["criterios"])!=null) {
                    foreach ($info[$keys->id_cuadro]["criterios"] as $key) {

                        //$this->MultiCell(5,10,"",0,'C',false);

                        $objPHPExcel->getActiveSheet()->mergeCells('G'.$incri.':R'.$incri)
                            ->getStyle('G'.$incri.':R'.$incri)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle('G'.$incri.':R'.$incri)->applyFromArray($border);
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('G'.$incri,$key->titulo);

                        foreach ($info[$keys->id_cuadro]["criterios_detalle"] as $ke) {
                            if ($key->id_criterio==$ke->id_criterio) {
                                $incri++;
                                $objPHPExcel->getActiveSheet()->mergeCells('G'.$incri.':R'.$incri)
                                    ->getStyle('G'.$incri.':R'.$incri)
                                    ->getAlignment()
                                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                $objPHPExcel->getActiveSheet()
                                    ->getStyle('G'.$incri.':R'.$incri)->applyFromArray($border);
                                $objPHPExcel->setActiveSheetIndex($e)
                                    ->setCellValue('G'.$incri,$ke->texto);
                            }
                        }
                        $incri++;
                        $incri++;
                    }
                }
                $a++;
                $a++;
                $a++;
                $a++;
                if (count($info[$keys->id_cuadro]["notas"])!=null) {
                    foreach ($info[$keys->id_cuadro]["notas"] as $key) {
                        $objPHPExcel->getActiveSheet()->mergeCells('B'.$a.':Q'.$a)
                            ->getStyle('B'.$a.':Q'.$a);
                        $vowels = array('<strong style="font-weight:700">','</strong>');
                        $objPHPExcel->setActiveSheetIndex($e)
                            ->setCellValue('B'.$a,str_replace($vowels, "", $key->texto));
                        $a++;
                    }
                }
            }
            $e++;
        }
        header('Content-Type: application/vmd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$archivo.'"');
        header('Cache-Control: max-age-0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
        $objWriter->save('php://output');
    }
    

?>
<?php
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
    return $rgb; //returns an array with the rgb values
}
if ($archivo=="pdf") {
	require("application/libraries/fpdf17/fpdf.php");
	require("application/libraries/jpgraph/src/jpgraph.php");
	require("application/libraries/jpgraph/src/jpgraph_pie.php");
	require("application/libraries/jpgraph/src/jpgraph_pie3d.php");
	require("application/libraries/clsReporte.php");
	$pdf=new Reporte();
		$a=0;
		foreach ($tablas as $key) {
			$b=array($a);
			$pdf->AddPage("L","A4");
		    $pdf->SetFont("Arial", "", 10);
		    $pdf->Cell(230,10,"Sistema Trazadores SIAS",0,0,"L");
		    $pdf->Cell(40,10,"Fecha:".date('d-m-Y')."",0,0,"C");
		    $pdf->Ln(10);
		    $pdf->Cell(230,10,$localizacion,0,0,"L");
		    $pdf->Ln(10);
		    $pdf->SetFont("Arial", "", 18);
		    $pdf->Cell(0,10,"Reporte Graficos Detallados",0,1,"C");
		    $pdf->Ln(30);
	    	$pdf->gaficoPDF($tablas[$a]["estadisticas"],'Grafico',array(25,40,250,150),utf8_decode($tablas[$a]["titulo"]),$b);
			$a++;
		}
	$pdf->Output(); 
}elseif ($archivo=="excel") {
	require("application/libraries/fpdf17/fpdf.php");
	require "application/libraries/phpexcel/PHPExcel.php";
	
	$archivo="_graficos.xlsx";

	$objPHPExcel = new PHPExcel();
	$objWorksheet = $objPHPExcel->getActiveSheet();

	$borders= array(
		'borders'=> array(
			'allborders'=>array(
				'style'=>PHPExcel_Style_Border::BORDER_THIN,
				'color'=> array('argb'=>'FF000000'
			)
		),
		'outline'     => array(
	      'style' => PHPExcel_Style_Border::BORDER_THIN,
	      'color' => array(
	         'argb' => 'FF000000'
	      )
	    )
	  )
	);
	$inicio_cuadros=0;
	$i=1;
	foreach ($tablas as $key) {
		//b2:d2
		$objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':D'.$i);
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B'.$i, "Fecha de impresion:".date("d-m-Y"));
		$objPHPExcel->getActiveSheet()->mergeCells('H'.$i.':J'.$i);
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('H'.$i, "Hora:".date("H:i"));
		$i++;
		//a3:k3
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':k'.$i);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':k'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':k'.$i)->getFont()->setSize(16);
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, "Sistema trazadores");
		$i++;
		//a4:k4
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':k'.$i);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':k'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':k'.$i)->getFont()->setSize(16);
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, "filtro localizacion:".$localizacion);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

		$dataseriesLabels = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$1', NULL, 1),	//	2011
			);
		// 'Worksheet!$E$28:$E$31
		$in_grafico=$i+24;
		$fi_grafico=($i+24)+count($tablas[$inicio_cuadros]["estadisticas"]);
		$xAxisTickValues = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$E$'.$in_grafico.':$E$'.$fi_grafico, NULL, 4),	//	Q1 to Q4
		);
		// 'Worksheet!$f$28:$f$31
		$dataSeriesValues = array(
			new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$F$'.$in_grafico.':$F$'.$fi_grafico, NULL, 4),
		);
		$series = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_PIECHART,				// plotType
			PHPExcel_Chart_DataSeries::GROUPING_STANDARD,			// plotGrouping
			range(0, count($dataSeriesValues)-1),					// plotOrder
			$dataseriesLabels,										// plotLabel
			$xAxisTickValues,										// plotCategory
			$dataSeriesValues								// plotValues
		);
		$layout = new PHPExcel_Chart_Layout();
		$layout->setShowVal(TRUE);
		$layout->setShowCatName(TRUE);
		$layout->setShowPercent(TRUE);
		$series-> setPlotDirection (PHPExcel_Chart_DataSeries :: DIRECTION_COL);
		$plotarea = new PHPExcel_Chart_PlotArea($layout, array($series));
		$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
		$title = new PHPExcel_Chart_Title($tablas[$inicio_cuadros]["titulo"]);
		$chart = new PHPExcel_Chart(
			'chart',		// name
			$title,			// title
			$legend,		// legend
			$plotarea,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			NULL			// yAxisLabel		- Pie charts don't have a Y-Axis
		);
		$i++;$i++;
		//b6
		$chart->setTopLeftPosition('B'.$i);
		//b27
		$i=$i+21;
		$chart->setBottomRightPosition('K'.$i);
		$objWorksheet->addChart($chart);

		$e=0;
		foreach ($tablas[$inicio_cuadros]["calificacion"] as $ke) {
			$i++;
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('E'.$i, $tablas[$inicio_cuadros]["calificacion"][$e])
				->setCellValue('F'.$i, $tablas[$inicio_cuadros]["promedio"][$e]);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$i.':F'.$i)->applyFromArray($borders);
			$e++;
		}
		$i++;$i++;$i++;$i++;
		$inicio_cuadros++;
	}

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$archivo.'"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->setIncludeCharts(TRUE);
	$objWriter->save('php://output');
}
?>



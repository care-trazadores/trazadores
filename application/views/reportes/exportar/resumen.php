<?php
function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
    return $rgb; //returns an array with the rgb values
}
    if ($archivo=="pdf") {
        /*require("application/libraries/fpdf17/cellfit.php");
        $pdf=new FPDF_CellFit();
        $pdf->AddPage("L","A4");
        $posicion=12;
        $inicio_encuesta=0;
        foreach ($tablas as $key => $value) {
            if ($posicion==12) {
                $pdf->SetFont("Arial", "", 10);
                $pdf->Cell(50,10,"",0,0);
                $pdf->Cell(150,10,"Sistema Trazadores SIAS",0,0,"C");
                $pdf->Cell(40,10,"Fecha:".date('d-m-Y')."",0,0,"C");
                $pdf->Ln(10);
                $pdf->SetFont("Arial", "", 18);
                $pdf->Cell(0,10,"Reporte Globales Resumen",0,1,"C");
                $pdf->Ln(10);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont("Arial", "", 7);
                $pdf->SetFillColor(58,157,202);
                $pdf->SetTextColor(255,255,255);
                $pdf->CellFitScale(30,10,"Localizacion",1,0,"C", True);
                $pdf->CellFitScale(30,10,"Localidad",1,0,"C", True);
                $a=1;
                foreach ($tablas[0]["result"] as $key => $value) {
                    $pdf->CellFitScale(30,10,$tablas[0]["result"][$a]["titulo"],1,0,"C", True);
                    $a++;
                }
                $pdf->CellFitScale(30,10,"Calificacion final",1,0,"C", True);
                $pdf->Ln(10);
                $a=0;
                $posicion=0;
            }else{
                $posicion++;
            }
            $e=1;
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFillColor(255,255,255); 
            $pdf->CellFitScale(30,10,utf8_decode($tablas[$inicio_encuesta]["localizacion"]),1,0,"C", True);
            $pdf->CellFitScale(30,10,utf8_decode($tablas[$inicio_encuesta]["comunidad"]),1,0,"C", True);
            foreach ($tablas[$inicio_encuesta]["result"] as $key => $value) {
                    $color=hex2rgb($tablas[$inicio_encuesta]["result"][$e]["calificacion_cuadro"]['background']);
                    $pdf->SetFillColor($color[0],$color[1],$color[2]); 
                    $pdf->CellFitScale(30,10,utf8_decode($tablas[$a]["result"][$e]["calificacion_cuadro"]["calificacion"]),1,0,"C", True);
                $e++;
            }
            $color=hex2rgb($tablas[$inicio_encuesta]["final"][$e]["calificacion_cuadro"]['background']);
            $pdf->SetFillColor($color[0],$color[1],$color[2]); 
            $pdf->SetTextColor(0,0,0);
            $pdf->CellFitScale(30,10,utf8_decode($tablas[$inicio_encuesta]["final"][$e]["calificacion_cuadro"]["calificacion"]),1,0,"C", True);
            $pdf->SetFillColor(58,157,202);
            $pdf->Ln(10);
            $inicio_encuesta++;
        }

        $pdf->Output();*/
        require("application/libraries/dompdf/dompdf_config.inc.php");
        $codigoHTML='
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Lista</title>
        </head>
        <body>
        <div align="center">
            <table style="margin-bottom:20px;width:95%;">';
            $codigoHTML.='<tr>';
                $codigoHTML.='<td>Sistemas Sias Trazadores</td>';
                $codigoHTML.='<td style="text-align:center;font-size:18px;">Reporte Resumen</td>';
                $codigoHTML.='<td style="text-align:right">Fecha de impresion:'.date('d-m-Y').'</td>';
            $codigoHTML.='</tr>';
        $codigoHTML.='</table>';
$codigoHTML.='<div style="display:none">Localización</div>';
        $codigoHTML.='<table cellpadding="0" cellspacing="0" style="width:98%;white-space: normal;margin-left:-10px">';
           $codigoHTML.='<tr style="font-size:14px;">';
                $codigoHTML.='<th style="border:1px solid #000;padding:10px 3px;color: white;background-color: #4594B9;">Localización</th>';
                $codigoHTML.='<th style="border:1px solid #000;padding:10px 3px;color: white;background-color: #4594B9;">Localidad</th>';
                $a=1;
                foreach ($tablas[0]["result"] as $ke) {
                    $codigoHTML.='<th style="border:1px solid #000;padding:10px 3px;color: white;background-color: #4594B9;">'.utf8_encode($tablas[0]["result"][$a]["titulo"]).'</th>';
                    $a++;
                }
                $codigoHTML.='<th style="border:1px solid #000;padding:10px 3px;color: white;background-color: #4594B9;">Calificación Final</th>';
            $codigoHTML.='</tr>';
            
                $inicio_encuesta=0;
                foreach ($tablas as $key) {
                    $e=1;
                    $codigoHTML.='<tr style="font-size:11px;">';
                    $codigoHTML.='<td style="border:1px solid #000;padding:2px">'.$tablas[$inicio_encuesta]["localizacion"].'</td>';
                    $codigoHTML.='<td style="border:1px solid #000;padding:2px">'.$tablas[$inicio_encuesta]["comunidad"].'</td>';
                    foreach ($tablas[$inicio_encuesta]["result"] as $ke) {
                        $codigoHTML.='<td style="text-align:center;border:1px solid #000;padding:2px;background-color:'.$tablas[$inicio_encuesta]["result"][$e]["calificacion_cuadro"]['background'].'">'.$tablas[$inicio_encuesta]["result"][$e]["calificacion_cuadro"]["calificacion"].'</td>';
                        $e++;
                    }
                    
                    $codigoHTML.='<td style="border:1px solid #000;padding:2px;background-color:'.$tablas[$inicio_encuesta]["final"][$e]["calificacion_cuadro"]['background'].'">'.$tablas[$inicio_encuesta]["final"][$e]["calificacion_cuadro"]["calificacion"].'</td>';

                    $inicio_encuesta++;   
                    $codigoHTML.='</tr>';  
                }

           
        $codigoHTML.='</table></div></body>';

        $codigoHTML=utf8_decode($codigoHTML);
        $dompdf=new DOMPDF();
        $dompdf->set_paper("letter","landscape");
        $dompdf->load_html($codigoHTML);
        ini_set("memory_limit","128M");
        $dompdf->render();
        $dompdf->stream("ListadoEmpleado.pdf", array("Attachment" => 0));
    }elseif ($archivo=="excel") {
        global $objPHPExcel;
        $posicion = array("A","B","C","D","E","F","G","H","I","J",
            "K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        require "application/libraries/phpexcel/PHPExcel.php";
        $objPHPExcel=new PHPExcel();

        $archivo="resumen.xls";
        $borders= array(
            'borders'=> array(
                'allborders'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                    'color'=> array('argb'=>'FF000000'
                )
            ),
            'outline'     => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN,
              'color' => array(
                 'argb' => 'FF000000'
              )
            )
          )
        );
        function cellColor($cells,$color){
            global $objPHPExcel;
            $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                     'rgb' => $color
                )
            ));
        }
        $objPHPExcel->getProperties()->setCreator("weblocalhost")
            ->setLastModifiedBy("weblocalhost")
            ->setTitle("Reporte XLS")
            ->setSubject("Réporte")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial Narrow');
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->mergeCells('B3:I3');
        $objPHPExcel->getActiveSheet()->getStyle('B3:I3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B3:I3')->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->mergeCells('B4:I4');
        $objPHPExcel->getActiveSheet()->getStyle('B4:I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B4:I4')->getFont()->setSize(16);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B2', "Fecha:".date("d-m-Y"))
            ->setCellValue('C2', "Hora:".date("H:i"))
            ->setCellValue('B3', "Sistema trazadores");
           // ->setCellValue('B4', "Distrito:");
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $i=6;
                $objPHPExcel->getActiveSheet()
                    ->getStyle('B'.$i.':'.$posicion[count($tablas[0]["result"])+3].$i)
                    ->getFill()
                    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FF00CCFF');
                $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':'.$posicion[count($tablas[0]["result"])+3].$i)->applyFromArray($borders);
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$i, "Localización")
                    ->setCellValue('C'.$i, "Localidad");
                $pos=3;
                $ini=1;
                foreach ($tablas[0]["result"] as $key) {
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($posicion[$pos].$i, $tablas[0]["result"][$ini]["titulo"]);
                    $pos++;
                    $ini++;
                }
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($posicion[$pos].$i, "Calificación final");
                    $pos++;
            $i++;    
                $inicio_encuesta=0;
                foreach ($tablas as $key) {
                    $pos=1;
                    $objPHPExcel->getActiveSheet()
                            ->getStyle($posicion[$pos].$i)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                            ->getStyle($posicion[$pos].$i)->applyFromArray($borders);
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($posicion[$pos].$i,$tablas[$inicio_encuesta]["localizacion"] );
                    $pos++;
                    $objPHPExcel->getActiveSheet()
                            ->getStyle($posicion[$pos].$i)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                            ->getStyle($posicion[$pos].$i)->applyFromArray($borders);
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($posicion[$pos].$i,$tablas[$inicio_encuesta]["comunidad"]);
                    $pos++;
                    $posicion_res=1;
                    foreach ($tablas[$inicio_encuesta]["result"] as $ke) {
                        $col = str_replace("#","",$tablas[$inicio_encuesta]["result"][$posicion_res]["calificacion_cuadro"]["background"]);
                        cellColor($posicion[$pos].$i, $col);
                        $objPHPExcel->getActiveSheet()
                            ->getStyle($posicion[$pos].$i)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                                ->getStyle($posicion[$pos].$i)->applyFromArray($borders);
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($posicion[$pos].$i, $tablas[$inicio_encuesta]["result"][$posicion_res]["calificacion_cuadro"]["calificacion"]);
                        $posicion_res++;
                        $pos++;
                    }
                    $col = str_replace("#","",$tablas[$inicio_encuesta]["final"][$posicion_res]["calificacion_cuadro"]["background"]);
                    cellColor($posicion[$pos].$i, $col);
                    $objPHPExcel->getActiveSheet()
                            ->getStyle($posicion[$pos].$i)
                            ->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
                            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $objPHPExcel->getActiveSheet()
                            ->getStyle($posicion[$pos].$i)->applyFromArray($borders);
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($posicion[$pos].$i,$tablas[$inicio_encuesta]["final"][$posicion_res]["calificacion_cuadro"]["calificacion"]);
                    

                    $inicio_encuesta++;
                    $i++;
                }
                
                
 
        header('Content-Type: application/vmd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$archivo.'"');
        header('Cache-Control: max-age-0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
        $objWriter->save('php://output');
    }
?>
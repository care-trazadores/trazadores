<div id="reportes">
  <?=$titulo?>
  <div class="row">
  	<div id="filtros">
          <form method="post" action="<?=$action?>" id="filtro">
            <?php
            if ($tipo_usuario!="usuario") {
            ?>
              <div>
                <span>Departamento</span>
                <select name="id_departamento">
                </select>
              </div>
              <div>
                <span>Provincia</span>
                <select name="id_provincia">
                </select>
              </div>
              <div>
                <span>Distrito</span>
                <select name="id_distrito">
                </select>
              </div>
            <?php
            }
            ?>
              <div><input type="submit" value="buscar" name="buscar"></div>
         </form>
      </div>
      <div id="exportar">
        <a href="<?=$base_url?>index.php?c=exportar&m=grafico&archivo=pdf<?=$datos_exportacion?>" target="_blank"><span class="fa fa-file-pdf-o">exportar a PDF</span></a>
        <a href="<?=$base_url?>index.php?c=exportar&m=grafico&archivo=excel<?=$datos_exportacion?>" target="_blank"><span class="fa fa-file-excel-o ">exportar a excel</span></a>
      </div>
      <div id="list-pie">
      <?=$paginador?>
      </div> 
      <div class="col12">
        <div class="popup">
          <div class="popup-container">
            <p style="text-align: center;">Espere un momento, estamos cargando la información.</p>
          </div>
        </div>
      	<?=$tablas?>
      </div>
      <div id="list-pie">
      <?=$paginador?>
      </div>  
  </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){
          function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
          }
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
          <?php if ($id_departamento!="") { ?>
            departamento(<?=$id_departamento?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if ($id_departamento!="" and $id_provincia!="") { ?>
            provincia(<?=$id_departamento?>,<?=$id_provincia?>);
          <?php }elseif($id_departamento!=""){ ?>
            provincia(<?=$id_departamento?>,null);
          <?php };?>
          <?php if ($id_provincia!="" and $id_distrito!="") { ?>
            distrito(<?=$id_provincia?>,<?=$id_distrito?>);
          <?php }elseif ($id_provincia!=""){ ?>
            distrito(<?=$id_provincia?>,null);
          <?php };?>

          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });
        })

        $(document).ready(function(){
          $("#filtro").submit(function() {
            $(".popup").fadeIn();   
          });
        });
      </script>
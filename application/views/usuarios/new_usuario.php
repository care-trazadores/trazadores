<section id="new_usuario">
	<h3 class="page-title">Nuevo usuario | Cuenta<small> Crear un nuevo usuario</small></h3>
	<div class="row">
		<div class="col12">
		<form id="enviarnew" enctype="multipart/form-data" method="post" action="<?=$base_url?>index.php?c=usuarios&m=nuevo">
			<div class="col12">
				<?=$errores?>
				<?=$success?>
			</div>
			<div class="col6">
				<div class="portlet light ">
					<div class="form-group">
		                <label class="control-label fa fa-user">Usuario</label>
		                <input  type="text" name="usuario" placeholder="Ejemplo: Carlos" class="form-control" value="<?=$usuario?>"> 
		            </div>
		            <div class="form-group">
		                <label class="control-label fa fa-key">Contraseña</label>
		                <input type="text" name="password" placeholder="Ejemplo: 123" class="form-control" value="<?=$password?>"> 
		            </div>
		            <div class="form-group">
		            	<div class="row">
		            		<div class="col4">
		                        <label class="control-label">Departamento</label>
		                        <select name='id_departamento'>
		                        </select> 
		                    </div>
		                    <div class="col4">
		                        <label class="control-label">Provincia</label>
		                        <select  name='id_provincia'>
		                        </select> 
		                    </div>
		                    <div class="col4">
		                        <label class="control-label">Distrito</label>
		                        <select name='id_distrito' value="">
		                        </select> 
		                    </div>
		            	</div>
	                </div>
					<div class="form-group" style="text-align: center;"> 
	                    <div class="fileinput fileinput-new" data-provides="fileinput">
	                        <div class="fileinput-new thumbnail" style="width: 150px; height: 150px;">
	                            <img src="<?=$base_url?>img/usuarios/usu.jpg" class="img-responsive" alt="">
	                        </div>
	                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;"> 
	                        </div>
	                        <div>
	                        	<span class="btn default btn-file">
	                            	<input id="file-foto" type="file" name="file-foto"> </span>
	                        	<a href="javascript:;" class="file-foto-remove btn default fileinput-exists hide" data-dismiss="fileinput"> Remove </a>
	                        </div>
	                    </div>
	                    <div class="clearfix margin-top-10">
	                        <span class="label label-danger">NOTA! </span>
	                        <span>La miniatura de la imagen adjunta se apoya en Últimas Versiones de Firefox , Chrome, Opera , Safari e Internet Explorer 10</span>
	                    </div>
	                </div>
				</div>
			</div>
			<div class="col6">
				<div class="portlet light">
	                <div class="form-group">
	                  	<label class="control-label ">Nombres</label>
	                       	<input type="text" name="nombres" placeholder="Ejemplo: Carlos jorge" class="form-control" value="<?=$nombres?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label">Apellidos Paterno</label>
	                       	<input type="text" name="apellido_p" placeholder="Ejemplo: palma" class="form-control" value="<?=$apellido_p?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label">Apellidos Materno</label>
	                       	<input type="text" name="apellido_m" placeholder="Ejemplo: bocanegra" class="form-control" value="<?=$apellido_m?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label fa fa-credit-card-alt">DNI</label>
	                       	<input type="text" name="dni" placeholder="Ejemplo: 46837941" class="form-control" value="<?=$dni?>"> 
	                </div><div class="form-group">
	                  	<label class="control-label fa fa-envelope">Correo</label>
	                       	<input type="text" name="correo" placeholder="Ejemplo: carlos.palma@yainso.com" class="form-control" value="<?=$correo?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label fa fa-building">Direccion</label>
	                       	<input type="text" name="direccion" placeholder="Ejemplo: jr.alfonso ugarte 1351 int 7 la perla-callao" class="form-control" value="<?=$direccion?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label fa fa-phone">Telefono</label>
	                       	<input type="text" name="telefono" placeholder="Ejemplo: 014294568" class="form-control" value="<?=$telefono?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label fa fa-mobile">Celular</label>
	                       	<input type="text" name="celular" placeholder="Ejemplo: 978498879" class="form-control" value="<?=$celular?>"> 
	                </div>
				</div>
			</div>
			<input id="enviarformulario" type="submit" name="enviar" value="enviar" style="display:none;">
		</form>
		</div>
		<div class="margiv-top-10">
	        <a id="enviar" class="btn green"> Guardar Usuario</a>
	    </div>
	</div>
</section>

<script src="<?=$base_url?>js/bootstrap-fileinput.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
          function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
          }
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
          <?php if (isset($datos_usuario['id_departamento'])) { ?>
            departamento(<?=$datos_usuario['id_departamento']?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if (isset($datos_usuario['id_departamento'])!="" and isset($datos_usuario['id_provincia'])!="") { ?>
            provincia(<?=$datos_usuario['id_departamento']?>,<?=$datos_usuario['id_provincia']?>);
          <?php }elseif(isset($datos_usuario['id_departamento'])){ ?>
            provincia(<?=$datos_usuario['id_departamento']?>,null);
          <?php };?>

          <?php if (isset($datos_usuario['id_provincia']) and isset($datos_usuario['id_distrito'])) { ?>
            distrito(<?=$datos_usuario['id_provincia']?>,<?=$datos_usuario['id_distrito']?>);
          <?php }elseif(isset($datos_usuario['id_provincia'])){ ?>
            distrito(<?=$datos_usuario['id_provincia']?>,null);
          <?php };?>

          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });



          $("#enviar").click(function(){
          	var error=0;
          	/*if ($("input[name='usuario']").val()=="") {
          		alertify.error("El campo usuario no puede estar vacio");
          		error=error+1;
          	}
          	if ($("input[name='password']").val()=="") {
          		alertify.error("El campo contraseña no puede estar vacio");
          		error=error+1;
          	}
          	if ($("input[name='id_distrito']").val()==null) {
          		alertify.error("Debe seleccionar un distrito");
          		error=error+1;
          	}
          	if ($("input[name='nombres']").val()=="") {
          		alertify.error("Campo nombre no puede estar vacìo");
          		error=error+1;
          	}
          	if ($("input[name='apellido_p']").val()=="") {
          		alertify.error("Campo apellido paterno no puede estar vacìo");
          		error=error+1;
          	}*/
          	if (error==0) {
          		$("#enviarformulario").click();
          	}
          })
        })
</script>
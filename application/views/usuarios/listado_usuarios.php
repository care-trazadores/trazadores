<h3 class="page-title">Usuarios <small>Listado</small></h3>
      <section class="row">
        <?=$cuadros;?>
      </section>
      <?php
      if ($_SESSION["usuario"]["tipo"]!="usuario") {
      ?>
      <section class="row">
        <div class="col12">
          <a href="<?=$base_url?>index.php?c=usuarios&m=nuevo" class="btn boton">
            <i class="fa fa-plus"></i> Crear nuevo usuario</a>
        </div>
      </section>
      <?php
      }
      ?>
      <section class="row">
        <div id="listado">
          <div id="list-cab">
            <i class="fa fa-list-alt"></i><span>Listado de Usuarios</span>
          </div>
          <div id="filtros">
            <form method="post" action="<?=$base_url?>index.php?c=usuarios">
              <input type="hidden" id="url_retorno" key="<?=$url_retorno?>">
            <div>
              <span>Usuario</span>
              <input type="text" name="busq_usuario" value="<?=$busq_usuario;?>">
            </div>
            <div>
              <span>Correo</span>
              <input type="text" name="busq_correo" value="<?=$busq_correo;?>">
            </div>
            <div>
              <span>Departamento</span>
              <select name="id_departamento">
              </select>
            </div>
            <div>
              <span>Provincia</span>
              <select name="id_provincia">
              </select>
            </div>
            <div>
              <span>Distrito</span>
              <select name="id_distrito">
              </select>
            </div>
            <div>
              <span>Estado</span>
              <select name="estado">
                <option <?php if ($estado=="2") {echo "selected";}?> value="2">Todos</option>
                <option <?php if ($estado=="1") {echo "selected";}?> value="1">Habilitados</option>
                <option <?php if ($estado=="0") {echo "selected";}?> value="0">Deshabilitados</option>
              </select>
            </div>
            <div>
              <span>cantidad</span>
              <select name="limite">
                <option <?php if ($limite=="5") {echo "selected";}?> value="5">5</option>
                <option <?php if ($limite=="10") {echo "selected";}?> value="10">10</option>
                <option <?php if ($limite=="50") {echo "selected";}?> value="50">50</option>
              </select>
            </div>
            <div><input type="submit" value="buscar" name="buscar"></div>
            </form>
          </div>
          <div id="list-cuerpo">
            <table class="listado">
              <tr>
                <th><input type="checkbox"></th>
                <th>Foto</th>
                <th>Usuario</th>
                <th>Correo</th>
                <th>Distrito</th>
                <th>Estado</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
              <?php
                foreach ($listado as $key) {
              ?>
              <tr>
                <td><input type="checkbox"></td>
                <td id="fot-list">
                <?php
                  if ($key->foto!="") {echo '<img src="'.$base_url.'img/usuarios/'.$key->foto.'">';}
                  else{echo '<img src="'.$base_url.'img/usuarios/usu.jpg">';}
                ?>
                </td>  
                <td><?=$key->user;?></td>
                <td><?=$key->correo;?></td>
                <td><?=$key->distrito;?></td>
              <?php
              if ($key->estado==1) {
              ?>
                <td><i class="estado-ajax fa fa-check green" key="<?=$key->id_usuario?>"></i></td>
              <?php
              }else{
              ?>
                <td><i class="estado-ajax fa fa-times red" key="<?=$key->id_usuario?>"></i></td>
              <?php
              }
              ?>
                <td><a href="<?=$base_url?>index.php?c=usuarios&m=user_config&id_usuario=<?=$key->id_usuario?>"><i class="fa fa-pencil-square-o"></a></i></td>
                <td><i class="fa fa-trash-o delete_user" key="<?=$key->id_usuario?>"></i></i></td>
              </tr>
              <?php
                }
              ?>
            </table>
            <div id="list-pie">
              <?=$paginador?>
            </div>  
          </div>
        </div>
      </section>
      <script type="text/javascript">
        $(document).ready(function(){
          function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
          }
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
          <?php if (isset($id_departamento)) { ?>
            departamento(<?=$id_departamento?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if (isset($id_departamento) and isset($id_provincia)) { ?>
            provincia(<?=$id_departamento?>,<?=$id_provincia?>);
          <?php }elseif(isset($id_departamento)){ ?>
            provincia(<?=$id_departamento?>,null);
          <?php };?>
          <?php if (isset($id_provincia) and isset($id_distrito)) { ?>
            distrito(<?=$id_provincia?>,<?=$id_distrito?>);
          <?php }elseif(isset($id_provincia)){ ?>
            distrito(<?=$id_provincia?>,null);
          <?php };?>

          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });
        })
      </script>
      <script type="text/javascript">
        $(".delete_user").on( 'click', function () {
          var url="<?=$base_url?>index.php?c=usuarios&m=delete_user";
          var url_retorno=$("#url_retorno").attr("key");
          var tipo="usuario";
          var id_usuario=$(this).attr("key");
          var parametros = {
            "id_usuario" : id_usuario,
            "tipo" : tipo
          };
          alertify.confirm("Esta seguro que desea eliminarlo", function (e) {
            if (e) {
              $.ajax({
                data:  parametros,
                type: "POST",
                url: url
              }).done(function( data ) {
                if (data=="si") {
                  alertify.alert("Se elimino correctamente",function(e){
                    window.location.href = url_retorno;
                  });
                }else{
                  alertify.error("Error al eliminar");
                };
              });
            }else{
              alertify.error("Transacción Cancelada");
            }
          });
          return false;
        });
        $(".estado-ajax").on( 'click', function () {
          var url="<?=$base_url?>index.php?c=usuarios&m=update_estado";
          var url_retorno=$("#url_retorno").attr("key");
          var id_usuario=$(this).attr("key");
          if ($(this).hasClass("green")) {var estado=0;}
          if ($(this).hasClass("red")) {var estado=1;}
          var parametros = {
                "id_usuario" : id_usuario,
                "estado" : estado
          };
          alertify.confirm("Esta seguro de cambiar estado", function (e) {
            if (e) {
              $.ajax({
                data:  parametros,
                type: "POST",
                url: url
              }).done(function( response ) {
                if (response=="") {
                  alertify.alert("Se cambio estado correctamente",function(e){
                    window.location.href = url_retorno;
                  });
                }else{
                  alert(response);
                  alertify.error("Error al cambiar estado");
                };
              });
            }else{
              alertify.error("Transacción Cancelada");
            }
          });
          return false;
        });
      </script>
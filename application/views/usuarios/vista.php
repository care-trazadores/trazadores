<h3 class="page-title">Acciones del usuario  | Cuenta<small> La página de cuenta de usuario</small></h3>
<div class="row">
    <div class="col3">
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                <?php
                if ($datos_usuario['foto']=="") {echo '<img src="'.$base_url.'img/usuarios/usu.jpg" class="img-responsive" alt=""> ';}
                else{echo '<img src="'.$base_url.'img/usuarios/'.$datos_usuario['foto'].'" class="img-responsive" alt=""> ';}
                ?>
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> <?=$datos_usuario['nombre']?> <?=$datos_usuario['apellido_p']?> <?=$datos_usuario['apellido_m']?> </div>
                    <div class="profile-usertitle-job"> <?=$datos_usuario['tipo']?> </div>
                    <div id="user-estado" class="btn btn-circle <?php if ($datos_usuario['estado']==1) {echo "green";}else{echo "red";}?> btn-sm"><?php if ($datos_usuario['estado']==1) {echo "Habilitado";}else{echo "Desahabilitado";}?></div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <!--<div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                    <button type="button" class="btn btn-circle red btn-sm">Message</button>
                </div>-->
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="<?=$base_url?>index.php?c=usuarios&m=vista&id_usuario=<?=$datos_usuario['id']?>">
                                <i class="icon-home"></i>Vista ràpida</a>
                        </li>
                        <li>
                            <a href="<?=$base_url?>index.php?c=usuarios&m=user_config&id_usuario=<?=$datos_usuario['id']?>">
                                <i class="icon-settings"></i>Configuraciones de la cuenta</a>
                        </li>
                        <!--<li>
                            <a href="page_user_profile_1_help.html">
                                <i class="icon-info"></i>Ayuda</a>
                        </li>-->
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="profile-region">
                        <div class="col12 profile-stat-separ">
                            <div class="uppercase profile-stat-title"> Departamento: </div>
                            <div id="user-departamento" class="uppercase profile-stat-text"> <?=$datos_usuario["departamento"]?> </div>
                        </div>
                        <div class="col12 profile-stat-separ">
                            <div class="uppercase profile-stat-title"> Provincia: </div>
                            <div id="user-provincia" class="uppercase profile-stat-text">  <?=$datos_usuario["provincia"]?> </div>
                        </div>
                        <div class="col12 profile-stat-separ">
                            <div class="uppercase profile-stat-title"> Distrito: </div>
                            <div id="user-distrito" class="uppercase profile-stat-text">  <?=$datos_usuario["distrito"]?> </div>
                        </div>
                    </div>
                    <!--<div class="col3">
                        <div class="uppercase profile-stat-title"> 37 </div>
                        <div class="uppercase profile-stat-text"> Encuestas</div>
                    </div>
                    <div class="col3">
                        <div class="uppercase profile-stat-title"> 51 </div>
                        <div class="uppercase profile-stat-text"> Tasks </div>
                    </div>
                    <div class="col3">
                        <div class="uppercase profile-stat-title"> 61 </div>
                        <div class="uppercase profile-stat-text"> Uploads </div>
                    </div>-->
                </div>
                <!-- END STAT -->
                <div>
                    <!--<h4 class="profile-desc-title">About Marcus Doe</h4>
                    <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>-->
                    <?php if ($datos_usuario['correo']!="") {
                    ?>
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-envelope"></i>
                        <a href="mailto:<?=$datos_usuario['correo']?>"><?=$datos_usuario['correo']?></a>
                    </div>
                    <?php
                    }?>
                    <?php if ($datos_usuario['telefono']!="") {
                    ?>
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-phone"></i>
                        <a href=""><?=$datos_usuario['telefono']?></a>
                    </div>
                    <?php
                    }?>
                    <?php if ($datos_usuario['celular']!="") {
                    ?>
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa fa-mobile"></i>
                        <a href=""><?=$datos_usuario['celular']?></a>
                    </div>
                    <?php
                    }?>
                </div>
            </div>
            <!-- END PORTLET MAIN -->
        </div>
    </div>
    <div class="col9">
        <?=$graficos?>
    </div>
</div>
<div id="llenado_encuesta">
<form method="post" action="<?=$base_url?>index.php?c=municipal&m=guardar">
	<input type="hidden" name="id_llenado" value='<?=$datos_llenado["id_llenado"]?>'>
	<h1 class="page-title">Lista de preguntas</h1>
	<div class="row">
		<div class="col12">
			<div class="portlet light bordered">
				<div class="datos_encuesta">
					<div class="row">
						<div class="col6">
							<span>Evaludador:</span>
							<input type="hidden" name="id_usuario" value="<?=$datos_llenado["id_usuario"]?>">
							<span class="input form-control" ><?=$datos_llenado["usuario"]?></span>
						</div>
						<div class="col3">
							<span>Fecha</span>
							<input type="hidden" name="fecha" value="<?=$datos_llenado["fecha"]?>">
							<span class="input form-control"><?=$datos_llenado["fecha"]?></span>
						</div>
					</div>
					<div class="row">
						<div class="col6">
							<div class="row">
								<div class="col6">
									<span>Departamento:</span>
									<?php if($datos_llenado["departamento"]){ ?>

									<span class="input form-control" ><?=$datos_llenado["departamento"]?></span>
									
									<?php }else{?>
									<select name="id_departamento">
              						</select>
									<?php } ?>
								</div>
								<div class="col6">
									<span>Provincia</span>
									<?php if($datos_llenado["provincia"]) {?>
									<span class="input form-control"><?=$datos_llenado["provincia"]?></span>

									<?php }else{ ?>
									<select name="id_provincia">
              						</select>
              						<?php } ?>

								</div>
							</div>
						</div>
						<div class="col6">
							<div class="row">
								<div class="col6">
									<span>Distrito</span>
									<?php if($datos_llenado["distrito"]) {?>
									<input type="hidden" name="id_distrito" value="<?=$datos_llenado["id_distrito"]?>">
									<span class="input form-control"><?=$datos_llenado["distrito"]?></span>
									<?php }else{?>
									<select name="id_distrito">
									</select>
									<?php } ?>
								</div>
								<div class="col6">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col12">
			<div class="portlet light bordered">
				<div class="row">
					<div id="tabs">
						<?=$tabs?>
					</div>
					<?=$cuadros_preguntas?>
				</div>
			</div>
		</div>
	</div>
	<span id="enviar" class="btn boton">Enviar</span>
</form>	

</div>
<script type="text/javascript">
	function anexo_cuadro(id_cuadro){
		mostrartab(id_cuadro);
		$("#enviar").fadeOut();
		$("#tabs").fadeOut();
		$("#tab"+id_cuadro).fadeIn();
	}	
	function mostrartab(valor){
		$(".tabs").fadeOut();
		$("#tabs").fadeIn();
		$("#tab"+valor).fadeIn();
		$("#tabs li").removeClass("selected");
		$("#tabs .tabs"+valor).addClass("selected");
	}
	function verificacion(id_cuadro,tipo){
			$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":"inherit"})
			$("#tab"+id_cuadro+" .calificacion_final").html("")
			$("#tab"+id_cuadro+" .sumatoria").html("");
			$("#tab"+id_cuadro+" .promedio").html("");
			suma_final=""
			promedio_final=""
			calificacion_final=""

			cantidad=cantidad_preguntas(id_cuadro)
			sumatoria_vac=sumatoria_vacio(id_cuadro)

			if (tipo=="cuadro") {
				if (sumatoria_vac!=cantidad) {
					suma_final=sumatoria(id_cuadro)
					promedio_final=promedio(suma_final,cantidad)
					calificacion_final=calificacion(id_cuadro,promedio_final)
					$("#tab"+id_cuadro+" .sumatoria").html(suma_final);
					$("#tab"+id_cuadro+" .promedio").html(promedio_final);
					$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":calificacion_final["background"]})
					$("#tab"+id_cuadro+" .calificacion_final").html(calificacion_final["texto"])
					$(this).css({"border": "1px solid #B2D4F5"})
				};
			};
			if (tipo=="anexo1") {
					cantidad=totalsi(id_cuadro)
					suma_final=sumatoria(id_cuadro)
					promedio_final=promedio(suma_final,cantidad)
					calificacion_final=calificacion(id_cuadro,promedio_final)
					$("#tab"+id_cuadro+" .sumatoria").html(suma_final);
					$("#tab"+id_cuadro+" .promedio").html(promedio_final.toFixed(2));
					$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":calificacion_final["background"]})
					$("#tab"+id_cuadro+" .calificacion_final").html(calificacion_final["texto"])
					$(this).css({"border": "1px solid #B2D4F5"})
			};
			if (tipo=="anexo2") {
				if (sumatoria_vac!=cantidad) {

					//cantidad=totalsi(id_cuadro)
					suma_final=sumatoria(id_cuadro)
					promedio_final=suma_final
					calificacion_final=calificacion(id_cuadro,promedio_final)
					$("#tab"+id_cuadro+" .sumatoria").html(suma_final);
					$("#tab"+id_cuadro+" .promedio").html(promedio_final);
					$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":calificacion_final["background"]})
					$("#tab"+id_cuadro+" .calificacion_final").html(calificacion_final["texto"])
					$(this).css({"border": "1px solid #B2D4F5"})
				};
			};
			if (tipo=="jass") {
				if (sumatoria_vac!=cantidad) {
					//cantidad=totalsi(id_cuadro)
					suma_final=sumatoria(id_cuadro)
					promedio_final=suma_final
					calificacion_final=calificacion(id_cuadro,promedio_final)
					$("#tab"+id_cuadro+" .sumatoria").html(suma_final);
					$("#tab"+id_cuadro+" .promedio").html(promedio_final);
					$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":calificacion_final["background"]})
					$("#tab"+id_cuadro+" .calificacion_final").html(calificacion_final["texto"])
					$(this).css({"border": "1px solid #B2D4F5"})
				};
			};
			ponderacion_copia(id_cuadro,promedio_final,calificacion_final)
		}
		function cantidad_preguntas(id_cuadro){
			var i=0
			$("#tab"+id_cuadro+" select").each(function(){
				i++;
			});
			return i;
		}
		function sumatoria_vacio(id_cuadro){
			var suma=0
			$("#tab"+id_cuadro+" select").each(function(){
				var valor=$(this).val();
				if (valor=="") {
					suma=suma+1;
				};
			});
			return suma
		}
		function sumatoria(id_cuadro){
			var suma=0;
			$("#tab"+id_cuadro+" select").each(function(){
				var valor=$(this).val();
				if (valor=="") {
					valor=0;
				};
				suma=suma+parseFloat(valor);

			});
			return suma
		}
		function promedio(sumatoria,cantidad){
			var promedio=sumatoria/cantidad
			console.log(promedio)
			return promedio
		}
		function calificacion(id_cuadro,promedio){
			var calificacion=[]
			$("#tab"+id_cuadro+" .calificacion .calificaciones").each(function(){
				var mayor=$(this).attr("mayor")
				var menor=$(this).attr("menor")
				var igual=$(this).attr("igual")
				var background=$(this).attr("background")
				var texto=$(this).attr("texto")
				if (igual!="") {
					if (igual==promedio) {
						calificacion["background"]=background
						calificacion["texto"]=texto
					}
				}else if (mayor!="" && menor!="") {
					if (promedio>=mayor && promedio<menor) {
						calificacion["background"]=background
						calificacion["texto"]=texto
					}
				}else if (mayor!="" && menor=="") {
					if (promedio>=mayor) {
						calificacion["background"]=background
						calificacion["texto"]=texto
					}
				}else if (menor!="" && mayor=="") {
					if (promedio<=menor) {
						calificacion["background"]=background
						calificacion["texto"]=texto
					}
				}
			})
			return calificacion
		}
		function ponderacion_copia(id_cuadro,promedio,calificacion_final){
			if ($(".aponderacion").hasClass('cuadro'+id_cuadro)){
				$(".cuadro_evaluado"+id_cuadro).html(promedio)
				var ponderacion=$(".aponderacion_valor"+id_cuadro).html();
				if (promedio!="" || promedio===0) {
					$(".aponderacion_valoracion_ponderada"+id_cuadro).html(promedio*ponderacion)
					$(".cuadro_calificacion"+id_cuadro).css({"background-color":calificacion_final["background"]})
					$(".cuadro_calificacion"+id_cuadro).html(calificacion_final["texto"])
				}else{
					$(".aponderacion_valoracion_ponderada"+id_cuadro).html("")
					$(".cuadro_calificacion"+id_cuadro).css({"background-color":"inherit"})
					$(".cuadro_calificacion"+id_cuadro).html("")
				};
				sumatoria_aponderacion()

				var promedio_final=promedio_aponderacion()
				calificacion_final=calificacion(11,promedio_final)
				$(".aponderacion_calificacion").html(calificacion_final["texto"])
				$(".aponderacion_calificacion").css({"background-color":calificacion_final["background"]})
				$(".aponderacion_global").css({"background-color":calificacion_final["background"]})
		    }
		}
		function sumatoria_aponderacion(){
			var suma=0
			$(".aponderacion").each(function(){
				id_cuadro=$(this).attr("id_cuadro")
				var valor=$(".cuadro_evaluado"+id_cuadro).html()
				if (valor=="") {
					valor=0;
				};
				suma=suma+parseFloat(valor)
			})
			$(".aponderacion_sumatoria").html(suma)
		}
		function promedio_aponderacion(){
			var total_valoracion=0
			$(".valoracion_ponderada").each(function(){
				var valor=$(this).html()
				
				if (valor=="") {
					valor=0;
				}
				total_valoracion=total_valoracion+parseFloat(valor)
			})
			var promedio=total_valoracion/parseFloat($(".aponderacion_valor_total").html())
			$(".aponderacion_global").html(promedio)
			return promedio
		}
		function mostrarocultar(valor,id_pregunta,id_cuadro,tipo){
			if (valor==0) {
				$(".filtro"+id_pregunta+" .respuestas").attr('disabled',false);
				$(".filtro"+id_pregunta+" .observacion").attr('disabled',false);
			}else if (valor==1) {
				$(".filtro"+id_pregunta+" .respuestas").prop('selectedIndex',0);
				$(".filtro"+id_pregunta+" .respuestas").attr('disabled',true);
				$(".filtro"+id_pregunta+" .observacion").attr('disabled',true);
				$(".filtro"+id_pregunta+" .respuestas").css({"border": "1px solid #B2D4F5"})
				$(".respuesta"+id_pregunta).html("")
				verificacion(id_cuadro,tipo)
			};
		}
		function totalsi(id_cuadro){
			var totalsi=0
			var totalno=0
			$("#tab"+id_cuadro+" .filtros").each(function(){
				if ($(this).is(':checked')) {
					var valor=$(this).val()
					if (valor==0) {
						totalsi++
					}else if (valor==1) {
						totalno++
					};
				};
				$("#tab"+id_cuadro+" .total_si").html(totalsi)
				$("#tab"+id_cuadro+" .total_no").html(totalno)
			})
			return totalsi
		}
		function jass(id_cuadro,texto){
			//alert(id_cuadro+"-"+texto)
			$("#tab"+id_cuadro+" select option").each(function(){
				if ($(this).attr("texto")==texto) {
					$(this).prop("selected", true )
					verificacion(id_cuadro,"jass")
				};
			})
		}
	$(document).ready(function(){
		$(".tabs").fadeOut();
		mostrartab(1);
		verificacion(1,"anexo2")
		$("#tabs li").click(function(){
			var posicion=$(this).attr("key");
			mostrartab(posicion);
		})
		$(".respuestas").change(function(){
			var id_cuadro=$(this).attr("id_cuadro");
			var tipo=$(this).attr("tipo");
			verificacion(id_cuadro,tipo)
			var id=$(this).attr("id")

			var valor
			if ($(this).val()==0) {
				valor="ADECUADO"
			}else if ($(this).val()==1) {
				valor="LIMITADO"
			}else if ($(this).val()==2) {
				valor="DETERIORADO"
			};
			$("#tab"+id_cuadro+" ."+id).html(valor);

			jass(12,$("#"+id+" option:selected").attr("texto"))
			$(this).css({"border": "1px solid #B2D4F5"})
		})
		$("#enviar").click(function(){
			var error=0;
			$("select").each(function(){
				var valor=$(this).val()
				var tipo=$(this).attr("tipo")
				if (valor=="") {
					$(this).css({"border":"1px solid #F93A3A"})
					error++
				};
			})
			if (error!=0) {
				alertify.error("debe llenar los campos que están con bordes rojos");
			}else{
				id_distrito=$("select[name=id_distrito]").val();
				$.ajax({
	              type: "POST",
	              url: "<?=$base_url?>index.php?c=municipal&m=ajaxLlenadoDistrito",
	              data: {ajax:"verificardistrito",id_distrito:id_distrito}
	            }).done(function( data ) {
	              if (data==1) {
	              	error++
	              	alertify.error("El distrito ya existe");
	              }else{
	              	$("#llenado_encuesta form").submit()
	              }
	            });
			};
		})
		$(".filtros").each(function(){
			if ($(this).is(':checked')) {
				var valor=$(this).val()
				var id_pregunta=$(this).attr("pregunta")
				mostrarocultar(valor,id_pregunta)
			};
		})
		$(".filtros").change(function(){
			var valor=$(this).val()
			var id_pregunta=$(this).attr("pregunta")
			var id_cuadro=$(this).attr("cuadro")
			var tipo=$(this).attr("tipo")
			mostrarocultar(valor,id_pregunta,id_cuadro,tipo)
			totalsi(id_cuadro)
		})

		function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
        }
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
          <?php if (isset($id_departamento)) { ?>
            departamento(<?=$id_departamento?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if (isset($id_departamento) and isset($id_provincia)) { ?>
            provincia(<?=$id_departamento?>,<?=$id_provincia?>);
          <?php }elseif(isset($id_departamento)){ ?>
            provincia(<?=$id_departamento?>,null);
          <?php };?>
          <?php if (isset($id_provincia) and isset($id_distrito)) { ?>
            distrito(<?=$id_provincia?>,<?=$id_distrito?>);
          <?php }elseif(isset($id_provincia)){ ?>
            distrito(<?=$id_provincia?>,null);
          <?php };?>

          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });
	})


	function verificar(retorno,id_pregunta,id_cuadro){
		var error=0
		var llenado=0;
		$("#tab"+id_cuadro+" .filtros").each(function(){
			if ($(this).is(':checked')) {
				var valor=$(this).val()
				var id_pregunta=$(this).attr("pregunta")
				if (valor==0) {
					var respuesta=$(".filtro"+id_pregunta+" .respuestas").val();
					if (respuesta=="") {
						error++
						$(".filtro"+id_pregunta+" .respuestas").css({"border":"1px solid #F93A3A"})
					}else{
						llenado++
					};
				}
			};
		})
		if (llenado!=0) {
			if (error==0) {
				mostrartab(retorno)
				var cali=$("#tab"+id_cuadro+" .calificacion_final").html()
				if (cali!="") {
					$("#respuesta"+id_pregunta+" option").each(function(){
						$(this).prop( "selected", false );
						if (cali==$(this).attr("texto")) {
							$("#respuesta"+id_pregunta+">option[value='"+$(this).attr("value")+"']").prop( "selected", true );
						};
					})
				}else{
					$("#respuesta"+id_pregunta+">option[value='']").attr("selected","selected")
				};
				verificacion(retorno,"cuadro")
				$("#enviar").fadeIn();
			};
		}else{
			if (error!=0) {
				alertify.error("Debe responder las preguntas marcadas en si");
			}else{
				alertify.error("Al menos debe responder una pregunta");
			}
		}
		
	}
	function verificar1(retorno,id_pregunta,id_cuadro){
		var error=0;
		$("#tab"+id_cuadro+" select").each(function(){
			var valor=$(this).val()
			var tipo=$(this).attr("tipo")
			if (valor=="") {
				$(this).css({"border":"1px solid #F93A3A"})
				error++
			};
		})
		if (error==0) {
			mostrartab(retorno)
			var cali=$("#tab"+id_cuadro+" .calificacion_final").html()
			if (cali!="") {
				$("#respuesta"+id_pregunta+" option").each(function(){
					$(this).prop( "selected", false );
					if (cali==$(this).attr("texto")) {
						$("#respuesta"+id_pregunta+">option[value='"+$(this).attr("value")+"']").prop( "selected", true );
					};
				})
			}else{
				$("#respuesta"+id_pregunta+">option[value='']").attr("selected","selected")
			};
			verificacion(retorno,"cuadro")
			$("#enviar").fadeIn();
		}else{
			alertify.error("Todas las preguntas deben ser calificadas");
		};
	}
</script>
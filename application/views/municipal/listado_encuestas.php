<h3 class="page-title">Informaciòn <small>ATM</small></h3>
      <section class="row"style="text-align: center;margin-left: 150px">
        <?=$cuadros;?>
      </section>
      <section class="row">
        <div class="col12">
          <a href="<?=$base_url?>index.php?c=municipal&m=llenado" class="btn boton">
            <span class="fa fa-plus"></span> Nueva encuesta ATM</a></div>
      </section>
      <section class="row">
        <div id="listado">
          <div id="list-cab">
            <i class="fa fa-list-alt"></i><span><?php echo $titulo; ?></span>
          </div>
          <div id="filtros">
              <form method="post" action="<?php echo base_url()?>index.php?c=municipal">
            <?php
              if ($tipo_usuario!="usuario") {
            ?>
            <div>
              <span>Usuario</span>
              <input type="text" name="busq_usuario" value="<?=$busq_usuario;?>">
            </div>
            <div>
              <span>Departamento</span>
              <select name="id_departamento">
              </select>
            </div>
            <div>
              <span>Provincia</span>
              <select name="id_provincia">
              </select>
            </div>
            <div>
              <span>Distrito</span>
              <select name="id_distrito">
              </select>
            </div>
            <?php
              }
            ?>
            <div>
              <span>cantidad</span>
              <select name="limite">
                <option <?php if ($limite=="5") {echo "selected";}?> value="5">5</option>
                <option <?php if ($limite=="10") {echo "selected";}?> value="10">10</option>
                <option <?php if ($limite=="50") {echo "selected";}?> value="50">50</option>
              </select>
            </div>
            <div><input type="submit" value="buscar" name="buscar"></div>
            </form>
          </div>
          <div id="list-cuerpo">
            <table class="listado">
              <tr>
                <th>ID</th>
                <th>Usuario</th>
                <!--<th>Correo</th>-->
                <!-- <th>Localidad</th> -->
                <th>Distrito</th>
                <!--<th>Provincia</th>-->
                <!--<th>Ver</th>-->
                <th>Editar</th>
               <!--  <th>Exportar pdf</th>
                <th>Exportar excel</th> -->
                <th>Eliminar</th>
              </tr>
              <?php
                foreach ($listado as $key) {
              ?>
              <tr>
                <td><?=$key->id_llenado?></td>
                <?php
                if ($key->user=="") {
                ?>
                  <td>Usuario eliminado</td>
                <?php
                }else{
                ?>
                  <td><?=$key->user;?></td>
                <?php
                }
                ?>
                <td><?=$key->distrito;?></td>
                <td><a href="<?=$base_url?>index.php?c=municipal&m=edit&id=<?=$key->id_llenado?>"><i class="fa fa-pencil-square-o"></a></i></td>
                <td><a href="<?=$base_url?>index.php?c=municipal&m=delete&id=<?=$key->id_llenado?>"><i class="fa fa-trash-o"></i></a></i></td>
              </tr>
              <?php
                }
              ?>
            </table>
            <div id="list-pie">
              <?=$paginador?>
            </div>  
          </div>
        </div>
      </section>
      <script type="text/javascript">
        $(document).ready(function(){
          function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
          }
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
          <?php if (isset($id_departamento)) { ?>
            departamento(<?=$id_departamento?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if (isset($id_departamento) and isset($id_provincia)) { ?>
            provincia(<?=$id_departamento?>,<?=$id_provincia?>);
          <?php }elseif(isset($id_departamento)){ ?>
            provincia(<?=$id_departamento?>,null);
          <?php };?>
          <?php if (isset($id_provincia) and isset($id_distrito)) { ?>
            distrito(<?=$id_provincia?>,<?=$id_distrito?>);
          <?php }elseif(isset($id_provincia)){ ?>
            distrito(<?=$id_provincia?>,null);
          <?php };?>

          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });
        })
      </script>
<h3 class="page-title">Acciones del usuario  | Cuenta<small> La página de cuenta de usuario</small></h3>
<div class="row">
    <div class="col3">
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                <?php
                if ($datos_usuario['foto']=="") {echo '<img src="'.$base_url.'img/usuarios/usu.jpg" class="img-responsive" alt=""> ';}
                else{echo '<img src="'.$base_url.'img/usuarios/'.$datos_usuario['foto'].'" class="img-responsive" alt=""> ';}
                ?>
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> <?=$datos_usuario['nombre']?> <?=$datos_usuario['apellido_p']?> <?=$datos_usuario['apellido_m']?> </div>
                    <div class="profile-usertitle-job"> <?=$datos_usuario['tipo']?> </div>
                    <div id="user-estado" class="btn btn-circle <?php if ($datos_usuario['estado']==1) {echo "green";}else{echo "red";}?> btn-sm"><?php if ($datos_usuario['estado']==1) {echo "Habilitado";}else{echo "Desahabilitado";}?></div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <!--<div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                    <button type="button" class="btn btn-circle red btn-sm">Message</button>
                </div>-->
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <!--<li>
                            <a href="<?=$base_url?>index.php?c=cuenta&m=vista&id_usuario=<?=$datos_usuario['id']?>">
                                <i class="icon-home"></i>Vista ràpida</a>
                        </li>-->
                        <li class="active">
                            <a href="<?=$base_url?>index.php?c=cuenta&m=user_config&id_usuario=<?=$datos_usuario['id']?>">
                                <i class="icon-settings"></i>Configuraciones de la cuenta</a>
                        </li>
                        <!--<li>
                            <a href="page_user_profile_1_help.html">
                                <i class="icon-info"></i>Ayuda</a>
                        </li>-->
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="profile-region">
                        <div class="col12 profile-stat-separ">
                            <div class="uppercase profile-stat-title"> Departamento: </div>
                            <div id="user-departamento" class="uppercase profile-stat-text"> <?=$datos_usuario["departamento"]?> </div>
                        </div>
                        <div class="col12 profile-stat-separ">
                            <div class="uppercase profile-stat-title"> Provincia: </div>
                            <div id="user-provincia" class="uppercase profile-stat-text">  <?=$datos_usuario["provincia"]?> </div>
                        </div>
                        <div class="col12 profile-stat-separ">
                            <div class="uppercase profile-stat-title"> Distrito: </div>
                            <div id="user-distrito" class="uppercase profile-stat-text">  <?=$datos_usuario["distrito"]?> </div>
                        </div>
                    </div>
                    <!--<div class="col3">
                        <div class="uppercase profile-stat-title"> 37 </div>
                        <div class="uppercase profile-stat-text"> Encuestas</div>
                    </div>
                    <div class="col3">
                        <div class="uppercase profile-stat-title"> 51 </div>
                        <div class="uppercase profile-stat-text"> Tasks </div>
                    </div>
                    <div class="col3">
                        <div class="uppercase profile-stat-title"> 61 </div>
                        <div class="uppercase profile-stat-text"> Uploads </div>
                    </div>-->
                </div>
                <!-- END STAT -->
                <div>
                    <!--<h4 class="profile-desc-title">About Marcus Doe</h4>
                    <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>-->
                    <?php if ($datos_usuario['correo']!="") {
                    ?>
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-envelope"></i>
                        <a href="mailto:<?=$datos_usuario['correo']?>"><?=$datos_usuario['correo']?></a>
                    </div>
                    <?php
                    }?>
                    <?php if ($datos_usuario['telefono']!="") {
                    ?>
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa-phone"></i>
                        <a href=""><?=$datos_usuario['telefono']?></a>
                    </div>
                    <?php
                    }?>
                    <?php if ($datos_usuario['celular']!="") {
                    ?>
                    <div class="margin-top-20 profile-desc-link">
                        <i class="fa fa fa-mobile"></i>
                        <a href=""><?=$datos_usuario['celular']?></a>
                    </div>
                    <?php
                    }?>
                </div>
            </div>
            <!-- END PORTLET MAIN -->
        </div>
    </div>
    <div class="col8">
        <div class="profile-content">
            
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li  class="tab1" key="1">
                                    <a  data-toggle="tab">Cuenta</a>
                                </li>
                                <li class="tab2" key="2">
                                    <a  data-toggle="tab">Información Personal</a>
                                </li>
                                <li class="tab3" key="3">
                                    <a  data-toggle="tab">Foto</a>
                                </li>
                                <li class="tab4" key="4" >
                                    <a data-toggle="tab">Cambiar Contraseña</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- CUENTA TAB -->
                                <div class="tab-pane" id="tab_1_1">
                                    <form id="formulario_cuenta" action="#">
                                        <input type="hidden" name="user_id" value="<?=$datos_usuario['id']?>"> 
                                        <div class="form-group">
                                            <label class="control-label">Usuario</label>
                                            <input type="text" name="user_usuario" placeholder="Carkll" class="form-control" value="<?=$datos_usuario["user"]?>"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Estado</label>
                                            <select name="user_estado">
                                                <option <?php if ($datos_usuario["estado"]==1) { echo "selected";}?> value="1">Habilitado</option>
                                                <option <?php if ($datos_usuario["estado"]==0) { echo "selected";}?> value="0">Desahabilitado</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col4">
                                                    <label class="control-label">Departamento</label>
                                                    <select name='id_departamento'>
                                                    </select> 
                                                </div>
                                                <div class="col4">
                                                    <label class="control-label">Provincia</label>
                                                    <select  name='id_provincia'>
                                                    </select> 
                                                </div>
                                                <div class="col4">
                                                    <label class="control-label">Distrito</label>
                                                    <select name='id_distrito'>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-top-20">
                                            <a id="guardar_cuenta" href="javascript:;" class="btn green"> Guardar Cambios </a>
                                            <a href="javascript:;" class="btn default"> Cancelar </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PRIVACY SETTINGS TAB -->
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane" id="tab_1_2">
                                    <form id="formulario_info" role="form" action="#">
                                        <input type="hidden" name="user_id" value="<?=$datos_usuario['id']?>"> 
                                        <div class="form-group">
                                            <label class="control-label">Nombres</label>
                                            <input type="text" name="user_nombre" placeholder="Carlos" class="form-control" value="<?=$datos_usuario['nombre']?>"> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Apellido Paterno</label>
                                            <input type="text" name="user_apellido_p" placeholder="Palma" class="form-control" value="<?=$datos_usuario['apellido_p']?>"> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Apellido Materno</label>
                                            <input type="text" name="user_apellido_m" placeholder="Bocanegra" class="form-control" value="<?=$datos_usuario['apellido_m']?>"> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">DNI</label>
                                            <input type="text" name="user_dni" placeholder="46837941" class="form-control" value="<?=$datos_usuario['dni']?>"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Correo</label>
                                            <input type="text" name="user_correo" placeholder="jorge0491@hotmail.com" class="form-control" value="<?=$datos_usuario['correo']?>"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Direccion</label>
                                            <input type="text" name="user_direccion" placeholder="Jr.alfonso ugarte 1351" class="form-control" value="<?=$datos_usuario['direccion']?>"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Telefono</label>
                                            <input type="text" name="user_telefono" placeholder="01456829" class="form-control" value="<?=$datos_usuario['telefono']?>"> </div>
                                         <div class="form-group">
                                            <label class="control-label">Celular</label>
                                            <input type="text" name="user_celular" placeholder="991352658" class="form-control" value="<?=$datos_usuario['celular']?>"> </div>
                                        <!--<div class="form-group">
                                            <label class="control-label">About</label>
                                            <textarea class="form-control" rows="3" placeholder="We are KeenThemes!!!"></textarea>
                                        </div>-->   
                                        <div class="margiv-top-10">
                                            <a id="guardar_info" href="javascript:;" class="btn green"> Guardar Cambios </a>
                                            <a href="javascript:;" class="btn default"> Cancelar </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                                <!-- CHANGE AVATAR TAB -->
                                <div class="tab-pane" id="tab_1_3">
                                    <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                                            eiusmod. </p>
                                    <form id="formulario_foto" enctype="multipart/form-data" >
                                        <input type="hidden" name="user_id" value="<?=$datos_usuario['id']?>"> 
                                        <div class="form-group"> 
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 150px; height: 150px;">
                                                <?php
                                                if ($datos_usuario['foto']=="") {echo '<img src="'.$base_url.'img/usuarios/usu.jpg" class="img-responsive" alt=""> ';}
                                                else{echo '<img src="'.$base_url.'img/usuarios/'.$datos_usuario['foto'].'" class="img-responsive" alt=""> ';}
                                                ?> 
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span id="open-foto" class="fileinput-new"> Seleccionar Foto</span>
                                                        <span id="changed-foto" class="fileinput-exists hide"> Cambiar </span>
                                                        <input id="file-foto"  type="file" name="file-foto"> </span>
                                                    <a href="javascript:;" class="file-foto-remove btn default fileinput-exists hide" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                            <div class="clearfix margin-top-10">
                                                <span class="label label-danger">NOTA! </span>
                                                <span>La miniatura de la imagen adjunta se apoya en Últimas Versiones de Firefox , Chrome, Opera , Safari e Internet Explorer 10</span>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <a id="guardar_foto" href="javascript:;" class="btn green"> Enviar </a>
                                            <a href="javascript:;" class="btn default"> Cancelar </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END CHANGE AVATAR TAB -->
                                <!-- CHANGE PASSWORD TAB -->
                                <div class="tab-pane" id="tab_1_4">
                                    <form id="formulario_password" action="#">
                                        <input type="hidden" name="user_id" value="<?=$datos_usuario['id']?>">
                                        <?php if ($tipo_usuario=="usuario") {
                                        ?>
                                        <div class="form-group">
                                            <label class="control-label">Contrasena Anterior</label>
                                            <input name="password_ant" type="password" class="form-control"> </div>
                                        <?php
                                        }
                                        ?>
                                        
                                        <div class="form-group">
                                            <label class="control-label">Contasena Nueva</label>
                                            <input name="password_new" type="password" class="form-control"> </div>
                                        <div class="form-group">
                                            <label class="control-label">Repetir Nueva Contrasena</label>
                                            <input name="password_rep" type="password" class="form-control"> </div>
                                        <div class="margin-top-10">
                                            <a id="guardar_password" href="javascript:;" class="btn green"> Guardar Cambios </a>
                                            <a href="javascript:;" class="btn default"> Cancelar </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END CHANGE PASSWORD TAB -->
                            </div>
                        </div>
                    </div>
                </div>
     
        </div>
    </div>
	<div class="col12">
		
		
	</div>
</div>
<script type="text/javascript">
    function tab_posicion(posicion){
        $(".nav.nav-tabs li").removeClass("active");
        $(".tab"+posicion).addClass("active");
        $(".tab-pane").removeClass("active");
        $("#tab_1_"+posicion).addClass("active");
    }
    $(document).ready(function(){
        <?php
            if ($tab_posicion!="") {
                echo "tab_posicion(".$tab_posicion.");";
            }else{
                echo "tab_posicion(1);";
            };
        ?>
        $(".nav.nav-tabs li").click(function(){
            var tab=$(this).attr("key");
            tab_posicion(tab);
        });
        $("#open-foto").click(function(){
            $("#file-foto").click();
        });
    })
</script>
<script src="<?=$base_url?>js/bootstrap-fileinput.js"></script>


<script type="text/javascript">
    $(document).ready(function(){
        $("#guardar_cuenta").click(function(){
            var form=$("#formulario_cuenta").serialize();
            var tab=$(".nav.nav-tabs .active").attr("key");
            $.ajax({
                type: "POST",
                data: form,
                url: "<?=$base_url?>index.php?c=cuenta&m=cuenta_update",
                success: function(data){
                    if (data!="") {
                        alertify.error(data);
                    }else{
                        alertify.alert("Guardado correctamente",function(e){
                            window.location.href = '<?=$base_url?>index.php?c=cuenta&m=user_config&id_usuario='+$("input[name=user_id]").val()+"&tab="+tab;
                        });
                    };
                }
            });
        })
        $("#guardar_info").click(function(){
            var form=$("#formulario_info").serialize();
            var tab=$(".nav.nav-tabs .active").attr("key");
            $.ajax({
                type: "POST",
                data: form,
                url: "<?=$base_url?>index.php?c=cuenta&m=info_update",
                success: function(data){
                    if (data!="") {
                        alertify.error(data);
                    }else{
                        alertify.alert("Guardado correctamente",function(e){
                            window.location.href = '<?=$base_url?>index.php?c=cuenta&m=user_config&id_usuario='+$("input[name=user_id]").val()+"&tab="+tab;
                        });
                    };
                }
            });
        })
        $("#guardar_foto").click(function(){
            var form= new FormData($("#formulario_foto")[0]);
            var tab=$(".nav.nav-tabs .active").attr("key");
            $.ajax({
                type: "POST",
                data: form,
                contentType: false,
                processData: false,
                url: "<?=$base_url?>index.php?c=cuenta&m=foto_update",
                success: function(data){
                    if (data!="") {
                        alertify.error(data);
                    }else{
                        alertify.alert("Foto cambiada correctamente",function(e){
                            window.location.href = '<?=$base_url?>index.php?c=cuenta&m=user_config&id_usuario='+$("input[name=user_id]").val()+"&tab="+tab;
                        });
                    };
                }
            });
        })
        $("#guardar_password").click(function(){
            var forms=$("#formulario_password").serialize();
            $.ajax({
                type: "POST",
                data: forms,
                url: "<?=$base_url?>index.php?c=cuenta&m=password_update",
                success: function(data){
                    if (data!="") {
                        alertify.error(data);
                    }else{
                        alertify.success("Contraseña Guardada");
                    };
                }
            });
        })
})
</script>

<script type="text/javascript">
        $(document).ready(function(){
          function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
          }
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
          <?php if (isset($datos_usuario['id_departamento'])) { ?>
            departamento(<?=$datos_usuario['id_departamento']?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if (isset($datos_usuario['id_departamento']) and isset($datos_usuario['id_provincia'])) { ?>
            provincia(<?=$datos_usuario['id_departamento']?>,<?=$datos_usuario['id_provincia']?>);
          <?php }elseif(isset($datos_usuario['id_departamento'])){ ?>
            provincia(<?=$datos_usuario['id_departamento']?>,null);
          <?php };?>
          <?php if (isset($datos_usuario['id_provincia']) and isset($datos_usuario['id_distrito'])) { ?>
            distrito(<?=$datos_usuario['id_provincia']?>,<?=$datos_usuario['id_distrito']?>);
          <?php }elseif(isset($datos_usuario['id_provincia'])){ ?>
            distrito(<?=$datos_usuario['id_provincia']?>,null);
          <?php };?>

          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });
        })
      </script>
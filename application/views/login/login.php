<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	<title>Trazadores | Login</title>
	<link rel="stylesheet" href="<?=$base_url?>css/login.css">
	<?php
		if (isset($css)) {
			foreach ($css as $a) {
	?>
		<link rel="stylesheet" href="<?=$base_url?>css/<?=$a?>" />
	<?php
			}
		}
	?>
</head>
<body class="page-body login-page " data-url="http://neon.dev">
	<div class="login-container">
		<div class="login-header login-caret">
			<div class="login-content">	
				<a href="" class="logo">
					<img src="<?=$base_url?>img/logo_pb.png" width="120" alt="" />
				</a>
				<p class="description">Logueese para tener acceso al sistema</p>
			</div>
		</div>
		<div class="login-form">
			<div class="login-content">
				<?php
					if ($error!="") {
				?>
				<div class="form-login-error">
					<h3>Login Invalido</h3>
					<p><?=$error_data?></p>
				</div>
				<?php
					}
				?>
				<form method="post" action="<?=$base_url?>index.php?c=login&m=sesion" role="form" id="form_login">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-user"></i>
							</div>
							<input type="text" class="form-control" name="username" id="username" placeholder="Usuario" autocomplete="off" />
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-key"></i>
							</div>
							<input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" autocomplete="off" />
						</div>	
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block btn-login">
							<i class="entypo-login"></i>
							Iniciar Sesión
						</button>
					</div>			
				</form>		
				<div class="login-bottom-links">			
					<a href="<?=$base_url?>index.php?c=login&m=re_password" class="link">Recuperar contraseña?</a>			
				</div>			
			</div>		
		</div>	
	</div>
</body>
</html>
<div id="reportes">
  <h1><?=$titulo?></h1>
  <div class="row">
  	<div id="filtros">
          <form method="post" action="<?=$action?>" id="filtro">
            <div class="fil">
              <span>
                <label>Departamento</label>
                <select name="id_departamento">
                </select>
              </span>
              <span>
                <label>Provincia</label>
                <select name="id_provincia">
                </select>
              </span>
              <span>
                <label>Distrito</label>
                <select name="id_distrito">
                </select>
              </span>
              <?php if ($tipo_reporte!="grafico") {
              ?>
             <span>
                <label>Cantidad</label>
                <select name="limite">
                  <option <?php if ($limite=="5") {echo "selected";}?> value="5">5</option>
                  <option <?php if ($limite=="10") {echo "selected";}?> value="10">10</option>
                  <option <?php if ($limite=="50") {echo "selected";}?> value="50">50</option>
                </select>
              </span>
              <?php
              }
              ?>
            </div>
            <div class="option">
              <span>
                <label>General</label>
                <input type="radio" name="tipo" value="general" <?php if ($tipo=="general") {echo "checked";}?>>
              </span>
               <span>
                <label>Detallado</label>
                <input type="radio" name="tipo" value="detallado" <?php if ($tipo=="detallado") {echo "checked";}?>>
              </span>
               <span>
                <label>Resumen</label>
                <input type="radio" name="tipo" value="resumen" <?php if ($tipo=="resumen") {echo "checked";}?>>
              </span>
               <span>
                <label>Gráfico</label>
                <input type="radio" name="tipo" value="grafico" <?php if ($tipo=="grafico") {echo "checked";}?>>
              </span>
               <span>
                <label>JASS</label>
                <input type="radio" name="tipo" value="jazz" <?php if ($tipo=="jazz") {echo "checked";}?>>
              </span>
              <span>
                <label>ATM</label>
                <input type="radio" name="tipo" value="atm" <?php if ($tipo=="atm") {echo "checked";}?>>
              </span>
            </div>
              <div><input type="submit" value="buscar" name="buscar"></div>
         </form>
      </div>
      <div id="exportar">
        <?php
        if ($tipo_reporte=="grafico") {
          echo '
          <a href="'.$base_url.'index.php?c=exportar&m=grafico&archivo=pdf'.$datos_exportacion.'" target="_blank"><span class="fa fa-file-pdf-o">exportar a PDF</span></a>
          <a href="'.$base_url.'index.php?c=exportar&m=grafico&archivo=excel'.$datos_exportacion.'" target="_blank"><span class="fa fa-file-excel-o ">exportar a excel</span></a>
          ';
        }else{
          echo '
          <a href="'.$base_url.'index.php?c=exportar&archivo=pdf'.$datos_exportacion.'" target="_blank"><span class="fa fa-file-pdf-o">exportar a PDF</span></a>
          <a href="'.$base_url.'index.php?c=exportar&archivo=excel'.$datos_exportacion.'" target="_blank"><span class="fa fa-file-excel-o ">exportar a excel</span></a>
          ';
        }
        ?>
      </div>
      <div class="info_reporte">
        <div id="list-pie">
        <?=$paginador?>
        </div> 
        <div class="col12">
          <div class="popup">
          <div class="popup-container">
            <p style="text-align: center;">Espere un momento, estamos cargando la información.</p>
          </div>
        </div>
          <?=$tablas?>
        </div>
        <div id="list-pie">
        <?=$paginador?>
        </div>
      </div>
  </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){
          function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
          }
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
          <?php if ($id_departamento!="") { ?>
            departamento(<?=$id_departamento?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if ($id_departamento!="" and $id_provincia!="") { ?>
            provincia(<?=$id_departamento?>,<?=$id_provincia?>);
          <?php }elseif($id_departamento!=""){ ?>
            provincia(<?=$id_departamento?>,null);
          <?php };?>
          <?php if ($id_provincia!="" and $id_distrito!="") { ?>
            distrito(<?=$id_provincia?>,<?=$id_distrito?>);
          <?php }elseif ($id_provincia!=""){ ?>
            distrito(<?=$id_provincia?>,null);
          <?php };?>

          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });
        })
        $(document).ready(function(){
          $("#filtro").submit(function() {
            $(".popup").fadeIn();   
          });
        });
      </script>
<style type="text/css">
    #rotador{
      position: absolute;
      right: 0px;
      float: right;
      /* margin: 13% 0% 0 0; */
      width: 100%;
      padding-left: 20px;
      background-color: rgba(0, 0, 0, 0.46);
      padding: 9px 5px;
      bottom: 0px;
      display: inline-block;
    }
      #rotador h1{
        text-align: left;
        color: #c95427;
        font-size: 32px;
        font-weight: 400;
        margin: 0 0 5px;
        padding: 0;
        text-shadow: 0 1px 0 rgba(255,255,255,.9);
      }
      #rotador p{
        color: #bfbfbf;
        font-size: 14px;
        line-height: 20px;
        margin: 0 0 10px;
        font-family: sans-serif;
      }
</style>
  <div id="rotador">
    <div style="float: left;">
      <h1>TRAZADORES EN AGUA Y SANEAMIENTO RURAL</h1>
      <p style="font-size: 16px;">Es una iniciativa apoyada por el Programa Global Iniciativas Agua de la Agencia Suiza para el Desarrollo y la Cooperación COSUDE</p>
      <p style="font-size: 12px;bottom: 0px;position: absolute;">Copyright © 2016 Trazadores</p>
    </div>
    <div style="float: right;">
      <p style="width: 250px;margin-right: 20px;"><img src="<?=$base_url?>img/cosude.png"></p>
    </div>
  </div>
  <script>
        $.backstretch([
		  "<?=$base_url?>img/home1.jpg",
          "<?=$base_url?>img/saba_4.jpg",
          "<?=$base_url?>img/saba_10.JPG"
        ], {
        	fade: 750,		//Speed of Fade
        	duration: 4000 	//Time of image display
        });
    </script>
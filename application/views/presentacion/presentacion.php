	<section>
		<div class="titulo">Presentación Indicadores Trazadores</div>
	</section>
	<section>
		<div class="presentacion">
			<div style="float: left;margin-bottom: 30px">
				<div style="float: left;width: 300px;"><img src="<?=$base_url?>img/trazador.jpg"></div>
				<div style="float: left;width: 513px;margin-left: 15px">
					<p>En el marco de las políticas públicas, instauradas por el Ministerio de Vivienda, Construcción y Saneamiento en la perspectiva de garantizar sostenibilidad, se viene levantando información a través de un diagnóstico situacional sobre el abastecimiento de agua y saneamiento y la gestión de los servicios en el ámbito rural, permitiendo de este modo tener un acercamiento a la realidad local de los servicios básicos.</p>
					<p>En este contexto, los indicadores trazadores son una herramienta de gestión muy versátil, de fácil aplicación, que en forma rápida y respondiendo al criterio básico de disponer de la información, sin requerir de mayor especialización del personal técnico responsable en la colecta de datos, permite valorar y calificar la operatividad, calidad y condiciones en las que se viene suministrando los servicios de agua y saneamiento rural.</p>
					
				</div>
			</div>
			<div style="float: left;">
				<p>Los trazadores son de mucha utilidad a nivel local, sobre todo para el personal responsable de las Áreas Técnicas Municipales y para los tomadores de decisiones en los Gobiernos Locales y Direcciones/Gerencias Regionales de Vivienda, Construcción y Saneamiento, a favor del seguimiento y fortalecimiento en la gestión de los servicios de agua y saneamiento a ese nivel.</p>
					<p>Cabe señalar, que esta herramienta de gestión, no se contrapone a la información recogida en el diagnóstico, más bien se constituye en un valioso complemento, potenciando el análisis y los resultados a obtener con reportes valiosos, facilitando la identificación de deficiencias en la gestión de los servicios de agua y saneamiento rural para su pronta solución.</p>
			</div>
			<div style="float: left;">
				<p>Los trazadores considerados como indicadores básicos se mencionan a continuación:</p>
				<UL type = circle style="list-style: inherit;width: 400px;margin-left: 50px;">
					<LI>Calidad del agua.
					<LI>Calidad del servicio básico.
					<LI>Estado en la infraestructura.
					<LI>Acciones de administración, operación y mantenimiento.
					<LI>Conductas sanitarias.
					<LI>Soporte institucional.
				</UL>
				<p></p>
				<p>Los criterios considerados para calificar cada uno de los trazadores son:</p>
				<div style="margin-top: 30px;">
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#agua" aria-controls="agua" role="tab" data-toggle="tab">Calidad del agua</a></li>
				    <li role="presentation"><a href="#servicio" aria-controls="servicio" role="tab" data-toggle="tab">Calidad del servicio</a></li>
				    <li role="presentation"><a href="#infraestructura" aria-controls="infraestructura" role="tab" data-toggle="tab">Estado de la infraestructura</a></li>
				    <li role="presentation"><a href="#aom" aria-controls="aom" role="tab" data-toggle="tab">AOM</a></li>
				    <li role="presentation"><a href="#sanitarias" aria-controls="sanitarias" role="tab" data-toggle="tab">Conductas sanitarias</a></li>
				    <li role="presentation"><a href="#soporte" aria-controls="soporte" role="tab" data-toggle="tab">Soporte institucional</a></li>
				    <li role="presentation"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">INDICE GENERAL</a></li>
				    <li role="presentation"><a href="#jass" aria-controls="jass" role="tab" data-toggle="tab">NIVEL DE GESTIÒN JASS</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="agua"><img src="<?=$base_url?>img/calidadagua.jpg"></div>
				    <div role="tabpanel" class="tab-pane" id="servicio"><img src="<?=$base_url?>img/calidadservicio.jpg"></div>
				    <div role="tabpanel" class="tab-pane" id="infraestructura">
				    	<img src="<?=$base_url?>img/estadoinfraestructura.jpg">
						<img src="<?=$base_url?>img/infraestructuraagua.jpg">
						<img src="<?=$base_url?>img/infraestructuraexcretas.jpg">
				    </div>
				    <div role="tabpanel" class="tab-pane" id="aom"><img src="<?=$base_url?>img/aom.jpg"></div>
				    <div role="tabpanel" class="tab-pane" id="sanitarias"><img src="<?=$base_url?>img/conductassanitarias.jpg"></div>
				    <div role="tabpanel" class="tab-pane" id="soporte"><img src="<?=$base_url?>img/soporteinstitucional.jpg"></div>
				    <div role="tabpanel" class="tab-pane" id="general"><img src="<?=$base_url?>img/indicegeneral.jpg"></div>
				    <div role="tabpanel" class="tab-pane" id="jass"><img src="<?=$base_url?>img/jass.jpg"></div>
				  </div>
				</div>
			</div>
		</div>
	</section>

<style type="text/css">
	#registro{
		margin: 0 auto;
		width: 50%;
	}
</style>
<h1>Formulario de registro</h1>

<div id="registro">
	<?=$error;?>
	<?=$succes;?>
	<form action="<?=$base_url?>index.php?c=presentacion&m=registro" method="POST">
	<div class="col12 ">
		<div class="portlet light ">
					<div class="form-group">
		                <label class="control-label fa fa-user">Usuario</label>
		                <input  type="text" name="usuario" placeholder="Ejemplo: Carlos" class="form-control" value="<?=$usuario?>"> 
		            </div>
		            <div class="form-group">
		                <label class="control-label fa fa-key">Contraseña</label>
		                <input type="password" name="pas" placeholder="Ejemplo: 123" class="form-control" value="<?=$pas?>"> 
		            </div>
		            <div class="form-group">
		                <label class="control-label fa fa-key">Repitir contraseña</label>
		                <input type="password" name="pas_re" placeholder="Ejemplo: 123" class="form-control" value="<?=$pas_re?>"> 
		            </div>
		            <div class="form-group">
		            	<div class="row">
		            		<div class="col4">
		                        <label class="control-label">Departamento</label>
		                        <select name='id_departamento'>
		                        </select> 
		                    </div>
		                    <div class="col4">
		                        <label class="control-label">Provincia</label>
		                        <select  name='id_provincia'>
		                        </select> 
		                    </div>
		                    <div class="col4">
		                        <label class="control-label">Distrito</label>
		                        <select name='id_distrito' value="">
		                        </select> 
		                    </div>
		            	</div>
	                </div>
				</div>
				<div class="portlet light">
	                <div class="form-group">
	                  	<label class="control-label ">Nombres</label>
	                       	<input type="text" name="nombres" placeholder="Ejemplo: Carlos jorge" class="form-control" value="<?=$nombres?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label">Apellidos Paterno</label>
	                       	<input type="text" name="apellido_p" placeholder="Ejemplo: palma" class="form-control" value="<?=$apellido_p?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label">Apellidos Materno</label>
	                       	<input type="text" name="apellido_m" placeholder="Ejemplo: bocanegra" class="form-control" value="<?=$apellido_m?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label fa fa-credit-card-alt">DNI</label>
	                       	<input type="text" name="dni" placeholder="Ejemplo: 46837941" class="form-control" value="<?=$dni?>"> 
	                </div><div class="form-group">
	                  	<label class="control-label fa fa-envelope">Correo</label>
	                       	<input type="text" name="correo" placeholder="Ejemplo: carlos.palma@yainso.com" class="form-control" value="<?=$correo?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label fa fa-building">Direccion</label>
	                       	<input type="text" name="direccion" placeholder="Ejemplo: jr.alfonso ugarte 1351 int 7 la perla-callao" class="form-control" value="<?=$direccion?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label fa fa-phone">Telefono</label>
	                       	<input type="text" name="telefono" placeholder="Ejemplo: 014294568" class="form-control" value="<?=$telefono?>"> 
	                </div>
	                <div class="form-group">
	                  	<label class="control-label fa fa-mobile">Celular</label>
	                       	<input type="text" name="celular" placeholder="Ejemplo: 978498879" class="form-control" value="<?=$celular?>"> 
	                </div>
				</div>
				<input type="submit" value="enviar" name="enviar"></input>
</div>
	
</form>
</div>
<script type="text/javascript">
	<?php if (isset($datos_usuario['id_departamento'])) { ?>
            departamento(<?=$datos_usuario['id_departamento']?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if (isset($datos_usuario['id_departamento'])!="" and isset($datos_usuario['id_provincia'])!="") { ?>
            provincia(<?=$datos_usuario['id_departamento']?>,<?=$datos_usuario['id_provincia']?>);
          <?php }elseif(isset($datos_usuario['id_departamento'])){ ?>
            provincia(<?=$datos_usuario['id_departamento']?>,null);
          <?php };?>

          <?php if (isset($datos_usuario['id_provincia']) and isset($datos_usuario['id_distrito'])) { ?>
            distrito(<?=$datos_usuario['id_provincia']?>,<?=$datos_usuario['id_distrito']?>);
          <?php }elseif(isset($datos_usuario['id_provincia'])){ ?>
            distrito(<?=$datos_usuario['id_provincia']?>,null);
          <?php };?>
</script>
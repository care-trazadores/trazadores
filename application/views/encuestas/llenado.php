<div id="llenado_encuesta">
<form method="post" action="<?=$base_url?>index.php?c=encuestas&m=guardar">
	<input type="hidden" name="id_llenado" value='<?=$datos_llenado["id_llenado"]?>'>
	<h1 class="page-title">LLenado de encuestas</h1>
	<div class="row">
		<div class="col12">
			<div class="portlet light bordered">
				<div class="datos_encuesta">
					<div class="row">
						<div class="col6">
							<span>Nombre del poblado/localidad:</span>
							<input class="form-control" id="comunidad" name="comunidad" type="text" value="<?=$datos_llenado["comunidad"]?>">
						</div>
						<div class="col6">
							<span>Evaluador:</span>
							<input type="hidden" name="id_usuario" value="<?=$datos_llenado["id_usuario"]?>">
							<span class="input form-control" ><?=$datos_llenado["usuario"]?></span>
						</div>
					</div>
					<div class="row">
						<div class="col6">
							<div class="row">
								<div class="col6">
									<span>Departamento:</span>
									<?php
									if ($datos_llenado["departamento"]=="") {
										echo '<select name="id_departamento"></select>';
									}else{
										echo '<span class="input form-control" >'.$datos_llenado["departamento"].'</span>';
									}
									?>
								</div>
								<div class="col6">
									<span>Provincia</span>
									<?php
									if ($datos_llenado["departamento"]=="") {
										echo '<select name="id_provincia"></select>';
									}else{
										echo '<span class="input form-control" >'.$datos_llenado["provincia"].'</span>';
									}
									?>
								</div>
							</div>
						</div>
						<div class="col6">
							<div class="row">
								<div class="col6">
									<span>Distrito</span>
									<span>Provincia</span>
									<?php
									if ($datos_llenado["departamento"]=="") {
										echo '<select name="id_distrito"></select>';
									}else{
										echo '<input type="hidden" name="id_distrito" value="'.$datos_llenado["id_distrito"].'">
											<span class="input form-control" >'.$datos_llenado["provincia"].'</span>';
									}
									?>
								</div>
								<div class="col6">
									<span>Fecha</span>
									<input type="hidden" name="fecha" value="<?=$datos_llenado["fecha"]?>">
									<span class="input form-control"><?=$datos_llenado["fecha"]?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col12">
			<div class="portlet light bordered">
				<div class="row">
					<div id="tabs">
						<?=$tabs?>
					</div>
					<?=$cuadros_preguntas?>
				</div>
			</div>
		</div>
	</div>
	<span id="enviar" class="btn boton">Enviar</span>
</form>	

</div>
<script type="text/javascript">
	function anexo_cuadro(id_cuadro){
		mostrartab(id_cuadro);
		$("#enviar").fadeOut();
		$("#tabs").fadeOut();
		$("#tab"+id_cuadro).fadeIn();
	}	
	function mostrartab(valor){

		$(".tabs").fadeOut();
		$("#tabs").fadeIn();
		$("#tab"+valor).fadeIn();
		$("#tabs li").removeClass("selected");
		$("#tabs .tabs"+valor).addClass("selected");
	}
	function verificacion(id_cuadro,tipo){
			$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":"inherit"})
			$("#tab"+id_cuadro+" .calificacion_final").html("")
			$("#tab"+id_cuadro+" .sumatoria").html("");
			$("#tab"+id_cuadro+" .promedio").html("");
			suma_final=""
			promedio_final=""
			calificacion_final=""

			cantidad=cantidad_preguntas(id_cuadro)
			sumatoria_vac=sumatoria_vacio(id_cuadro)

			if (tipo=="cuadro") {
				if (sumatoria_vac!=cantidad) {
					suma_final=sumatoria(id_cuadro)
					promedio_final=promedio(suma_final,cantidad)
					calificacion_final=calificacion(id_cuadro,promedio_final)
					$("#tab"+id_cuadro+" .sumatoria").html(suma_final);
					$("#tab"+id_cuadro+" .promedio").html(promedio_final);
					$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":calificacion_final["background"]})
					$("#tab"+id_cuadro+" .calificacion_final").html(calificacion_final["texto"])
					$(this).css({"border": "1px solid #B2D4F5"})
				};
			};
			if (tipo=="anexo1") {
				
					cantidad=totalsi(id_cuadro)
					suma_final=sumatoria(id_cuadro)
					promedio_final=promedio(suma_final,cantidad)
					calificacion_final=calificacion(id_cuadro,promedio_final)
					$("#tab"+id_cuadro+" .sumatoria").html(suma_final);
					$("#tab"+id_cuadro+" .promedio").html(promedio_final.toFixed(2));
					$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":calificacion_final["background"]})
					$("#tab"+id_cuadro+" .calificacion_final").html(calificacion_final["texto"])
					$(this).css({"border": "1px solid #B2D4F5"})
				
			};
			if (tipo=="anexo2") {
				if (sumatoria_vac!=cantidad) {
					//cantidad=totalsi(id_cuadro)
					suma_final=sumatoria(id_cuadro)
					promedio_final=suma_final
					calificacion_final=calificacion(id_cuadro,promedio_final)
					$("#tab"+id_cuadro+" .sumatoria").html(suma_final);
					$("#tab"+id_cuadro+" .promedio").html(promedio_final);
					$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":calificacion_final["background"]})
					$("#tab"+id_cuadro+" .calificacion_final").html(calificacion_final["texto"])
					$(this).css({"border": "1px solid #B2D4F5"})
				};
			};
			if (tipo=="jass") {
				if (sumatoria_vac!=cantidad) {
					//cantidad=totalsi(id_cuadro)
					suma_final=sumatoria(id_cuadro)
					promedio_final=suma_final
					calificacion_final=calificacion(id_cuadro,promedio_final)
					$("#tab"+id_cuadro+" .sumatoria").html(suma_final);
					$("#tab"+id_cuadro+" .promedio").html(promedio_final);
					$("#tab"+id_cuadro+" .calificacion_final").css({"background-color":calificacion_final["background"]})
					$("#tab"+id_cuadro+" .calificacion_final").html(calificacion_final["texto"])
					$(this).css({"border": "1px solid #B2D4F5"})
				};
			};
			ponderacion_copia(id_cuadro,promedio_final,calificacion_final)
		}
		function cantidad_preguntas(id_cuadro){
			var i=0
			$("#tab"+id_cuadro+" select").each(function(){
				i++;
			});
			return i;
		}
		function sumatoria_vacio(id_cuadro){
			var suma=0
			$("#tab"+id_cuadro+" select").each(function(){
				var valor=$(this).val();
				if (valor=="") {
					suma=suma+1;
				};
			});
			return suma
		}
		function sumatoria(id_cuadro){
			var suma=0;
			$("#tab"+id_cuadro+" select").each(function(){
				var valor=$(this).val();
				if (valor=="") {
					valor=0;
				};
				suma=suma+parseFloat(valor);
			});
			return suma
		}
		function promedio(sumatoria,cantidad){
			var promedio=sumatoria/cantidad
			console.log(promedio)
			return promedio
		}
		function calificacion(id_cuadro,promedio){
			var calificacion=[]
			$("#tab"+id_cuadro+" .calificacion .calificaciones").each(function(){
				var mayor=$(this).attr("mayor")
				var menor=$(this).attr("menor")
				var igual=$(this).attr("igual")
				var background=$(this).attr("background")
				var texto=$(this).attr("texto")
				if (igual!="") {
					if (igual==promedio) {
						calificacion["background"]=background
						calificacion["texto"]=texto
					}
				}else if (mayor!="" && menor!="") {
					if (promedio>=mayor && promedio<menor) {
						calificacion["background"]=background
						calificacion["texto"]=texto
					}
				}else if (mayor!="" && menor=="") {
					if (promedio>=mayor) {
						calificacion["background"]=background
						calificacion["texto"]=texto
					}
				}else if (menor!="" && mayor=="") {
					if (promedio<=menor) {
						calificacion["background"]=background
						calificacion["texto"]=texto
					}
				}
			})
			return calificacion
		}
		function ponderacion_copia(id_cuadro,promedio,calificacion_final){
			if ($(".aponderacion").hasClass('cuadro'+id_cuadro)){
				$(".cuadro_evaluado"+id_cuadro).html(promedio)
				var ponderacion=$(".aponderacion_valor"+id_cuadro).html();
				if (promedio!="" || promedio===0) {
					$(".aponderacion_valoracion_ponderada"+id_cuadro).html(promedio*ponderacion)
					$(".cuadro_calificacion"+id_cuadro).css({"background-color":calificacion_final["background"]})
					$(".cuadro_calificacion"+id_cuadro).html(calificacion_final["texto"])
				}else{
					$(".aponderacion_valoracion_ponderada"+id_cuadro).html("")
					$(".cuadro_calificacion"+id_cuadro).css({"background-color":"inherit"})
					$(".cuadro_calificacion"+id_cuadro).html("")
				};
				sumatoria_aponderacion()

				var promedio_final=promedio_aponderacion()
				calificacion_final=calificacion(11,promedio_final)
				$(".aponderacion_calificacion").html(calificacion_final["texto"])
				$(".aponderacion_calificacion").css({"background-color":calificacion_final["background"]})
				$(".aponderacion_global").css({"background-color":calificacion_final["background"]})
		    }
		}
		function sumatoria_aponderacion(){
			var suma=0
			$(".aponderacion").each(function(){
				id_cuadro=$(this).attr("id_cuadro")
				var valor=$(".cuadro_evaluado"+id_cuadro).html()
				if (valor=="") {
					valor=0;
				};
				suma=suma+parseFloat(valor)
			})
			$(".aponderacion_sumatoria").html(suma)
		}
		function promedio_aponderacion(){
			var total_valoracion=0
			$(".valoracion_ponderada").each(function(){
				var valor=$(this).html()
				
				if (valor=="") {
					valor=0;
				}
				total_valoracion=total_valoracion+parseFloat(valor)
			})
			var promedio=total_valoracion/parseFloat($(".aponderacion_valor_total").html())
			$(".aponderacion_global").html(promedio)
			return promedio
		}
		function mostrarocultar(valor,id_pregunta,id_cuadro,tipo){
			if (valor==0) {
				$(".filtro"+id_pregunta+" .respuestas").attr('disabled',false);
				$(".filtro"+id_pregunta+" .observacion").attr('disabled',false);
			}else if (valor==1) {
				$(".filtro"+id_pregunta+" .respuestas").prop('selectedIndex',0);
				$(".filtro"+id_pregunta+" .respuestas").attr('disabled',true);
				$(".filtro"+id_pregunta+" .observacion").attr('disabled',true);
				$(".filtro"+id_pregunta+" .respuestas").css({"border": "1px solid #B2D4F5"})
				$(".respuesta"+id_pregunta).html("")
				verificacion(id_cuadro,tipo)
			};
		}
		function totalsi(id_cuadro){
			var totalsi=0
			var totalno=0
			$("#tab"+id_cuadro+" .filtros").each(function(){
				if ($(this).is(':checked')) {
					var valor=$(this).val()
					if (valor==0) {
						totalsi++
					}else if (valor==1) {
						totalno++
					};
				};
				$("#tab"+id_cuadro+" .total_si").html(totalsi)
				$("#tab"+id_cuadro+" .total_no").html(totalno)
			})
			return totalsi
		}
		function jass(id_cuadro,texto){
			//alert(id_cuadro+"-"+texto)
			$("#tab"+id_cuadro+" select option").each(function(){
				if ($(this).attr("texto")==texto) {
					$(this).attr('disabled',false);
					$(this).prop("selected", true )
					verificacion(id_cuadro,"jass")
				}
			})
			$("#tab"+id_cuadro+" select option:not(:selected)").attr('disabled',true);
		}
	$(document).ready(function(){
		$("#tab12 select option:not(:selected)").attr('disabled',true);
		$(".tabs").fadeOut();
		mostrartab(1);
		$("#tabs li").click(function(){
			var posicion=$(this).attr("key");
			mostrartab(posicion);
		})
		$(".respuestas").change(function(){
			var id_cuadro=$(this).attr("id_cuadro");
			var tipo=$(this).attr("tipo");
			verificacion(id_cuadro,tipo)
			var id=$(this).attr("id")

			var valor
			if ($(this).val()==0) {
				valor="ADECUADO"
			}else if ($(this).val()==1) {
				valor="LIMITADO"
			}else if ($(this).val()==2) {
				valor="DETERIORADO"
			};
			$("#tab"+id_cuadro+" ."+id).html(valor);

			jass(12,$("#"+id+" option:selected").attr("texto"))
			$(this).css({"border": "1px solid #B2D4F5"})
		})
		$("#enviar").click(function(){
			var error=0
			$("select").each(function(){
				var valor=$(this).val()
				var tipo=$(this).attr("tipo")
				if (tipo=="cuadro" || tipo=="jass") {
					if (valor=="") {
						$(this).css({"border":"1px solid #F93A3A"})
						error++
					};
				};
			})
			if ($("input[name='respuestas[42]']").val().length<7) {
				error++
				$("input[name='respuestas[42]']").css({"border":"1px solid #F93A3A"})
			}
			if ($("input[name='respuestas[43]']").val().length<6) {
				error++
				$("input[name='respuestas[43]']").css({"border":"1px solid #F93A3A"})
			}
			if ($("#respuesta45").val()=="") {
				error++
				$("#respuesta45").css({"border":"1px solid #F93A3A"})
			}
			if ($("#respuesta46").val()=="") {
				error++
				$("#respuesta46").css({"border":"1px solid #F93A3A"})
			}
			if ($("#zona").val()=="") {
				error++
				$("#zona").css({"border":"1px solid #F93A3A"})
			}
			if ($("#arrastre").val()=="") {
				error++
				$("#arrastre").css({"border":"1px solid #F93A3A"})
			}
			if ($("#comunidad").val()=="") {
				alertify.error("la comunidad no puede ser vacio");
				error++
			};
			if (error!=0) {
				alertify.error("debe llenar los campos que están con bordes rojos");
			}else{
				$("#llenado_encuesta form").submit()
			};
		})
		$(".filtros").each(function(){
			if ($(this).is(':checked')) {
				var valor=$(this).val()
				var id_pregunta=$(this).attr("pregunta")
				mostrarocultar(valor,id_pregunta)
			};
		})
		$(".filtros").change(function(){
			var valor=$(this).val()
			var id_pregunta=$(this).attr("pregunta")
			var id_cuadro=$(this).attr("cuadro")
			var tipo=$(this).attr("tipo")
			mostrarocultar(valor,id_pregunta,id_cuadro,tipo)
			totalsi(id_cuadro)
		})
		
	})
	function verificar(retorno,id_pregunta,id_cuadro){
		var error=0
		var llenado=0;
		$("#tab"+id_cuadro+" .filtros").each(function(){
			if ($(this).is(':checked')) {
				var valor=$(this).val()
				var id_pregunta=$(this).attr("pregunta")
				if (valor==0) {
					var respuesta=$(".filtro"+id_pregunta+" .respuestas").val();
					if (respuesta=="") {
						error++
						$(".filtro"+id_pregunta+" .respuestas").css({"border":"1px solid #F93A3A"})
					}else{
						llenado++
					};
				}
			};
		})
		if (llenado!=0) {
			if (error==0) {
				mostrartab(retorno)
				var cali=$("#tab"+id_cuadro+" .calificacion_final").html()
				if (cali!="") {
					$("#respuesta"+id_pregunta+" option").each(function(){
						$(this).prop( "selected", false );
						if (cali==$(this).attr("texto")) {
							$("#respuesta"+id_pregunta+">option[value='"+$(this).attr("value")+"']").prop( "selected", true );
						};
					})
				}else{
					$("#respuesta"+id_pregunta+">option[value='']").attr("selected","selected")
				};
				verificacion(retorno,"cuadro")
				$("#enviar").fadeIn();
			};
		}else{
			if (error!=0) {
				alertify.error("Debe responder las preguntas marcadas en si");
			}else{
				alertify.error("Al menos debe responder una pregunta");
			}
		}
		
	}
	function verificar1(retorno,id_pregunta,id_cuadro){
		var error=0;
		$("#tab"+id_cuadro+" select").each(function(){
			var valor=$(this).val()
			var tipo=$(this).attr("tipo")
			if (valor=="") {
				$(this).css({"border":"1px solid #F93A3A"})
				error++
			};
		})
		if (error==0) {
			mostrartab(retorno)
			var cali=$("#tab"+id_cuadro+" .calificacion_final").html()
			if (cali!="") {
				$("#respuesta"+id_pregunta+" option").each(function(){
					$(this).prop( "selected", false );
					if (cali==$(this).attr("texto")) {
						$("#respuesta"+id_pregunta+">option[value='"+$(this).attr("value")+"']").prop( "selected", true );
					};
				})
			}else{
				$("#respuesta"+id_pregunta+">option[value='']").attr("selected","selected")
			};
			verificacion(retorno,"cuadro")
			$("#enviar").fadeIn();
		}else{
			alertify.error("Todas las preguntas deben ser calificadas");
		};
	}
	function qp(personas,lt){
		var qp=(personas*lt)/86400
		return qp
	}
	function zona_geografica(){
		var zona=$("#zona").val()
		var arrastre=$("#arrastre").val()

		var personas=$("#respuesta45").val()
		var caudal=$("#respuesta46").val()

		if (personas!="" && caudal!="") {
			if (zona!="" && arrastre!="") {
				if (zona==0 && arrastre==0) {
					$("#lthab").val(90)
				};
				if (zona==0 && arrastre==1) {
					$("#lthab").val(60)
				};
				if (zona==1 && arrastre==0) {
					$("#lthab").val(80)
				};
				if (zona==1 && arrastre==1) {
					$("#lthab").val(50)
				};
				if (zona==2 && arrastre==0) {
					$("#lthab").val(100)
				};
				if (zona==2 && arrastre==1) {
					$("#lthab").val(70)
				};

				
				var total=qp(personas,$("#lthab").val())
				if (caudal>=total) {
					var texto="Oferta igual o mayor a demanda (Qp)"
				}else if(caudal>=(total*0.85) && caudal<=(total*0.99)){
					var texto="Oferta rinde hasta el 85% de la demanda"
				}else if(caudal<(total*0.85)){
					var texto="Oferta es menor al 85% de la demanda"
				}
				jass(12,texto)
				console.log(texto)
			}else{
				$("#lthab").val("")
			};
		}
	}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#respuesta45").change(function(){
			var valor=$(this).val()
			if (valor=="") {
				$("#zona option").eq(0).prop('selected',true);
				$("#arrastre option").eq(0).prop('selected',true);
				$("#lthab").val("")
			}else{
				zona_geografica()
			}
		})

		$("#respuesta46").change(function(){
			var valor=$(this).val()
			if (valor=="") {
				$("#zona option").eq(0).prop('selected',true);
				$("#arrastre option").eq(0).prop('selected',true);
				$("#lthab").val("")
			}else{
				zona_geografica()
			}
		})
		$("#zona").change(function(){
			var personas=$("#respuesta45").val()
			var caudal=$("#respuesta46").val()
			if (personas!="" && caudal!="") {
				zona_geografica()
			}else{
				$("#zona option").eq(0).prop('selected',true);
				if (personas=="") {
					alertify.error("Debe llenar el numero de personas");
				}
				if (caudal=="") {
					alertify.error("Debe llenar el caudal de producción");
				}
			}
		})
		$("#arrastre").change(function(){
			var personas=$("#respuesta45").val()
			var caudal=$("#respuesta46").val()

			if (personas!="" && caudal!="") {
				zona_geografica()
			}else{
				$("#arrastre option").eq(0).prop('selected',true);
				if (personas=="") {
					alertify.error("Debe llenar el numero de personas");
				}
				if (caudal=="") {
					alertify.error("Debe llenar el caudal de producción");
				}
			}
		})
		$(".chec").click(function(){
			var id_tabla=$(this).attr("tabla")
			if ($(this).is(':checked')) {
				var total=2;
				$("#checkbox"+id_tabla+" .chec").each(function(){
					if ($(this).is(':checked')) {
						if ($(this).attr("texto")=="Mixto (dos o mas opciones)") {
							total++;				
						}else{
							total--;	
						}
					}
				})
				if (total<1) {
					$(this).prop("checked", false)
				}else{
					$(this).prop("checked", true)
				}
			}else{
				if ($(this).attr("texto")=="Mixto (dos o mas opciones)") {
					$("#checkbox"+id_tabla+" .chec").each(function(){
						$(this).prop("checked", false)
					})
				}
			}
		})
	})
</script>






<?php
	if (isset($footer_js)) {
		foreach ($footer_js as $js) {
	    echo '<script src="'.$base_url.'js/'.$js.'"></script>';
	  }
	}
?>
<script type="text/javascript">
$(document).ready(function(){
  <?php if (isset($datos_llenado["id_departamento"])) { ?>
            departamento(<?=$datos_llenado["id_departamento"]?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if (isset($datos_llenado["id_departamento"]) and isset($datos_llenado["id_provincia"]) and $datos_llenado["id_departamento"]!="" and $datos_llenado["id_provincia"]!="") { ?>
            provincia(<?=$datos_llenado["id_departamento"]?>,<?=$datos_llenado["id_provincia"]?>);
          <?php }elseif(isset($datos_llenado["id_departamento"]) and $datos_llenado["id_departamento"]!=""){ ?>
            provincia(<?=$datos_llenado["id_departamento"]?>,null);
          <?php };?>
          <?php if (isset($datos_llenado["id_provincia"]) and isset($datos_llenado["id_distrito"]) and $datos_llenado["id_provincia"]!="" and $datos_llenado["id_distrito"]!="") { ?>
            distrito(<?=$datos_llenado["id_provincia"]?>,<?=$datos_llenado["id_distrito"]?>);
          <?php }elseif(isset($datos_llenado["id_provincia"]) and $datos_llenado["id_provincia"]!=""){ ?>
            distrito(<?=$datos_llenado["id_provincia"]?>,null);
          <?php };?>
})  
</script>

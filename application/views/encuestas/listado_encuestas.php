<h3 class="page-title">Encuestas <small>Listado</small></h3>
      <section class="row"style="text-align: center;margin-left: 150px">
        <?=$cuadros;?>
      </section>
      <section class="row">
        <div class="col12">
          <a href="<?=$base_url?>index.php?c=encuestas&m=llenado" class="btn boton">
            <span class="fa fa-plus"></span> Nueva encuesta</a></div>
      </section>
      <section class="row">
        <div id="listado">
          <div id="list-cab">
            <i class="fa fa-list-alt"></i><span><?php echo $titulo; ?></span>
          </div>
          <div id="filtros">
              <form method="post" action="<?php echo base_url()?>index.php?c=encuestas">
            <?php
              if ($tipo_usuario!="usuario") {
            ?>
            <div>
              <span>Id</span>
              <input type="text" name="busq_id_llenado" value="<?=$busq_id_llenado;?>">
            </div>
            <div>
              <span>Usuario</span>
              <input type="text" name="busq_usuario" value="<?=$busq_usuario;?>">
            </div>
            
            <div>
              <span>Departamento</span>
              <select name="id_departamento">
              </select>
            </div>
            <div>
              <span>Provincia</span>
              <select name="id_provincia">
              </select>
            </div>
            <div>
              <span>Distrito</span>
              <select name="id_distrito">
              </select>
            </div>
            <?php
              }
            ?>
            <div>
              <span>Localidad</span>
              <input type="text" name="busq_localidad" value="<?=$busq_localidad;?>">
            </div>
            <div>
              <span>cantidad</span>
              <select name="limite">
                <option <?php if ($limite=="5") {echo "selected";}?> value="5">5</option>
                <option <?php if ($limite=="10") {echo "selected";}?> value="10">10</option>
                <option <?php if ($limite=="50") {echo "selected";}?> value="50">50</option>
              </select>
            </div>
            <div><input type="submit" value="buscar" name="buscar"></div>
            </form>
          </div>
          <form id="campos">
            <div id="list-cuerpo">
              <table class="listado">
                <tr>
                  <th></th>
                  <th>ID</th>
                  <th>Usuario</th>
                  <!--<th>Correo</th>-->
                  <th>Localidad</th>
                  <th>Distrito</th>
                  <!--<th>Provincia</th>-->
                  <!--<th>Ver</th>-->
                  <th>Editar</th>
                  <th>Exportar pdf</th>
                  <th>Exportar excel</th>
                  <th>Eliminar</th>
                </tr>
                <?php
                  foreach ($listado as $key) {
                ?>
                <tr>
                  <td><input type="checkbox" name="id_llenado[]" value="<?=$key->id_llenado?>"/></td>
                  <td><?=$key->id_llenado?></td>
                  <?php
                  if ($key->user=="") {
                  ?>
                    <td>Usuario eliminado</td>
                  <?php
                  }else{
                  ?>
                    <td><?=$key->user;?></td>
                  <?php
                  }
                  ?>
                  
                  <!--<td><?=$key->correo;?></td>-->
                  <td><?=$key->comunidad;?></td>
                  <td><?=$key->distrito;?></td>
                  <!--<td><?=$key->provincia;?></td>-->
                   <!--<td><a href="<?=$base_url?>index.php?c=encuestas&m=ver&id=<?=$key->id_llenado?>"><i class="fa fa-book"></a></i></td>-->
                  <td><a href="<?=$base_url?>index.php?c=encuestas&m=edit&id=<?=$key->id_llenado?>"><i class="fa fa-pencil-square-o"></a></i></td>
                  <td><a href="<?=$base_url?>index.php?c=exportar&m=encuesta&id_llenado=<?=$key->id_llenado?>&archivo=pdf" target="_blank"><i class="fa fa-file-pdf-o"></i></a></i></td>
                  <td><a href="<?=$base_url?>index.php?c=exportar&m=encuesta&id_llenado=<?=$key->id_llenado?>&archivo=excel" target="_blank"><i class="fa fa-file-excel-o"></i></a></i></td>
                  <td><a href="<?=$base_url?>index.php?c=encuestas&m=delete&id=<?=$key->id_llenado?>"><i class="fa fa-trash-o"></i></a></i></td>
                </tr>
                <?php
                  }
                ?>
              </table>
              <div>
                <span class="btn boton" id="eliminar_grupo" style="cursor:pointer;">Eliminar grupo</span>
              </div>
              <div id="list-pie">
                <?=$paginador?>
              </div>  
            </div>
          </form>
        </div>
      </section>
      <script type="text/javascript">
        $(document).ready(function(){
          function eliminar_encuestas(){
            var data=$("#campos").serialize();
            $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=api&m=delete_encuesta",
              data: data
            }).done(function( data ) {
              console.log(data);
              if (data.type=="success") {
                alert(data.message);
                window.location.reload();
              }else{
                alert(data.message)
              }
              
            });
          }

          function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
          }
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: "<?=$base_url?>index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
          <?php if (isset($id_departamento)) { ?>
            departamento(<?=$id_departamento?>);
          <?php }else{ ?>
            departamento();
          <?php };?>

          <?php if (isset($id_departamento) and isset($id_provincia)) { ?>
            provincia(<?=$id_departamento?>,<?=$id_provincia?>);
          <?php }elseif(isset($id_departamento)){ ?>
            provincia(<?=$id_departamento?>,null);
          <?php };?>
          <?php if (isset($id_provincia) and isset($id_distrito)) { ?>
            distrito(<?=$id_provincia?>,<?=$id_distrito?>);
          <?php }elseif(isset($id_provincia)){ ?>
            distrito(<?=$id_provincia?>,null);
          <?php };?>

          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });

          $("#eliminar_grupo").click(function(){
            eliminar_encuestas();
          })
        })
      </script>
<script type="text/javascript">
    var distrito;
    function  EditProvincia(id_provincia) {
        $.ajax({
            url: "<?= $base_url ?>index.php?c=provincia&m=edit_provincia",
            data: {id_provincia: id_provincia},
            type: 'post',
            dataType: 'json',
            success: function (r) {

                if (r.estado) {
                    var p = r.data;
                    $('#txtId_provincia').val(r.idProv);
                    $('#txtProvincia').val(r.provincia);
                    $('#id_departamento').val(r.idDepa);


                } else {
                    alert("Sin datos--");
                    mensaje('success', r.mensaje);
                }
            }

        });
    }
    function DeleteProvincia($this, id_provincia) {
        var $tr = $this.closest('tr');

        if (!confirm('Desea Eliminar el Registro ')) {
            return false;
        }

        $.ajax({
            url: '<?= $base_url ?>index.php?c=provincia&m=delete_provincia',
            data: {id_provincia: id_provincia},
            type: 'post',
            dataType: 'json',
            success: function (r) {
                $tr.hide();
                mensaje('success', r.mensaje);




            }

        });
    }
    
    function mensaje(msn, a) {
        var titulo, html, clase;
        switch (a) {
            case 1:
                titulo = 'OK.!';
                clase = 'msn_success';
                break;
            case 2:
                titulo = 'ERROR.!';
                clase = 'msn_error';
                break;
            case 3:
                titulo = 'ALERTA.!';
                clase = 'msn_warn';
                break;
            case 4:
                titulo = 'INFO.!';
                clase = 'msn_info';
                break;
            default:
                titulo = 'AVISO.!';
                clase = 'msn_warn';
                break;
        }
        html = "<b>" + titulo + "</b><br><label>" + msn + "</label>";
        $("#div_msn").attr('class', '').addClass(clase);
        $("#div_msn").html(html).fadeIn('fast').delay(3000).fadeOut('slow');
    }
</script>
<section class="row">
    <div id="listado">
        <div id="list-cab">
            <i class="fa fa-list-alt"></i><span><?php echo $titulo; ?></span>
        </div>
        <div id="filtros2">
            <form method="post" action="<?php echo base_url() ?>index.php?c=provincia&m=insertar_provincia">
                <div class="cod" style="display: none">
                    <span>Id Provincia</span>
                    <input type="text" name="txtId_provincia" id="txtId_provincia" value="<?php
                    foreach ($maxprovincia as $p) {
                        echo $p->max + 1;
                    }
                    ?>" >
                </div>


                <div>
                    <span>Region</span>
                    <select name="id_departamento" id="id_departamento" required="required">

                    </select>
                </div>
                <div>
                    <span>Provincia</span>
                    <input type="text" name="txtProvincia" id="txtProvincia" required="required" placeholder="Edit Provincia" value="">
                </div>

                <div><input type="submit" value="Guardar" name="Guardar"></div>
            </form>
        </div>
        <div id="list-cuerpo" collapsible="true">
            <div id="filtros2">
                <h3 style="background-color: #7fd17f;color: #ffffff;padding: 5px 20px;margin: 0 10px;text-align: center;font-size: 18px">Buscar Provincia</h3>
                <form method="post" action="<?php echo base_url() ?>index.php?c=provincia">

                    <div>
                        <span>Region</span>
                        <select name="id_departamento" id="id_departamento">
                            <option></option>

                        </select>
                    </div>
                    <div>
                        <span>Provincia</span>
                        <input type="text" name="provincia" placeholder="Buscar Provincia"value="<?php echo $prov ?>">
                    </div>
                    <div><input type="submit" value="Buscar Provincia" name="buscar"></div>
                </form>
            </div>
            <div>
                <table class="listado" cellspacing="1">
                    <tr>
                        <th>Item</th>
                        <th>Departamento</th>
                        <th>Provoncia</th>                    
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                    <?php
                    $i = 1;
                    foreach ($listadoprov as $key) {
                        ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?= $key->departamento; ?></td>
                            <td><?= $key->provincia; ?></td>                        
                            <td><a onclick="EditProvincia('<?php echo $key->idProv; ?>')"><i class="fa fa-pencil-square-o"></a></i></td>
                            <td><a onclick="DeleteProvincia($(this), '<?php echo $key->idProv; ?>')"><i class="fa fa-trash-o"></i></a></i></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </table>
            </div>
            <div id="list-pie">
                <?php echo $paginador;?>
            </div>

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        function departamento(id_departamento) {
            $.ajax({
                type: "POST",
                url: "<?= $base_url ?>index.php?c=distritos&m=departamento",
                data: {id_departamento: id_departamento}
            }).done(function (data) {
                $("select[id=id_departamentos]").html(data);
                $("select[name=id_departamento]").html(data);
            });
        }
   
        function provincia(id_departamento, id_provincia) {
            $.ajax({
                type: "POST",
                url: "<?= $base_url ?>index.php?c=distritos&m=provincia",
                data: {id_departamento: id_departamento, id_provincia: id_provincia}
            }).done(function (data) {
                $("select[name=id_provincia]").html(data);
            });
        }
//        function provincias(id_departamentos, id_provincias) {
//            $.ajax({
//                type: "POST",
//                url: "<?= $base_url ?>index.php?c=distritos&m=provincia",
//                data: {id_departamento: id_departamentos, id_provincia: id_provincias}
//            }).done(function (data) {
//                $("select[id=id_provincias]").html(data);
//            });
//        }
        function distrito(id_provincia, id_distrito) {
            $.ajax({
                type: "POST",
                url: "<?= $base_url ?>index.php?c=distritos&m=distrito",
                data: {id_provincia: id_provincia, id_distrito: id_distrito}
            }).done(function (data) {
                $("select[name=id_distrito]").html(data);
            });
        }
<?php if (isset($id_departamento)) { ?>
            departamento(<?= $id_departamento ?>);
<?php } else { ?>
            departamento();
<?php }; ?>


<?php if (isset($id_departamento) and isset($id_provincia)) { ?>
            provincia(<?= $id_departamento ?>,<?= $id_provincia ?>);
<?php } elseif (isset($id_departamento)) { ?>
            provincia(<?= $id_departamento ?>, null);
<?php }; ?>
<?php if (isset($id_provincia) and isset($id_distrito)) { ?>
            distrito(<?= $id_provincia ?>,<?= $id_distrito ?>);
<?php } elseif (isset($id_provincia)) { ?>
            distrito(<?= $id_provincia ?>, null);
<?php }; ?>

        $("select[name=id_departamento]").change(function () {
            var departamento = $(this).val();
            provincia(departamento, null);
        });
        $("select[name=id_departamentos]").change(function () {
            var departamento = $(this).val();
            provincias(departamento, null);
        });
       
        $("select[name=id_provincia]").change(function () {
            var provincia = $(this).val();
            distrito(provincia, null);
        });

    });
    function depa() {
        $(document).ready(function () {
            // Así accedemos al Valor de la opción seleccionada
            var id_departamentos = $("#id_departamentos").val();
//    alert(id_departamentos);
            $.ajax({
                type: "POST",
                url: "<?= $base_url ?>index.php?c=distritos&m=provincia",
                data: {id_departamento: id_departamentos, id_provincia: ""}
            }).done(function (data) {

                $("select[id=id_provincias]").html(data);
                $('#id_provincias').val(distrito);
            });

            // Si seleccionamos la opción "Texto 1"
            // nos mostrará por pantalla "1"
        });


    }
</script>
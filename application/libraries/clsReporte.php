<?php 
class Reporte extends FPDF
{
    public function __construct($orientation='L', $unit='mm', $format='A4'){
        parent::__construct($orientation, $unit, $format);
    }
    
    public function gaficoPDF($datos = array(),$nombreGrafico = NULL,$ubicacionTamamo = array(),$titulo = NULL,$a){ 
        //construccion de los arrays de los ejes x e y
        if(!is_array($datos) || !is_array($ubicacionTamamo)){
            echo "los datos del grafico y la ubicacion deben de ser arreglos";
        }
        elseif($nombreGrafico == NULL){
            echo "debe indicar el nombre del grafico a crear";
        }
        else{ 
           #obtenemos los datos del grafico  
           foreach ($datos as $key => $value){
            $data[] = $value[0];
            $nombres[] = $key; 
            $color[] = $value[1];
           } 
           $x = $ubicacionTamamo[0];
           $y = $ubicacionTamamo[1]; 
           $ancho = $ubicacionTamamo[2];  
           $altura = $ubicacionTamamo[3];  
           #Creamos un grafico vacio
           $graph = new PieGraph(800,500);
           #indicamos titulo del grafico si lo indicamos como parametro
           if(!empty($titulo)){
            $graph->title->Set($titulo);
            $graph->title->SetFont(FF_FONT1,FS_BOLD,18);
            $graph->title->SetColor('black');
            $graph->SetBox(true);
           }   
           //Creamos el plot de tipo tarta
           $p1 = new PiePlot($data);
           $graph->Add($p1);
           $p1->ShowBorder();
           $p1->SetColor('black');
           $p1->SetSliceColors($color);
           $p1->SetSize(0.3);
           
           #indicamos la leyenda para cada porcion de la tarta
           $p1->SetLegends($nombres);
           
           //Añadirmos el plot al grafico
           
           $a=$a[0];
           //mostramos el grafico en pantalla
           $graph->Stroke("$nombreGrafico".$a.".png"); 
           $this->Image("$nombreGrafico".$a.".png",$x,$y,$ancho,$altura); 
  } 
   unlink($nombreGrafico.$a.".png");
 } 
}

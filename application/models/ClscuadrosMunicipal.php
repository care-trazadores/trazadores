<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clscuadrosMunicipal extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function cuadros(){
        $this->db->order_by("posicion", "ASC"); 
        $query = $this->db->get('cuadrosMunicipal');
        return $query->result();
    }
    function insertar($data){
    	$this->db->insert("cuadrosMunicipal",$data);
        $id = $this->db->insert_id();
        return $id;

    }
    function ultima_posicion(){
        $this->db->select("max(posicion) as posicion");
        $query = $this->db->get("cuadrosMunicipal");
        $ret = $query->row();
        return $ret->posicion;
    }

    function buscar_id($id_llenado){
    	$this->db->select("id_cuadro");
        $this->db->where("id_llenado",$id_llenado);
    	$query = $this->db->get("cuadrosMunicipal");
    	$ret = $query->row();
    	return $ret->id_cuadro;
    }
}
?>
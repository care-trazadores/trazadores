<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsjassalternativa extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function jassalternativa() {
    	$this->db->join('alternativas', 'jass_alternativa.id_alternativa = alternativas.id_alternativas', 'INNER');
        $query = $this->db->get('jass_alternativa');
        return $query->result();
    }
}

?>
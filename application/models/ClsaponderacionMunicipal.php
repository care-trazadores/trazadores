<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clsaponderacionMunicipal extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function aponderacion() {
        $this->db->join('cuadrosMunicipal', 'cuadrosMunicipal.id_cuadro=aponderacionMunicipal.id_cuadro', 'INNER');
        $query=$this->db->get('aponderacionMunicipal');
        return $query->result();
    }

    function insertar($data){
    	$this->insert("aponderacionMunicipal",$data);
    }

    function ultima_posicion(){
    	$this->db-select("max(posicion) as posicion");
    	$query = $this->db->get("aponderacionMunicipal");
    	$ret = $query->row();

    	return $ret->posicion;

    }
}

?>
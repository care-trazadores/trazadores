<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsencuestasMunicipal extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function listado_total($estado = null, $limit = null, $comienzo = null, $usuario = null, 
            $id_departamento = null, $id_provincia = null, $id_distrito = null,$id_usuario,$tipo=null,
            $grupo_departamento=null) {
        $this->db->select('*');
        $this->db->from('llenado');
        $this->db->join('usuario', 'llenado.id_usuario = usuario.id', 'INNER');
        $this->db->join('detalle_usuario', 'usuario.id = detalle_usuario.id_usuario', 'INNER');
        $this->db->join('ubdistrito', 'llenado.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        $this->db->where('usuario.tipo', 'usuario');
        if ($tipo=="usuario" && $id_usuario!="") {
            $this->db->where('llenado.id_usuario',$id_usuario);
        }

        if ($grupo_departamento!="") {
            $i=0;
            if ($id_departamento!="" || $id_provincia!="" || $id_distrito!="") {
                if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
                if ($id_provincia!="") {$this->db->where('ubprovincia.idProv', $id_provincia);}
                if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
                if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
            }else{
               foreach ($grupo_departamento as $key) {
                    if ($i==0) {
                        $this->db->where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }
                    else{
                        $this->db->or_where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }
                    $i++;
                } 
            }
            
        }else{
            if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
            if ($id_provincia!="") {$this->db->where('ubprovincia.idProv', $id_provincia);}
            if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
            if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
            if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
        }
        
        $this->db->limit($limit, $comienzo);
//        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    function total_encuestas($estado = null, $limit = null, $comienzo = null, $usuario = null, 
            $id_departamento = null, $id_provincia = null, $id_distrito = null,$id_usuario=null,$tipo=null,$grupo_departamento=null) {
        $this->db->select('*');
        $this->db->from('llenado');
        $this->db->join('usuario', 'llenado.id_usuario = usuario.id', 'INNER');
        $this->db->join('detalle_usuario', 'usuario.id = detalle_usuario.id_usuario', 'INNER');
        $this->db->join('ubdistrito', 'llenado.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        $this->db->where('usuario.tipo', 'usuario');
        if ($tipo=="usuario" && $id_usuario!="") {
            $this->db->where('llenado.id_usuario',$id_usuario);
        }
        if ($grupo_departamento!="") {
            $i=0;
            if ($id_departamento!="" || $id_provincia!="" || $id_distrito!="") {
                if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
                if ($id_provincia!="") {$this->db->where('ubprovincia.idProv', $id_provincia);}
                if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
                if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
            }else{
               foreach ($grupo_departamento as $key) {
                    if ($i==0) {
                        $this->db->where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }
                    else{
                        $this->db->or_where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }
                    $i++;
                } 
            }
            
        }else{
            if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
            if ($id_provincia!="") {$this->db->where('ubprovincia.idProv', $id_provincia);}
            if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
            if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
            if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
        }
        $query = $this->db->count_all_results();
        return $query;
    }
    function count_encuestas($tipo=null,$grupo_departamento=null,$id_usuario=null){
        $this->db->select('*');
        $this->db->from('llenado');
        $this->db->join('ubdistrito', 'llenado.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        if ($tipo=="usuario" && $id_usuario!="") {$this->db->where('llenado.id_usuario',$id_usuario);}
        if ($grupo_departamento!="") {
            $i=0;
            foreach ($grupo_departamento as $key) {
                if ($i==0) {$this->db->where('ubdepartamento.idDepa',$key);}
                else{$this->db->or_where('ubdepartamento.idDepa',$key);}
                $i++;
            }
        }
        $query = $this->db->count_all_results();
        return $query;
    }

    //encuestas


    function insertar($data){
        $this->db->insert("llenadoMunicipal",$data);
        $id = $this->db->insert_id();
        return $id;
    }

    function verificar_encuesta($id_distrito){
        $this->db->where("id_distrito",$id_distrito);
        $query = $this->db->count_all_results("llenadoMunicipal");
        return $query;

    }




}

?>
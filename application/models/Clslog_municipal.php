<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clslog_municipal extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function log($limite=null,$comienzo=null,$usuario=null,$id_departamento=null,
                        $id_provincia=null,$id_distrito=null,$localidad=null,$accion=null) {
    	
    	$this->db->join('ubdistrito', 'log_municipal.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');

        if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
        if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
       	if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
        if ($usuario!="") {$this->db->like('log_municipal.usuario', $usuario);}
        if ($localidad!="") {$this->db->like('log_municipal.localidad', $localidad);}
        if ($accion!="") {$this->db->like('log_municipal.accion', $accion);}
        
        if ($limite!="") {
            $this->db->limit($limite,$comienzo);
        }

        $query = $this->db->get('log_municipal');
        return $query->result();
    }
    function log_total($usuario=null,$id_departamento=null,
                        $id_provincia=null,$id_distrito=null,$localidad=null,$accion=null) {

    	$this->db->join('ubdistrito', 'log_municipal.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        
    	if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
        if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
       	if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
        if ($usuario!="") {$this->db->like('log_municipal.usuario', $usuario);}
        if ($localidad!="") {$this->db->like('log_municipal.localidad', $localidad);}
        if ($accion!="") {$this->db->like('log_municipal.accion', $accion);}
    	
        $query=$this->db->count_all_results('log_municipal');
 		return $query;
    }
    function insertar($data){
    	$this->db->insert('log_municipal', $data);
    	$id=$this->db->insert_id();
        return $id;
    }
}

?>
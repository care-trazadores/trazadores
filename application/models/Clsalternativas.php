<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clsalternativas extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function alternativas(){
        $query = $this->db->get('alternativas');
        return $query->result();
    }
    function alternativa_idpregunta($id_pregunta,$valor){
    	$this->db->where('id_pregunta', $id_pregunta);
    	$this->db->where('valor', $valor);
        $query = $this->db->get('alternativas');
        return $query->result();
    }
}

?>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsdepartamentos extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function departamentos() {
        $query = $this->db->get('ubdepartamento');
        return $query->result();
    }
}

?>
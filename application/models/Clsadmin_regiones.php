<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsadmin_regiones extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function departamento_admi($id_usuario) {
        $this->db->where('id_usuario', $id_usuario);
        $query = $this->db->get('admin_regiones');
        return $query->result();
    }
    function delete_user($id_usuario){
    	$this->db->where('id_usuario', $id_usuario);
		$this->db->delete('admin_regiones'); 
    }
    function inserte($data){
    	$this->db->insert('admin_regiones', $data);
    }
    function admin_distrito($id_distrito){
        $this->db->join('detalle_usuario', 'detalle_usuario.id_usuario = admin_regiones.id_usuario', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = admin_regiones.id_departamento', 'INNER');
        $this->db->join('ubprovincia', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        $this->db->join('ubdistrito', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->where('ubdistrito.idDist', $id_distrito);
        $query = $this->db->get('admin_regiones');
        return $query->result();
    }
}

?>
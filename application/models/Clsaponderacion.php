<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clsaponderacion extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function aponderacion() {
        $this->db->join('cuadros', 'cuadros.id_cuadro=aponderacion.id_cuadro', 'INNER');
        $query=$this->db->get('aponderacion');
        return $query->result();
    }
}

?>
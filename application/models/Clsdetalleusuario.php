<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 class clsdetalleusuario extends CI_Model 
 {
 	function __construct(){
 		parent::__construct();
 		$this->load->database();
 	}
 	function actualizar_usuario_info($id_usuario,$datos) {
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->update('detalle_usuario', $datos);
    }
    function delete_user($id_usuario){
    	$this->db->where('id_usuario', $id_usuario);
		$this->db->delete('detalle_usuario'); 
    }
    function insertar($data){
    	$this->db->insert('detalle_usuario', $data);
    }
 }
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clsreporte_calificacion extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function total_calificacion($id_departamento=null,$id_provincia=null,$id_distrito=null,$tipo=null,$id_usuario=null,
                        $grupo_departamento=null) {
    	$this->db->select("id_cuadro,calificacion,titulo,background,count(calificacion) as total");
    	$this->db->join('ubdistrito', 'reporte_calificacion.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');

        if ($tipo=="usuario" && $id_usuario!="") {
            $this->db->where('reporte_calificacion.id_distrito', $_SESSION["usuario"]["id_distrito"]);
        }else{
            if ($grupo_departamento!="" && $id_departamento=="") {
                $i=0;
                foreach ($grupo_departamento as $key) {
                    if ($i==0) {
                        $this->db->where('ubdepartamento.idDepa',$key);
                    }else{
                        $this->db->or_where('ubdepartamento.idDepa',$key);
                    }
                    $i++;
                }
            }else{      
                if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
                if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
                if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
            }
        }


    	$this->db->group_by("id_calificacion");
    	$this->db->order_by('id_cuadro', 'ASC');
        $query=$this->db->get('reporte_calificacion');
        return $query->result();
    }
    function total_aponderacion($id_departamento=null,$id_provincia=null,$id_distrito=null,$tipo=null,$id_usuario=null,
                        $grupo_departamento=null){
        $this->db->select("calificacion,background,count(calificacion) as total");
        $this->db->join('ubdistrito', 'reporte_aponderacion.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');

        if ($tipo=="usuario" && $id_usuario!="") {
            $this->db->where('reporte_aponderacion.id_distrito', $_SESSION["usuario"]["id_distrito"]);
        }else{
            if ($grupo_departamento!="" && $id_departamento=="") {
                $i=0;
                foreach ($grupo_departamento as $key) {
                    if ($i==0) {
                        $this->db->where('ubdepartamento.idDepa',$key);
                    }else{
                        $this->db->or_where('ubdepartamento.idDepa',$key);
                    }
                    $i++;
                }
            }else{      
                if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
                if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
                if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
            }
        }
        $this->db->group_by("calificacion");
        $this->db->order_by('id_llenado', 'ASC');
        $query=$this->db->get('reporte_aponderacion');
        return $query->result();
    }
    function cuadros(){
        $this->db->join('calificacion', 'calificacion.id_cuadro = cuadros.id_cuadro', 'INNER');
        $this->db->where('tipo',1);
        $this->db->or_where('tipo',4);
        $query=$this->db->get('cuadros');
        return $query->result();
    }
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clscalificacionMunicipal extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function calificacion($id_cuadro){
        $this->db->where('id_cuadro', $id_cuadro);
        $query = $this->db->get('calificacionMunicipal');
        return $query->result();
    }

    function insertar($data){
    	$this->db->insert("calificacionMunicipal",$data);
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 class clsusuarios extends CI_Model 
 {
 	function __construct(){
 		parent::__construct();
 		$this->load->database();
 	}
 	function listado_total($estado=null,$limit=null,$comienzo=null,$usuario=null,
 		$correo=null,$id_departamento=null,$id_provincia=null,$id_distrito=null,$grupo_departamento=null){
	 		$this->db->select('*');
	 		$this->db->from('usuario');  
			$this->db->join('detalle_usuario', 'usuario.id_usuario = detalle_usuario.id_usuario', 'INNER');
			$this->db->join('ubdistrito', 'usuario.id_distrito = ubdistrito.idDist', 'INNER');
			$this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
			$this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
			if ($grupo_departamento!="" && $id_departamento=="") {
			 	$i=0;	
			 	foreach ($grupo_departamento as $key) {
			        if ($i==0) {
			            $this->db->where('ubdepartamento.idDepa',$key);
						if ($estado==1) {$this->db->where('estado', 1);}
						else if ($estado==0) {$this->db->where('estado', 0); }
						if ($usuario!="") {$this->db->like('user', $usuario);}
	 					if ($correo!="") {$this->db->like('detalle_usuario.correo', $correo);}
			       	}else{
			            $this->db->or_where('ubdepartamento.idDepa',$key);
						if ($estado==1) {$this->db->where('estado', 1);}
						else if ($estado==0) {$this->db->where('estado', 0); }
						if ($usuario!="") {$this->db->like('user', $usuario);}
	 					if ($correo!="") {$this->db->like('detalle_usuario.correo', $correo);}
			        }
			        $i++;
			    }
			}else{
				if ($estado==1) {$this->db->where('estado', 1);}
				else if ($estado==0) {$this->db->where('estado', 0); }
				if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
		 		if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
		 		if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
		 		if ($usuario!="") {$this->db->like('user', $usuario);}
	 			if ($correo!="") {$this->db->like('detalle_usuario.correo', $correo);}
			}
	 		$this->db->limit($limit,$comienzo);
	 		$this->db->order_by("usuario.id_usuario", "asc"); 
	 		$query = $this->db->get();
	        return $query->result();
 	}
 	function busq_total_usuarios($estado,$usuario=null,$correo=null,$id_departamento=null,$id_provincia=null,$id_distrito=null,$grupo_departamento=null){
 		$this->db->select('*');
 		$this->db->from('usuario');  
		$this->db->join('detalle_usuario', 'usuario.id_usuario = detalle_usuario.id_usuario', 'INNER');
		$this->db->join('ubdistrito', 'usuario.id_distrito = ubdistrito.idDist', 'INNER');
		$this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
		$this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
		if ($grupo_departamento!="" && $id_departamento=="") {
			 	$i=0;	
			 	foreach ($grupo_departamento as $key) {
			        if ($i==0) {
			            $this->db->where('ubdepartamento.idDepa',$key);
						if ($estado==1) {$this->db->where('estado', 1);}
						else if ($estado==0) {$this->db->where('estado', 0); }
						if ($usuario!="") {$this->db->like('user', $usuario);}
	 					if ($correo!="") {$this->db->like('detalle_usuario.correo', $correo);}
			       	}else{
			            $this->db->or_where('ubdepartamento.idDepa',$key);
						if ($estado==1) {$this->db->where('estado', 1);}
						else if ($estado==0) {$this->db->where('estado', 0); }
						if ($usuario!="") {$this->db->like('user', $usuario);}
	 					if ($correo!="") {$this->db->like('detalle_usuario.correo', $correo);}
			        }
			        $i++;
			    }
			}else{
				if ($estado==1) {$this->db->where('estado', 1);}
				else if ($estado==0) {$this->db->where('estado', 0); }
				if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
		 		if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
		 		if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
		 		if ($usuario!="") {$this->db->like('user', $usuario);}
	 			if ($correo!="") {$this->db->like('detalle_usuario.correo', $correo);}
			}
		$query=$this->db->count_all_results();
 		return $query;
 	}
 	function count_usuarios($estado=null,$grupo_departamento=null){
 		$this->db->select('*');
 		$this->db->from('usuario');
		$this->db->join('ubdistrito', 'usuario.id_distrito = ubdistrito.idDist', 'INNER');
		$this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
		$this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
		if ($grupo_departamento!="") {
            $i=0;
            foreach ($grupo_departamento as $key) {
                if ($i==0) {
                	$this->db->where('ubdepartamento.idDepa',$key);
                	if ($estado==1) {$this->db->where('estado', 1);}
					else if ($estado==0) {$this->db->where('estado', 0);}
                }
                else{
                	$this->db->or_where('ubdepartamento.idDepa',$key);
                	if ($estado==1) {$this->db->where('estado', 1);}
					else if ($estado==0) {$this->db->where('estado', 0);}
           		 }
                $i++;
            }
        }else{
        	if ($estado==1) {$this->db->where('estado', 1);}
			else if ($estado==0) {$this->db->where('estado', 0);}
        }
		$query=$this->db->count_all_results();
 		return $query;
 	}
 	function total_usuarios($grupo_departamento=null){
 		$this->db->select('*');
 		$this->db->from('usuario');
 		$this->db->join('ubdistrito', 'usuario.id_distrito = ubdistrito.idDist', 'INNER');
		$this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
		$this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER'); 
 		if ($grupo_departamento!="") {
            $i=0;
            foreach ($grupo_departamento as $key) {
                if ($i==0) {
                	$this->db->where('ubdepartamento.idDepa',$key);
                }
                else{
                	$this->db->or_where('ubdepartamento.idDepa',$key);
           		 }
                $i++;
            }
        }
        $query=$this->db->count_all_results();
 		return $query;
 	}
 	function total_administradores(){
 		$this->db->select('*');
 		$this->db->from('usuario'); 
 		$this->db->where('tipo', "administrador");
		$query=$this->db->count_all_results();
 		return $query;
 	}
 	function usuario_delete($tipo=null,$id_usuario=null){
 		$this->db->where('id_usuario', $id_usuario);
		$this->db->delete('usuario'); 

		if ($tipo=="administrador") {
	 		$this->db->where('id_usuario', $id_usuario);
			$this->db->delete('admin_regiones'); 
 		}
 	}
 	function busqueda_user($id_usuario=null,$tipo=null){
 		$this->db->select('*');
 		$this->db->from('usuario');  
 		$this->db->join('detalle_usuario', 'usuario.id_usuario = detalle_usuario.id_usuario', 'INNER');
 		if ($tipo=="usuario") {
	 		$this->db->join('ubdistrito', 'usuario.id_distrito = ubdistrito.idDist', 'INNER');
			$this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
			$this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
 		}
 		if ($tipo=="administrador") {
 			$this->db->join('admin_regiones', 'usuario.id_usuario=admin_regiones.id_usuario', 'INNER');
 			$this->db->join('ubdepartamento', 'ubdepartamento.idDepa = admin_regiones.id_departamento', 'INNER');
 		}
 		if ($id_usuario!=null) {
 			$this->db->where('usuario.id_usuario', $id_usuario);
 		}
 		if ($tipo!=null) {
 			$this->db->where('usuario.tipo', $tipo);
 		}
 		$query = $this->db->get();
        return $query->result();
 	}

 	function actualizar_usuario_info($id_usuario,$datos) {
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->update('detalle_usuario', $datos);
    }
    function actualizar_usuario($id_usuario,$datos) {
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->update('usuario', $datos);
    }

     function verificar($user){ 
     	$this->db->join('detalle_usuario', 'usuario.id_usuario = detalle_usuario.id_usuario', 'INNER');
     	$this->db->join('ubdistrito', 'usuario.id_distrito = ubdistrito.idDist', 'LEFT');
		$this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'LEFT');
		$this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'LEFT');
 		$this->db->where('user', $user);
 		$query = $this->db->get('usuario');
        return $query->result();
    }
    function insertar($data){
    	$this->db->insert('usuario', $data);
    	$id=$this->db->insert_id();
        return $id;
    }
    function verificar_user($user){
    	$this->db->where('user', $user);
       	$query=$this->db->count_all_results('usuario');
 		return $query;
    }
 }
?>
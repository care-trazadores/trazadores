<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsdesarrollo extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function datos_llenado($id_llenado){
        $this->db->join('usuario', 'usuario.id = llenado.id_usuario', 'INNER');
        $this->db->join('ubdistrito', 'usuario.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        $this->db->where('id_llenado', $id_llenado);
        $query = $this->db->get('llenado');
        return $query->result();
    }
    function cuadros() {
        $this->db->order_by("id_cuadro", "ASC"); 
        $query = $this->db->get('cuadros');
        return $query->result();
    }
    function calificacion($id_cuadro){
        $this->db->where('id_cuadro', $id_cuadro);
        $query = $this->db->get('calificacion');
        return $query->result();
    }
    function criterios($id_cuadro){
        $this->db->where('id_cuadro', $id_cuadro);
        $query = $this->db->get('criterios');
        return $query->result();
    }
    function criterios_detalle($criterios){
        $i=0;
        foreach ($criterios as $key) {
            if ($i==0) {$this->db->where('id_criterio', $key->id_criterio);}
            else{ $this->db->or_where('id_criterio', $key->id_criterio);}
            $i++;
        }
        $query = $this->db->get('criterios_detalle');
        return $query->result();
    }
    function alternativas($preguntas){
        $i=0;
        foreach ($preguntas as $key) {
            if ($i==0) {$this->db->where('id_pregunta', $key->id_pregunta);}
            else{ $this->db->or_where('id_pregunta', $key->id_pregunta);}
            $i++;
        }
        $this->db->order_by("id_pregunta", "ASC"); 
        $query = $this->db->get('alternativas');
        return $query->result();
    }
    function notas($id_cuadro){
        $this->db->where('id_cuadro', $id_cuadro);
        $query = $this->db->get('notas');
        return $query->result();
    }
    function tablas($id_cuadro){
        $this->db->where('id_cuadro', $id_cuadro);
        $this->db->order_by("posicion", "ASC"); 
        $query = $this->db->get('tablas');
        return $query->result();
    }
    function preguntas($id_cuadro){
        $this->db->where('id_cuadro', $id_cuadro);
        $this->db->order_by("posicion", "ASC"); 
        $query = $this->db->get('preguntas');
        return $query->result();
    }
    function respuestas($id_llenado,$id_pregunta){
        $this->db->where('id_llenado',$id_llenado);
        $this->db->where('id_pregunta',$id_pregunta);    
        $this->db->order_by("id_pregunta", "ASC"); 
        $query = $this->db->get('deetalle_llenado');
        return $query->result();
    }
}

?>
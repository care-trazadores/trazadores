<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsllenado extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function llenado_por_id($id_llenado){
    	$this->db->join('usuario', 'usuario.id_usuario = llenado.id_usuario', 'left');
        $this->db->join('ubdistrito', 'llenado.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        $this->db->where('id_llenado', $id_llenado);
        $query = $this->db->get('llenado');
        return $query->result();
    }
    function insertar($data){
        $this->db->insert('llenado', $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function delete($id_llenado){
        $this->db->where('id_llenado', $id_llenado);
        $this->db->delete('llenado'); 
    }

    function reportes($limite=null,$comienzo=null,$usuario=null,$id_departamento=null,
                        $id_provincia=null,$id_distrito=null,$tipo=null,$id_usuario=null,
                        $grupo_departamento=null,$comunidad=null,$id_llenado=null){
        $this->db->join('usuario', 'usuario.id_usuario = llenado.id_usuario', 'left');
        $this->db->join('ubdistrito', 'llenado.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        if($id_llenado!=""){ $this->db->like("id_llenado",$id_llenado);}
        if($comunidad!=""){ $this->db->like("comunidad",$comunidad);}

        if ($tipo=="usuario" && $id_usuario!="") {
            $this->db->where('llenado.id_distrito', $_SESSION["usuario"]["id_distrito"]);
        }else{
            if ($grupo_departamento!="" && $id_departamento=="") {
                $i=0;
                foreach ($grupo_departamento as $key) {
                    if ($i==0) {
                        $this->db->where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }else{
                        $this->db->or_where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }
                    $i++;
                }
            }else{      
                if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
                if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
                if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
                if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
            }
        }
        if ($limite!="") {
            $this->db->limit($limite,$comienzo);
        }
        $this->db->order_by("llenado.id_llenado", "ASC"); 
        $query = $this->db->get('llenado');
        return $query->result();

    }

    function total_encuestas($usuario=null,$id_departamento=null,
                        $id_provincia=null,$id_distrito=null,$tipo=null,$id_usuario=null,$grupo_departamento=null,$comunidad=null,$id_llenado=null){
        $this->db->join('usuario', 'usuario.id_usuario = llenado.id_usuario', 'left');
        $this->db->join('ubdistrito', 'llenado.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        if($id_llenado!=""){ $this->db->like("id_llenado",$id_llenado);}
        if($comunidad!=""){ $this->db->like("comunidad",$comunidad);}
        if ($tipo=="usuario" && $id_usuario!="") {
            $this->db->where('llenado.id_distrito', $_SESSION["usuario"]["id_distrito"]);
        }else{
            if ($grupo_departamento!="" && $id_departamento=="") {
                $i=0;
                foreach ($grupo_departamento as $key) {
                    if ($i==0) {
                        $this->db->where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }else{
                        $this->db->or_where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }
                    $i++;
                }
            }else{      
                if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
                if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
                if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
                if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
            }
        }
        $query=$this->db->count_all_results('llenado');
        return $query;
    }

    function count_encuestas($tipo=null,$grupo_departamento=null,$id_usuario=null){

        $this->db->select('*');

        $this->db->from('llenado');

        $this->db->join('ubdistrito', 'llenado.id_distrito = ubdistrito.idDist', 'INNER');

        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');

        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');

        if ($tipo=="usuario" && $id_usuario!="") {$this->db->where('llenado.id_usuario',$id_usuario);}

        if ($grupo_departamento!="") {

            $i=0;

            foreach ($grupo_departamento as $key) {

                if ($i==0) {$this->db->where('ubdepartamento.idDepa',$key);}

                else{$this->db->or_where('ubdepartamento.idDepa',$key);}

                $i++;

            }

        }

        $query = $this->db->count_all_results();

        return $query;

    }

    function encuestas_fechas($fecha_inicio,$fecha_fin){

        $query = $this->db->get('llenado');

        return $query->result();

    }

}

?>
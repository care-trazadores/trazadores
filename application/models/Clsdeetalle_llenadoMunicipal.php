<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clsdeetalle_llenadoMunicipal extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function cuadros(){
    	$this->db->order_by("posicion", "ASC"); 
        $query = $this->db->get('cuadrosMunicipal');
        return $query->result();
    }
    function insertar($data){
        $this->db->insert('deetalle_llenadoMunicipal', $data);
    }
    function editar($data,$id_llenado,$id_pregunta){
        $this->db->where("id_llenado",$id_llenado);
        $this->db->where("id_pregunta",$id_pregunta);
        $this->db->update("deetalle_llenadoMunicipal",$data);
    }
    function delete($id_llenado){
        $this->db->where('id_llenado', $id_llenado);
        $this->db->delete('deetalle_llenadoMunicipal'); 
    }
    function detalle_id($id_llenado){
        $this->db->where('id_llenado', $id_llenado);
        $query = $this->db->get('deetalle_llenado');
        return $query->result();
    }
    function respuestas($id_llenado=null,$id_pregunta=null,$encuestas=null){
        if ($id_llenado!="") {
            $this->db->where('id_llenado',$id_llenado);
        }
        if ($id_pregunta!="") {
            $this->db->where('id_pregunta',$id_pregunta);  
        }
        if ($encuestas!="") {
            $i=0;
            $this->db->select('deetalle_llenadoMunicipal.id_llenado,deetalle_llenadoMunicipal.id_pregunta,deetalle_llenadoMunicipal.respuesta,deetalle_llenadoMunicipal.observacion,alternativasMunicipal.texto,preguntasMunicipal.pregunta');
            $this->db->join('alternativasMunicipal', 'deetalle_llenadoMunicipal.respuesta = alternativasMunicipal.valor and deetalle_llenadoMunicipal.id_pregunta = alternativasMunicipal.id_pregunta', 'left');
            $this->db->join('preguntasMunicipal', 'deetalle_llenadoMunicipal.id_pregunta = preguntasMunicipal.id_pregunta', 'left');
            foreach ($encuestas as $key) {
                if ($i==0) {
                    $this->db->where('id_llenado',$key->id_llenado);
                }else{
                    $this->db->or_where('id_llenado',$key->id_llenado);
                }
                $i++;
            }
        }
        $this->db->order_by("id_llenado", "ASC"); 
        $query = $this->db->get('deetalle_llenadoMunicipal');
        return $query->result();
    }
    function verificar_detalle($id_llenado,$idpregunta){
        $this->db->where("id_llenado",$id_llenado);
        $this->db->where("id_pregunta",$idpregunta);
        $query = $this->db->count_all_results("deetalle_llenadoMunicipal");
        return $query;
    }
}
?>
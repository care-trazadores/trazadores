<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clstablasMunicipal extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertar($data){
    	$this->db->insert("tablasMunicipal",$data);

    }

    function buscar_id($id_cuadro){
    	$this->db->select("id_tabla");
    	$this->db->where("id_cuadro",$id_cuadro);
    	$query = $this->db->get("tablasMunicipal");
    	$ret = $query->row();
    	return $ret->id_tabla;

    }
}
?>
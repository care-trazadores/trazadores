<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clsllenadoMunicipal extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function verificarIdDistrito($id_distrito){
        $this->db->where('id_distrito',$id_distrito);
        $query=$this->db->get("llenadoMunicipal");
        if($query->num_rows() > 0 )
            {return 1;}
        else{return 0;}
    }
    function llenado_por_id($id_llenado){
    	// $this->db->join('usuario', 'usuario.id_usuario = llenadoMunicipal.id_usuario', 'left');
        $this->db->join('ubdistrito', 'llenadoMunicipal.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        $this->db->where('id_llenado', $id_llenado);
        $query = $this->db->get('llenadoMunicipal');
        return $query->result();
    }
    function insertar($data){
        $this->db->insert('llenadoMunicipal', $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function delete($id_llenado){
        $this->db->where('id_llenado', $id_llenado);
        $this->db->delete('llenadoMunicipal'); 
    }
    function reportes($limite=null,$comienzo=null,$usuario=null,$id_departamento=null,
                        $id_provincia=null,$id_distrito=null,$tipo=null,$id_usuario=null,
                        $grupo_departamento=null){
        $this->db->join('usuario', 'usuario.id_usuario = llenadoMunicipal.id_usuario', 'left');
        $this->db->join('ubdistrito', 'llenadoMunicipal.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        if ($tipo=="usuario" && $id_usuario!="") {
            $this->db->where('llenadoMunicipal.id_usuario',$id_usuario);
        }else{
            if ($grupo_departamento!="" && $id_departamento=="") {
                $i=0;
                foreach ($grupo_departamento as $key) {
                    if ($i==0) {
                        $this->db->where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }else{
                        $this->db->or_where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }
                    $i++;
                }
            }else{      
                if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
                if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
                if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
                if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
            }
        }
        if ($limite!="") {
            $this->db->limit($limite,$comienzo);
        }
        $this->db->order_by("llenadoMunicipal.id_llenado", "ASC"); 
        $query = $this->db->get('llenadoMunicipal');
        return $query->result();
    }
    function total_encuestas($usuario=null,$id_departamento=null,
                        $id_provincia=null,$id_distrito=null,$tipo=null,$id_usuario=null,$grupo_departamento=null){
        $this->db->join('usuario', 'usuario.id_usuario = llenadoMunicipal.id_usuario', 'left');
        $this->db->join('ubdistrito', 'llenadoMunicipal.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        if ($tipo=="usuario" && $id_usuario!="") {
            $this->db->where('llenadoMunicipal.id_usuario',$id_usuario);
        }else{
            if ($grupo_departamento!="" && $id_departamento=="") {
                $i=0;
                foreach ($grupo_departamento as $key) {
                    if ($i==0) {
                        $this->db->where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }else{
                        $this->db->or_where('ubdepartamento.idDepa',$key);
                        if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
                    }
                    $i++;
                }
            }else{      
                if ($id_departamento!="") { $this->db->where('ubdepartamento.idDepa', $id_departamento);}
                if ($id_provincia!="") { $this->db->where('ubprovincia.idProv', $id_provincia);}
                if ($id_distrito!="") {$this->db->where('ubdistrito.idDist', $id_distrito);}
                if ($usuario!="") {$this->db->like('usuario.user', $usuario);}
            }
        }
        $query=$this->db->count_all_results('llenadoMunicipal');
        return $query;

    }

    function count_encuestas($tipo=null,$grupo_departamento=null,$id_usuario=null){
        $this->db->select('*');
        $this->db->from('llenado');
        $this->db->join('ubdistrito', 'llenado.id_distrito = ubdistrito.idDist', 'INNER');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = ubprovincia.idDepa', 'INNER');
        if ($tipo=="usuario" && $id_usuario!="") {$this->db->where('llenado.id_usuario',$id_usuario);}
        if ($grupo_departamento!="") {
            $i=0;
            foreach ($grupo_departamento as $key) {
                if ($i==0) {$this->db->where('ubdepartamento.idDepa',$key);}
                else{$this->db->or_where('ubdepartamento.idDepa',$key);}
                $i++;
            }
        }
        $query = $this->db->count_all_results();
        return $query;
    }

    function encuestas_fechas($fecha_inicio,$fecha_fin){
        $query = $this->db->get('llenadoMunicipal');
        return $query->result();
    }

}

?>
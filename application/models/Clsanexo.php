<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsanexo extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function anexo_cuadro($id_cuadro) {
        $this->db->select('preguntas.id_cuadro,anexos.id_pregunta');
        $this->db->from('anexos');  
        $this->db->join('preguntas', 'anexos.id_pregunta=preguntas.id_pregunta', 'INNER');
        $this->db->where('anexos.id_cuadro',$id_cuadro);
        $query = $this->db->get();
        return $query->result();
    }
    function anexo_pregunta($id_pregunta){
    	$this->db->where('id_pregunta', $id_pregunta);
		$query = $this->db->get('anexos');
        return $query->result();
    }
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clsnotasMunicipal extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }


    function insertar($data){
    	$this->db->insert("notasMunicipal",$data);
    }


    function ultima_posicion($id_cuadro){
    	$this->db->select("max(posicion) as posicion");
    	$this->db->where("id_cuadro",$id_cuadro);
    	$query = $this->db->get("notasMunicipal");
    	$ret = $query->row();

    	return $ret->posicion;
    }
}
?>
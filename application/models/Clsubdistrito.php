<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    class clsubdistrito extends CI_Model {
        function __construct(){
            parent::__construct();
            $this->load->database();
        }
        function distrito($id_distrito){
            $this->db->where('idDist', $id_distrito);
            $query=$this->db->get('ubdistrito');
            return $query->result();
        }
    }
?>
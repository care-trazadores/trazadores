<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsanexoMunicipal extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function anexo_cuadro($id_cuadro) {
        $this->db->select('preguntasMunicipal.id_cuadro,anexosMunicipal.id_pregunta');
        $this->db->from('anexosMunicipal');  
        $this->db->join('preguntasMunicipal', 'anexosMunicipal.id_pregunta=preguntasMunicipal.id_pregunta', 'INNER');
        $this->db->where('anexosMunicipal.id_cuadro',$id_cuadro);
        $query = $this->db->get();
        return $query->result();
    }
    function anexo_pregunta($id_pregunta){
    	$this->db->where('id_pregunta', $id_pregunta);
		$query = $this->db->get('anexosMunicipal');
        return $query->result();
    }
}

?>
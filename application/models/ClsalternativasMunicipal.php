<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clsalternativasMunicipal extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function alternativas(){
        $query = $this->db->get('alternativas');
        return $query->result();
    }
    function alternativa_idpregunta($id_pregunta,$valor){
    	$this->db->where('id_pregunta', $id_pregunta);
    	$this->db->where('valor', $valor);
        $query = $this->db->get('alternativas');
        return $query->result();
    }

    //alternativas

    function insertar($data){
        $this->db->insert("alternativasMunicipal",$data);

    }

    function editar($data,$id_alternativas){

        $this->db->where("id_alternativas",$id_alternativas);
        $this->db->update("alternativasMunicipal",$data);
    }

    function delete($id_alternativas){
        $this->db->where("id_alternativas",$id_alternativas);
        $this->db->delete("alternativasMunicipal");
    }


    function alternativa($id_alternativas){
        $this->db->where("id_alternativas",$id_alternativas);
        $query = $this->db->get("alternativasMunicipal");
        
        return $query->row();

    }


    function verificar_alternativa($alternativa,$id_pregunta){
        $this->db->where('texto', $alternativa);
        $this->db->where('id_pregunta', $id_pregunta);
        $query= $this->db->count_all_results('alternativasMunicipal');
        return $query;

    }
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clscuadros extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function cuadros(){
    	$this->db->order_by("posicion", "ASC"); 
        $query = $this->db->get('cuadros');
        return $query->result();
    }
}
?>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsadministradores extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function listado_total_admi($estado = null, $limit = null, $comienzo = null, $administrador = null, $correo = null, $id_departamento = null) {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->join('detalle_usuario', 'detalle_usuario.id_usuario = usuario.id_usuario', 'INNER');
        $this->db->where('tipo', 'administrador');
        if ($estado == 1) {
            $this->db->where('estado', 1);
        } else if ($estado == 0) {
            $this->db->where('estado', 0);
        }
        if ($id_departamento != "") {
            $this->db->join('admin_regiones', 'usuario.id_usuario = admin_regiones.id_usuario', 'INNER');
            $this->db->where('admin_regiones.id_departamento', $id_departamento);
            
        }
        if ($administrador != "") {
            $this->db->like('usuario.user', $administrador);
        }
        if ($correo != "") {
            $this->db->like('detalle_usuario.correo', $correo);
        }
        $this->db->limit($limit, $comienzo);
// 		$this->db->order_by("id", "asc"); 
        $query = $this->db->get();
        return $query->result();
    }

    function total_usuarios_admi($estado, $usuario = null, $correo = null, $id_departamento = null) {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->join('detalle_usuario', 'usuario.id_usuario = detalle_usuario.id_usuario', 'INNER');
        $this->db->where('tipo', 'administrador');
        if ($estado == 1) {
            $this->db->where('estado', 1);
        } else if ($estado == 0) {
            $this->db->where('estado', 0);
        }
        if ($id_departamento != "") {
            $this->db->join('admin_regiones', 'usuario.id_usuario = admin_regiones.id_usuario', 'INNER');
            $this->db->where('admin_regiones.id_departamento', $id_departamento);
        }
        if ($usuario != "") {
            $this->db->like('usuario.user', $usuario);
        }
        if ($correo != "") {
            $this->db->like('detalle_usuario.correo', $correo);
        }
        $query = $this->db->count_all_results();
        return $query;
    }

    function departamento_admi($id_usuario) {
        $this->db->join('ubdepartamento', 'ubdepartamento.idDepa = admin_regiones.id_departamento', 'INNER');
        $this->db->where('id_usuario', $id_usuario);

        $query = $this->db->get('admin_regiones');
        return $query->result();
    }

}

?>
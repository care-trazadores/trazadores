<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 class clsdistritos extends CI_Model 
 {
    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    function regiones(){
        $query=$this->db->get('ubdepartamento');
        return $query->result();
    }
    function provincias($departamento){
        $this->db->where('idDepa', $departamento);
        $query=$this->db->get('ubprovincia');
        return $query;
    }
    function distritos($provincia){
        $this->db->where('idProv', $provincia);
        $query=$this->db->get('ubdistrito');
        return $query;
    }
    function listado_distritos($limit=NULL,$comienzo=NULL,$distrito=NULL,$id_provincia=NULL,$id_departamento=NULL) {
        $this->db->select('*');
        $this->db->from('ubdistrito');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubprovincia.idDepa = ubdepartamento.idDepa', 'INNER');    
        if ($distrito != "") {$this->db->like('ubdistrito.distrito', $distrito);}
        if ($id_provincia != "") {$this->db->where('ubdistrito.idProv', $id_provincia);}
        if ($id_departamento != "") { $this->db->where('ubprovincia.idDepa', $id_departamento);}
        $this->db->limit($limit, $comienzo);
//        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        return $query->result();
    }
    
    function total_distritos($distrito = null, $id_provincia = null,$id_departamento=NULL) {
        $this->db->select('*');

        $this->db->from('ubdistrito');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubprovincia.idDepa = ubdepartamento.idDepa', 'INNER');
        if ($id_provincia != "") { $this->db->where('ubdistrito.idProv', $id_provincia);}
        if ($id_departamento != "") { $this->db->where('ubprovincia.idDepa', $id_departamento);}
        if ($distrito!= "") {$this->db->like('ubdistrito.distrito', $distrito);}
        $query = $this->db->count_all_results();
        return $query;
    }
    function insert($tabla, $data = array()) {
        $query = $this->db->insert($tabla, $data);
        return $query ? TRUE : FALSE;
    }
    function update($tabla, $update = array(), $where = array()) {
        $query = $this->db->update($tabla, $update, $where);
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
     function delete($tabla, $where = array()) {
        $query = $this->db->delete($tabla, $where);
        return $query ? TRUE : FALSE;
    }
    function max($tabla,$col) {
//        $query = $this->db->select(" MAX({$col}) as max")->from($tabla)->get();
//        if ($query->num_rows > 0) {
//            return $query->result();
//        } else {
//            return FALSE;
//        }
        $query=  $this->db->query("SELECT max($col) as max FROM $tabla ");
     if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }
       function get_distrito($id_distrito) {
        $this->db->select('*');

        $this->db->from('ubdistrito');
        $this->db->join('ubprovincia', 'ubdistrito.idProv = ubprovincia.idProv', 'INNER');
        $this->db->join('ubdepartamento', 'ubprovincia.idDepa = ubdepartamento.idDepa', 'INNER');    
        

        if ($id_distrito != "") {
            $this->db->where('ubdistrito.idDist', $id_distrito);
        }
//        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        return $query->result();
    }
        function listado_provincia($limit=NULL,$comienzoprov=NULL,$provincia=NULL,$id_departamento=NULL) {
        $this->db->select('*');

        $this->db->from('ubprovincia');
        $this->db->join('ubdepartamento', 'ubprovincia.idDepa = ubdepartamento.idDepa', 'INNER');    
        
         if ($id_departamento != "") {
            $this->db->where('ubprovincia.idDepa', $id_departamento);
        }
        if ($provincia != "") {
            $this->db->like('ubprovincia.provincia', $provincia);
        }
        
        $this->db->limit($limit, $comienzoprov);
//        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        return $query->result();
    }
 
    function total_provincia($provincia = null, $id_departamento = null) {
        $this->db->select('*');

        $this->db->from('ubprovincia');
        $this->db->join('ubdepartamento', 'ubprovincia.idDepa = ubdepartamento.idDepa', 'INNER');

       
        
        if ($id_departamento != "") {
            $this->db->where('ubprovincia.idDepa', $id_departamento);
        }
       

        if ($provincia!= "") {
            $this->db->like('ubprovincia.provincia', $provincia);
        }
        
        $query = $this->db->count_all_results();
        return $query;
    }
       function get_provincia($id_provincia) {
        $this->db->where('idProv', $id_provincia);
        $query = $this->db->get('ubprovincia');
        return $query->result();
    }
    function count($tabla) {
        $this->db->select('*');

        $this->db->from($tabla);   

        $query = $this->db->count_all_results();
        return $query;
    }


 }
?>
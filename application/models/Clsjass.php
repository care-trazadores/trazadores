<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsjass extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function jass() {
    	$this->db->join('preguntas', 'preguntas.id_pregunta = jass.id_pregunta', 'INNER');
        $query = $this->db->get('jass');
        return $query->result();
    }
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    class clsubprovincia extends CI_Model {
        function __construct(){
            parent::__construct();
            $this->load->database();
        }
        function provincia($id_provincia){
            $this->db->where('idProv', $id_provincia);
            $query=$this->db->get('ubprovincia');
            return $query->result();
        }
    }
?>
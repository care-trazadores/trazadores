<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clsdeetalle_llenado extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function cuadros(){
    	$this->db->order_by("posicion", "ASC"); 
        $query = $this->db->get('cuadros');
        return $query->result();
    }
    function insertar($data){
        $this->db->insert('deetalle_llenado', $data);
    }
    function delete($id_llenado){
        $this->db->where('id_llenado', $id_llenado);
        $this->db->delete('deetalle_llenado'); 
    }
    function detalle_id($id_llenado){
        $this->db->where('id_llenado', $id_llenado);
        $query = $this->db->get('deetalle_llenado');
        return $query->result();
    }
    function respuestas($id_llenado=null,$id_pregunta=null,$encuestas=null){
        
        if ($id_llenado!="") {
            $this->db->where('id_llenado',$id_llenado);
        }
        if ($id_pregunta!="") {
            $this->db->where('id_pregunta',$id_pregunta);  
        }
        if ($encuestas!="") {
            $i=0;
            $this->db->select('deetalle_llenado.id_llenado,deetalle_llenado.id_pregunta,deetalle_llenado.respuesta,deetalle_llenado.observacion,alternativas.texto,preguntas.pregunta');
            $this->db->join('alternativas', 'deetalle_llenado.respuesta = alternativas.valor and deetalle_llenado.id_pregunta = alternativas.id_pregunta', 'left');
            $this->db->join('preguntas', 'deetalle_llenado.id_pregunta = preguntas.id_pregunta', 'left');
            foreach ($encuestas as $key) {
                if ($i==0) {
                    $this->db->where('id_llenado',$key->id_llenado);
                }else{
                    $this->db->or_where('id_llenado',$key->id_llenado);
                }
                $i++;
            }
        }
        $this->db->order_by("id_llenado", "ASC"); 
        $query = $this->db->get('deetalle_llenado');
        return $query->result();
    }
    function respuestas_todo(){
        $this->db->order_by("id_llenado", "ASC"); 
        $query = $this->db->get('deetalle_llenado');
        return $query->result();
    }
}
?>
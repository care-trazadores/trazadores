<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class clspreguntas extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function preguntas($id_cuadro){
    	$this->db->where('id_cuadro', $id_cuadro);
        $query = $this->db->get('preguntas');
        return $query->result();
    }
}
?>
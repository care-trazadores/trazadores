<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class clsjasstablas extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function tablas() {
        $query = $this->db->get('jass_tabla');
        return $query->result();
    }
}

?>
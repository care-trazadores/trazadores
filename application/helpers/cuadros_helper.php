<?php
	function cuadros($array){
		$cadena="";
		$i=0;
		foreach ($array as $key) {
			$cadena=$cadena."<div class='col3 pad15'><div class='dashboard-stat ".$array[$i]["color"]."'><div class='visual'><i class='fa fa-".$array[$i]["icon"]."'></i></div><div class='details'><div class='number'><span data-counter='counterup' data-value='".$array[$i]["cantidad"]."'>".$array[$i]["cantidad"]."</span></div><div class='desc'>".$array[$i]["titulo"]."</div></div> </div></div>";
			$i++;
		}
		return $cadena;
	}
	function cuadros_link($array){
		$cadena="";
		$i=0;
		foreach ($array as $key) {
			$cadena=$cadena."<div class='col3 pad15'><div class='dashboard-stat ".$array[$i]["color"]."'><div class='visual'><i class='fa fa-".$array[$i]["icon"]."'></i></div><div class='details'><div class='number'><span data-counter='counterup' data-value='".$array[$i]["cantidad"]."'>".$array[$i]["cantidad"]."</span></div><div class='desc'>".$array[$i]["titulo"]."</div></div> <a class='more' href='".$array[$i]["link"]."'>Ver más<i class='fa fa-arrow-circle-o-right'></i></a></div></div>";
			$i++;
		}
		return $cadena;
	}
?>

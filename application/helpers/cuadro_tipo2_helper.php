<?php
	function cuadro_tipo_2($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$anexo_cuadro){
		$impresion='';
		$impresion.="<div id='tab".$id_cuadro."' class='tabs'>";
		$impresion.="<h3>".$titulo."</h3>";
		$impresion.="<div class='col12'>";
		$impresion.="<table class='listado'>";
		$impresion.="<tr >";
		$impresion.="<th class='center' rowspan='3' colspan='2' width='350'>TRAZADOR</th>";
		$impresion.="<th class='center' rowspan='2' colspan='2'  width='50'>EXISTENCIA DE COMPONENTES EN EL PROYECTO</th>";
		$impresion.="<th class='center' colspan='3'  width='100'>Criterio N°1</th>";
		$impresion.="<th class='center' rowspan='4'  width='170'>Estado del componente</th>";
		$impresion.="<th class='center' rowspan='4'  width='150'>Observaciones</th>";
		$impresion.="</tr>";
		$impresion.="<tr>";
		$impresion.="<th class='center' colspan='3'>CALIFICACION</th>";
		$impresion.="</tr>";
		$impresion.="<tr>";
		$impresion.="<th class='center' width='25'>SI</th>";
		$impresion.="<th class='center' width='25'>NO</th>";
		$impresion.="<th class='center' width='30'>DETERIORADO</th>";
		$impresion.="<th class='center' width='30'>LIMITADO</th>";
		$impresion.="<th class='center' width='30'>ADECUADO</th>";
		$impresion.="</tr>";
		$impresion.="<tr>";
		$impresion.="<th class='center' colspan='2'>COMPONENTES DEL SISTEMA</th>";
		$impresion.="<th class='center' colspan='2'>Seleccionar</th>";
		$impresion.="<th class='center'>2</th>";
		$impresion.="<th class='center'>1</th>";
		$impresion.="<th class='center'>0</th>";
		$impresion.="</tr>";
		foreach ($tablas as $key) {
			$impresion.="<tr>";
			$impresion.="<td colspan='2'>".$key->titulo."</td>";
			$impresion.="<td colspan='2'>Marcar Valor</td>";
			$impresion.="<td colspan='3'>Seleccionar valor</td>";
			$impresion.="<td></td>";
			$impresion.="<td></td>";
			$impresion.="</tr>";
			$a=1;
			if (count($preguntas)!==null) {
				foreach ($preguntas as $keys) {
					if ($keys->id_tabla==$key->id_tabla) {
						$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$keys->id_pregunta."'>";
						$impresion.="<tr class='fond filtro filtro".$keys->id_pregunta."' pregunta='".$keys->id_pregunta."'>";
						$impresion.="<td width='1'>".$a."</td>";
						$impresion.="<td>".$keys->pregunta."</td>";
						$si="";
						$no="checked";
						if ($respuestas!=null) {
							foreach ($respuestas[$keys->id_pregunta] as $pre) {
								if ($pre->respuesta!="") {
									$si="checked";
									$no="";
								}else{
									$si="";
									$no="checked";
								}
							}
						}
						$impresion.="<td>";
						$impresion.="<span>";
						$impresion.="<input class='form-control filtros' ".$si." name='filtro".$keys->id_pregunta."' pregunta='".$keys->id_pregunta."' value='0' cuadro='".$id_cuadro."' tipo='anexo1' type='radio'>Si";
						$impresion.="</span>";
						$impresion.="</td>";
						$impresion.="<td>";
						$impresion.="<span>";
						$impresion.="<input class='form-control filtros' ".$no." name='filtro".$keys->id_pregunta."' pregunta='".$keys->id_pregunta."' value='1' cuadro='".$id_cuadro."' tipo='anexo1' type='radio'>no";
						$impresion.="</span>";
						$impresion.="</td>";
						$impresion.="<td colspan='3'>";
						$impresion.="<select id='respuesta".$keys->id_pregunta."' class='respuestas' name=\"respuestas[".$keys->id_pregunta."]\" id_cuadro='".$id_cuadro."' tipo='anexo1'><option value=''>-</option>";
						foreach ($alternativas as $al) {
							if ($keys->id_pregunta==$al->id_pregunta) {
								$impresion.="<option texto='".$al->texto."' value='".$al->valor."'";
								if ($respuestas!=null) {
									foreach ($respuestas[$keys->id_pregunta] as $res) {
										if ($res->id_pregunta==$al->id_pregunta && $res->respuesta==$al->valor) {
											$impresion.="selected";
										}
									}
								}
								$impresion.=">".$al->valor."</option>";
							}
						}
						$impresion.="</select>";
						$impresion.="</td>";
						$impresion.="<td class='estado center respuesta".$keys->id_pregunta."'>";
						if ($respuestas!=null) {
							foreach ($respuestas[$keys->id_pregunta] as $res) {
								if ($res->id_pregunta==$keys->id_pregunta) {
									if ($res->respuesta==0) {
										$impresion.="ADECUADO";
									}elseif ($res->respuesta==1) {
										$impresion.="LIMITADO";
									}elseif ($res->respuesta==2) {
										$impresion.="DETERIORADO";
									}
								}
							}
						}
						$impresion.="</td>";
						$impresion.="<td><textarea class='observacion' name=\"observacion[".$keys->id_pregunta."]\">";
						if ($respuestas!=null) {
							foreach ($respuestas[$keys->id_pregunta] as $res) {
								if ($res->id_pregunta==$keys->id_pregunta) {
									$impresion.=$res->observacion;
								}
							}
						}
						$impresion.="</textarea></td>";
						$impresion.="</tr>";
					}
					$a++;
				}
			}
		}
		$impresion.="<tr>";
		$impresion.="<td colspan='2'>Promedio de Sumatoria</td>";
		$total_sumatoria="";
		$promedio="";
		$total_si=0;
		$total_no=0;
		if ($respuestas!=null) {
			foreach ($respuestas as $key) {
				foreach ($key as $resp) {
					if ($resp->respuesta!="") {
						$total_si=$total_si+1;
					}else {
						$total_no=$total_no+1;
					}
					$total_sumatoria=$total_sumatoria+$resp->respuesta;
				}
			}
			if ($total_si!=0) {
				if ($total_sumatoria==0) {
					$promedio=0;
				}else{
					$promedio=number_format($total_sumatoria/$total_si, 2, '.', ' ');
				}
			}
			if ($total_sumatoria==0) {
				$total_sumatoria="";
			}
		}else{
			$total_si=0;
			$total_no=count($preguntas);
		}
		$impresion.="<td class='total_si'>".$total_si."</td>";
		$impresion.="<td class='total_no'>".$total_no."</td>";
		$impresion.="<td class='sumatoria' colspan='3'>".$total_sumatoria."</td>";
		$impresion.="<td>GLOBAL COMPONENTES</td>";

		$calificaciones_final["background"]="";
		$calificaciones_final["calificacion"]="";
		if ($respuestas!=null) {
			$calificaciones_final=calificaciones_final($calificacion,$promedio);
		}
		$impresion.="<td></td>";
		$impresion.="</tr>";

		$impresion.="<tr>";
		$impresion.="<td colspan='2'>TRAZADOR COMPONENTES</td>";
		$impresion.="<td colspan='2'></td>";
		$impresion.="<td colspan='3'></td>";
		$impresion.="<td class='promedio'>".$promedio."</td>";
		$impresion.="<td class='calificacion_final' style='background-color:".$calificaciones_final["background"].";'>".$calificaciones_final["calificacion"]."</td>";
		$impresion.="</tr>";
		$impresion.="</table>";
		$impresion.="<div class='col6'>";
		$impresion.="<table class='calificacion'>";
		$impresion.="<tr>";
		$impresion.="<th>Calificación</th>";
		$impresion.="<th>Rango</th>";
		$impresion.="</tr>";
			foreach ($calificacion as $key) {
				$impresion.="<tr class='calificaciones' mayor='".$key->mayor."' menor='".$key->menor."' igual='".$key->igual."' background='".$key->background."' texto='".$key->calificacion."'>";
				$impresion.="<td>".$key->calificacion."</td>";
				$impresion.="<td>".$key->texto_rango."</td>";
				$impresion.="</tr>";
			}
		$impresion.="</table>";
		$impresion.="</div>";
		$impresion.="<div class='col6'>";
		$impresion.="</div>";
		if (count($criterios)!==null) {
			
			foreach ($criterios as $key) {
				$impresion.="<div class='col6'>";
				$impresion.="<table class='criterios'>";
				$impresion.="<tr>";
				$impresion.="<th></th>";
				$impresion.="<th>".$key->titulo."</th>";
				$impresion.="</tr>";
				foreach ($criterios_detalle as $keys) {
					if ($keys->id_criterio==$key->id_criterio) {
						$impresion.="<tr>";
						if ($keys->valor!='') {
							$impresion.="<td>".$keys->valor."</td>";
							
						}else{$impresion.="<td></td>";}
						$impresion.="<td>".$keys->texto."</td>";
						$impresion.="</tr>";
					}
				}
				$impresion.="</table>";
				$impresion.="</div>";
				$impresion.="<div class='col6'>";
				$impresion.="</div>";
			}
			
			}
		if (count($notas)!==null) {
			$impresion.="<div class='col12'>";
				foreach ($notas as $key) {
					$impresion.=$key->texto."<br>";
				}
			$impresion.="</div>";
		}
		$impresion.="</div>";
		if ($anexo_cuadro!=null) {
			foreach ($anexo_cuadro as $ae) {
				$impresion.="<span class='btn boton' onclick='verificar(".$ae->id_cuadro.",".$ae->id_pregunta.",".$id_cuadro.")'>Continuar</span>";
			}
			
		}
		$impresion.="</div>";
		return $impresion;
	}
?>
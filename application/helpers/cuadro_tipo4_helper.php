<?php
	function cuadro_tipo_4($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$aponderacion,$calificaciones){
		$impresion="";
		$impresion.="<div id='tab".$id_cuadro."' class='tabs'>";
		$impresion.="<h3>".$titulo."</h3>";
		$impresion.="<div class='col12'>";
		$impresion.="<table class='listado'>";
		$impresion.="<tr>";
		$impresion.="<th colspan='2'>TRAZADORES</th>";
		$impresion.="<th>VALOR EVALUADO</th>";
		$impresion.="<th>CALIFICACION</th>";
		$impresion.="<th>PONDERACION</th>";
		$impresion.="<th>VALORACION PONDERADA</th>";
		$impresion.="</tr>";
		$impresion.="<tr>";
		$impresion.="<td colspan='2'>INIDICADORES TRAZADORES</td>";
		$impresion.="<td></td>";
		$impresion.="<td></td>";
		$impresion.="<td></td>";
		$impresion.="<td></td>";
		$impresion.="</tr>";
		$a=1;
		$valor_evaluado="";
		$cali["background"]="";
		$cali["calificacion"]="";
		$valoracion_ponderada="";
		$sumatoria="";
		$ponderacion_total="";
		$total_valoracion_ponderada="";

		$promedio="";
		$califi_fi["background"]="";
		$califi_fi["calificacion"]="";
		foreach ($aponderacion as $key) {
			$impresion.="<tr class='aponderacion cuadro".$key->id_cuadro."' id_cuadro='".$key->id_cuadro."'>";
			$impresion.="<td>".$a."</td>";
			$impresion.="<td>".$key->titulo."</td>";
			if ($respuestas!=null) {
				$valor_evaluado=sumatoria($respuestas[$key->id_cuadro]);
				$cali=calificaciones_final($calificaciones[$key->id_cuadro],$valor_evaluado);
				$valoracion_ponderada=$valor_evaluado*$key->ponderacion;
				$sumatoria=$sumatoria+$valor_evaluado;
				$total_valoracion_ponderada=$total_valoracion_ponderada+$valoracion_ponderada;
			}
			$impresion.="<td class='cuadro_evaluado".$key->id_cuadro."'>".$valor_evaluado."</td>";
			$impresion.="<td class='cuadro_calificacion".$key->id_cuadro."' style='background-color:".$cali["background"]."'>".$cali["calificacion"]."</td>";
			$impresion.="<td class='aponderacion_valor aponderacion_valor".$key->id_cuadro."'>".$key->ponderacion."</td>";
			$impresion.="<td class='valoracion_ponderada aponderacion_valoracion_ponderada".$key->id_cuadro."'>".$valoracion_ponderada."</td>";
			$impresion.="</tr>";
			$a++;
			$ponderacion_total=$ponderacion_total+$key->ponderacion;
		}
		if ($respuestas!=null) {
			$promedio=number_format($total_valoracion_ponderada/$ponderacion_total, 2, '.', ' ');
			$califi_fi=calificaciones_final($calificacion,$promedio);
		}
		
		$impresion.="<tr>";
		$impresion.="<td colspan='2'>Sumatoria</td>";
		$impresion.="<td class='aponderacion_sumatoria'>".$sumatoria."</td>";
		$impresion.="<td></td>";
		$impresion.="<td class='aponderacion_valor_total'>".$ponderacion_total."</td>";
		$impresion.="<td></td>";
		$impresion.="</tr>";
		$impresion.="<tr>";
		$impresion.="<td colspan='2'>Promedio Global</td>";
		$impresion.="<td></td>";
		$impresion.="<td></td>";
		$impresion.="<td class='aponderacion_calificacion' style='background-color:".$califi_fi["background"]."'>".$califi_fi["calificacion"]."</td>";
		$impresion.="<td class='aponderacion_global' style='background-color:".$califi_fi["background"]."'>".$promedio."</td>";
		$impresion.="</tr>";
		$impresion.="</table>";
		$impresion.="</div>";
		$impresion.="<div class='col6'>";
		$impresion.="<table class='calificacion'>";
		$impresion.="<tr>";
		$impresion.="<th>Calificación</th>";
		$impresion.="<th>Rango</th>";
		$impresion.="</tr>";

		foreach ($calificacion as $key) {
			$impresion.="<tr class='calificaciones' mayor='".$key->mayor."' menor='".$key->menor."' igual='".$key->igual."' background='".$key->background."' texto='".$key->calificacion."'>";				$impresion.="<td>".$key->calificacion."</td>";
			$impresion.="<td>".$key->texto_rango."</td>";
			$impresion.="</tr>";
		}

		$impresion.="</table>";
		$impresion.="</div>";
		$impresion.="</div>";
		return $impresion;
	}
?>
<?php
	function reporte_detallado($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion){
		$impresion="";
		foreach ($encuestas as $key) {
			$impresion.="<div id='llenado".$key->id_llenado."' class='reporte_detallado'>";
				$impresion.="<h4>".$key->departamento."--".$key->provincia."--".$key->distrito."</h4>";
				$impresion.="<span class='fecha'>Fecha de registro:".$key->fecha."   </span>";
				$impresion.="<span style='float:right;' class='comunidad'>Localidad: ".$key->comunidad."</span>";
				$impresion.="<table class='listado'>";
		    		$impresion.="<tbody>";
		    			$impresion.="<tr>";
		    				$impresion.="<th colspan='2'>TRAZADORES</th>";
		    				$impresion.="<th>VALOR EVALUADO</th>";
		    				$impresion.="<th>CALIFICACION</th>";
		    				$impresion.="<th>PONDERACION</th>";
		    				$impresion.="<th>VALORACION PONDERADA</th>";
		    			$impresion.="</tr>";
		    			$impresion.="<tr>";
		    				$impresion.="<td colspan='2'>INIDICADORES TRAZADORES</td>";
		    				$impresion.="<td></td>";
		    				$impresion.="<td></td>";
		    				$impresion.="<td></td>";
		    				$impresion.="<td></td>";
		    			$impresion.="</tr>";
		    			$a=1;
		    			$total_valoracion_ponderada="";
		    			$ponderacion_total="";
		    			$sumatoria="";
		    			foreach ($aponderacion as $ke) {
		    				$impresion.="<tr class='aponderacion cuadro2' id_cuadro='2'>";
			    				$impresion.="<td>".$a."</td>";
			    				$impresion.="<td>".$ke->titulo."</td>";
			    				$suma=0;
								$promedio="";
								$i=0;
								foreach ($preguntas[$ke->id_cuadro] as $k) {
									if (array_key_exists($k,$respuestas[$key->id_llenado])) {
										$suma=$suma+$respuestas[$key->id_llenado][$k];
										$i++;
									}
								}
								$promedio=number_format($suma/$i, 2, '.', ' ');
								$valores_promedio[$key->id_llenado][$ke->id_cuadro]=$promedio;
								$calificacion_cuadro=calificaciones_final($calificaciones[$ke->id_cuadro],$promedio);
								$valoracion_ponderada=$promedio*$ke->ponderacion;
			    				$impresion.="<td>".$promedio."</td>";
			    				$impresion.="<td style='background-color:".$calificacion_cuadro["background"]."'>".$calificacion_cuadro["calificacion"]."</td>";
			    				$impresion.="<td>".$ke->ponderacion."</td>";
			    				$impresion.="<td>".$valoracion_ponderada."</td>";
		    				$impresion.="</tr>";
		    				$a++;
		    				$ponderacion_total=$ponderacion_total+$ke->ponderacion;
		    			}

		    			foreach ($aponderacion as $ke) {
		    				$valor_evaluado="";
							$valoracion_ponderada="";
							$total_valoracion_ponderada="";
							$ponderacion_total="";
							foreach ($aponderacion as $ke) {
								$valor_evaluado=$valores_promedio[$key->id_llenado][$ke->id_cuadro];
								$valoracion_ponderada=$valor_evaluado*$ke->ponderacion;
								$total_valoracion_ponderada=$total_valoracion_ponderada+$valoracion_ponderada;
								$ponderacion_total=$ponderacion_total+$ke->ponderacion;
							}
							$promedio_aponderacion=number_format($total_valoracion_ponderada/$ponderacion_total, 2, '.', ' ');
							$calificacion_cuadro=calificaciones_final($calificacion,$promedio_aponderacion);
		    			}
						$califi_fi=calificaciones_final($calificacion,$promedio_aponderacion);
			    		$impresion.="<tr>";
			    			$impresion.="<td colspan='2'>Sumatoria</td>";
			    			$impresion.="<td class='aponderacion_sumatoria'>".$sumatoria."</td>";
			    			$impresion.="<td></td>";
			    			$impresion.="<td class='aponderacion_valor_total'>".$ponderacion_total."</td>";
			    			$impresion.="<td>".$total_valoracion_ponderada."</td>";	    			
			    		$impresion.="</tr>";
			    		$impresion.="<tr>";
			    			$impresion.="<td colspan='2'>Promedio Global</td>";
			    			$impresion.="<td></td>";
			    			$impresion.="<td></td>";
			    			$impresion.="<td style='background-color:".$calificacion_cuadro["background"]."'>Promedio: ".$promedio_aponderacion."</td>";
			    			$impresion.="<td class='aponderacion_global' style='background-color:".$calificacion_cuadro["background"]."'>".$calificacion_cuadro["calificacion"]."</td>";
			    		$impresion.="</tr>";
			    	$impresion.="</tbody>";
		   	 	$impresion.="</table>";
		   	$impresion.="</div>";
	   	}
		return $impresion;
	}
	function exportar_detallado($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion){
		$impresion=[];
		$a=0;
		foreach ($encuestas as $key) {
			$impresion[$a]["id_llenado"]=$key->id_llenado;
			$impresion[$a]["fecha"]=$key->fecha;
			$impresion[$a]["localizacion"]=$key->departamento."-".$key->provincia."-".$key->distrito;
			$impresion[$a]["comunidad"]=$key->comunidad;
		    $promedio="";
		    $valores_promedio="";
		    $valoracion_ponderada="";
		    $i=1;
			foreach ($aponderacion as $ke) {
				$e=0;
				$suma="";
				foreach ($preguntas[$ke->id_cuadro] as $k) {
					if (array_key_exists($k,$respuestas[$key->id_llenado])) {
						$suma=$suma+$respuestas[$key->id_llenado][$k];
						$e++;
					}
				}
				$promedio=number_format($suma/$e, 2, '.', ' ');;
				$valores_promedio[$key->id_llenado][$ke->id_cuadro]=$promedio;
				$calificacion_cuadro=calificaciones_final($calificaciones[$ke->id_cuadro],$promedio);
				$valoracion_ponderada=$promedio*$ke->ponderacion;
				$impresion[$a]["result"][$i]["titulo"]=$ke->tab;
				$impresion[$a]["result"][$i]["promedio"]=$promedio;
				$impresion[$a]["result"][$i]["calificacion_cuadro"]=$calificacion_cuadro;
				$impresion[$a]["result"][$i]["ponderacion"]=$ke->ponderacion;
				$impresion[$a]["result"][$i]["valoracion_ponderada"]=$valoracion_ponderada;
				$i++;
			}
		    	$valor_evaluado="";
		    	$total_valoracion="";
				$valoracion_ponderada="";
				$total_valoracion_ponderada="";
				$ponderacion_total="";
				foreach ($aponderacion as $ke) {
					$valor_evaluado=$valores_promedio[$key->id_llenado][$ke->id_cuadro];
					$total_valoracion=$total_valoracion+$valor_evaluado;
					$valoracion_ponderada=$valor_evaluado*$ke->ponderacion;
					$total_valoracion_ponderada=$total_valoracion_ponderada+$valoracion_ponderada;
					$ponderacion_total=$ponderacion_total+$ke->ponderacion;
				}
				$promedio_aponderacion=number_format($total_valoracion_ponderada/$ponderacion_total, 2, '.', ' ');
				$calificacion_cuadro=calificaciones_final($calificacion,$promedio_aponderacion);
		    	$impresion[$a]["final"][$i]["sumatoria_evaluado"]=$total_valoracion;
				$impresion[$a]["final"][$i]["ponderacion"]=$ponderacion_total;
				$impresion[$a]["final"][$i]["sumatoria_ponderacion"]=$total_valoracion_ponderada;
				$impresion[$a]["final"][$i]["promedio_aponderacion"]=$promedio_aponderacion;
				$impresion[$a]["final"][$i]["calificacion_cuadro"]=$calificacion_cuadro;
			$a++;
		}
		return $impresion;
	}

?>
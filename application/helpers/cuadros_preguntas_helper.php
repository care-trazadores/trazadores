<?php
	function tabs($array){
		$tab="";
		$tab.="<ul class='tabscuadros'>";
		foreach ($array as $key) {
			if ($key->tipo==2 || $key->tipo==3 || $key->tipo==4 || $key->tipo==5) {
			}else{	
			$tab.="<li class='tabs".$key->id_cuadro."' key='".$key->id_cuadro."'>".$key->tab."</li>";	
			}
		}
		$tab.="</ul>";
		$tab.="<ul class='tabsreultados'>";
		foreach ($array as $key) {
			if ($key->tipo==2 || $key->tipo==3 || $key->tipo==1 || $key->tipo==0) {
			}else{	
			$tab.="<li class='tabs".$key->id_cuadro."' key='".$key->id_cuadro."'>".$key->tab."</li>";	
			}
		}
		$tab.="</ul>";
		return $tab;
	}
	function cuadrostipo($id_cuadro=null,$tipo=null,$titulo=null,
			$calificacion=null,$criterios=null,$criterios_detalle=null,
			$notas=null,$tablas=null,$preguntas=null,$alternativas=null,
			$respuestas=null,$aponderacion=null,$calificaciones=null,
			$anexo_cuadro=null,$anexo_pregunta=null){
		$impresion="";
		if ($tipo==0) {
			$impresion=cuadro_tipo_0($id_cuadro,$titulo,$tablas,$preguntas,$alternativas,$respuestas);
		}elseif ($tipo==1) {
			$impresion=cuadro_tipo_1($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$anexo_cuadro,$anexo_pregunta);
		}elseif ($tipo==2) {
			$impresion=cuadro_tipo_2($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$anexo_cuadro,$anexo_pregunta);
		}elseif ($tipo==3) {
			$impresion=cuadro_tipo_3($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$anexo_cuadro,$anexo_pregunta);
		}elseif ($tipo==4) {
			$impresion=cuadro_tipo_4($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$aponderacion,$calificaciones);
		}
		elseif ($tipo==5) {
			$impresion=cuadro_tipo_5($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$aponderacion,$calificaciones);
		}
		return $impresion;
	}
	function sumatoria($respuestas){
		$suma="";
		foreach ($respuestas as $res) {
			foreach ($res as $key ) {
				$suma=$suma+$key->respuesta;
			}
		}
		$total=$suma/count($respuestas);
		return number_format($total, 2, '.', ' ');
	}
	function calificaciones_final($calificacion,$promedio){
		$calificacion_final["background"]="";
		$calificacion_final["calificacion"]="";
		foreach ($calificacion as $key) {
				if ($key->igual!="") {
					if ($key->igual==$promedio) {
						$calificacion_final["background"]=$key->background;
						$calificacion_final["calificacion"]=$key->calificacion;
					}
				}elseif ($key->mayor!="" && $key->menor!="") {
					if ($promedio>=$key->mayor && $promedio<$key->menor) {
						$calificacion_final["background"]=$key->background;
						$calificacion_final["calificacion"]=$key->calificacion;
					}
				}elseif ($key->mayor!="" && $key->menor=="") {
					if ($promedio>=$key->mayor) {
						$calificacion_final["background"]=$key->background;
						$calificacion_final["calificacion"]=$key->calificacion;
					}
				}elseif ($key->menor!="" && $key->mayor=="") {
					if ($promedio<$key->menor) {
						$calificacion_final["background"]=$key->background;
						$calificacion_final["calificacion"]=$key->calificacion;
					}
				}				
			}
		return $calificacion_final;
	}
?>
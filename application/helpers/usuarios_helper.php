<?php
	function tabla_usuarios($usuarios,$base_url){
		$impresion="";
        $impresion.="<div class='col12'>";
          $impresion.="<div class='portlet light bordered'>";
            $impresion.="<div class='portlet-title'>";
                $impresion.="<div class='caption'>";
                    $impresion.="<i class='fa fa-area-chart font-green'></i>";
                    $impresion.="<span class='caption-subject font-green bold uppercase'>Usuarios</span>";
                    $impresion.="<span class='caption-helper'>Registrados en sus regiones...</span>";
                $impresion.="</div>";
                $impresion.="<!--<div class='actions'>";
                    $impresion.="<div class='btn-group btn-group-devided' data-toggle='buttons'>";
                        $impresion.="<label class='btn red btn-outline btn-circle btn-sm active'>";
                            $impresion.="<input type='radio' name='options' class='toggle' id='option1'>New</label>";
                        $impresion.="<label class='btn red btn-outline btn-circle btn-sm'>";
                            $impresion.="<input type='radio' name='options' class='toggle' id='option2'>Returning</label>";
                    $impresion.="</div>";
                $impresion.="</div>-->";
            $impresion.="</div>";
            $impresion.="<div id='tabla_usuarios class='portlet-body' style='overflow: auto;max-height: 400px;'>";
             $impresion.="<table class='listado'>";
				 $impresion.="<tr>";
				 	$impresion.="<th>Usuario</th>";
					$impresion.="<th>Nombres</th>";
					$impresion.="<th>Estado</th>";
					$impresion.="<th>Editar</th>";
					$impresion.="<th>Eliminar</th>";
				 $impresion.="</tr>";
				foreach ($usuarios as $key) {
				$impresion.="<tr id='user".$key->id_usuario."''>";
					$impresion.="<td>".$key->user."</td>";
					$impresion.="<td>".$key->nombre."</td>";
					if ($key->estado==1) {
						$impresion.="<td><i class='estado-ajax fa fa-check green' key='".$key->id_usuario."'></i></td>";
					}else{
						$impresion.="<td><i class='estado-ajax fa fa-check red' key='".$key->id_usuario."'></i></td>";
					}		
					$impresion.="<td><a href='".$base_url."index.php?c=usuarios&m=user_config&id_usuario=".$key->id_usuario."'><i class='fa fa-pencil-square-o'></a></i></td>";
			        $impresion.="<td><i class='delete_user fa fa-trash-o' key='".$key->id_usuario."'></i></i></td>";
				$impresion.="</tr>";
				}
			 $impresion.="</table>";
            $impresion.="</div>";
          $impresion.="</div>";
        $impresion.="</div>";
        return $impresion;
	}
?>
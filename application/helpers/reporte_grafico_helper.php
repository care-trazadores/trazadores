<?php
	function reporte_grafico($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion){
		$impresion="";
		foreach ($aponderacion as $key) {
			$impresion.="<div id='cuadro".$key->id_cuadro."' class='col12 graficos'>";
			$impresion.="<div id='grafico".$key->id_cuadro."'>";
			$impresion.="</div>";
				foreach ($calificaciones[$key->id_cuadro] as $cal) {
					$puntaje[$cal->calificacion]=0;
				}
				foreach ($encuestas as $ke) {
					$suma=0;
					$promedio="";
					$i=0;
					foreach ($preguntas[$key->id_cuadro] as $k) {
						if (array_key_exists($k,$respuestas[$ke->id_llenado])) {
							$suma=$suma+$respuestas[$ke->id_llenado][$k];
							$i++;
						}
					}
					$promedio=$suma/$i;

					$valores_promedio[$ke->id_llenado][$key->id_cuadro]=$promedio;

					$calificacion_cuadro=calificaciones_final($calificaciones[$key->id_cuadro],$promedio);
					$puntaje[$calificacion_cuadro["calificacion"]]=$puntaje[$calificacion_cuadro["calificacion"]]+1;
				}
				$e=0;
				$data="";
				$data.="[";
				foreach ($calificaciones[$key->id_cuadro] as $k) {
					if ($e==0) {
						$data.="{name: '".$k->calificacion."',color: '".$k->background."',y: ".$puntaje[$k->calificacion].",sliced: true,selected: true}";
					}else{
						$data.=",{name: '".$k->calificacion."',color: '".$k->background."',y: ".$puntaje[$k->calificacion]."}";
					}
					$e++;
				}
				$data.="]";
			$impresion.="</div>";
			$impresion.="<script>grafico_tipo_pie(".$key->id_cuadro.",".$data.",\"".$key->titulo."\")</script>";
		}
		$impresion.="<div id='cuadro11' class='col12 graficos'>";
		$impresion.="<div id='grafico11'>";
		$impresion.="</div>";
		$impresion.="</div>";
		foreach ($calificacion as $cal) {
			$cali[$cal->calificacion]=0;
		}
		foreach ($encuestas as $key) {
			$valor_evaluado="";
			$valoracion_ponderada="";
			$total_valoracion_ponderada="";
			$ponderacion_total="";
			foreach ($aponderacion as $ke) {
				$valor_evaluado=$valores_promedio[$key->id_llenado][$ke->id_cuadro];
				$valoracion_ponderada=$valor_evaluado*$ke->ponderacion;
				$total_valoracion_ponderada=$total_valoracion_ponderada+$valoracion_ponderada;
				$ponderacion_total=$ponderacion_total+$ke->ponderacion;
			}
			$promedio_aponderacion=$total_valoracion_ponderada/$ponderacion_total;
			$calificacion_cuadro=calificaciones_final($calificacion,$promedio_aponderacion);
			$cali[$calificacion_cuadro["calificacion"]]=$cali[$calificacion_cuadro["calificacion"]]+1;

		}
				$data="";
				$data.="[";
				$i=0;
				foreach ($calificacion as $k) {
					if ($i==0) {
						$data.="{name: '".$k->calificacion."',color: '".$k->background."',y: ".$cali[$k->calificacion].",sliced: true,selected: true}";
					}else{
						$data.=",{name: '".$k->calificacion."',color: '".$k->background."',y: ".$cali[$k->calificacion]."}";
					}
					$i++;
				}
				$data.="]";
		$impresion.="<script>grafico_tipo_pie(11,".$data.",\"Resultados globales\")</script>";
		return $impresion;
	}
	function exportar_grafico($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion){
		$impresion=[];
		$a=0;
		foreach ($aponderacion as $key) {
			$impresion[$a]["titulo"]=$key->titulo;
			foreach ($calificaciones[$key->id_cuadro] as $cal) {
				$puntaje[$cal->calificacion]=0;
			}
			foreach ($encuestas as $ke) {
				$suma=0;
				$promedio="";
				$i=0;
				foreach ($preguntas[$key->id_cuadro] as $k) {
					if (array_key_exists($k,$respuestas[$ke->id_llenado])) {
						$suma=$suma+$respuestas[$ke->id_llenado][$k];
						$i++;
					}
				}
				$promedio=$suma/$i;
				$valores_promedio[$ke->id_llenado][$key->id_cuadro]=$promedio;
				$calificacion_cuadro=calificaciones_final($calificaciones[$key->id_cuadro],$promedio);
				$puntaje[$calificacion_cuadro["calificacion"]]=$puntaje[$calificacion_cuadro["calificacion"]]+1;
			}
			$e=0;
			foreach ($calificaciones[$key->id_cuadro] as $k) {
				$impresion[$a]["calificacion"][$e]=$k->calificacion;
				$impresion[$a]["estadisticas"][$k->calificacion]=array($puntaje[$k->calificacion],$k->background);
				$impresion[$a]["promedio"][$e]=$puntaje[$k->calificacion];
				$e++;
			}
			$a++;
		}
		$impresion[$a]["titulo"]=utf8_decode("Resultados globales");
		foreach ($calificacion as $cal) {
			$cali[$cal->calificacion]=0;
		}
		foreach ($encuestas as $key) {
			$valor_evaluado="";
			$valoracion_ponderada="";
			$total_valoracion_ponderada="";
			$ponderacion_total="";
			foreach ($aponderacion as $ke) {
				$valor_evaluado=$valores_promedio[$key->id_llenado][$ke->id_cuadro];
				$valoracion_ponderada=$valor_evaluado*$ke->ponderacion;
				$total_valoracion_ponderada=$total_valoracion_ponderada+$valoracion_ponderada;
				$ponderacion_total=$ponderacion_total+$ke->ponderacion;
			}
			$promedio_aponderacion=$total_valoracion_ponderada/$ponderacion_total;
			$calificacion_cuadro=calificaciones_final($calificacion,$promedio_aponderacion);
			$cali[$calificacion_cuadro["calificacion"]]=$cali[$calificacion_cuadro["calificacion"]]+1;
		}
		$e=0;
		foreach ($calificacion as $k) {
			$impresion[$a]["calificacion"][$e]=utf8_decode($k->calificacion);
			$impresion[$a]["estadisticas"][$k->calificacion]=array($cali[$k->calificacion],$k->background);
			$impresion[$a]["promedio"][$e]=utf8_decode($cali[$k->calificacion]);
			$e++;
		}
		var_dump($impresion);
		//return $impresion;
	}

	function re_porgrafico($datos){
		$impresion="";
		foreach ($datos as $key=>$value) {
			$data="";
			$titulo="";
			$impresion.='<div id="cuadro'.$key.'" class="col12 graficos">';
			$impresion.='<div id="grafico'.$key.'">';
			$impresion.='</div>';
			$impresion.='</div>';
			$data="";
			$data.="[";
			$i=0;
			foreach ($value as $ke) {
				if ($i==0) {
					$data.="{name: '".$ke["calificacion"]."',color: '".$ke["background"]."',y: ".$ke["total"].",sliced: true,selected: true}";
				}else{
					$data.=",{name: '".$ke["calificacion"]."',color: '".$ke["background"]."',y: ".$ke["total"]."}";
				}
				$titulo=$ke["titulo"];
				$i++;
			}
			$data.="]";
			$impresion.='<script>grafico_tipo_pie('.$key.','.$data.',"'.$titulo.'")</script>';
		}
		return $impresion;
	}
	function ex_grafico($datos){
		$impresion="";
		$a=0;
		foreach ($datos as $key=>$value) {
			foreach ($value as $ke) {
				$impresion[$a]["titulo"]=$ke["titulo"];
				$impresion[$a]["calificacion"][]=$ke["calificacion"];
				$impresion[$a]["estadisticas"][$ke["calificacion"]]=array($ke["total"],$ke["background"]);
				$impresion[$a]["promedio"][]=$ke["total"];
			}
			$a++;
		}
		return $impresion;
	}

?>
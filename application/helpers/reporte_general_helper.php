<?php
	function reporte_general($encuestas=null,$preguntas=null,$respuestas=null){
		$impresion="";
		$impresion.="<div class='reporte_general'>";
			$impresion.="<table class='listado'>";
				$impresion.="<tr>";
					$impresion.="<th>Localización</th>";
					$impresion.="<th>Localidad</th>";
				foreach ($preguntas as $key) {
					if ($key->id_pregunta==49 || $key->id_pregunta==50) {
					}else{	
						$impresion.="<th>".$key->pregunta."</th>";
					}
				}
				$impresion.="</tr>";
			foreach ($encuestas as $key) {
				$impresion.="<tr>";
					$impresion.="<td>".$key->departamento."-".$key->provincia."-".$key->distrito."</td>";
					$impresion.="<td>".$key->comunidad."</td>";
					foreach ($preguntas as $ke) {
						$respuesta="---";
						if ($ke->id_pregunta==49 || $ke->id_pregunta==50) {
						}else{
							if (array_key_exists($ke->id_pregunta,$respuestas[$key->id_llenado])) {
								if ($respuestas[$key->id_llenado][$ke->id_pregunta]!="") {
									$respuesta=$respuestas[$key->id_llenado][$ke->id_pregunta];
								}
							}
							$impresion.="<td>".$respuesta."</td>";
						}
					}
				$impresion.="</tr>";
			}
			$impresion.="</table>";
		$impresion.="</div>";

		return $impresion;
	}
	function exportar_general($encuestas=null,$preguntas=null,$respuestas=null){
		$a=0;
		foreach ($encuestas as $key) {
			$impresion[$a]["id_llenado"]=$key->id_llenado;
			$impresion[$a]["comunidad"]=$key->comunidad;
			$impresion[$a]["Localizacion"]=$key->departamento."---".$key->provincia."---".$key->distrito;
			$e=0;
			foreach ($preguntas as $ke) {
				if ($ke->id_pregunta==49 || $ke->id_pregunta==50) {
				}else{
					$impresion[$a]["result"][$e]["titulo"]=$ke->pregunta;
					$impresion[$a]["result"][$e]["respuesta"]="---";
					if (array_key_exists($ke->id_pregunta,$respuestas[$key->id_llenado])) {
						if ($respuestas[$key->id_llenado][$ke->id_pregunta]!="") {
							$impresion[$a]["result"][$e]["respuesta"]=$respuestas[$key->id_llenado][$ke->id_pregunta];
						}
					}
				$e++;
				}
			}
			$a++;
		}
		return $impresion;
	}
?>

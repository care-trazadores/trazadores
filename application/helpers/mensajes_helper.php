<?php
	function admin($usuario,$pas,$depa,$prov,$dist,$nombres,$apellido_p,$apellido_m,$dni,$correo,$direccion,$telefono,$celular,$base_url){
		$mensaje="";
		$mensaje.='	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
				<html>
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<title>Mensaje desde {shop_name}</title>
						
						
						<style>	@media only screen and (max-width: 300px){ 
								body {
									width:218px !important;
									margin:auto !important;
								}
								.table {width:195px !important;margin:auto !important;}
								.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto !important;display: block !important;}		
								span.title{font-size:20px !important;line-height: 23px !important}
								span.subtitle{font-size: 14px !important;line-height: 18px !important;padding-top:10px !important;display:block !important;}		
								td.box p{font-size: 12px !important;font-weight: bold !important;}
								.table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr { 
									display: block !important; 
								}
								.table-recap{width: 200px!important;}
								.table-recap tr td, .conf_body td{text-align:center !important;}	
								.address{display: block !important;margin-bottom: 10px !important;}
								.space_address{display: none !important;}	
							}
					@media only screen and (min-width: 301px) and (max-width: 500px) { 
								body {width:308px!important;margin:auto!important;}
								.table {width:285px!important;margin:auto!important;}	
								.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}	
								.table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr { 
									display: block !important; 
								}
								.table-recap{width: 295px !important;}
								.table-recap tr td, .conf_body td{text-align:center !important;}
								
							}
					@media only screen and (min-width: 501px) and (max-width: 768px) {
								body {width:478px!important;margin:auto!important;}
								.table {width:450px!important;margin:auto!important;}	
								.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}			
							}
					@media only screen and (max-device-width: 480px) { 
								body {width:308px!important;margin:auto!important;}
								.table {width:285px;margin:auto!important;}	
								.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
								
								.table-recap{width: 295px!important;}
								.table-recap tr td, .conf_body td{text-align:center!important;}	
								.address{display: block !important;margin-bottom: 10px !important;}
								.space_address{display: none !important;}	
							}
				</style>

					</head>
					<body style="-webkit-text-size-adjust:none;background-color:#fff;width:650px;font-family:Open-sans, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
						<table class="table table-mail" style="width:100%;margin-top:10px;-moz-box-shadow:0 0 5px #afafaf;-webkit-box-shadow:0 0 5px #afafaf;-o-box-shadow:0 0 5px #afafaf;box-shadow:0 0 5px #afafaf;filter:progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5)">
							<tr>
								<td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
								<td align="center" style="padding:7px 0">
									<table class="table" bgcolor="#ffffff" style="width:100%">
										<tr>
									<td align="center" class="logo" style="border-bottom:4px solid #333333;padding:7px 0">
										<a title="trazadores" href="'.$base_url.'trazadores/" style="color:#337ff1">
											<img width="250" src="'.$base_url.'img/logo_pb.png" alt="agencia suiza" />
										</a>
									</td>
								</tr>

				<tr>
					<td align="center" class="titleblock" style="padding:7px 0">
						<font size="2" face="Open-sans, sans-serif" color="#555454">
							<span class="title" style="font-weight:500;font-size:28px;text-transform:uppercase;line-height:33px">Nueva solicitud de usuario</span>
						</font>
					</td>
				</tr>
				<tr>
					<td class="space_footer" style="padding:0!important">&nbsp;</td>
				</tr>
				<tr>
					<td class="box" style="border:1px solid #D6D4D4;background-color:#f8f8f8;padding:7px 0">
						<table class="table" style="width:100%">
							<tr>
								<td width="10" style="padding:7px 0">&nbsp;</td>
								<td style="padding:7px 0">
									<font size="2" face="Open-sans, sans-serif" color="#555454">
										<span style="color:#333"><strong>Usuario: </strong></span>'.$usuario.'<br />
										<span style="color:#333"><strong>distrito a administrar: </strong></span>'.$depa.'--'.$prov.'--'.$dist.'<br />
										<span style="color:#333"><strong>Nombres: </strong></span>'.$nombres.'<br />
										<span style="color:#333"><strong>Apellido paterno: </strong></span>'.$apellido_p.'<br />
										<span style="color:#333"><strong>Apellido materno: </strong></span>'.$apellido_m.'<br />
										<span style="color:#333"><strong>Dirección email: <a href="mailto:'.$correo.'" style="color:#337ff1">'.$correo.'</a></strong></span><br />
										<span style="color:#333"><strong>DNI: </strong></span>'.$dni.'<br />
										<span style="color:#333"><strong>Telefono: </strong></span>'.$telefono.'<br />
										<span style="color:#333"><strong>Celular: </strong></span>'.$celular.'<br />
									</font>
								</td>
								<td width="10" style="padding:7px 0">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>

										<tr>
											<td class="space_footer" style="padding:0!important">&nbsp;</td>
										</tr>
									</table>
								</td>
								<td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
							</tr>
						</table>
					</body>
				</html>
					';
		return $mensaje;
	}
	function usuario($usuario,$pas,$depa,$prov,$dist,$nombres,$apellido_p,$apellido_m,$dni,$correo,$direccion,$telefono,$celular,$base_url){
		$mensaje="";
		$mensaje.='<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
				<title>Mensaje desde {shop_name}</title>
				
				
				<style>	@media only screen and (max-width: 300px){ 
						body {
							width:218px !important;
							margin:auto !important;
						}
						.table {width:195px !important;margin:auto !important;}
						.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto !important;display: block !important;}		
						span.title{font-size:20px !important;line-height: 23px !important}
						span.subtitle{font-size: 14px !important;line-height: 18px !important;padding-top:10px !important;display:block !important;}		
						td.box p{font-size: 12px !important;font-weight: bold !important;}
						.table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr { 
							display: block !important; 
						}
						.table-recap{width: 200px!important;}
						.table-recap tr td, .conf_body td{text-align:center !important;}	
						.address{display: block !important;margin-bottom: 10px !important;}
						.space_address{display: none !important;}	
					}
			@media only screen and (min-width: 301px) and (max-width: 500px) { 
						body {width:308px!important;margin:auto!important;}
						.table {width:285px!important;margin:auto!important;}	
						.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}	
						.table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr { 
							display: block !important; 
						}
						.table-recap{width: 295px !important;}
						.table-recap tr td, .conf_body td{text-align:center !important;}
						
					}
			@media only screen and (min-width: 501px) and (max-width: 768px) {
						body {width:478px!important;margin:auto!important;}
						.table {width:450px!important;margin:auto!important;}	
						.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}			
					}
			@media only screen and (max-device-width: 480px) { 
						body {width:308px!important;margin:auto!important;}
						.table {width:285px;margin:auto!important;}	
						.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
						
						.table-recap{width: 295px!important;}
						.table-recap tr td, .conf_body td{text-align:center!important;}	
						.address{display: block !important;margin-bottom: 10px !important;}
						.space_address{display: none !important;}	
					}
		</style>

			</head>
			<body style="-webkit-text-size-adjust:none;background-color:#fff;width:650px;font-family:Open-sans, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
				<table class="table table-mail" style="width:100%;margin-top:10px;-moz-box-shadow:0 0 5px #afafaf;-webkit-box-shadow:0 0 5px #afafaf;-o-box-shadow:0 0 5px #afafaf;box-shadow:0 0 5px #afafaf;filter:progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5)">
					<tr>
						<td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
						<td align="center" style="padding:7px 0">
							<table class="table" bgcolor="#ffffff" style="width:100%">
								<tr>
									<td align="center" class="logo" style="border-bottom:4px solid #333333;padding:7px 0">
										<a title="trazadores" href="'.$base_url.'trazadores/" style="color:#337ff1">
											<img width="250" src="'.$base_url.'img/logo_pb.png" alt="agencia suiza" />
										</a>
									</td>
								</tr>

		<tr>
			<td align="center" class="titleblock" style="padding:7px 0">
				<font size="2" face="Open-sans, sans-serif" color="#555454">
					<span class="title" style="font-weight:500;font-size:28px;text-transform:uppercase;line-height:33px">Hola '.$nombres.' '.$apellido_p.',</span><br/>
					<span class="subtitle" style="font-weight:500;font-size:16px;text-transform:uppercase;line-height:25px">Gracias por crear una cuenta en nuestro sistema trazadores.</span>
				</font>
			</td>
		</tr>
		<tr>
			<td class="space_footer" style="padding:0!important">&nbsp;</td>
		</tr>
		<tr>
			<td class="box" style="border:1px solid #D6D4D4;background-color:#f8f8f8;padding:7px 0">
				<table class="table" style="width:100%">
					<tr>
						<td width="10" style="padding:7px 0">&nbsp;</td>
						<td style="padding:7px 0">
							<font size="2" face="Open-sans, sans-serif" color="#555454">
								<p data-html-only="1" style="border-bottom:1px solid #D6D4D4;margin:3px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px">
									Detalles de su cuenta</p>
								<span style="color:#777">
									Estos son sus datos de acceso:<br /> 
									<span style="color:#333"><strong>Usuario: </strong></span>'.$usuario.'<br />
									<span style="color:#333"><strong>password: </strong></span>'.$pas.'<br />
									<span style="color:#333"><strong>distrito a administrar: </strong></span>'.$dist.'<br />
									<span style="color:#333"><strong>Dirección email: <a href="mailto:'.$correo.'" style="color:#337ff1">'.$correo.'</a></strong></span><br />
									<span style="color:#333"><strong>DNI: </strong></span>'.$dni.'<br />
									<span style="color:#333"><strong>Telefono: </strong></span>'.$telefono.'<br />
									<span style="color:#333"><strong>Celular: </strong></span>'.$celular.'<br />

								</span>
							</font>
						</td>
						<td width="10" style="padding:7px 0">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="space_footer" style="padding:0!important">&nbsp;</td>
		</tr>
		<tr>
			<td class="box" style="border:1px solid #D6D4D4;background-color:#f8f8f8;padding:7px 0">
				<table class="table" style="width:100%">
					<tr>
						<td width="10" style="padding:7px 0">&nbsp;</td>
						<td style="padding:7px 0">
							<font size="2" face="Open-sans, sans-serif" color="#555454">
								<p style="border-bottom:1px solid #D6D4D4;margin:3px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px">Consejos de Seguridad:</p>
								<ol style="margin-bottom:0">
									<li>Se activará su cuenta después de confirmar los datos.</li>
									<li>Mantenga los datos de su cuenta en un lugar seguro.</li>
									<li>No comparta los detalles de su cuenta con otras personas.</li>
									<li>Cambie su clave regularmente.</li>
									<li>Si sospecha que alguien está usando ilegalmente su cuenta, avísenos inmediatamente.</li>
								</ol>
							</font>
						</td>
						<td width="10" style="padding:7px 0">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="space_footer" style="padding:0!important">&nbsp;</td>
		</tr>

								<tr>
									<td class="space_footer" style="padding:0!important">&nbsp;</td>
								</tr>
							</table>
						</td>
						<td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
					</tr>
				</table>
			</body>
		</html>';
		return $mensaje;
	}
?>
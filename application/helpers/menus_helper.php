<?php
	function menus($base_url,$controlador,$tipo){
		$selected="";
		if ($controlador=='inicio') {$selected='selected';}else{$selected='';}
		$cadena="<a href='".$base_url."index.php?c=inicio'><li class='".$selected."'>Inicio</li></a>";
		if ($tipo!="usuario") {
			if ($controlador=='usuarios') {$selected='selected';}else{$selected='';}
			$cadena=$cadena."<a href='".$base_url."index.php?c=usuarios'><li class='".$selected."'>Usuarios</li></a>";		
		}
		if ($tipo=="master") {
			if ($controlador=='administradores') {$selected='selected';}else{$selected='';}
			$cadena=$cadena."<a href='".$base_url."index.php?c=administradores'><li class='".$selected."'>Administradores</li></a>";	
		}
		if ($controlador=='encuestas') {$selected='selected';}else{$selected='';}
		$cadena=$cadena."<a href='".$base_url."index.php?c=encuestas'><li class='".$selected."'>Encuestas</li></a>";
		if ($controlador=='reportes') {$selected='selected';}else{$selected='';}
		$cadena=$cadena."<a href='".$base_url."index.php?c=reportes'><li class='".$selected."'>Reportes</li></a>";
		if ($tipo=="master") {
			if ($controlador=='distritos') {$selected='selected';}else{$selected='';}
			$cadena=$cadena."<a href='".$base_url."index.php?c=distritos'><li class='".$selected."'>Distritos</li></a>";		
		}
		if ($tipo!="usuario") {
			if ($controlador=='municipal') {$selected='selected';}else{$selected='';}
			$cadena=$cadena."<a href='".$base_url."index.php?c=municipal'><li class='".$selected."'>Area municipal</li></a>";
		}
		if ($tipo!="usuario") {
			if ($controlador=='log') {$selected='selected';}else{$selected='';}
			$cadena=$cadena."<a href='".$base_url."index.php?c=log'><li class='".$selected."'>LOG</li></a>";
		}
		/*if ($tipo=="master") {

			if ($controlador=='desarrollo') {$selected='selected';}else{$selected='';}

			$cadena=$cadena."<a href='".$base_url."index.php?c=desarrollo'><li class='".$selected."'>Desarrollo</li></a>";		

		}*/
		return $cadena;
	}
	function menus_mobile($base_url,$controlador,$tipo){
		$selected="";
		if ($controlador=='inicio') {$selected='selected';}else{$selected='';}
		$cadena="<option ".$selected." value='".$base_url."index.php?c=inicio'>Dashboard</option>";
		if ($tipo!="usuario") {
			if ($controlador=='usuarios') {$selected='selected';}else{$selected='';}
			$cadena=$cadena."<option ".$selected." value='".$base_url."index.php?c=inicio'>Usuarios</option>";	
		}
		if ($tipo=="master") {
			if ($controlador=='administradores') {$selected='selected';}else{$selected='';}
			$cadena=$cadena."<option ".$selected." value='".$base_url."index.php?c=inicio'>Administradores</option>";
		}
		if ($controlador=='encuestas') {$selected='selected';}else{$selected='';}
		$cadena=$cadena."<option ".$selected." value='".$base_url."index.php?c=inicio'>Encuestas</option>";
		if ($controlador=='reportes') {$selected='selected';}else{$selected='';}
		$cadena=$cadena."<option ".$selected." value='".$base_url."index.php?c=inicio'>Reportes</option>";
		if ($tipo=="master") {
			if ($controlador=='distritos') {$selected='selected';}else{$selected='';}
			$cadena=$cadena."<option ".$selected." value='".$base_url."index.php?c=inicio'>Distritos</option>";
		}
		return $cadena;
	}

	function nav($base_url=null,$nombre=null,$apellido_p=null,$foto=null,$id_usuario=null){
		$cadena="";
		$cadena.="<div class='page-logo'>";
            //$cadena.="<a href='".$base_url."index.php?c=inicio'>";
              //  $cadena.="<img src='".$base_url."img/logo_yainso2.png' alt='logo' class='logo-default' style='width:86px'> </a>";
            //$cadena.="<div class='menu-toggler sidebar-toggler'></div>";
        $cadena.="</div>";
        $cadena.="<div class='top-menu'>";
            $cadena.="<ul class='nav navbar-nav pull-right'>";
                /*   $cadena.="<li class='dropdown dropdown-extended dropdown-notification' id='header_notification_bar'>";
                    $cadena.="<a href='javascript:;'' class='dropdown-toggle' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' aria-expanded='false'>";

                        $cadena.="<i class='icon-bell'></i>";

                        $cadena.="<span class='badge badge-default'> 7 </span>";

                    $cadena.="</a>";

	                $cadena.="<ul class='dropdown-menu'>";

	                    $cadena.="<li class='external'>";

	                       	$cadena.="<h3><span class='bold'>12 pending</span> notifications</h3>";

	                            $cadena.="<a href='page_user_profile_1.html'>view all</a>";

	                    $cadena.="</li>";

	                    $cadena.="<li>";

	                        $cadena.="<div class='slimScrollDiv' style='position: relative; overflow: hidden; width: auto; height: 250px;''>";

		                        $cadena.="<ul class='dropdown-menu-list scroller' style='height: 250px; overflow: hidden; width: auto;overflow:auto;' data-handle-color='#637283' data-initialized='1'>";

				                    $cadena.="<li>";

				                        $cadena.="<a href='javascript:;''>";

				                            $cadena.="<span class='time'>just now</span>";

				                            $cadena.="<span class='details'>";

				                                $cadena.="<span class='label label-sm label-icon label-success'>";

				                                    $cadena.="<i class='fa fa-plus'></i>";

				                                $cadena.="</span> New user registered. </span>";

				                        $cadena.="</a>";

				                    $cadena.="</li>";

				                    $cadena.="<li>";

				                        $cadena.="<a href='javascript:;''>";

				                            $cadena.="<span class='time'>3 mins</span>";

				                            $cadena.="<span class='details'>";

				                                $cadena.="<span class='label label-sm label-icon label-danger'>";

				                                    $cadena.="<i class='fa fa-bolt'></i>";

				                                $cadena.="</span> Server #12 overloaded. </span>";

				                        $cadena.="</a>";

				                    $cadena.="</li>";

				                    $cadena.="<li>";

				                       	$cadena.="<a href='javascript:;'>";

				                           	$cadena.="<span class='time'>10 mins</span>";

				                           	$cadena.="<span class='details'>";

				                               	$cadena.="<span class='label label-sm label-icon label-warning'>";

				                                   	$cadena.="<i class='fa fa-bell-o'></i>";

				                               	$cadena.="</span> Server #2 not responding. </span>";

				                       	$cadena.="</a>";

				                   	$cadena.="</li>";

				                   	$cadena.="<li>";

				                       	$cadena.="<a href='javascript:;'>";

				                           	$cadena.="<span class='time'>14 hrs</span>";

				                           	$cadena.="<span class='details'>";

				                               	$cadena.="<span class='label label-sm label-icon label-info'>";

				                                   	$cadena.="<i class='fa fa-bullhorn'></i>";

				                               	$cadena.="</span> Application error. </span>";

				                       	$cadena.="</a>";

				                   	$cadena.="</li>";

				                    $cadena.="<li>";

				                        $cadena.="<a href='javascript:;'>";

				                            $cadena.="<span class='time'>4 days</span>";

				                            $cadena.="<span class='details'>";

				                                $cadena.="<span class='label label-sm label-icon label-warning'>";

				                                    $cadena.="<i class='fa fa-bell-o'></i>";

				                                $cadena.="</span> Storage Server #4 not responding dfdfdfd. </span>";

				                        $cadena.="</a>";

				                    $cadena.="</li>";

				                    $cadena.="<li>";

				                        $cadena.="<a href='javascript:;'>";

				                            $cadena.="<span class='time'>5 days</span>";

				                            $cadena.="<span class='details'>";

				                                $cadena.="<span class='label label-sm label-icon label-info'>";

				                                    $cadena.="<i class='fa fa-bullhorn'></i>";

				                                $cadena.="</span> System Error. </span>";

				                        $cadena.="</a>";

				                    $cadena.="</li>";

		                		$cadena.="</ul>";

	               		 	$cadena.="</div>";

			        	$cadena.="</li>";

			    	$cadena.="</ul>";

				$cadena.="</li>";

                $cadena.="<li class='dropdown dropdown-extended dropdown-inbox' id='header_inbox_bar'>";

                    $cadena.="<a href='javascript:;' class='dropdown-toggle' data-toggle='dropdown' data-hover='dropdown' data-close-others='true'>";

                        $cadena.="<i class='icon-envelope-open'></i>";

                        $cadena.="<span class='badge badge-default'> 4 </span>";

                    $cadena.="</a>";

                    $cadena.="<ul class='dropdown-menu'>";

                        $cadena.="<li class='external'>";

                            $cadena.="<h3>You have";

                                $cadena.="<span class='bold'>7 New</span> Messages</h3>";

                            $cadena.="<a href='app_inbox.html'>view all</a>";

                        $cadena.="</li>";

                        $cadena.="<li>";

                            $cadena.="<div class='slimScrollDiv' style='position: relative; overflow: hidden; width: auto; height: 275px;'>";

                            	$cadena.="<ul class='dropdown-menu-list scroller' style='height: 275px; overflow: hidden; width: auto;overflow:auto;' data-handle-color='#637283' data-initialized='1'>";

	                                $cadena.="<li>";

	                                    $cadena.="<a href='#''>";

	                                        $cadena.="<span class='photo'>";

	                                            $cadena.="<img src='".$base_url."img/usuarios/team1.jpg' class='img-circle' alt=''> </span>";

	                                        $cadena.="<span class='subject'>";

	                                            $cadena.="<span class='from'> Lisa Wong </span>";

	                                            $cadena.="<span class='time'>Just Now </span>";

	                                        $cadena.="</span>";

	                                        $cadena.="<span class='message'> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>";

	                                    $cadena.="</a>";

	                                $cadena.="</li>";

                                    $cadena.="<li>";

                                        $cadena.="<a href='#''>";

                                           $cadena.="<span class='photo'>";

                                                $cadena.="<img src='".$base_url."img/usuarios/team2.jpg' class='img-circle' alt=''> </span>";

                                            $cadena.="<span class='subject'>";

                                                $cadena.="<span class='from'> Richard Doe </span>";

                                                $cadena.="<span class='time'>16 mins </span>";

                                            $cadena.="</span>";

                                            $cadena.="<span class='message'> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>";

                                        $cadena.="</a>";

                                    $cadena.="</li>";

                                    $cadena.="<li>";

                                        $cadena.="<a href='#''>";

                                            $cadena.="<span class='photo'>";

                                                $cadena.="<img src='".$base_url."img/usuarios/team5.jpg' class='img-circle' alt=''> </span>";

                                            $cadena.="<span class='subject'>";

                                                $cadena.="<span class='from'> Bob Nilson </span>";

                                                $cadena.="<span class='time'>2 hrs </span>";

                                            $cadena.="</span>";

                                            $cadena.="<span class='message'> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>";

                                        $cadena.="</a>";

                                    $cadena.="</li>";

                                    $cadena.="<li>";

                                        $cadena.="<a href='#''>";

                                            $cadena.="<span class='photo'>";

                                                $cadena.="<img src='../assets/layouts/layout3/img/avatar2.jpg' class='img-circle' alt=''> </span>";

                                            $cadena.="<span class='subject'>";

                                                $cadena.="<span class='from'> Lisa Wong </span>";

                                                $cadena.="<span class='time'>40 mins </span>";

                                            $cadena.="</span>";

                                            $cadena.="<span class='message'> Vivamus sed auctor 40% nibh congue nibh... </span>";

                                        $cadena.="</a>";

                                    $cadena.="</li>";

                                    $cadena.="<li>";

                                        $cadena.="<a href='#''>";

                                            $cadena.="<span class='photo'>";

                                                $cadena.="<img src='../assets/layouts/layout3/img/avatar3.jpg' class='img-circle' alt=''> </span>";

                                            $cadena.="<span class='subject'>";

                                                $cadena.="<span class='from'> Richard Doe </span>";

                                                $cadena.="<span class='time'>46 mins </span>";

                                            $cadena.="</span>";

                                            $cadena.="<span class='message'> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>";

                                        $cadena.="</a>";

                                    $cadena.="</li>";

                                $cadena.="</ul>";

                            $cadena.="</div>";

                        $cadena.="</li>";

                    $cadena.="</ul>";

                $cadena.="</li>";

           		<li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">

                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">

                                <i class="icon-calendar"></i>

                                <span class="badge badge-default"> 3 </span>

                            </a>

                            <ul class="dropdown-menu extended tasks">

                                <li class="external">

                                    <h3>You have

                                        <span class="bold">12 pending</span> tasks</h3>

                                    <a href="app_todo.html">view all</a>

                                </li>

                                <li>

                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 275px;"><ul class="dropdown-menu-list scroller" style="height: 275px; overflow: hidden; width: auto;" data-handle-color="#637283" data-initialized="1">

                                        <li>

                                            <a href="javascript:;">

                                                <span class="task">

                                                    <span class="desc">New release v1.2 </span>

                                                    <span class="percent">30%</span>

                                                </span>

                                                <span class="progress">

                                                    <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">

                                                        <span class="sr-only">40% Complete</span>

                                                    </span>

                                                </span>

                                            </a>

                                        </li>

                                        <li>

                                            <a href="javascript:;">

                                                <span class="task">

                                                    <span class="desc">Application deployment</span>

                                                    <span class="percent">65%</span>

                                                </span>

                                                <span class="progress">

                                                    <span style="width: 65%;" class="progress-bar progress-bar-danger" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">

                                                        <span class="sr-only">65% Complete</span>

                                                    </span>

                                                </span>

                                            </a>

                                        </li>

                                        <li>

                                            <a href="javascript:;">

                                                <span class="task">

                                                    <span class="desc">Mobile app release</span>

                                                    <span class="percent">98%</span>

                                                </span>

                                                <span class="progress">

                                                    <span style="width: 98%;" class="progress-bar progress-bar-success" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">

                                                        <span class="sr-only">98% Complete</span>

                                                    </span>

                                                </span>

                                            </a>

                                        </li>

                                        <li>

                                            <a href="javascript:;">

                                                <span class="task">

                                                    <span class="desc">Database migration</span>

                                                    <span class="percent">10%</span>

                                                </span>

                                                <span class="progress">

                                                    <span style="width: 10%;" class="progress-bar progress-bar-warning" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">

                                                        <span class="sr-only">10% Complete</span>

                                                    </span>

                                                </span>

                                            </a>

                                        </li>

                                        <li>

                                            <a href="javascript:;">

                                                <span class="task">

                                                    <span class="desc">Web server upgrade</span>

                                                    <span class="percent">58%</span>

                                                </span>

                                                <span class="progress">

                                                    <span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">

                                                        <span class="sr-only">58% Complete</span>

                                                    </span>

                                                </span>

                                            </a>

                                        </li>

                                        <li>

                                            <a href="javascript:;">

                                                <span class="task">

                                                    <span class="desc">Mobile development</span>

                                                    <span class="percent">85%</span>

                                                </span>

                                                <span class="progress">

                                                    <span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">

                                                        <span class="sr-only">85% Complete</span>

                                                    </span>

                                                </span>

                                            </a>

                                        </li>

                                        <li>

                                            <a href="javascript:;">

                                                <span class="task">

                                                    <span class="desc">New UI release</span>

                                                    <span class="percent">38%</span>

                                                </span>

                                                <span class="progress progress-striped">

                                                    <span style="width: 38%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">

                                                        <span class="sr-only">38% Complete</span>

                                                    </span>

                                                </span>

                                            </a>

                                        </li>

                                    </ul><div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 147.994px; background: rgb(99, 114, 131);"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div></div>

                                </li>

                            </ul>

                </li>*/

               	$cadena.="<li class='dropdown dropdown-user'>";

                   	$cadena.="<a href='javascript:;' class='dropdown-toggle' data-toggle='dropdown' data-hover='dropdown' data-close-others='true'>";

                   	if ($foto!="") {

                   		$cadena.="<img alt='' class='img-circle' src='".$base_url."img/usuarios/".$foto."'>";

					}else{

						$cadena.="<img alt='' class='img-circle' src='".$base_url."img/usuarios/usu.jpg'>";

					}

                       	

                       	$cadena.="<span class='username username-hide-on-mobile'>".$nombre." ".$apellido_p."</span>";

                       	$cadena.="<i class='fa fa-angle-down'></i>";

                   	$cadena.="</a>";

                   	$cadena.="<ul class='dropdown-menu dropdown-menu-default'>";

                       	$cadena.="<li>";

                           	$cadena.="<a href='".$base_url."index.php?c=cuenta&m=user_config&id_usuario=".$id_usuario."'>";

                               	$cadena.="<i class='icon-user'></i> Mi Perfil </a>";

                       	$cadena.="</li>";

                       	/*$cadena.="<li>";

                           	$cadena.="<a href='app_calendar.html'>";

                               	$cadena.="<i class='icon-calendar'></i> My Calendar </a>";

                       	$cadena.="</li>";

                       	$cadena.="<li>";

                          	$cadena.="<a href='app_inbox.html'>";

                               	$cadena.="<i class='icon-envelope-open'></i> My Inbox";

                               	$cadena.="<span class='badge badge-danger'> 3 </span>";

                           	$cadena.="</a>";

                       	$cadena.="</li>";

                       	$cadena.="<li>";

                           	$cadena.="<a href='app_todo.html'>";

                               	$cadena.="<i class='icon-rocket'></i> My Tasks";

                               	$cadena.="<span class='badge badge-success'> 7 </span>";

                           	$cadena.="</a>";

                       	$cadena.="</li>";

                       	$cadena.="<li class='divider'> </li>";

                       	$cadena.="<li>";

                           	$cadena.="<a href='page_user_lock_1.html'>";

                               	$cadena.="<i class='icon-lock'></i> Lock Screen </a>";

                       	$cadena.="</li>";*/

                       	$cadena.="<li>";

                           	$cadena.="<a href='".$base_url."index.php?c=login&m=close'>";

                               	$cadena.="<i class='icon-key'></i> Log Out </a>";

                      	$cadena.="</li>";

                   	$cadena.="</ul>";

               	$cadena.="</li>";

                $cadena.="<li class='dropdown dropdown-quick-sidebar-toggler'>";

                    $cadena.="<a href='".$base_url."index.php?c=login&m=close' class='dropdown-toggle'>";

                        $cadena.="<i class='icon-logout'></i>";

                    $cadena.="</a>";

                $cadena.="</li>";

            $cadena.="</ul>";

        $cadena.="</div>";

		return $cadena;

	}

?>

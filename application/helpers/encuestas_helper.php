<?php
	function encuestas_graficos($encuestas){
        $anos=[];
        $datagra=[];
        foreach ($encuestas as $key) {
            list($anio, $mes, $dia) = explode("-",$key->fecha);
            if (in_array($anio, $anos)) {
            }else{
                $anos[]=$anio;
                for ($i=1; $i <13 ; $i++) { 
                    if ($i<10) {
                        $pos="0".$i;
                    }else{$pos=$i;}
                    $cate[$anio][$pos]=0;
                }
            }
        }
        foreach ($encuestas as $key) {
            list($anio, $mes, $dia) = explode("-",$key->fecha);
            $cate[$anio][$mes]=$cate[$anio][$mes]+1;
        }
        foreach ($anos as $key) {
            $data["data"]="";
            $data["name"]=$key;
            foreach ($cate[$key] as $ke) {
                $data["data"][]=$ke;
            }
            $datagra[]=$data;
        }
        $data_grafico=json_encode($datagra);
        $categorias=json_encode(array("categories"=>array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago','Sep','Oct','Nov','Dic')));
        
		$impresion="";
		$impresion.="<div class='col6'>";
          $impresion.="<div class='portlet light bordered'>";
            $impresion.="<div class='portlet-title'>";
                $impresion.="<div class='caption'>";
                    $impresion.="<i class='fa fa-area-chart font-green'></i>";
                    $impresion.="<span class='caption-subject font-green bold uppercase'>Encuestas</span>";
                    $impresion.="<span class='caption-helper'>Vista por año y mes...</span>";
                $impresion.="</div>";
                $impresion.="<!--<div class='actions'>";
                    $impresion.="<div class='btn-group btn-group-devided' data-toggle='buttons'>";
                        $impresion.="<label class='btn red btn-outline btn-circle btn-sm active'>";
                            $impresion.="<input type='radio' name='options' class='toggle' id='option1'>New</label>";
                        $impresion.="<label class='btn red btn-outline btn-circle btn-sm'>";
                            $impresion.="<input type='radio' name='options' class='toggle' id='option2'>Returning</label>";
                    $impresion.="</div>";
                $impresion.="</div>-->";
            $impresion.="</div>";
            $impresion.="<div id='grafico_encuesta' class='portlet-body'>";           
            $impresion.="</div>";
          $impresion.="</div>";
        $impresion.="</div>";
        $impresion.="<script type='text/javascript'>";
        $impresion.="grafico_line(\"grafico_encuesta\",".$categorias.",".$data_grafico.")";
        $impresion.="</script>";
        return $impresion;
	}
	function encuestas_tabla_comunidad($encuestas,$base_url){
        $impresion="";
        $impresion.="<div class='col6'>";
          $impresion.="<div class='portlet light bordered'>";
            $impresion.="<div class='portlet-title'>";
                $impresion.="<div class='caption'>";
                    $impresion.="<i class='fa fa-area-chart font-green'></i>";
                    $impresion.="<span class='caption-subject font-green bold uppercase'>Localidades</span>";
                    $impresion.="<span class='caption-helper'>Localidades registradas...</span>";
                $impresion.="</div>";
                $impresion.="<!--<div class='actions'>";
                    $impresion.="<div class='btn-group btn-group-devided' data-toggle='buttons'>";
                        $impresion.="<label class='btn red btn-outline btn-circle btn-sm active'>";
                            $impresion.="<input type='radio' name='options' class='toggle' id='option1'>New</label>";
                        $impresion.="<label class='btn red btn-outline btn-circle btn-sm'>";
                            $impresion.="<input type='radio' name='options' class='toggle' id='option2'>Returning</label>";
                    $impresion.="</div>";
                $impresion.="</div>-->";
            $impresion.="</div>";
            $impresion.="<div id='tabla_comunidad' class='portlet-body'>";
                $impresion.="<ul style='overflow: auto;max-height: 400px;'>";
                if ($encuestas!=null) {
                    foreach ($encuestas as $key) {
                       $impresion.="<a href='".$base_url."index.php?c=encuestas&m=edit&id=".$key->id_llenado."'><li>".$key->comunidad."</li></a>";
                    }
                }else{
                    $impresion.="Lo sentimos no se encontraron datos";
                }
                
                $impresion.="</ul>";           
            $impresion.="</div>";
          $impresion.="</div>";
        $impresion.="</div>";
        return $impresion;
    }
?>
<?php
	function reporte_jazz($encuestas=null,$preguntas=null,$respuestas=null,$calificacion=null){
		$impresion="";
		$impresion.="<div class='reporte_general'>";
			$impresion.="<table class='listado'>";
				$impresion.="<tr>";
					$impresion.="<th>Localización</th>";
					$impresion.="<th>Comunidad</th>";
				foreach ($preguntas as $key) {
					$impresion.="<th>".$key->pregunta."</th>";
				}
				$impresion.="<th>Calificación Final</th>";
				$impresion.="</tr>";
			foreach ($encuestas as $key) {
				$impresion.="<tr>";
					$impresion.="<td>".$key->departamento."-".$key->provincia."-".$key->distrito."</td>";
					$impresion.="<td>".$key->comunidad."</td>";
					$suma=0;
					$respuesta="---";
					foreach ($preguntas as $ke) {
						if (array_key_exists($ke->id_pregunta,$respuestas[$key->id_llenado]["respuesta"])) {
							if ($respuestas[$key->id_llenado]["respuesta"][$ke->id_pregunta]!="") {
								$respuesta=$respuestas[$key->id_llenado]["texto"][$ke->id_pregunta];
								$suma=$suma+$respuestas[$key->id_llenado]["respuesta"][$ke->id_pregunta];
							}
						}
						$impresion.="<td>".$respuesta."</td>";
					}
					if ($suma!="") {
						$promedio=$suma;
						$calificacion_jazz=calificaciones_final($calificacion,$promedio);
					}else{
						$calificacion_jazz["background"]="---";
						$calificacion_jazz["calificacion"]="---";
					}
					$impresion.="<td style='background-color:".$calificacion_jazz["background"]."'>".$calificacion_jazz["calificacion"]."</td>";
				$impresion.="</tr>";
			}
			$impresion.="</table>";
		$impresion.="</div>";

		return $impresion;
	}
	function exportar_jazz($encuestas=null,$preguntas=null,$respuestas=null,$calificacion=null){
		$a=0;
		foreach ($encuestas as $key) {
			$impresion[$a]["id_llenado"]=$key->id_llenado;
			$impresion[$a]["comunidad"]=$key->comunidad;
			$impresion[$a]["Localizacion"]=$key->departamento."-".$key->provincia."-".$key->distrito;
			$e=0;
			$suma="";
			foreach ($preguntas as $ke) {
				$impresion[$a]["result"][$e]["titulo"]=$ke->pregunta;
				$impresion[$a]["result"][$e]["respuesta"]="---";
				if (array_key_exists($ke->id_pregunta,$respuestas[$key->id_llenado]["respuesta"])) {
					if ($respuestas[$key->id_llenado]["texto"][$ke->id_pregunta]!="") {
						$impresion[$a]["result"][$e]["respuesta"]=$respuestas[$key->id_llenado]["texto"][$ke->id_pregunta];
						$suma=$suma+$respuestas[$key->id_llenado]["respuesta"][$ke->id_pregunta];
					}
				}
				$e++;
			}
			$impresion[$a]["cas"][$e]["titulo"]="Calificación final";
			if ($suma!="") {
				$promedio=$suma;
				$calificacion_jazz=calificaciones_final($calificacion,$promedio);
				//$impresion[$a]["result"][$e]["respuesta"]=$calificacion_jazz["calificacion"];
				$impresion[$a]["cas"][$e]["cali"]["background"]=$calificacion_jazz["background"];
				$impresion[$a]["cas"][$e]["cali"]["calificacion"]=$calificacion_jazz["calificacion"];
			}else{
				//$impresion[$a]["result"][$e]["respuesta"]="---";
				$impresion[$a]["cas"][$e]["cali"]["background"]="#ffffff";
				$impresion[$a]["cas"][$e]["cali"]["calificacion"]="---";
				$suma="";
			}
			$a++;
		}
		return $impresion;
	}
?>

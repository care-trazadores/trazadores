<?php
	function cuadro_tipo_0($id_cuadro,$titulo,$tablas,$preguntas,$alternativas,$respuestas){
		//var_dump($tablas);
		//echo "<br><br><br>";
		//var_dump($preguntas);
		$impresion='';
		$impresion.="<div id='tab".$id_cuadro."' class='tabs'>";
		$impresion.="<h3>".$titulo."</h3>";
		$impresion.="<div class='col12'>";
		$impresion.="<div class='row'>";
		foreach ($tablas as $key) {
			if ($key->tipo==1) {
				$impresion.="<div class='col6'>";
				$impresion.="<div id='coordenadas'>";
				$impresion.="<table class='listado border'>";
				$impresion.="<tr>";
				$impresion.="<th class='center' colspan='3'>Georeferenciación del centro poblado</th>";
				$impresion.="</tr>";
				$impresion.="<tr>";
				$impresion.="<td colspan='2' class='center'>Coordenadas UTM</td>";
				$impresion.="<td rowspan='2' class='center'>Altitud msnm</td>";
				$impresion.="</tr>";
				$impresion.="<tr>";
				$impresion.="<td class='center'>Norte</td>";
				$impresion.="<td class='center'>Este</td>";
				$impresion.="</tr>";
				$impresion.="<tr>";
				foreach ($preguntas as $ke) {
					if ($ke->id_tabla==$key->id_tabla) {
						if ($respuestas[$ke->id_pregunta]!=null) {
							foreach ($respuestas[$ke->id_pregunta] as $res) {
								if ($res->respuesta!="") {
									$respuesta=$res->respuesta;
								}else{$respuesta="";}
							}
						}else{$respuesta="";}
						
						$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$ke->id_pregunta."'>";
						$impresion.="<td><input class='form-control' type='text' name='respuestas[".$ke->id_pregunta."]' value='".$respuesta."'></td>";
					}
				}
				$impresion.="</tr>";
				$impresion.="</table>";
				$impresion.="</div>";
				$impresion.="</div>";
			}
			if ($key->tipo==2) {
				$impresion.="<div class='col6'>";
				$id_pregunta=[];
				$i=0;
				foreach ($preguntas as $ke) {
					if ($ke->id_tabla==$key->id_tabla) {
						//echo $ke->id_pregunta;
						$id_pregunta[$i]=$ke->id_pregunta;
						$i++;
					}
				}
				$impresion.="<div id='informacion_basica'>";
				$impresion.="<table class='listado center border'>";
				$impresion.="<tr>";
				$impresion.="<th colspan='2' class='center'>Información básica</th>";
				$impresion.="<th class='center'>Llenar</th>";
				$impresion.="</tr>";
				$impresion.="<tr>";
				$impresion.="<td colspan='2'>Número de habitantes totales en el sistema</td>";
				if ($respuestas!=null) {
					foreach ($respuestas[$id_pregunta[0]] as $res) {
						if ($res->respuesta!="") {
							$respuesta=$res->respuesta;
						}else{$respuesta="";}
					}
				}else{$respuesta="";}
				
				$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$id_pregunta[0]."'>";
				$impresion.="<td><input id='respuesta".$id_pregunta[0]."' class='form-control' name='respuestas[".$id_pregunta[0]."]' value='".$respuesta."' type='text'></td>";
				$impresion.="</tr>";
				$impresion.="<tr>";
				$impresion.="<td colspan='2'>Caudal de producción promedio de las fuentes de agua (lt/seg)</td>";
				if ($respuestas!=null) {
					foreach ($respuestas[$id_pregunta[1]] as $res) {
						if ($res->respuesta!="") {
							$respuesta=$res->respuesta;
						}else{$respuesta="";}
					}
				}else{$respuesta="";}
				$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$id_pregunta[1]."'>";
				$impresion.="<td><input id='respuesta".$id_pregunta[1]."' class='form-control' name='respuestas[".$id_pregunta[1]."]' value='".$respuesta."' type='text'></td>";
				$impresion.="</tr>";
				$impresion.="<tr>";
				$impresion.="<td colspan='2'>Monto promedio de la cuota mensual S/</td>";
				if ($respuestas!=null) {
					foreach ($respuestas[$id_pregunta[2]] as $res) {
						if ($res->respuesta!="") {
							$respuesta=$res->respuesta;
						}else{$respuesta="";}
					}
				}else{$respuesta="";}
				$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$id_pregunta[2]."'>";
				$impresion.="<td><input class='form-control' name='respuestas[".$id_pregunta[2]."]' value='".$respuesta."' type='text'></td>";
				$impresion.="</tr>";
				$impresion.="<tr>";
				$impresion.="<td colspan='3' class='center'>Dotación de agua</td>";
				$impresion.="</tr>";
				$impresion.="<tr>";
				$impresion.="<td class='center'>Zona geográfica</td>";
				$impresion.="<td class='center'>Sistema de baño</td>";
				$impresion.="<td class='center'>Lt/hab/día</td>";
				$impresion.="</tr>";
				$impresion.="<tr>";
				if ($respuestas!=null) {
					foreach ($respuestas[$id_pregunta[3]] as $res) {
						if ($res->respuesta!="") {
							$respuesta=$res->respuesta;
							
						}else{$respuesta="";}
					}
				}else{$respuesta="";}
				$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$id_pregunta[3]."'>";
				$impresion.="<td><select id='zona' name='respuestas[".$id_pregunta[3]."]'>";
				$impresion.="<option value=''>-</option>";
				
				foreach ($alternativas as $alter) {
					$rsim="";
					if ($alter->id_pregunta==$id_pregunta[3]) {
						if ($respuesta==$alter->valor) {
							$rsim="selected";
						}
						$impresion.="<option value='".$alter->valor."' ".$rsim.">".$alter->texto."</option>";
					}
				}
				$impresion.="</select></td>";
				if ($respuestas!=null) {
					foreach ($respuestas[$id_pregunta[4]] as $res) {
						if ($res->respuesta!="") {
							$respuesta=$res->respuesta;
						}else{$respuesta="";}
					}
				}else{$respuesta="";}
				$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$id_pregunta[4]."'>";
				$impresion.="<td><select id='arrastre' name='respuestas[".$id_pregunta[4]."]'>";
				$impresion.="<option value=''>-</option>";
				foreach ($alternativas as $alter) {
					$rsim="";
					if ($alter->id_pregunta==$id_pregunta[4]) {
						if ($respuesta==$alter->valor) {
							$rsim="selected";
						}
						$impresion.="<option value='".$alter->valor."' ".$rsim.">".$alter->texto."</option>";
					}
				}
				$impresion.="</select></td>";
				if ($respuestas!=null) {
					foreach ($respuestas[$id_pregunta[5]] as $res) {
						if ($res->respuesta!="") {
							$respuesta=$res->respuesta;
						}else{$respuesta="";}
					}
				}else{$respuesta="";}
				$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$id_pregunta[5]."'>";
				$impresion.="<td><input class='form-control' id='lthab' readonly type='text' name='respuestas[".$id_pregunta[5]."]' value='".$respuesta."'></td>";
				$impresion.="</tr>";
				$impresion.="</table>";
				$impresion.="</div>";
				$impresion.="</div>";
			}
		
			if ($key->tipo==3) {
				$impresion.="<div class='col6'>";
				$impresion.="<div id='checkbox".$key->id_tabla."' class='tabla_check'>";
				$impresion.="<table class='listado'>";
					$impresion.="<tr>";
					$impresion.="<th class='center'>".$key->titulo."</th>";
					$impresion.="<th class='center'>Marcar</th>";
					$impresion.="</tr>";
					foreach ($preguntas as $ke) {
						if ($ke->id_tabla==$key->id_tabla) {
							$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$ke->id_pregunta."'>";
							foreach ($alternativas as $k) {
								if ($k->id_pregunta==$ke->id_pregunta) {
									$impresion.="<tr>";
									$impresion.="<td>".$k->texto."</td>";
									$respuesta="";
									if ($respuestas!=null) {
										foreach ($respuestas[$ke->id_pregunta] as $res) {
											$valores=explode("/", $res->respuesta);
											$respuesta="";
											foreach ($valores as $var) {
												if ($var==$k->valor) {
													$respuesta="checked";
												}
											}
										}
									}else{$respuesta="";}
									$impresion.="<td><input class='form-control chec' texto='".$k->texto."' name='respuestas[".$ke->id_pregunta."][]' value='".$k->valor."' ".$respuesta." type='checkbox' tabla='".$key->id_tabla."'></td>";
									$impresion.="</tr>";
								}
							}
						}
					}
				$impresion.="</table>";
				$impresion.="</div>";
				$impresion.="</div>";
			}
			
			
		}
		$impresion.="</div>";
		$impresion.="</div>";
		$impresion.="</div>";
		return $impresion;
	}
?>
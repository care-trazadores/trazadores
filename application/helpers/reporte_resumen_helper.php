<?php
	function reporte_resumen($encuestas=null,$aponderacion=null,$preguntas=null,$respuestas=null,$calificaciones=null,$calificacion=null){
		$impresion="";
		$impresion.="<div class='row'>";
			$impresion.="<div class='col12'>";
				$impresion.="<h3>Reportes Resumen de las Localidades</h3>";
				$impresion.="<table class='listado'>";
					$impresion.="<tr>";
						$impresion.="<th>Localizaciòn</th>";
						$impresion.="<th>Localidad</th>";
						foreach ($aponderacion as $key) {
							$impresion.="<th>".$key->titulo."</th>";
						}
						$impresion.="<th>Calificación final</th>";
					$impresion.="</tr>";
					foreach ($encuestas as $key) {
						$impresion.="<tr>";
							$impresion.="<td>".$key->departamento."-".$key->provincia."-".$key->distrito."</td>";
							$impresion.="<td>".$key->comunidad."</td>";
							foreach ($aponderacion as $ke) {
								$suma=0;
								$promedio="";
								$i=0;
								foreach ($preguntas[$ke->id_cuadro] as $k) {
									if (array_key_exists($k,$respuestas[$key->id_llenado])) {
										$suma=$suma+$respuestas[$key->id_llenado][$k];
										$i++;
									}
								}
								$promedio=$suma/$i;
								$valores_promedio[$key->id_llenado][$ke->id_cuadro]=$promedio;
								$calificacion_cuadro=calificaciones_final($calificaciones[$ke->id_cuadro],$promedio);
								$impresion.="<td style='background-color:".$calificacion_cuadro["background"]."'>".$calificacion_cuadro["calificacion"]."</td>";
							}
							$valor_evaluado="";
							$valoracion_ponderada="";
							$total_valoracion_ponderada="";
							$ponderacion_total="";
							foreach ($aponderacion as $ke) {
								$valor_evaluado=$valores_promedio[$key->id_llenado][$ke->id_cuadro];
								$valoracion_ponderada=$valor_evaluado*$ke->ponderacion;
								$total_valoracion_ponderada=$total_valoracion_ponderada+$valoracion_ponderada;
								$ponderacion_total=$ponderacion_total+$ke->ponderacion;
							}
							$promedio_aponderacion=$total_valoracion_ponderada/$ponderacion_total;
							$calificacion_cuadro=calificaciones_final($calificacion,$promedio_aponderacion);
							$impresion.="<td style='background-color:".$calificacion_cuadro["background"]."'>".$calificacion_cuadro["calificacion"]."</td>";
					$impresion.="</tr>";
					}
				$impresion.="</table>";
			$impresion.="</div>";
		$impresion.="</div>";
		return $impresion;
	}
	function exportar_resumen($encuestas=null,$aponderacion=null,$preguntas=null,$respuestas=null,$calificaciones=null,$calificacion=null){
		$impresion=[];
		$a=0;
		foreach ($encuestas as $key) {
			$impresion[$a]["id_llenado"]=$key->id_llenado;
			$impresion[$a]["fecha"]=$key->fecha;
			$impresion[$a]["localizacion"]=$key->departamento."-".$key->provincia."-".$key->distrito;
			$impresion[$a]["comunidad"]=$key->comunidad;
		    $promedio="";
		    $valores_promedio="";
		    $valoracion_ponderada="";
		    $i=1;
			foreach ($aponderacion as $ke) {
				$e=0;
				$suma="";
				foreach ($preguntas[$ke->id_cuadro] as $k) {
					if (array_key_exists($k,$respuestas[$key->id_llenado])) {
						$suma=$suma+$respuestas[$key->id_llenado][$k];
						$e++;
					}
				}
				$promedio=number_format($suma/$e, 2, '.', ' ');;
				$valores_promedio[$key->id_llenado][$ke->id_cuadro]=$promedio;
				$calificacion_cuadro=calificaciones_final($calificaciones[$ke->id_cuadro],$promedio);
				$valoracion_ponderada=$promedio*$ke->ponderacion;

				$impresion[$a]["result"][$i]["titulo"]=$ke->tab;
				$impresion[$a]["result"][$i]["promedio"]=$promedio;
				$impresion[$a]["result"][$i]["calificacion_cuadro"]=$calificacion_cuadro;
				$impresion[$a]["result"][$i]["ponderacion"]=$ke->ponderacion;
				$impresion[$a]["result"][$i]["valoracion_ponderada"]=$valoracion_ponderada;
				$i++;
			}
		    	$valor_evaluado="";
		    	$total_valoracion="";
				$valoracion_ponderada="";
				$total_valoracion_ponderada="";
				$ponderacion_total="";
				foreach ($aponderacion as $ke) {
					$valor_evaluado=$valores_promedio[$key->id_llenado][$ke->id_cuadro];
					$total_valoracion=$total_valoracion+$valor_evaluado;
					$valoracion_ponderada=$valor_evaluado*$ke->ponderacion;
					$total_valoracion_ponderada=$total_valoracion_ponderada+$valoracion_ponderada;
					$ponderacion_total=$ponderacion_total+$ke->ponderacion;
				}
				$promedio_aponderacion=number_format($total_valoracion_ponderada/$ponderacion_total, 2, '.', ' ');
				$calificacion_cuadro=calificaciones_final($calificacion,$promedio_aponderacion);

				$impresion[$a]["final"][$i]["calificacion_cuadro"]=$calificacion_cuadro;
			$a++;
		}
		return $impresion;
	}

?>
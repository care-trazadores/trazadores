<?php
	function cuadro_tipo_5($id_cuadro,$titulo,$calificacion,$criterios,$criterios_detalle,$notas,$tablas,$preguntas,$alternativas,$respuestas,$aponderacion,$calificaciones){
		$impresion="";
		$impresion.="<div id='tab".$id_cuadro."' class='tabs'>";
		$impresion.="<h3>".$titulo."</h3>";

		$impresion.="<div class='col12'>";
		$impresion.="<table class='listado'>";
		$impresion.="<tr>";
		$impresion.="<th colspan=2 width='400'>TRAZADOR</th>";
		$impresion.="<th width'150'>CRITERIOS DE VALORACIÓN</th>";
		$impresion.="<th width='100'>CALIFICACIÓN SEGÚN CRITERIOS</th>";
		$impresion.="<th width='100'>OBSERVACIONES</th>";
		$impresion.="</tr>";
		foreach ($tablas as $key) {
			$impresion.="<tr>";
			$impresion.="<td colspan='2'>".$key->titulo."</td>";
			$impresion.="<td></td>";
			if ($key->tipo==0) {
				$impresion.="<td>Marcar Valor</td>";
			}else{
				$impresion.="<td>Porcentaje</td>";
			}
			$impresion.="<td></td>";
			$impresion.="</tr>";
			$a=1;

			foreach ($preguntas as $keys) {
				if ($keys->id_tabla==$key->id_tabla) {
					$impresion.="<input type='hidden' name=\"id_respuestas[]\" value='".$keys->id_pregunta."'>";
					$impresion.="<tr>";
					$impresion.="<td width='10'>".$a."</td>";
					$impresion.="<td>".$keys->pregunta."</td>";
					$impresion.="<td style='padding:0px 18px;'>";
					if (count($alternativas!=null)) {
						foreach ($alternativas as $al) {
							if ($keys->id_pregunta==$al->id_pregunta) {
									$impresion.="<div>";
									$impresion.="<span style='width:20px'>".$al->valor.":</span>";
									$impresion.="<span style='width:80%'>".$al->texto."</span>";
									$impresion.="</div>";
							}
						}
						$impresion.="</td>";
						$impresion.="<td>";
						$onfocus="onfocus='this.defaultIndex=this.selectedIndex;' onchange='this.selectedIndex=this.defaultIndex;'";
						$impresion.="<select ".$onfocus." id='respuesta".$keys->id_pregunta."' ";
						$impresion.="class='respuestas' name=\"respuestas[".$keys->id_pregunta."]\" id_cuadro='".$id_cuadro."' tipo='jass'>";
						$impresion.="<option value='XXXXX'>-</option>";
						foreach ($alternativas as $al) {
							if ($keys->id_pregunta==$al->id_pregunta) {
								$impresion.="<option texto='".$al->texto."' value='".$al->valor."'";
								if ($respuestas!=null) {
									foreach ($respuestas[$keys->id_pregunta] as $res) {
										if ($res->id_pregunta==$al->id_pregunta && $res->respuesta==$al->valor) {
											$impresion.="selected='selected'";
										}
									}
								}
								$impresion.=">".$al->valor."</option>";
							}
						}
						$impresion.="</select>";
						$impresion.="</td>";
					}
					$impresion.="<td><textarea name=\"observacion[".$keys->id_pregunta."]\">";
					if ($respuestas!=null) {
						foreach ($respuestas[$keys->id_pregunta] as $res) {
							if ($res->id_pregunta==$keys->id_pregunta) {
								$impresion.=$res->observacion;
							}
						}
					}
					$impresion.="</textarea></td>";
					$impresion.="</tr>";
					$a++;
				}
				
			}
		}
		//echo count($respuestas);
		$impresion.="<tr>";
		$impresion.="<td colspan='2'>Sumatoria</td>";

		$total_sumatoria="";
		$promedio="";
		if ($respuestas!=null) {
			foreach ($respuestas as $key) {
				foreach ($key as $resp) {
					$total_sumatoria=$total_sumatoria+$resp->respuesta;
				}
			}
			if ($total_sumatoria==0) {
				$promedio=0;
			}else{
				$promedio=number_format($total_sumatoria, 2, '.', ' ');
			}
		}
		$impresion.="<td></td>";
		$impresion.="<td class='sumatoria'>".$total_sumatoria."</td>";
		$impresion.="<td></td>";
		$impresion.="</tr>";

		$impresion.="<tr>";
		$impresion.="<td colspan='3'>Promedio ".$titulo."</td>";
		$impresion.="<td class='promedio'>".$promedio."</td>";
		$calificacion_final="";
		$background="";
		if ($respuestas!=null) {
			foreach ($calificacion as $key) {
				if ($key->igual!="") {
					if ($key->igual==$promedio) {
						$background=$key->background;
						$calificacion_final=$key->calificacion;
					}
				}elseif ($key->mayor!="" && $key->menor!="") {
					if ($promedio>=$key->mayor && $promedio<$key->menor) {
						$background=$key->background;
						$calificacion_final=$key->calificacion;
					}
				}elseif ($key->mayor!="" && $key->menor=="") {
					if ($promedio>$key->mayor) {
						$background=$key->background;
						$calificacion_final=$key->calificacion;
					}
				}elseif ($key->menor!="" && $key->mayor=="") {
					if ($promedio<=$key->menor) {
						$background=$key->background;
						$calificacion_final=$key->calificacion;
					}
				}				
			}
		}
		$impresion.="<td class='calificacion_final' style='background-color:".$background.";'>".$calificacion_final."</td>";
		$impresion.="</tr>";
		$impresion.="</div>";

		$impresion.="</table>";
		$impresion.="<div class='col6'>";
		$impresion.="<table class='calificacion'>";
		$impresion.="<tr>";
		$impresion.="<th>Calificación</th>";
		$impresion.="<th>Rango</th>";
		$impresion.="</tr>";
			foreach ($calificacion as $key) {
				$impresion.="<tr class='calificaciones' mayor='".$key->mayor."' menor='".$key->menor."' igual='".$key->igual."' background='".$key->background."' texto='".$key->calificacion."'>";	
				$impresion.="<td>".$key->calificacion."</td>";
				$impresion.="<td>".$key->texto_rango."</td>";
				$impresion.="</tr>";
			}
		$impresion.="</table>";
		$impresion.="</div>";
		$impresion.="</div>";
		return $impresion;
	}
?>
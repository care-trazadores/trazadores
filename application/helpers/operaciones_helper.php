<?php
	function sumatoria($respuestas){
		$suma="";
		foreach ($respuestas as $res) {
			foreach ($res as $key ) {
				$suma=$suma+$key->respuesta;
			}
		}
		$total=$suma/count($respuestas);
		return number_format($total, 2, '.', ' ');
	}
	function calificaciones_final($calificacion,$promedio){
		$calificacion_final["background"]="";
		$calificacion_final["calificacion"]="";
		foreach ($calificacion as $key) {
				if ($key->igual!="") {
					if ($key->igual==$promedio) {
						$calificacion_final["background"]=$key->background;
						$calificacion_final["calificacion"]=$key->calificacion;
					}
				}elseif ($key->mayor!="" && $key->menor!="") {
					if ($promedio>=$key->mayor && $promedio<$key->menor) {
						$calificacion_final["background"]=$key->background;
						$calificacion_final["calificacion"]=$key->calificacion;
					}
				}elseif ($key->mayor!="" && $key->menor=="") {
					if ($promedio>=$key->mayor) {
						$calificacion_final["background"]=$key->background;
						$calificacion_final["calificacion"]=$key->calificacion;
					}
				}elseif ($key->menor!="" && $key->mayor=="") {
					if ($promedio<$key->menor) {
						$calificacion_final["background"]=$key->background;
						$calificacion_final["calificacion"]=$key->calificacion;
					}
				}				
			}
		return $calificacion_final;
	}
?>
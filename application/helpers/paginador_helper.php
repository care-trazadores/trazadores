<?php
	function paginador($url=null,$total_registros=null,$limite=null,
			$pagina=null,$registros=null,$busq_usuario=null,$busq_correo=null,
			$id_departamento=null,$id_provincia=null,$id_distrito=null,$tipo=null,$busq_localidad=null,$busq_accion=null){
		$i=0;
		$paginador="";
		foreach ($registros as $key) {
			$i++;
		}
		if ($limite!="") {
			$total_paginas=ceil($total_registros/$limite);
			$mostrando_inicio=(($pagina-1)*$limite)+1;
		}else{
			$total_paginas=1;
			$mostrando_inicio=1;
		}
		if ($i>0) {
			if ($total_paginas>0) {
				if ($pagina==$total_paginas) {
					$mostrando_final=$total_registros;
				}else{
					$mostrando_final=$pagina*$limite;
				}
				$mostrando="<div class='text-pos'>Mostrando ".$mostrando_inicio." a ".$mostrando_final." de ".$total_registros." entradas</div>";
			}else{
				$mostrando="<div class='text-pos'>Lo sentimos no se encontraron entradas</div>";
			}
		}else{
			$mostrando="<div class='text-pos'>Lo sentimos no se encontraron entradas</div>";
		}
		if ($tipo!="") { $url=$url."&tipo=".$tipo; }
		if ($busq_usuario!="") { $url=$url."&busq_usuario=".$busq_usuario; }
		if ($busq_correo!="") { $url=$url."&busq_correo=".$busq_correo; }
		
		if ($busq_localidad!="") { $url=$url."&busq_localidad=".$busq_localidad; }
		if ($busq_accion!="") { $url=$url."&busq_accion=".$busq_accion; }

		if ($id_departamento!="") { $url=$url."&id_departamento=".$id_departamento; }
		if ($id_provincia!="") { $url=$url."&id_provincia=".$id_provincia; }
		if ($id_distrito!="") { $url=$url."&id_distrito=".$id_distrito; }

		if ($limite!="") { $url=$url."&limite=".$limite; }
		if ($i>0) {
			if(($pagina-1) > 0) 
			{
				$paginador="<ul class='paginador'><li><a href='".$url."&pagina=".($pagina-1)."'>&#60;</a></li>";
			}else{
				$paginador="<ul class='paginador'><li><a>&#60;</a></li>";
			}

				$paginador=$paginador."<li><a>".$pagina."</a></li>";
			if(($pagina+1)<=$total_paginas) 
			{ 
			$paginador=$paginador."<li><a href='".$url."&pagina=".($pagina+1)."'>&#62;</a></li>";
			}else{
				$paginador=$paginador."<li><a>&#62;</a></li></ul>";
			}
		}else{$paginador="";}
		
		$final=$mostrando.$paginador;
		return $final;
	}
?>
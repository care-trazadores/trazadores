<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presentacion extends CI_Controller {
	public  function __construct() {
		parent::__construct();
		session_start();
		session_destroy();
		$this->data["base_url"]=$this->config->config['base_url'];
		$this->data["css"]=array('reset.css','bootstrap.css','presentacion.css','estilos.css');
		$this->data["js"]=array('jquery-2.1.3.min.js','direc.js','higchats/highcharts.js','js.js','bootstrap.min.js');
   	}
	public function index()
	{
		$this->data["title"]="Trazadores|Presentación";
		$this->data["controlador"]="presentacion";
		$this->data["contenido"]=$this->load->view('presentacion/presentacion',$this->data,TRUE);
		$this->load->view('template2',$this->data);
	}
	public function trazador(){
		$this->data["css"][]='presentacion/trazador.css';
		$this->data["css"][]='font-awesome.css';
		$this->data["title"]="Trazadores|Reportes";
		$this->data["controlador"]="trazador";
		$this->data["action"]=$this->data["base_url"]."index.php?c=presentacion&m=trazador";

		$limite=($this->input->get_post('limite', TRUE))?$this->input->get_post('limite', TRUE):5;
		$id_departamento=($this->input->get_post('id_departamento', TRUE))?$this->input->get_post('id_departamento', TRUE):"";
       	$id_provincia=($this->input->get_post('id_provincia', TRUE))?$this->input->get_post('id_provincia', TRUE):"";
       	$id_distrito=($this->input->get_post('id_distrito', TRUE))?$this->input->get_post('id_distrito', TRUE):"";
       	$pagina=($this->input->get_post('pagina', TRUE))?$this->input->get_post('pagina', TRUE):1;
		$tipo=($this->input->get_post('tipo', TRUE))?$this->input->get_post('tipo', TRUE):"";


		$this->data["datos_exportacion"]="";
    	
    	if ($id_departamento!="") {$this->data["datos_exportacion"].="&id_departamento=".$id_departamento;}
    	if ($id_provincia!="") {$this->data["datos_exportacion"].="&id_provincia=".$id_provincia;}
    	if ($id_distrito!="") {$this->data["datos_exportacion"].="&id_distrito=".$id_distrito;}
    	

		if ($tipo!="grafico") {
			if ($pagina!="") {$this->data["datos_exportacion"].="&pagina=".$pagina;}
			if ($limite!="") {$this->data["datos_exportacion"].="&limite=".$limite;}
			$this->load->helper('paginador');
			$this->load->helper('operaciones');
			
			$this->load->model('clsaponderacion');
			$this->load->model('clscalificacion');

			$this->load->model('clspreguntas');
			$this->load->model('clsalternativas');
			$this->load->model('clsllenado');
			$this->load->model('clsdeetalle_llenado');
			$this->data["tipo_reporte"]="";
			$this->data["tablas"]="";
			$this->data["paginador"]="";
				
			
			
			$comienzo=($pagina-1)*$limite;
			$encuestas="";
			$respuestas="";
			$calificacion="";
			$calificaciones=[];

			if ($tipo=="grafico") {
				$limite="";
				$pagina = "";
				$comienzo="";
			}
			$encuestas=$this->clsllenado->reportes($limite,$comienzo,null,$id_departamento,$id_provincia,$id_distrito,null,null,null);
			if ($tipo=="general") {
				$preguntas=$this->clspreguntas->preguntas(1);
				$respuestas="";
				foreach ($encuestas as $key) {
					foreach ($preguntas as $ke) {
						$respuestas[$key->id_llenado][$ke->id_pregunta]="---";
						foreach ($this->clsdeetalle_llenado->respuestas($key->id_llenado,$ke->id_pregunta,null) as $k) {
							if ($ke->id_pregunta==48 || $ke->id_pregunta==51 || $ke->id_pregunta==52 || $ke->id_pregunta==53 || $ke->id_pregunta==61) {
								$cadena="";
								$verificacion = strpos($k->respuesta,"/");
								if ($verificacion!==false) {
									$porciones = explode("/",$k->respuesta);
									foreach ($porciones as $val) {
										foreach ($this->clsalternativas->alternativa_idpregunta($ke->id_pregunta,$val) as $alter_res) {
											$cadena.=$alter_res->texto." --- ";
										}
									}
								}else{
									foreach ($this->clsalternativas->alternativa_idpregunta($ke->id_pregunta,$k->respuesta) as $alter_res) {
										$cadena.=$alter_res->texto;
									}
								}
								$respuestas[$key->id_llenado][$ke->id_pregunta]=$cadena;
							}else{
								$respuestas[$key->id_llenado][$ke->id_pregunta]=$k->respuesta;
							}
						}
					}
				}
			}elseif ($tipo=="resumen" or $tipo=="detallado") {
				$aponderacion=$this->clsaponderacion->aponderacion();
				$i=0;
				foreach ($aponderacion as $cuadros) {
					foreach ($this->clspreguntas->preguntas($cuadros->id_cuadro) as $key) {
						$preguntas[$cuadros->id_cuadro][$i]=$key->id_pregunta;
						$i++;
					}
				}
				if (count($encuestas)!=0) {
					foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
						$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
					}
				}
				
				foreach ($aponderacion as $key) {
					$calificaciones[$key->id_cuadro]=$this->clscalificacion->calificacion($key->id_cuadro);
				}
				$calificacion=$this->clscalificacion->calificacion(11);
			}elseif ($tipo=="jazz") {
				$preguntas=$this->clspreguntas->preguntas(12);
				if (count($encuestas)!=0) {
					foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
							$respuestas[$key->id_llenado]["respuesta"][$key->id_pregunta]=$key->respuesta;
							$respuestas[$key->id_llenado]["texto"][$key->id_pregunta]=$key->texto;
					}
				}
				
				$calificacion=$this->clscalificacion->calificacion(12);
			}elseif ($tipo=="atm") {
				$this->load->model('clsllenadoMunicipal');
				$this->load->model('clspreguntasMunicipal');
				$this->load->model('clsdeetalle_llenadoMunicipal');
				$this->load->model('clscalificacionMunicipal');

				$encuestas=$this->clsllenadoMunicipal->reportes($limite,$comienzo,null,$id_departamento,$id_provincia,$id_distrito,null,null,null);

				$preguntas=$this->clspreguntasMunicipal->preguntas(1);
				foreach ($this->clsdeetalle_llenadoMunicipal->respuestas(null,null,$encuestas) as $key) {
						$respuestas[$key->id_llenado]["respuesta"][$key->id_pregunta]=$key->respuesta;
						$respuestas[$key->id_llenado]["texto"][$key->id_pregunta]=$key->texto;
				}
				$calificacion=$this->clscalificacionMunicipal->calificacion(1);
			}elseif ($tipo=="grafico") {
				$aponderacion=$this->clsaponderacion->aponderacion();
				$i=0;
				foreach ($aponderacion as $cuadros) {
					foreach ($this->clspreguntas->preguntas($cuadros->id_cuadro) as $key) {
						$preguntas[$cuadros->id_cuadro][$i]=$key->id_pregunta;
						$i++;
					}
				}
				if ($id_departamento!="" || $id_provincia!="" || $id_distrito!="") {
					if (count($encuestas)!=0) {
						foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
							$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
						}
					}
					
				}else{
					$encuestas="";
					/*
					foreach ($this->clsdeetalle_llenado->respuestas() as $key) {
						$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
					}*/
				}
				foreach ($aponderacion as $key) {
					$calificaciones[$key->id_cuadro]=$this->clscalificacion->calificacion($key->id_cuadro);
				}
				$calificacion=$this->clscalificacion->calificacion(11);
			}
			if ($tipo=="atm") {
				$total=$this->clsllenadoMunicipal->total_encuestas(null,$id_departamento,$id_provincia,$id_distrito,null,null,null);
			}else{
				$total=$this->clsllenado->total_encuestas(null,$id_departamento,$id_provincia,$id_distrito,null,null,null);
			}
			

			$url_paginador=$this->data["base_url"]."index.php?c=presentacion&m=trazador";
	    	if ($tipo!="grafico") {
	    		$this->data["paginador"]=paginador($url_paginador,$total,$limite,$pagina,$encuestas,null,null,$id_departamento,$id_provincia,$id_distrito,$tipo);
	    	}
	    	
			
	    	if ($tipo=="general") {
				$this->load->helper('reporte_general');
				$this->data["tipo_reporte"]="general";
				$this->data["datos_exportacion"].="&tipo=general";
				$this->data["tablas"]=reporte_general($encuestas,$preguntas,$respuestas);		
			}elseif ($tipo=="detallado") {
				$this->load->helper('reporte_detallado');
				$this->data["tipo_reporte"]="detallado";
				$this->data["datos_exportacion"].="&tipo=detallado";
				$this->data["tablas"]=reporte_detallado($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion);
			}elseif ($tipo=="resumen") {
				$this->load->helper('reporte_resumen');
				$this->data["tipo_reporte"]="resumen";
				$this->data["datos_exportacion"].="&tipo=resumen";
				$this->data["tablas"]=reporte_resumen($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion);
			}elseif ($tipo=="grafico") {
				if ($encuestas!="") {
					$this->load->helper('reporte_grafico');
					$this->data["tipo_reporte"]="grafico";
					$this->data["datos_exportacion"].="&tipo=grafico";
					$this->data["tablas"]=reporte_grafico($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion);
		    		$this->data["paginador"]="";
				}
				
			}elseif ($tipo=="jazz") {
				$this->load->helper('reporte_jazz');
				$this->data["tipo_reporte"]="jazz";
				$this->data["datos_exportacion"].="&tipo=jazz";
				$this->data["tablas"]=reporte_jazz($encuestas,$preguntas,$respuestas,$calificacion);
			}elseif ($tipo=="atm") {
				$this->load->helper('reporte_atm');
				$this->data["tipo_reporte"]="atm";
				$this->data["datos_exportacion"].="&tipo=atm";
				$this->data["tablas"]=reporte_atm($encuestas,$preguntas,$respuestas,$calificacion);
			}
		}else{
			$this->data["tipo_reporte"]="grafico";
			$this->data["paginador"]="";
			$this->data["tablas"]="";
			$this->load->model("clsreporte_calificacion");
			$this->load->helper('reporte_grafico');
			foreach ($this->clsreporte_calificacion->cuadros() as $key) {
				$array[$key->id_cuadro][$key->calificacion]["id_cuadro"]=$key->id_cuadro;
				$array[$key->id_cuadro][$key->calificacion]["titulo"]=$key->titulo;
				$array[$key->id_cuadro][$key->calificacion]["total"]=0;
				$array[$key->id_cuadro][$key->calificacion]["calificacion"]=$key->calificacion;
				$array[$key->id_cuadro][$key->calificacion]["background"]=$key->background;
			}
			$datos=$this->clsreporte_calificacion->total_calificacion($id_departamento,$id_provincia,$id_distrito,null,null,null);
			if ($datos!=null) {
				foreach ($datos as $key) {
					$array[$key->id_cuadro][$key->calificacion]["total"]=$key->total;
				}
			}
			$dat=$this->clsreporte_calificacion->total_aponderacion($id_departamento,$id_provincia,$id_distrito,null,null,null);
			if ($dat!=null) {
				foreach ($dat as $key) {
					$array[11][$key->calificacion]["total"]=$key->total;
				}
			}
			$this->data["tablas"]=re_porgrafico($array);
			
		}
		
    	$this->data["tipo"]=$tipo;
		$this->data["limite"]=$limite;
    	$this->data["id_departamento"]=$id_departamento;
    	$this->data["id_provincia"]=$id_provincia;
    	$this->data["id_distrito"]=$id_distrito;
		$this->data["titulo"]="Sondeo rápido del estado de los servicios de agua y saneamiento en localidades rurales, Perú
";
		$this->data["contenido"]=$this->load->view('presentacion/reportes',$this->data,TRUE);
		$this->load->view('template2',$this->data);
	}
	public function registro(){
		$this->data["usuario"]="";
		$this->data["pas"]="";
		$this->data["pas_re"]="";
		$this->data["id_departamento"]="";
		$this->data["id_provincia"]="";
		$this->data["id_distrito"]="";
		$this->data["nombres"]="";
		$this->data["apellido_p"]="";
		$this->data["apellido_m"]="";
		$this->data["dni"]="";
		$this->data["correo"]="";
		$this->data["direccion"]="";
		$this->data["telefono"]="";
		$this->data["celular"]="";
		$error="";
		$this->data["error"]="";
		$this->data["succes"]="";

		if (isset($_POST["enviar"])) {
			if ($this->input->post('usuario', TRUE)=="") {$usuario="";$error.="-El campo usuario es obligatorio<br>";}
			else{$usuario=$this->input->post('usuario', TRUE);}

			if ($this->input->post('pas', TRUE)=="") {$pas="";$error.="-El campo contraseña es obligatorio<br>";}
			else{$pas=$this->input->post('pas', TRUE);}

			if ($this->input->post('pas_re', TRUE)=="") {$pas_re="";$error.="-El campo repetir contraseña es obligatorio<br>";
			}else{$pas_re=$this->input->post('pas_re', TRUE);}

			if ($this->input->post('id_distrito', TRUE)=="") {$id_distrito="";$error.="-Debe seleccionar un distrito<br>";
			}else{$id_distrito=$this->input->post('id_distrito', TRUE);$this->data["datos_usuario"]['id_distrito']=$id_distrito;}

			if ($this->input->post('nombres', TRUE)=="") {$nombres="";$error.="-El campo nombres es obligatorio<br>";
			}else{$nombres=$this->input->post('nombres', TRUE);}

			if ($this->input->post('apellido_p', TRUE)=="") {$apellido_p="";$error.="-El campo apellido paterno es obligatorio<br>";
			}else{$apellido_p=$this->input->post('apellido_p', TRUE);}

			if ($this->input->post('correo', TRUE)=="") {$correo="";$error.="-El campo correo es obligatorio<br>";
			}else{$correo=$this->input->post('correo', TRUE);}

			if ($this->input->post('id_departamento', TRUE)=="") {
			}else{$this->data["datos_usuario"]['id_departamento']=$this->input->post('id_departamento', TRUE);}
			if ($this->input->post('id_provincia', TRUE)=="") {
			}else{$this->data["datos_usuario"]['id_provincia']=$this->input->post('id_provincia', TRUE);}

			$id_departamento=$this->input->post('id_departamento', TRUE);
			$id_provincia=$this->input->post('id_provincia', TRUE);
			$apellido_m=$this->input->post('apellido_m', TRUE);
			$dni=$this->input->post('dni', TRUE);
			$direccion=$this->input->post('direccion', TRUE);
			$telefono=$this->input->post('telefono', TRUE);
			$celular=$this->input->post('celular', TRUE);

			if ($pas!=$pas_re) {
				$error.="Las contraseñas deben ser iguales";
			}

			$this->data["usuario"]=$usuario;
			$this->data["pas"]=$pas;
			$this->data["pas_re"]=$pas_re;
			$this->data["nombres"]=$nombres;
			$this->data["id_distrito"]=$id_distrito;
			$this->data["apellido_p"]=$apellido_p;
			$this->data["apellido_m"]=$apellido_m;
			$this->data["dni"]=$dni;
			$this->data["correo"]=$correo;
			$this->data["direccion"]=$direccion;
			$this->data["telefono"]=$telefono;
			$this->data["celular"]=$celular;
			if ($error!="") {
				$this->data["error"]="<p class='alert alert-danger'>".$error."</p>";
			}else{
				$this->load->model('clsusuarios');
				if ($this->clsusuarios->verificar_user($usuario)==0) {
					$data = array(
			            'user' => $usuario,
			            'pass' => md5($pas),
			            'id_distrito' =>$id_distrito,
			            'tipo' =>"usuario",
			            'estado' =>0
			        );
			        $id_usuario=$this->clsusuarios->insertar($data);
			        $this->load->model('clsdetalleusuario');
			        $datos = array(
			            'id_usuario' => $id_usuario,
			            'nombre' => $nombres,
			            'apellido_p' =>$apellido_p,
			            'apellido_m' =>$apellido_m,
			            'dni' =>$dni,
			            'correo' =>$correo,
			            'direccion' =>$direccion,
			            'telefono' =>$telefono,
			            'celular' =>$celular,
			            'foto' =>''
			        );
			       	$this->clsdetalleusuario->insertar($datos);
			       	$this->data["succes"]="<p class='alert alert-success'>El usuario fue creado correctamente.</p>";
					$this->data["usuario"]="";
					$this->data["pas"]="";
					$this->data["pas_re"]="";
					$this->data["nombres"]="";
					$this->data["id_departamento"]="";
					$this->data["id_provincia"]="";
					$this->data["id_distrito"]="";
					$this->data["apellido_p"]="";
					$this->data["apellido_m"]="";
					$this->data["dni"]="";
					$this->data["correo"]="";
					$this->data["direccion"]="";
					$this->data["telefono"]="";
					$this->data["celular"]="";

					/*$this->load->library('email');

					$this->email->from('carlos.palma@yainso.com', 'Carlos');
					$this->email->subject('Email Test');
					$this->email->message('Testing the email class.');	
					$this->email->send();*/
					$this->load->helper('mensajes');
					$this->load->helper('email');
					$this->load->model('clsadmin_regiones');
					$this->load->model('clsdistritos');
					foreach ($this->clsdistritos->get_distrito($id_distrito) as $key) {
						$depa=$key->departamento;
						$prov=$key->provincia;
						$dist=$key->distrito;
					}
					$mensajeadmin=admin($usuario,$pas,$depa,$prov,$dist,$nombres,$apellido_p,$apellido_m,$dni,$correo,$direccion,$telefono,$celular,$this->data["base_url"]);
					$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
					$cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					foreach ($this->clsadmin_regiones->admin_distrito($id_distrito) as $key) {
						//echo $key->id_usuario."<br>";
						mail($key->correo, $key->nombre." ".$key->correo, $mensajeadmin,$cabeceras);
					}
					foreach ($this->clsusuarios->busqueda_user(null,"master") as $key) {
						//echo $key->id_usuario."<br>";
						mail($key->correo, $key->nombre." ".$key->correo, $mensajeadmin,$cabeceras);
					}

					$mensajeuser=usuario($usuario,$pas,$depa,$prov,$dist,$nombres,$apellido_p,$apellido_m,$dni,$correo,$direccion,$telefono,$celular,$this->data["base_url"]);
					mail($correo, $usuario." ".$correo, $mensajeuser,$cabeceras);
			    }else{
					$error.="-lo sentimos el usuario ya existe<br>";
					$this->data["error"]="<p class='alert alert-danger'>-".$error."</p>";
				}
			}

		}


		$this->data["title"]="Trazadores|Registro";
		$this->data["controlador"]="registro";
		$this->data["contenido"]=$this->load->view('presentacion/registro',$this->data,TRUE);
		$this->load->view('template2',$this->data);
	}
}

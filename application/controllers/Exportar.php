<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class exportar extends CI_Controller {
	public $controlador;
	public  function __construct() {
		parent::__construct();
		session_start();
		$this->data["base_url"]=$this->config->config['base_url'];
		//if ($_SESSION["usuario"]["active"]!=TRUE) {
		//	header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
		//}	
	}
	public function index(){
		$limite=5;
		$id_departamento = "";
		$id_provincia = "";
		$id_distrito = "";
		$pagina = 1;

		if (isset($_GET['tipo'])){$tipo=$this->input->get('tipo', TRUE);}
		else{header("Location:http://google.com");}
		if (isset($_GET['id_departamento'])){$id_departamento=$this->input->get('id_departamento', TRUE);}
		else{$id_departamento="";}
		if (isset($_GET['id_provincia'])){$id_provincia=$this->input->get('id_provincia', TRUE);}
		else{$id_provincia="";}
		if (isset($_GET['id_distrito'])){$id_distrito=$this->input->get('id_distrito', TRUE);}
		else{$id_distrito="";}
		if (isset($_GET['limite'])){$limite=$this->input->get('limite', TRUE);}
		else{$limite="";}
		if (isset($_GET['pagina'])){$pagina=$this->input->get('pagina', TRUE);}
		else{$pagina="";}
		if (isset($_GET['archivo'])){$archivo=$this->input->get('archivo', TRUE);}
		else{$archivo="";}
		$comienzo=($pagina-1)*$limite;

		$this->load->helper('operaciones');
		
		$this->load->model('clsaponderacion');
		$this->load->model('clscalificacion');
		$this->load->model('clspreguntas');
		$this->load->model('clsalternativas');
		$this->load->model('clsllenado');
		$this->load->model('clsdeetalle_llenado');

		if (isset($_SESSION['usuario'])){ 
			$encuestas=$this->clsllenado->reportes($limite,$comienzo,null,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
		}else{
			$encuestas=$this->clsllenado->reportes($limite,$comienzo,null,$id_departamento,$id_provincia,$id_distrito,null,null,null);
		}

		
		if ($tipo=="general") {
			$preguntas=$this->clspreguntas->preguntas(1);
			$respuestas="";
			foreach ($encuestas as $key) {
				foreach ($preguntas as $ke) {
					$respuestas[$key->id_llenado][$ke->id_pregunta]="---";
					foreach ($this->clsdeetalle_llenado->respuestas($key->id_llenado,$ke->id_pregunta,null) as $k) {
						if ($ke->id_pregunta==48 || $ke->id_pregunta==51 || $ke->id_pregunta==52 || $ke->id_pregunta==53 || $ke->id_pregunta==61) {
							$cadena="";
							$verificacion = strpos($k->respuesta,"/");
							if ($verificacion!==false) {
								$porciones = explode("/",$k->respuesta);
								foreach ($porciones as $val) {
									foreach ($this->clsalternativas->alternativa_idpregunta($ke->id_pregunta,$val) as $alter_res) {
										$cadena.=$alter_res->texto." --- ";
									}
								}
							}else{
								foreach ($this->clsalternativas->alternativa_idpregunta($ke->id_pregunta,$k->respuesta) as $alter_res) {
									$cadena.=$alter_res->texto;
								}
							}
							$respuestas[$key->id_llenado][$ke->id_pregunta]=$cadena;
						}else{
							$respuestas[$key->id_llenado][$ke->id_pregunta]=$k->respuesta;
						}
					}
				}
			}
		}elseif ($tipo=="resumen" or $tipo=="detallado") {
			$aponderacion=$this->clsaponderacion->aponderacion();
			$i=0;
			foreach ($aponderacion as $cuadros) {
				foreach ($this->clspreguntas->preguntas($cuadros->id_cuadro) as $key) {
					$preguntas[$cuadros->id_cuadro][$i]=$key->id_pregunta;
					$i++;
				}
			}
			if (count($encuestas)!=0) {
				foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
					$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
				}
			}
			
			foreach ($aponderacion as $key) {
				$calificaciones[$key->id_cuadro]=$this->clscalificacion->calificacion($key->id_cuadro);
			}
			$calificacion=$this->clscalificacion->calificacion(11);
		}elseif ($tipo=="jazz") {
			$preguntas=$this->clspreguntas->preguntas(12);
			if (count($encuestas)!=0) {
				foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
						$respuestas[$key->id_llenado]["respuesta"][$key->id_pregunta]=$key->respuesta;
						$respuestas[$key->id_llenado]["texto"][$key->id_pregunta]=$key->texto;
				}
			}
			
			$calificacion=$this->clscalificacion->calificacion(12);
		}elseif ($tipo=="atm") {
			$this->load->model('clsllenadoMunicipal');
			$this->load->model('clspreguntasMunicipal');
			$this->load->model('clsdeetalle_llenadoMunicipal');
			$this->load->model('clscalificacionMunicipal');

			$encuestas=$this->clsllenadoMunicipal->reportes($limite,$comienzo,null,$id_departamento,$id_provincia,$id_distrito,null,null,null);

			$preguntas=$this->clspreguntasMunicipal->preguntas(1);
			foreach ($this->clsdeetalle_llenadoMunicipal->respuestas(null,null,$encuestas) as $key) {
					$respuestas[$key->id_llenado]["respuesta"][$key->id_pregunta]=$key->respuesta;
					$respuestas[$key->id_llenado]["texto"][$key->id_pregunta]=$key->texto;
			}
			$calificacion=$this->clscalificacionMunicipal->calificacion(1);
		}elseif ($tipo=="grafico") {
			$aponderacion=$this->clsaponderacion->aponderacion();
			$i=0;
			foreach ($aponderacion as $cuadros) {
				foreach ($this->clspreguntas->preguntas($cuadros->id_cuadro) as $key) {
					$preguntas[$cuadros->id_cuadro][$i]=$key->id_pregunta;
					$i++;
				}
			}
			if ($id_departamento!="" || $id_provincia!="" || $id_distrito!="") {
				if (count($encuestas)!=0) {
					foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
						$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
					}
				}else{
					$respuestas="";
				}
				
			}else{
				$encuestas="";
				$respuestas[]="";
				/*
				foreach ($this->clsdeetalle_llenado->respuestas() as $key) {
					$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
				}*/
			}
			foreach ($aponderacion as $key) {
				$calificaciones[$key->id_cuadro]=$this->clscalificacion->calificacion($key->id_cuadro);
			}
			$calificacion=$this->clscalificacion->calificacion(11);
		}
		
		$this->data["archivo"]=$archivo;
		if ($tipo=="general") {
			$this->load->helper('reporte_general');

			$this->data["tablas"]=exportar_general($encuestas,$preguntas,$respuestas);
			$this->load->view('reportes/exportar/general', $this->data);
		}
		if ($tipo=="jazz") {
			$this->load->helper('reporte_jazz');
			$this->data["tablas"]=exportar_jazz($encuestas,$preguntas,$respuestas,$calificacion);
			$this->load->view('reportes/exportar/jazz', $this->data);
		}
		if ($tipo=="atm") {
			$this->load->helper('reporte_atm');
			$this->data["tablas"]=exportar_atm($encuestas,$preguntas,$respuestas,$calificacion);
			$this->load->view('reportes/exportar/atm', $this->data);
		}
		if ($tipo=="detallado") {
			$this->load->helper('reporte_detallado');
			$this->data["tablas"]=exportar_detallado($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion);
			$this->load->view('reportes/exportar/detallado', $this->data);
		}elseif ($tipo=="resumen") {
			$this->load->helper('reporte_resumen');
			$this->data["tablas"]=exportar_resumen($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion);
			$this->load->view('reportes/exportar/resumen', $this->data);
		}elseif ($tipo=="grafico") {
			$this->data["localizacion"]="";
			if ($id_departamento!="") {
				$this->load->model('clsubdepartamento');
				foreach ($this->clsubdepartamento->departamento($id_departamento) as $key) {
					$this->data["localizacion"].=$key->departamento;
				}
			}
			if ($id_provincia!="") {
				$this->load->model('clsubprovincia');
				foreach ($this->clsubprovincia->provincia($id_provincia) as $key) {
					$this->data["localizacion"].="--".$key->provincia;
				}
			}	
			if ($id_distrito!="") {
				$this->load->model('clsubdistrito');
				foreach ($this->clsubdistrito->distrito($id_distrito) as $key) {
					$this->data["localizacion"].="--".$key->distrito;
				}
			}	
			$this->load->helper('reporte_grafico');
			$this->data["tablas"]=exportar_grafico($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion);
			$this->load->view('reportes/exportar/grafico', $this->data);
		}
		
	}
	public function encuesta(){
		//primero verificamos si se envio la variable de id sino reedireccionamos
        if (!isset($_GET["id_llenado"])) {header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        $id_llenado=$this->input->get('id_llenado', TRUE);
        $this->data["datos_llenado"]["fecha"]=date("d-m-Y"); 
        //luego cargamos los modelos que se usaran
        $this->load->model('clsdesarrollo');
        $this->load->model('clsaponderacion');
        $this->load->model('clsllenado');
        $this->load->model('clsdeetalle_llenado');
        $this->load->model('clscuadros');
        $this->load->model('clstablas');
        $this->load->model('clspreguntas');
        $this->load->model('clscalificacion');
        $this->load->model('clscriterios');
        $this->load->model('clsnotas');
        $this->load->model('clsanexo');
        //declaramos los datos de la encuesta
        foreach ($this->clsllenado->llenado_por_id($id_llenado) as $key) {
            $this->data["datos_llenado"]["id_llenado"]=$id_llenado;
            $this->data["datos_llenado"]["comunidad"]=$key->comunidad;
            $this->data["datos_llenado"]["id_usuario"]=$key->id_usuario;
            $this->data["datos_llenado"]["usuario"]=$key->user;
            $this->data["datos_llenado"]["departamento"]=$key->departamento;
            $this->data["datos_llenado"]["provincia"]=$key->provincia;
            $this->data["datos_llenado"]["distrito"]=$key->distrito;
            $this->data["datos_llenado"]["id_distrito"]=$key->idDist;
        }
        if ($_SESSION["usuario"]["tipo"]=="usuario") {
            if ($this->data["datos_llenado"]["id_distrito"]!=$_SESSION["usuario"]["id_distrito"]) {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
            }
        }
        //rescatamos todos los cuadros
        $cuadros=$this->clscuadros->cuadros();
        //declaramos la variable tabs con la funcion tabs y enviamos los cuadros esto devolvera una cadena de texto de los tabs
        //que se usaran para cambvios modificar el helper cuadros_preguntas function tabs
        $respuestas=null;
        $calificacion=null;
        $criterios=null;
        $criterios_detalle=null;
        $notas=null;
        $tablas=null;
        $preguntas=null;
        $alternativas=null;
        $aponderacion=null;
        $calificaciones=null;
        //luego recorremos la variable cuadro uno por uno con foreach
        foreach ($cuadros as $key) {
            //rescatamos todas la calificaciones y las guardamos en la variable calificacion
            $calificacion=$this->clsdesarrollo->calificacion($key->id_cuadro);
            //rescatamos todos los cuadros de criterios y los guardamos en las variables
            $criterios=$this->clsdesarrollo->criterios($key->id_cuadro);
            $criterios_detalle=$this->clsdesarrollo->criterios_detalle($criterios);
            $notas=$this->clsdesarrollo->notas($key->id_cuadro);
            $tablas=$this->clsdesarrollo->tablas($key->id_cuadro);
            $preguntas=$this->clsdesarrollo->preguntas($key->id_cuadro);
            $alternativas=$this->clsdesarrollo->alternativas($preguntas);

            //$respuestas=$this->clsdesarrollo->respuestas($id_llenado,$preguntas);
            foreach ($preguntas as $keys) {
                $respuestas[$keys->id_pregunta]=$this->clsdeetalle_llenado->respuestas($id_llenado,$keys->id_pregunta,null);
            }
           
            if ($key->tipo==0) {
                
            }elseif ($key->tipo==3) {
                
            }else if ($key->tipo==4) {
                $criterios=null;
                $criterios_detalle=null;
                $notas=null;
                $tablas=null;
                $preguntas=null;
                $alternativas=null;
                $respuestas=null;
                $aponderacion=$this->clsaponderacion->aponderacion();
                foreach ($aponderacion as $cuad) {
                    $preguntas=$this->clspreguntas->preguntas($cuad->id_cuadro);
                    $i=0;
                    foreach ($preguntas as $keys) {
                        $respuestas[$cuad->id_cuadro][$i]=$this->clsdeetalle_llenado->respuestas($id_llenado,$keys->id_pregunta);
                        $i++;
                    }
                    $calificaciones[$cuad->id_cuadro]=$this->clscalificacion->calificacion($cuad->id_cuadro);
                }
            }
            $this->data["info"][$key->id_cuadro]["id_cuadro"]=$key->id_cuadro;
            $this->data["info"][$key->id_cuadro]["tipo"]=$key->tipo;
            $this->data["info"][$key->id_cuadro]["titulo"]=$key->titulo;
            $this->data["info"][$key->id_cuadro]["tab"]=$key->tab;
            $this->data["info"][$key->id_cuadro]["calificacion"]=$calificacion;
            $this->data["info"][$key->id_cuadro]["criterios"]=$criterios;
            $this->data["info"][$key->id_cuadro]["criterios_detalle"]=$criterios_detalle;
            $this->data["info"][$key->id_cuadro]["notas"]=$notas;
            $this->data["info"][$key->id_cuadro]["tablas"]=$tablas;
            $this->data["info"][$key->id_cuadro]["preguntas"]=$preguntas;
            $this->data["info"][$key->id_cuadro]["alternativas"]=$alternativas;
            $this->data["info"][$key->id_cuadro]["respuestas"]=$respuestas;
            $this->data["info"][$key->id_cuadro]["aponderacion"]=$aponderacion;
            $this->data["info"][$key->id_cuadro]["calificaciones"]=$calificaciones;

        }
        $this->data["archivo"]=$this->input->get('archivo', TRUE);
        $this->data["cuadros"]=$cuadros;
        //declaramos el titulo de la pagina 
        $this->data["title"] = "Encuestas|Actualizar";
       	$this->load->view('reportes/exportar/encuesta', $this->data);
	}
	public function grafico(){
		$id_departamento=($this->input->get_post('id_departamento', TRUE))?$this->input->get_post('id_departamento', TRUE):"";
       	$id_provincia=($this->input->get_post('id_provincia', TRUE))?$this->input->get_post('id_provincia', TRUE):"";
       	$id_distrito=($this->input->get_post('id_distrito', TRUE))?$this->input->get_post('id_distrito', TRUE):"";

       	$usuarioTipo=(isset($_SESSION["usuario"]["tipo"]))?$_SESSION["usuario"]["tipo"]:null;
       	$usuarioIdUsuario=(isset($_SESSION["usuario"]["id_usuario"]))?$_SESSION["usuario"]["id_usuario"]:null;
       	$usuarioGrupoDepartamento=(isset($_SESSION["usuario"]["grupo_departamento"]))?$_SESSION["usuario"]["grupo_departamento"]:null;

       	$this->data["localizacion"]="";
		if ($id_departamento!="") {
			$this->load->model('clsubdepartamento');
			foreach ($this->clsubdepartamento->departamento($id_departamento) as $key) {
				$this->data["localizacion"].=$key->departamento;
			}
		}
		if ($id_provincia!="") {
			$this->load->model('clsubprovincia');
			foreach ($this->clsubprovincia->provincia($id_provincia) as $key) {
				$this->data["localizacion"].="--".$key->provincia;
			}
		}	
		if ($id_distrito!="") {
			$this->load->model('clsubdistrito');
			foreach ($this->clsubdistrito->distrito($id_distrito) as $key) {
				$this->data["localizacion"].="--".$key->distrito;
			}
		}	
       	$this->data["archivo"]=$this->input->get("archivo",TRUE);
       	$this->load->model("clsreporte_calificacion");
		foreach ($this->clsreporte_calificacion->cuadros() as $key) {
			$array[$key->id_cuadro][$key->calificacion]["id_cuadro"]=$key->id_cuadro;
			$array[$key->id_cuadro][$key->calificacion]["titulo"]=$key->titulo;
			$array[$key->id_cuadro][$key->calificacion]["total"]=0;
			$array[$key->id_cuadro][$key->calificacion]["calificacion"]=$key->calificacion;
			$array[$key->id_cuadro][$key->calificacion]["background"]=$key->background;
		}
		$datos=$this->clsreporte_calificacion->total_calificacion($id_departamento,$id_provincia,$id_distrito,$usuarioTipo,$usuarioIdUsuario,$usuarioGrupoDepartamento);
		if ($datos!=null) {
			foreach ($datos as $key) {
				$array[$key->id_cuadro][$key->calificacion]["total"]=$key->total;
			}
		}
		$dat=$this->clsreporte_calificacion->total_aponderacion($id_departamento,$id_provincia,$id_distrito,$usuarioTipo,$usuarioIdUsuario,$usuarioGrupoDepartamento);
		if ($dat!=null) {
			foreach ($dat as $key) {
				$array[11][$key->calificacion]["total"]=$key->total;
			}
		}
		$this->load->helper('reporte_grafico');
		$this->data["tablas"]=ex_grafico($array);
		$this->load->view('reportes/exportar/grafico', $this->data);
	}
}

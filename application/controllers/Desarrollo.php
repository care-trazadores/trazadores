<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class desarrollo extends CI_Controller {
	public $usuario=[];
	public $controlador;
	public $css;
	public $js;
	public  function __construct() {
		parent::__construct();
		$this->base_url=$this->config->config['base_url'];
		$this->css=$this->config->config['css'];
		$this->js=$this->config->config['js'];

		$this->load->helper('menus');
		$this->load->helper('cuadros');
		$this->load->model('clsusuarios');

		$this->usuario['user']="saba";
		$this->usuario['tipo']="master";
		$this->usuario['nombre']="saba";
		$this->usuario['apellido_p']="saba";
		$this->usuario['foto']="team1.jpg";
		$this->usuario['region']="";
		$this->usuario['distrito']="";
		$this->usuario['group_departamento']="";
		$this->usuario['id_distrito']="";
		$this->controlador="usuarios";
   	}
	public function index(){
		$data["title"]="Desarrollo|Listado";
		$data["base_url"]=$this->base_url;
		$data["menus"]=menus($this->base_url,$this->controlador,$this->usuario['tipo']);
		$data["nav"]=nav($this->base_url,$this->usuario['nombre'],$this->usuario['apellido_p'],$this->usuario['foto']);
		$data["css"]=$this->css;
		$data["js"]=$this->js;

		$data["contenido"]="<h1>En Proceso de construccion</h1>";
		$this->load->view('template',$data);
	}
	public function panel()
	{

		
	}
	
}

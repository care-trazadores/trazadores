<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Log extends CI_Controller {
	public  function __construct() {
		parent::__construct();
		session_start();
		$this->data["base_url"]=$this->config->config['base_url'];
		$this->data["css"]=$this->config->config['css'];
		$this->data["js"]=$this->config->config['js'];
		$this->load->helper('menus');
		if (!isset($_SESSION["usuario"])) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
		}
		if ($_SESSION["usuario"]["active"]!=TRUE) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
		}
		$this->controlador="log";
   	}
	public function index()
	{
		$this->data["title"]="Panel";
		$this->data["base_url"]=$this->data["base_url"];
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]["tipo"]);
		$this->data["menus_mobile"]=menus_mobile($this->data["base_url"],$this->controlador,$_SESSION["usuario"]["tipo"]);
		$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

		$this->data["titulo"]="Registros log";
		$this->load->helper('cuadros');

		$i=0;
        $cuadros[$i]["color"]="green";
		$cuadros[$i]["icon"]="clipboard";
		$cuadros[$i]["titulo"]="Registros log Encuestas";
		$cuadros[$i]["cantidad"]="";
		$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=log&m=encuestas";
		$i++;

        $cuadros[$i]["color"]="red";
		$cuadros[$i]["icon"]="clipboard";
		$cuadros[$i]["titulo"]="Registros log Municipal";
		$cuadros[$i]["cantidad"]="";
		$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=log&m=municipal";
		$i++;

		$this->data["links"]=cuadros_link($cuadros);
		$datos["contenido"]=$this->load->view('log/botones',$this->data,TRUE);
		$this->load->view('template',$datos);
	}
	public function encuestas(){
		$this->data["footer_js"][]="filtro_departamento.js";

		$this->load->helper('cuadros');
		$this->data["title"]="Log | Encuestas";
		$this->data["titulo"]="Listado log Encuestas";
		$this->data["base_url"]=$this->data["base_url"];
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]["tipo"]);
		$this->data["menus_mobile"]=menus_mobile($this->data["base_url"],$this->controlador,$_SESSION["usuario"]["tipo"]);
		$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);
		

		$this->data["pagina"]=$this->input->get_post('pagina', TRUE) ? $this->input->get_post('pagina'):1;
		$this->data["limite"]=$this->input->get_post('limite', TRUE) ? $this->input->get_post('limite'):5;
		$this->data["busq_usuario"]=$this->input->get_post('busq_usuario', TRUE) ? $this->input->get_post('busq_usuario'):"";
		$this->data["id_departamento"]=$this->input->get_post('id_departamento', TRUE) ? $this->input->get_post('id_departamento'):"";
		$this->data["id_provincia"]=$this->input->get_post('id_provincia', TRUE) ? $this->input->get_post('id_provincia'):"";
		$this->data["id_distrito"]=$this->input->get_post('id_distrito', TRUE) ? $this->input->get_post('id_distrito'):"";
		$this->data["busq_localidad"]=$this->input->get_post('busq_localidad', TRUE) ? $this->input->get_post('busq_localidad'):"";
		$this->data["busq_accion"]=$this->input->get_post('busq_accion', TRUE) ? $this->input->get_post('busq_accion'):"";

		$comienzo = ($this->data["pagina"] - 1) * $this->data["limite"];

		$this->load->model("clslog_encuesta");
		$this->data["registros"]=$this->clslog_encuesta->log($this->data["limite"],$comienzo,$this->data["busq_usuario"],$this->data["id_departamento"],$this->data["id_provincia"],$this->data["id_distrito"],$this->data["busq_localidad"],$this->data["busq_accion"]);
		$this->data["total"]=$this->clslog_encuesta->log_total($this->data["busq_usuario"],$this->data["id_departamento"],$this->data["id_provincia"],$this->data["id_distrito"],$this->data["busq_localidad"],$this->data["busq_accion"]);
		
		$url_paginador = $this->data["base_url"]."index.php?c=log&m=encuestas";
        
		$this->load->helper('paginador');
        $this->data["paginador"] = paginador($url_paginador, $this->data["total"], $this->data["limite"], $this->data["pagina"], $this->data["registros"], $this->data["busq_usuario"],"", $this->data["id_departamento"], $this->data["id_provincia"], $this->data["id_distrito"],null,$this->data["busq_localidad"],$this->data["busq_accion"]);


		$this->data["titulo_filtro"]="Registros log";
		$datos["contenido"]=$this->load->view('log/encuestas/panel',$this->data,TRUE);
		$this->load->view('template',$datos);
	}
	function municipal(){
		$this->data["footer_js"][]="filtro_departamento.js";

		$this->load->helper('cuadros');
		$this->data["title"]="Log | Municipal";
		$this->data["titulo"]="Listado log Municipal";
		$this->data["base_url"]=$this->data["base_url"];
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]["tipo"]);
		$this->data["menus_mobile"]=menus_mobile($this->data["base_url"],$this->controlador,$_SESSION["usuario"]["tipo"]);
		$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);
		

		$this->data["pagina"]=$this->input->get_post('pagina', TRUE) ? $this->input->get_post('pagina'):1;
		$this->data["limite"]=$this->input->get_post('limite', TRUE) ? $this->input->get_post('limite'):5;
		$this->data["busq_usuario"]=$this->input->get_post('busq_usuario', TRUE) ? $this->input->get_post('busq_usuario'):"";
		$this->data["id_departamento"]=$this->input->get_post('id_departamento', TRUE) ? $this->input->get_post('id_departamento'):"";
		$this->data["id_provincia"]=$this->input->get_post('id_provincia', TRUE) ? $this->input->get_post('id_provincia'):"";
		$this->data["id_distrito"]=$this->input->get_post('id_distrito', TRUE) ? $this->input->get_post('id_distrito'):"";
		$this->data["busq_localidad"]=$this->input->get_post('busq_localidad', TRUE) ? $this->input->get_post('busq_localidad'):"";
		$this->data["busq_accion"]=$this->input->get_post('busq_accion', TRUE) ? $this->input->get_post('busq_accion'):"";

		$comienzo = ($this->data["pagina"] - 1) * $this->data["limite"];

		$this->load->model("clslog_municipal");
		$this->data["registros"]=$this->clslog_municipal->log($this->data["limite"],$comienzo,$this->data["busq_usuario"],$this->data["id_departamento"],$this->data["id_provincia"],$this->data["id_distrito"],$this->data["busq_localidad"],$this->data["busq_accion"]);
		$this->data["total"]=$this->clslog_municipal->log_total($this->data["busq_usuario"],$this->data["id_departamento"],$this->data["id_provincia"],$this->data["id_distrito"],$this->data["busq_localidad"],$this->data["busq_accion"]);
		
		$url_paginador = $this->data["base_url"]."index.php?c=log&m=municipal";
        
		$this->load->helper('paginador');
        $this->data["paginador"] = paginador($url_paginador, $this->data["total"], $this->data["limite"], $this->data["pagina"], $this->data["registros"], $this->data["busq_usuario"],"", $this->data["id_departamento"], $this->data["id_provincia"], $this->data["id_distrito"],null,$this->data["busq_localidad"],$this->data["busq_accion"]);


		$this->data["titulo_filtro"]="Registros log";
		$datos["contenido"]=$this->load->view('log/municipal/panel',$this->data,TRUE);
		$this->load->view('template',$datos);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class administradores extends CI_Controller {
    public $controlador;
    public  function __construct() {
        parent::__construct();
        session_start();
        $this->data["base_url"]=$this->config->config['base_url'];
        if (!isset($_SESSION["usuario"])) {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        if ($_SESSION["usuario"]["active"]!=TRUE) {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        if ($_SESSION["usuario"]["tipo"]=="usuario") {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        
        $this->data["css"]=$this->config->config['css'];
        $this->data["js"]=$this->config->config['js'];
        $this->load->helper('menus');
        $this->load->helper('cuadros');
        $this->load->model('clsadministradores');
        $this->load->model('clsusuarios');
        $this->controlador="administradores";
    }
    public function index() {
        if (!isset($_POST['estado'])) {$estado = 1;} 
        else {$estado = $this->input->post('estado', TRUE);}
        if (!isset($_POST['limite'])) {$limite = 5;} 
        else {$limite = $this->input->post('limite', TRUE);}
        if (!isset($_POST['busq_administrador'])) {$busq_administrador = "";} 
        else {$busq_administrador = $this->input->post('busq_administrador', TRUE);}
        if (!isset($_POST['busq_correo'])) {$busq_correo = "";} 
        else {$busq_correo = $this->input->post('busq_correo', TRUE);}
        if (!isset($_POST['id_departamento'])) {$id_departamento = "";}
        else {$id_departamento = $this->input->post('id_departamento', TRUE);}
        if (!isset($_POST['pagina'])) {$pagina = 1;} 
        else {$pagina = $this->input->post('pagina', TRUE);}
        $url = $this->data["base_url"] . 'index.php?c=administradores&m=panel&estado=' . $estado . '&limite=' . $limite;
        if ($busq_administrador != "") {$url = $url . '&busq_administrador=' . $busq_administrador;}
        if ($busq_correo != "") { $url = $url . '&busq_correo=' . $busq_correo;}
        if ($id_departamento != "") {$url = $url . '&id_departamento=' . $id_departamento;}
        $url = $url . '&pagina=' . $pagina;
        header('Location:' . $url);
    }
    public function panel() {
        /* recibimos todas las variables */
        $estado = $this->input->get('estado', TRUE);
        $limite = $this->input->get('limite', TRUE);
        $pagina = $this->input->get('pagina', TRUE);
        $busq_administrador=$this->input->get('busq_administrador', TRUE);
        $busq_correo = $this->input->get('busq_correo', TRUE);
        $id_departamento = $this->input->get('id_departamento', TRUE);
        /* consultamos si una de las 3 variables es nula no redericcionara a modo default ya que esos datos son prioritarios */
        if ($limite === null or $pagina === null or $estado === null) {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }

        $url_retorno=$this->data["base_url"].'index.php?c=administradores&m=panel&estado='.$estado.'&limite='.$limite;
        if ($busq_administrador!="") { $url_retorno=$url_retorno.'&busq_administrador='.$busq_administrador;}
        if ($busq_correo!="") { $url_retorno=$url_retorno.'&busq_correo='.$busq_correo;}
        if ($id_departamento!="") { $url_retorno=$url_retorno.'&id_departamento='.$id_departamento;}
        $url_retorno=$url_retorno.'&pagina='.$pagina;
        $this->data["url_retorno"]=$url_retorno;

        /* declaramos la variable comienzo que sera dedonde es que empezara a llamar los datos */
        $comienzo = ($pagina - 1) * $limite;
        $this->load->helper('paginador');
        $this->data["title"] = "Administrador|Listado";
        $this->data["base_url"] = $this->data["base_url"];
        $this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto']);


        $cuadros[0]["color"] = "blue";
        $cuadros[0]["icon"] = "users";
        $cuadros[0]["titulo"] = "Total Administrador";
        $cuadros[0]["cantidad"] = $this->clsadministradores->total_usuarios_admi(2);
        $cuadros[1]["color"] = "green";
        $cuadros[1]["icon"] = "users";
        $cuadros[1]["titulo"] = "Administrador Habilitados";
        $cuadros[1]["cantidad"] = $this->clsadministradores->total_usuarios_admi(1);
        $cuadros[2]["color"] = "red";
        $cuadros[2]["icon"] = "users";
        $cuadros[2]["titulo"] = "Administrador Deshabilitados";
        $cuadros[2]["cantidad"] = $this->clsadministradores->total_usuarios_admi(0);
        $this->data["cuadros"] = cuadros($cuadros);

        $usuarios = $this->clsadministradores->listado_total_admi($estado, $limite, $comienzo, $busq_administrador, $busq_correo, $id_departamento);
        $dep = "";
        //var_dump($usuarios);
        $administradores = [];
        /* un recorrido por cada administrador y obteniendo sus regiones por cada administra*/
        foreach ($usuarios as $usu) {
            $id=$usu->id_usuario;
            $user = $usu->user;
            $correo = $usu->correo;
            $estados = $usu->estado;
            $foto = $usu->foto;
            if($id_departamento!=NULL){
            $depa=$usu->id_departamento;
            }  else {
                $depa="";
            }
            /* obteniendo regiones por cada administrador*/
            $region = $this->clsadministradores->departamento_admi($usu->id_usuario);

            $i = 0;

            foreach ($region as $key) {
                if ($i == 0) {
                    $dep = $key->departamento;
                } else {
                    $dep = $dep . "," . $key->departamento;
                }
                $i++;
            }/* fin de obteción de regiones*/
            $departamento = $dep;
            $administradores[]=array($id,$user,$correo,$estados,$depa,$departamento,$foto);
            
            $i=0;
            $dep = "";
            
        }
        $this->data["listado"] = $administradores;

        $total = $this->clsadministradores->total_usuarios_admi($estado, $busq_administrador, $busq_correo, $id_departamento);

        $url_paginador = $this->data["base_url"] . "index.php?c=administradores&m=panel&estado=" . $estado . "&limite=" . $limite;
        $this->data["paginador"] = paginador($url_paginador, $total, $limite, $pagina, $usuarios, $busq_administrador, $busq_correo, $id_departamento);


        $this->data["estado"] = $estado;
        $this->data["limite"] = $limite;
        $this->data["busq_administrador"] = $busq_administrador;
        $this->data["busq_correo"] = $busq_correo;
        $this->data["id_departamento"] = $id_departamento;
        $this->data['titulo'] = "Listado de Administradores";
        $this->data["contenido"] = $this->load->view('administradores/listado_administradores', $this->data, TRUE);
        $this->load->view('template', $this->data);
    }
    function update_estado(){
        if ( ! isset($_POST['id_usuario'])){header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        else{$id_usuario=$this->input->post('id_usuario');}
        if ( ! isset($_POST['estado'])){header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        else{$estado=$this->input->post('estado');}
        $data = array(
            'estado' =>$estado
        );
        $actualizar = $this->clsusuarios->actualizar_usuario($id_usuario,$data);
        //si la actualización ha sido correcta creamos una sesión flashdata para decirlo
        if($actualizar){}
        else{echo "error al guardar";}
    }
    function delete_user(){
        if ( ! isset($_POST['tipo'])){$tipo = "";}
        else{$tipo=$this->input->post('tipo', TRUE);}
        if ( ! isset($_POST['id_usuario'])){$id_usuario = "";}
        else{$id_usuario=$this->input->post('id_usuario');}

        if ($tipo=="" or $id_usuario=="") {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close"); 
        }
        $this->load->model('clsdetalleusuario');
        $this->load->model('clsadmin_regiones');
        $this->clsadmin_regiones->delete_user($id_usuario);
        $this->clsdetalleusuario->delete_user($id_usuario);
        $this->clsusuarios->usuario_delete($tipo,$id_usuario);
        $confirma=$this->clsusuarios->busqueda_user($id_usuario,null);
        if (count($confirma)==0) {
            echo "si";
        }else{echo "no";}
        
    }
    function admin_config(){
        if ( ! isset($_GET['id'])){$id_admin = "";}
        else{$id_admin=$this->input->get('id');}
        if ($id_admin=="") {
           header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        if ( ! isset($_GET['tab'])){$tab = "";}
        else{$tab=$this->input->get('tab');}

        $this->data["title"]="Usuario Actualizar";
        $this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto']);
        $datos=$this->clsusuarios->busqueda_user($id_admin,"administrador");
        foreach ($datos as $key) {
            $this->data["datos_usuario"]=array(
                'id'=>$key->id_usuario,
                'user'=>$key->user,
                'estado'=>$key->estado,
                'nombre' =>$key->nombre,
                'apellido_p'=>$key->apellido_p,
                'apellido_m'=>$key->apellido_m,
                'tipo'=>$key->tipo,
                'correo'=>$key->correo,
                'dni'=>$key->dni,
                'direccion'=>$key->direccion,
                'telefono'=>$key->telefono,
                'celular'=>$key->celular,
                'foto'=>$key->foto
                );
            $this->data["usuario_id_departamentos"][]=$key->id_departamento;
            $this->data["usuario_departamentos"][]=$key->departamento;
        }
        $this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
        $this->load->model("clsdepartamentos");
        $this->data["departamentos"]=$this->clsdepartamentos->departamentos();
        $this->data["tab_posicion"]=$tab;

        $this->data["contenido"]=$this->load->view('administradores/admin_config',$this->data,TRUE);
        $this->load->view('template',$this->data);
    }    
    
    function cuenta_update(){
        $error="";
        if (!isset($_POST['user_id'])){header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        else{$user_id=$this->input->post('user_id');}

        if ($this->input->post('user_usuario')=="") { $error="El campo usuario no puede ser vacio";}
        if ($error=='') {
            $data = array(
                'user' => $this->input->post('user_usuario'),
                'estado' => $this->input->post('user_estado')
            );
        $this->load->model('clsadmin_regiones');
        $this->clsadmin_regiones->delete_user($user_id);
        foreach ($this->input->post('id_departamentos') as $key ) {
            $data_depa=array('id_usuario' =>$user_id,'id_departamento'=>$key);
            $this->clsadmin_regiones->inserte($data_depa);
        }

        $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
            if($actualizar){}
            else{echo "error al guardar";}
        }else{
            echo $error;
        }
    }
    function info_update(){
        $this->load->model('clsdetalleusuario');
        if ( ! isset($_POST['user_id'])){header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        else{$user_id=$this->input->post('user_id');}
        $data = array(
            'nombre' => $this->input->post('user_nombre'),
            'apellido_p' => $this->input->post('user_apellido_p'),
            'apellido_m' => $this->input->post('user_apellido_m'),
            'dni' => $this->input->post('user_dni'),
            'correo' => $this->input->post('user_correo'),
            'direccion' => $this->input->post('user_direccion'),
            'telefono' => $this->input->post('user_telefono'),
            'celular' => $this->input->post('user_celular')
        );
        $actualizar = $this->clsdetalleusuario->actualizar_usuario_info($user_id,$data);
        //si la actualización ha sido correcta creamos una sesión flashdata para decirlo
        if($actualizar){}
        else{echo "error al guardar";}
    }
    function foto_update(){
        $id_usuario=$this->input->post('user_id');
        $error="";
        $file = $_FILES["file-foto"];
        $nombre = $file["name"];
        if ($nombre=="") {
            echo "Lo sentimos, debe seleccionar una imagen";
        }else{
            $tipo=$file["type"];
            $ruta_provisional=$file["tmp_name"];
            $size=$file["size"];
            $dimensiones=getimagesize($ruta_provisional);
            $width=$dimensiones[0];
            $height=$dimensiones[1];
            $carpeta="img/usuarios/";
            if ($tipo!='image/jpg' && $tipo!='image/jpeg' && $tipo!='image/png' && $tipo!='image/gif') {
                $error="Error, el archivo no es una imagen";
            }elseif($size>1024*1024){
                $error="Error, el tamaño maximo permitido es 1mb";
            }elseif ($width>500 && $width<60 && $height>500 && $height<60) {
                $error="Error, la anchura y la altura de la imagen debe de ser inferior 500px y mayor a 60 px";
            }
            if ($error=="") {
                if ($tipo=='image/jpg' || $tipo=='image/jpeg') {$extension=".jpg";}
            
                if ($tipo=='image/png') {$extension=".png";}
                if ($tipo=='image/gif') {$extension=".gif";}
                $nombre_foto='usuario'.$id_usuario.$extension;
                $src=$carpeta.$nombre_foto;
                move_uploaded_file($ruta_provisional, $src);
                $data = array(
                    'foto' => $nombre_foto
                );
                
                $this->load->model('clsdetalleusuario');
                $actualizar = $this->clsdetalleusuario->actualizar_usuario_info($id_usuario,$data);
                //si la actualización ha sido correcta creamos una sesión flashdata para decirlo
                if($actualizar){}
                else{echo "error al guardar";}
                //$this->redimensionar_jpeg($nombre_foto);
            }
        }

        
    }
    function password_update(){
        $error="";
        if (!isset($_POST['user_id'])){header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        else{$user_id=$this->input->post('user_id');}

        if ($this->input->post('password_new')=="") { $password_new='';$error="Ingrese nueva clave";}
        else{$password_new=md5($this->input->post('password_new'));}

        if ($this->input->post('password_rep')=="") { $password_rep='';$error="Debe repetir la clave";}
        else{$password_rep=md5($this->input->post('password_rep'));}

        if ($_SESSION["usuario"]["tipo"]!="usuario") {
            if ($password_new!=$password_rep) {
                $error="Las claves nuevas no coinciden";
            }else{
                $data = array(
                    'pass' => $password_new
                );
                $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
                if($actualizar){}
                else{echo "error al guardar";}
            }
        }else{
            if ($this->input->post('password_ant')=="") {$password_ant='';$error="Necesita colocar la clave anterior";}
            else{$password_ant=md5($this->input->post('password_ant'));}

            $datos=$this->clsusuarios->busqueda_user($user_id,"usuario");
            foreach ($datos as $key) {
                $pass_anterior=$key->pass;
            }
            if ($password_new!=$password_rep) {
                $error="Las claves nuevas no coinciden";
            }elseif ($pass_anterior!=$password_ant) {
                $error="Las clave anterior no coinciden";
            }

            if ($error!="") {
                echo $error;
            }else{
                $data = array(
                    'pass' => $password_new
                );
                $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
                if($actualizar){}
                else{echo "error al guardar";}
            }
        }
        echo $error;
    }
    function nuevo(){
        if ($_SESSION["usuario"]["tipo"]=="usuario" || $_SESSION["usuario"]["tipo"]=="administrador") {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        $error=0;
        $errores="";
        $this->data["usuario"]="";
        $this->data["password"]="";
        $this->data["id_distrito"]="";
        $this->data["nombres"]="";
        $this->data["apellido_p"]="";
        $this->data["apellido_m"]="";
        $this->data["dni"]="";
        $this->data["direccion"]="";
        $this->data["correo"]="";
        $this->data["telefono"]="";
        $this->data["celular"]="";
        $this->data["success"]="";
        $nombre_foto="";

        if (isset($_POST["enviar"])) {
            $errores="Por favor verificar los siguientes campos:<br>";
            if ($this->input->post('usuario')=="") {
                $usuario='';$error++;$errores.="- Necesita Ingresar un nombre de usuario<br>";}
            else{
                $usuario=$this->input->post('usuario');
                $this->data["usuario"]=$usuario;}

            if ($this->input->post('password')=="") {
                $password='';$error++;$errores.="- Necesita Ingresar una contraseña<br>";}
            else{
                $password=$this->input->post('password');
                $this->data["password"]=$password;}

            
    
            if ($this->input->post('nombres')=="") {$nombres='';$error++;$errores.="- El campo nombre es obligatorio<br>";}
            else{$nombres=$this->input->post('nombres');$this->data["nombres"]=$nombres;}

            if ($this->input->post('apellido_p')=="") {$apellido_p='';$error++;$errores.="- El campo apellido paterno es obligatorio<br>";}
            else{$apellido_p=$this->input->post('apellido_p');$this->data["apellido_p"]=$apellido_p;}

            if ($this->input->post('id_departamentos')=="") {$id_departamentos='';$error++;$errores.="- Debe elegir al menos una region<br>";}
            else{$id_departamentos=$this->input->post('id_departamentos');$this->data["id_departamentos"]=$id_departamentos;}

            $apellido_m=$this->input->post('apellido_m');
            $this->data["apellido_m"]=$apellido_m;
            $dni=$this->input->post('dni');
            $this->data["dni"]=$dni;
            $correo=$this->input->post('correo');
            $this->data["correo"]=$correo;
            $direccion=$this->input->post('direccion');
            $this->data["direccion"]=$direccion;
            $telefono=$this->input->post('telefono');
            $this->data["telefono"]=$telefono;
            $celular=$this->input->post('celular');
            $this->data["celular"]=$celular;


            if ($error==0) {
                if ($this->clsusuarios->verificar_user($usuario)==0) {
                    $data = array(
                        'user' => $usuario,
                        'pass' => md5($password),
                        'id_distrito' =>NULL,
                        'tipo' =>"administrador",
                        'estado' =>1
                    );
                    $id_usuario=$this->clsusuarios->insertar($data);
                    $file=$_FILES['file-foto'];
                    if ($file!="") {
                        $nombre=$file["name"];
                        if ($nombre!="") {
                            $tipo=$file["type"];
                            $ruta_provisional=$file["tmp_name"];
                            $size=$file["size"];
                            $dimensiones=getimagesize($ruta_provisional);
                            $width=$dimensiones[0];
                            $height=$dimensiones[1];
                            $carpeta="img/usuarios/";
                            $error_foto=0;
                            if ($tipo!='image/jpg' && $tipo!='image/jpeg' && $tipo!='image/png' && $tipo!='image/gif') {
                                $error++;
                                $error_foto++;
                                $errores.="- Error, el archivo no es una imagen<br>";
                            }elseif($size>1024*1024){
                                $error++;
                                $error_foto++;
                                $errores.="- Error, el tamaño maximo permitido es 1mb";
                            }elseif ($width>500 && $width<60 && $height>500 && $height<60) {
                                $error++;
                                $error_foto++;
                                $errores.="- Error, la anchura y la altura de la imagen debe de ser inferior 500px y mayor a 60 px";
                            }
                            if ($error_foto==0) {
                                if ($tipo=='image/jpg' || $tipo=='image/jpeg') {$extension=".jpg";}
                                if ($tipo=='image/png') {$extension=".png";}
                                if ($tipo=='image/gif') {$extension=".gif";}
                                $nombre_foto='usuario'.$id_usuario.$extension;
                                $src=$carpeta.$nombre_foto;
                                move_uploaded_file($ruta_provisional, $src);
                            }
                        }
                    }
                    $this->load->model('clsdetalleusuario');
                    $datos = array(
                        'id_usuario' => $id_usuario,
                        'nombre' => $nombres,
                        'apellido_p' =>$apellido_p,
                        'apellido_m' =>$apellido_m,
                        'dni' =>$dni,
                        'correo' =>$correo,
                        'direccion' =>$direccion,
                        'telefono' =>$telefono,
                        'celular' =>$celular,
                        'foto' =>$nombre_foto
                    );
                    $this->clsdetalleusuario->insertar($datos);
                    $this->load->model('clsadmin_regiones');
                    foreach ($id_departamentos as $key) {
                        $datos = array(
                            'id_usuario' => $id_usuario,
                            'id_departamento' => $key
                        );
                        $this->clsadmin_regiones->inserte($datos);

                        $this->data["success"]="<p class='alert alert-success'>El Administrador fue creado correctamente.</p>";
                        $this->data["usuario"]="";
                        $this->data["password"]="";
                        $this->data["nombres"]="";
                        $this->data["apellido_p"]="";
                        $this->data["apellido_m"]="";
                        $this->data["dni"]="";
                        $this->data["direccion"]="";
                        $this->data["correo"]="";
                        $this->data["telefono"]="";
                        $this->data["celular"]="";
                    }
                }else{
                    $errores.="- lo sentimos el usuario ya existe";
                }
            }           
        }
        $this->data["errores"]=$errores;
        $this->data["title"]="usuarios|Nuevo";
        $this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],
            $_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto']);

        $this->load->model('clsdepartamentos');
        $this->data["departamentos"]=$this->clsdepartamentos->departamentos();

        $this->data["contenido"]=$this->load->view('administradores/new_administrador',$this->data,TRUE);
        $this->load->view('template',$this->data);
    }
    function vista(){
        if (!isset($_GET["id_usuario"])){header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        else{$id_usuario=$this->input->get('id_usuario');}
        if ( ! isset($_GET['tab'])){$tab = "";}
        else{$tab=$this->input->get('tab');}

        $datos=$this->clsusuarios->busqueda_user($id_usuario,"administrador");
        foreach ($datos as $key) {
            $this->data["datos_usuario"]=array(
                'id'=>$key->id_usuario,
                'user'=>$key->user,
                'estado'=>$key->estado,
                'nombre' =>$key->nombre,
                'apellido_p'=>$key->apellido_p,
                'apellido_m'=>$key->apellido_m,
                'tipo'=>$key->tipo,
                'correo'=>$key->correo,
                'dni'=>$key->dni,
                'direccion'=>$key->direccion,
                'telefono'=>$key->telefono,
                'celular'=>$key->celular,
                'foto'=>$key->foto
                );
            $this->data["usuario_id_departamentos"][]=$key->id_departamento;
            $this->data["usuario_departamentos"][]=$key->departamento;
        }
        $this->load->model('clsllenado');
        $encuestas=$this->clsllenado->reportes(null,null,null,null,
                        null,null,"administrador",$id_usuario,
                        $this->data["usuario_id_departamentos"]);
        $usuarios=$this->clsusuarios->listado_total(2,null,null,null,
                        null,null,null,null,$this->data["usuario_id_departamentos"]);
        $this->load->helper('encuestas');
        $this->load->helper('usuarios');
        $this->data["graficos"]="";
        $this->data["graficos"].=encuestas_graficos($encuestas);
        $this->data["graficos"].=encuestas_tabla_comunidad($encuestas,$this->data["base_url"]);

        $this->data["graficos"].=tabla_usuarios($usuarios,$this->data["base_url"]);
        $this->data["title"]="usuarios|Nuevo";
        $this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],
            $_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto']);
        $this->data["contenido"]=$this->load->view('administradores/vista',$this->data,TRUE);
        $this->load->view('template',$this->data);
    } 
}

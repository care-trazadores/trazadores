<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	public $controlador;
	public  function __construct() {
		parent::__construct();
		session_start();
		$this->data["base_url"]=$this->config->config['base_url'];
		$this->data["css"]=$this->config->config['css'];
		$this->data["js"]=$this->config->config['js'];
		$this->load->helper('menus');
		$this->load->helper('cuadros');
		$this->load->model('clsusuarios');
		$this->load->model('clsllenado');
		if (!isset($_SESSION["usuario"])) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
		}
		if ($_SESSION["usuario"]["active"]!=TRUE) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
		}
		$this->controlador="inicio";
   	}
	public function index()
	{
		$this->data["title"]="Panel";
		$this->data["base_url"]=$this->data["base_url"];
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]["tipo"]);
		$this->data["menus_mobile"]=menus_mobile($this->data["base_url"],$this->controlador,$_SESSION["usuario"]["tipo"]);
		$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

		$i=0;
		if ($_SESSION["usuario"]['tipo']!=="usuario") {
			$cuadros[$i]["color"]="blue";
			$cuadros[$i]["icon"]="users";
			$cuadros[$i]["titulo"]="Usuarios";
			$cuadros[$i]["cantidad"]=$this->clsusuarios->total_usuarios($_SESSION["usuario"]["grupo_departamento"]);
			$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=usuarios";
			$i++;
		}
		$cuadros[$i]["color"]="red";
		$cuadros[$i]["icon"]="clipboard";
		$cuadros[$i]["titulo"]="Encuestas";
		$cuadros[$i]["cantidad"]=$this->clsllenado->count_encuestas(
			$_SESSION["usuario"]['tipo'],
			$_SESSION["usuario"]['grupo_departamento'],
			$_SESSION["usuario"]['id_usuario']);
		$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=encuestas";
		$i++;
		if ($_SESSION["usuario"]['tipo']=="master") {
			$cuadros[$i]["color"]="green";
			$cuadros[$i]["icon"]="comments";
			$cuadros[$i]["titulo"]="Administradores";
			$cuadros[$i]["cantidad"]=$this->clsusuarios->total_administradores();
			$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=administradores";
			$i++;
		}
		$cuadros[$i]["color"]="purple";
		$cuadros[$i]["icon"]="comments";
		$cuadros[$i]["titulo"]="Localidades";
		$cuadros[$i]["cantidad"]=$this->clsllenado->count_encuestas(
			$_SESSION["usuario"]['tipo'],
			$_SESSION["usuario"]['grupo_departamento'],
			$_SESSION["usuario"]['id_usuario']);
		$cuadros[$i]["link"]="";

		$this->data["cuadros_link"]=cuadros_link($cuadros);
		
		$this->load->model('clsllenado');
		$encuestas=$this->clsllenado->reportes(null,null,null,null,
                        null,null,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],
                        $_SESSION["usuario"]["grupo_departamento"]);


		$this->load->helper('encuestas');
		$this->data["graficos"]="";
		$this->data["graficos"].=encuestas_graficos($encuestas);
		$this->data["graficos"].=encuestas_tabla_comunidad($encuestas,$this->data["base_url"]);


		$datos["contenido"]=$this->load->view('dashboard',$this->data,TRUE);
		$this->load->view('template',$datos);
	}
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class provincia extends CI_Controller {

    public $usuario = [];
    public $controlador;
    public $css;
    public $js;

    public function __construct() {
        parent::__construct();
        $this->base_url = $this->config->config['base_url'];
        $this->css = $this->config->config['css'];
        $this->js = $this->config->config['js'];
        $this->load->helper('menus');
        $this->load->helper('cuadros');
        $this->load->helper('paginador');

        $this->usuario['user'] = "saba";
        $this->usuario['tipo'] = "master";
        $this->usuario['nombre'] = "saba";
        $this->usuario['apellido_p'] = "saba";
        $this->usuario['foto'] = "team1.jpg";
        $this->usuario['region'] = "";
        $this->usuario['distrito'] = "";
        $this->usuario['group_departamento'] = "";
        $this->usuario['id_distrito'] = "";
        $this->controlador = "provincia";
        /* Modelos */
        $this->load->model('clsdistritos');
    }

    public function index() {
        if (!isset($_POST['limite'])) {
            $limite = 5;
        } else {
            $limite = $this->input->post('limite', TRUE);
        }


        if (!isset($_POST['provincia'])) {
            $provincia = "";
        } else {
            $provincia = $this->input->post('provincia', TRUE);
        }
       
        if (!isset($_POST['pagina'])) {
            $pagina = 1;
        } else {
            $pagina = $this->input->post('pagina', TRUE);
        }
        
        if (!isset($_POST['id_departamento'])) {
            $id_departamento = "";
        } else {
            $id_departamento = $this->input->post('id_departamento', TRUE);
        }
        if (!isset($_POST['id_provincia'])) {
            $id_provincia = "";
        } else {
            $id_provincia = $this->input->post('id_provincia', TRUE);
        }
        $url = $this->base_url . 'index.php?c=provincia&m=panel&limite=' . $limite;


        if ($provincia != "") {
            $url = $url . '&provincia=' . $provincia;
        }
        
         if ($id_departamento != "") {
            $url = $url . '&id_departamento=' . $id_departamento;
        }
         
         if ($id_provincia != "") {
            $url = $url . '&id_provincia=' . $id_provincia;
        }
        $url = $url . '&pagina=' . $pagina;
        header('Location:' . $url);
    }
   

    public function panel() {
        
        if (!isset($_POST['limite'])) {
            $limite = 5;
        } else {
            $limite = $this->input->post('limite', TRUE);
        }


        if (!isset($_POST['provincia'])) {
            $provincia = "";
        } else {
            $provincia = $this->input->post('provincia', TRUE);
        }
        
        if (!isset($_POST['pagina'])) {
            $pagina = 1;
        } else {
            $pagina = $this->input->post('pagina', TRUE);
        }
       
        if (!isset($_POST['id_departamento'])) {
            $id_departamento = "";
        } else {
            $id_departamento = $this->input->post('id_departamento', TRUE);
        }
        if (!isset($_POST['id_provincia'])) {
            $id_provincia = "";
        } else {
            $id_provincia = $this->input->post('id_provincia', TRUE);
        }

        $data["title"] = "Distrito";
        $data["base_url"] = $this->base_url;
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data["titulo"] = "Ingresar Datos de Provincia";
        $data["menus"] = menus($this->base_url, $this->controlador, $this->usuario['tipo']);
        $data["nav"] = nav($this->base_url, $this->usuario['nombre'], $this->usuario['apellido_p'], $this->usuario['foto']);
      
        $data["maxprovincia"] = $this->clsdistritos->max('ubprovincia','idProv');


        /* recibimos todas las variables */
        $limite = $this->input->get('limite', TRUE);
        $pagina = $this->input->get('pagina', TRUE);
        
        $provincia = $this->input->get('provincia', TRUE);
        $id_provincia = $this->input->get('id_provincia', TRUE);
        $id_departamento = $this->input->get('id_departamento', TRUE);
        /* consultamos si una de las 3 variables es nula no redericcionara a modo default ya que esos datos son prioritarios */
//        if ($limite === null or $pagina === null or $estado === null) {
//            header('Location:' . $this->base . 'index.php?c=busq_distritos');
//        }
        /* declaramos la variable comienzo que sera dedonde es que empezara a llamar los datos */
       
        $comienzo = ($pagina - 1) * $limite;

        
        /* Para Provincia */
        $provincias = $this->clsdistritos->listado_provincia($limite, $comienzo, $provincia,$id_departamento);
        $data["listadoprov"] = $provincias;

        $total_prov = $this->clsdistritos->total_provincia($provincia,$id_departamento);

        $url_paginador_prov = $this->base_url . "index.php?c=provincia&m=panel&limite=" . $limite;
        $data["paginador"] = paginador($url_paginador_prov, $total_prov, $limite, $pagina, $provincias, $provincia, "", $id_departamento, "", "");

        
        $data["limite"] = $limite;
        $data["prov"] = $provincia;
        $data["id_provincia"] = $id_provincia;
        $data["id_departamento"] = $id_departamento;
        $data["contenido"] = $this->load->view('provincias', $data, TRUE);
        $this->load->view('template', $data);
    }

    public function insertar_provincia() {
        $id_provincia = $this->input->post('txtId_provincia');
        $provincia = $this->input->post('txtProvincia');
        $id_departamento = $this->input->post('id_departamento');
        $prov = $this->clsdistritos->get_provincia($id_provincia);
        if ($prov) {
            $insetProvincia = $this->clsdistritos->update('ubprovincia', array('provincia' => $provincia, 'idDepa' => $id_departamento), array('idProv' => $id_provincia));
        } else {
            $insetProvincia = $this->clsdistritos->insert('ubprovincia', array('idProv' => $id_provincia, 'provincia' => $provincia, 'idDepa' => $id_departamento));
        }


        redirect('?c=provincia');
    }
    public function edit_provincia() {
        $id_provincia = $this->input->post('id_provincia', TRUE);
        $provincia = $this->clsdistritos->get_provincia($id_provincia);

        if ($provincia) {
            foreach ($provincia as $prov) {
                $idProv = $prov->idProv;
                $provin = $prov->provincia;
                $idDepa = $prov->idDepa;
            }
            $js = array(
                'estado' => TRUE,
                'idProv' => $idProv,
                'provincia' => $provin,
                'idDepa' => $idDepa,
                'mensaje' => "Tiene Datos"
            );
        } else {
            $js = array(
                'estado' => FALSE,
                'mensaje' => "No se encontro registros"
            );
        }
        echo json_encode($js);
    }

    public function delete_provincia() {
     $id_provincia = $this->input->post('id_provincia', TRUE);
      $objProv=  $this->clsdistritos->delete('ubprovincia',array('idProv'=>$id_provincia));
      
      if($objProv){
          $json=array(
              'estado'=>TRUE,
              'mensaje'=>"Se elimino correctamentte"
          );
      }  else {
          $json=array(
              'estado'=>FALSE,
              'mensaje'=>"Ocurrio un Error"
          );
      }
      echo json_encode($json);   
    }
    

   

}

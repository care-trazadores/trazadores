<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Municipal extends CI_Controller {

    public function __construct() {
        parent::__construct();
        session_start();
        $this->data["base_url"]=$this->config->config['base_url'];
        if (!isset($_SESSION["usuario"])) {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        if ($_SESSION["usuario"]["active"]!=TRUE) {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        $this->data["css"]=$this->config->config['css'];
        $this->data["js"]=$this->config->config['js'];

        $this->load->helper('menus');
        $this->controlador="municipal";
        $this->data["base_url"] = $this->data["base_url"];
        $this->data["menus"] = menus($this->data["base_url"], $this->controlador, $_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

    }

    public function index() {
        if (!isset($_POST['estado'])) {$estado = 2;} else {$estado = $this->input->post('estado', TRUE);}
        if (!isset($_POST['limite'])) {$limite = 5;} else {$limite = $this->input->post('limite', TRUE); }
        if (!isset($_POST['busq_usuario'])) {$busq_usuario = "";} else {$busq_usuario = $this->input->post('busq_usuario', TRUE); }
        if (!isset($_POST['id_departamento'])) {$id_departamento = "";} else {$id_departamento = $this->input->post('id_departamento', TRUE);}
        if (!isset($_POST['id_provincia'])) { $id_provincia = "";} else {$id_provincia = $this->input->post('id_provincia', TRUE); }
        if (!isset($_POST['id_distrito'])) { $id_distrito = "";} else {$id_distrito = $this->input->post('id_distrito', TRUE);}
        if (!isset($_POST['pagina'])) {$pagina = 1;} else {$pagina = $this->input->post('pagina', TRUE);} 
        $url = $this->data["base_url"] . 'index.php?c=municipal&m=panel&estado=' . $estado . '&limite=' . $limite;

        if ($busq_usuario != "") { $url = $url . '&busq_usuario=' . $busq_usuario;}
        if ($id_departamento != "") {$url = $url . '&id_departamento=' . $id_departamento;}
        if ($id_provincia != "") {$url = $url . '&id_provincia=' . $id_provincia;}
        if ($id_distrito != "") { $url = $url . '&id_distrito=' . $id_distrito;}
        $url = $url . '&pagina=' . $pagina;
        header('Location:' . $url);
    }



    public function panel() {
        /* recibimos todas las variables */
        $estado = $this->input->get('estado', TRUE);
        $limite = $this->input->get('limite', TRUE);
        $pagina = $this->input->get('pagina', TRUE);
        $busq_usuario = $this->input->get('busq_usuario', TRUE);
        $id_departamento = $this->input->get('id_departamento', TRUE);
        $id_provincia = $this->input->get('id_provincia', TRUE);
        $id_distrito = $this->input->get('id_distrito', TRUE);
        /* consultamos si una de las 3 variables es nula no redericcionara a modo default ya que esos datos son prioritarios */
        if ($limite === null or $pagina === null or $estado === null) {
           header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }

        /* declaramos la variable comienzo que sera dedonde es que empezara a llamar los datos */
        $comienzo = ($pagina - 1) * $limite;
        /* incluimos elmodelo */
        $this->load->model('clsllenadoMunicipal');
        /* incluimos helper */
        $this->load->helper('cuadros');
        $this->load->helper('paginador');

        $this->data["title"] = "preguntas|Listado";
        $this->data["menus"] = menus($this->data["base_url"], $this->controlador, $_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

        //creamos el cuadro de las encuestas

        $cuadros[0]["color"] = "blue";
        $cuadros[0]["icon"] = "users";
        $cuadros[0]["titulo"] = "Total ATM";
        $cuadros[0]["cantidad"] = $this->clsllenadoMunicipal->total_encuestas(null, null, null, null,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
        $this->data["cuadros"] = cuadros($cuadros);

        $encuestas = $this->clsllenadoMunicipal->reportes($limite, $comienzo, $busq_usuario, $id_departamento, $id_provincia, $id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
        $this->data["listado"] = $encuestas;
        $total = $this->clsllenadoMunicipal->total_encuestas($busq_usuario, $id_departamento, $id_provincia, $id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
        $url_paginador = $this->data["base_url"] . "index.php?c=municipal&m=panel&estado=" . $estado . "&limite=" . $limite;
        $this->data["paginador"] = paginador($url_paginador, $total, $limite, $pagina, $encuestas, $busq_usuario,"", $id_departamento, $id_provincia, $id_distrito);

        if ($_SESSION["usuario"]["tipo"]=="usuario") {$this->data["id_usuario"]=$_SESSION["usuario"]["id_usuario"];}
        else{$this->data["id_usuario"]=null;}

        $this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
        $this->data["estado"] = $estado;
        $this->data["limite"] = $limite;
        $this->data["busq_usuario"] = $busq_usuario;
        $this->data["id_departamento"] = $id_departamento;
        $this->data["id_provincia"] = $id_provincia;
        $this->data["id_distrito"] = $id_distrito;
        $this->data['titulo'] = "Listado de Encuestas ATM ";
        $this->data["contenido"] = $this->load->view('municipal/listado_encuestas', $this->data, TRUE);
        $this->load->view('template', $this->data);

    }
    function llenado(){
        // if ($_SESSION["usuario"]["tipo"]!="usuario") {header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        $this->data["datos_llenado"]["id_llenado"]="";
        $this->data["datos_llenado"]["comunidad"]="";
        $this->data["datos_llenado"]["usuario"]=$_SESSION["usuario"]["user"];
        $this->data["datos_llenado"]["id_usuario"]=$_SESSION["usuario"]["id_usuario"];
        $this->data["datos_llenado"]["departamento"]="";
        $this->data["datos_llenado"]["provincia"]="";
        $this->data["datos_llenado"]["distrito"]="";
        $this->data["datos_llenado"]["id_distrito"]="";
        $this->data["datos_llenado"]["fecha"]=date("d-m-Y");
        $this->load->helper('cuadros_preguntas');
        $this->load->helper('cuadro_tipo0');
        $this->load->helper('cuadro_tipo1');
        $this->load->helper('cuadro_tipo2');
        $this->load->helper('cuadro_tipo3');
        $this->load->helper('cuadro_tipo4');
        $this->load->helper('cuadro_tipo5');
        $this->data["title"] = "Encuestas ATM|Actualizar";

        //luego cargamos los modelos que se usaran
        $this->load->model('clsdesarrolloMunicipal');
        $this->load->model('clsaponderacionMunicipal');
        $this->load->model('clsllenadoMunicipal');
        $this->load->model('clscuadrosMunicipal');
        $this->load->model('clstablasMunicipal');
        $this->load->model('clspreguntasMunicipal');
        $this->load->model('clscalificacionMunicipal');
        $this->load->model('clscriteriosMunicipal');
        $this->load->model('clsnotasMunicipal');
        $this->load->model('clsanexoMunicipal');

        $cuadros_impresion='';

        $cuadros=$this->clsdesarrolloMunicipal->cuadros();
        $this->data["tabs"]=tabs($cuadros);
        $anexo=null;
        $anexo_cuadro=null;
        $anexo_pregunta=null;
        $respuestas=null;
        $aponderacion=null;
        $calificaciones=null;
        foreach ($cuadros as $key) {
            $calificacion=$this->clsdesarrolloMunicipal->calificacion($key->id_cuadro);
            $criterios=$this->clsdesarrolloMunicipal->criterios($key->id_cuadro);
            $criterios_detalle=$this->clsdesarrolloMunicipal->criterios_detalle($criterios);
            $notas=$this->clsdesarrolloMunicipal->notas($key->id_cuadro);
            $tablas=$this->clsdesarrolloMunicipal->tablas($key->id_cuadro);
            $preguntas=$this->clsdesarrolloMunicipal->preguntas($key->id_cuadro);
            $alternativas=$this->clsdesarrolloMunicipal->alternativas($preguntas);
            $anexo_cuadro=$this->clsanexoMunicipal->anexo_cuadro($key->id_cuadro);
            foreach ($preguntas as $k) {
                $anexo=$this->clsanexoMunicipal->anexo_pregunta($k->id_pregunta);
                if ($anexo!=null) {
                    foreach ($anexo as $ke) {
                       $anexo_pregunta[$k->id_pregunta]=$ke->id_cuadro;
                    }
                }
            }
            //$repuestas=$this->clsdesarrollo->respuestas();
            if ($key->tipo==0) {
            }elseif ($key->tipo==3) {
            }else if ($key->tipo==4) {
                $aponderacion=$this->clsaponderacion->aponderacion();
            }elseif ($key->tipo==5) {
            }
            $cuadros_impresion.=cuadrostipo(
                $key->id_cuadro,
                $key->tipo,
                $key->titulo,
                $calificacion,
                $criterios,
                $criterios_detalle,
                $notas,
                $tablas,
                $preguntas,
                $alternativas,
                $respuestas,
                $aponderacion,
                $calificaciones,
                $anexo_cuadro,
                $anexo_pregunta);
        }
        $this->data["cuadros_preguntas"]=$cuadros_impresion;
        $this->data["contenido"]=$this->load->view('municipal/llenado', $this->data, TRUE);
        $this->load->view('template', $this->data);

    }

    function edit(){        
        //primero verificamos si se envio la variable de id sino reedireccionamos
        if (!isset($_GET["id"])) {header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        $id_llenado=$this->input->get('id', TRUE);
        $this->data["datos_llenado"]["fecha"]=date("d-m-Y"); 
        $this->load->helper('cuadro_tipo0');
        $this->load->helper('cuadro_tipo1');
        $this->load->helper('cuadro_tipo2');
        $this->load->helper('cuadro_tipo3');

        //luego cargamos los modelos que se usaran
        $this->load->model('clsdesarrolloMunicipal');
        $this->load->model('clsaponderacionMunicipal');
        $this->load->model('clsllenadoMunicipal');
        $this->load->model('clsdeetalle_llenadoMunicipal');
        $this->load->model('clscuadrosMunicipal');
        $this->load->model('clstablasMunicipal');
        $this->load->model('clspreguntasMunicipal');
        $this->load->model('clscalificacionMunicipal');
        $this->load->model('clscriteriosMunicipal');
        $this->load->model('clsnotasMunicipal');
        $this->load->model('clsanexoMunicipal');
        //declaramos los datos de la encuesta
        foreach ($this->clsllenadoMunicipal->llenado_por_id($id_llenado) as $key) {
            $this->data["datos_llenado"]["id_llenado"]=$id_llenado;
            // $this->data["datos_llenado"]["comunidad"]=$key->comunidad;
            $this->data["datos_llenado"]["id_usuario"]=$_SESSION["usuario"]["id_usuario"];
            $this->data["datos_llenado"]["usuario"]=$_SESSION["usuario"]["user"];
            $this->data["datos_llenado"]["departamento"]=$key->departamento;
            $this->data["datos_llenado"]["provincia"]=$key->provincia;
            $this->data["datos_llenado"]["distrito"]=$key->distrito;
            $this->data["datos_llenado"]["id_distrito"]=$key->idDist;
        }
        /*if ($_SESSION["usuario"]["tipo"]=="usuario") {       
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }*/
        //cargamos el helper que realiza los cuadros de llenado o edicion
        $this->load->helper('cuadros_preguntas');
        //declaramos la variable cuadros_impresion que guardara todo una cadena de texto de los cuadros
        $cuadros_impresion='';
        //rescatamos todos los cuadros
        $cuadros=$this->clscuadrosMunicipal->cuadros($id_llenado);
        $this->data["cuadros"] = $cuadros;
        //declaramos la variable tabs con la funcion tabs y enviamos los cuadros esto devolvera una cadena de texto de los tabs
        //que se usaran para cambvios modificar el helper cuadros_preguntas function tabs
        $this->data["tabs"]=tabs($cuadros);
        //declaramos la variable respuestas como un array vacio;
        $respuestas=[];
        $calificacion="";       
        $criterios="";
        $criterios_detalle="";
        $notas="";
        $tablas="";
        $preguntas="";
        $alternativas="";
        $anexo=null;
        $anexo_cuadro=null;
        $anexo_pregunta=null;
        $jass=[];
        $jass_alternativa=null;
        //luego recorremos la variable cuadro uno por uno con foreach
        foreach ($cuadros as $key) {
            //rescatamos todas la calificaciones y las guardamos en la variable calificacion
            $calificacion=$this->clsdesarrolloMunicipal->calificacion($key->id_cuadro);
            //rescatamos todos los cuadros de criterios y los guardamos en las variables
            $criterios=$this->clsdesarrolloMunicipal->criterios($key->id_cuadro);
            $criterios_detalle=$this->clsdesarrolloMunicipal->criterios_detalle($criterios);
            $notas=$this->clsdesarrolloMunicipal->notas($key->id_cuadro);
            $tablas=$this->clsdesarrolloMunicipal->tablas($key->id_cuadro);
            $preguntas=$this->clsdesarrolloMunicipal->preguntas($key->id_cuadro);
            $alternativas=$this->clsdesarrolloMunicipal->alternativas($preguntas);
            $anexo_cuadro=$this->clsanexoMunicipal->anexo_cuadro($key->id_cuadro);
           // var_dump($anexo_cuadro);
            foreach ($preguntas as $k) {
                $anexo=$this->clsanexoMunicipal->anexo_pregunta($k->id_pregunta);
                if ($anexo!=null) {
                    foreach ($anexo as $ke) {
                       $anexo_pregunta[$k->id_pregunta]=$ke->id_cuadro;
                    }
                }
            }
            $aponderacion=null;
            $calificaciones=null;
            //$respuestas=$this->clsdesarrollo->respuestas($id_llenado,$preguntas);
            foreach ($preguntas as $keys) {
                $respuestas[$keys->id_pregunta]=$this->clsdesarrolloMunicipal->respuestas($id_llenado,$keys->id_pregunta);
            }
            if ($key->tipo==0) {
            }elseif ($key->tipo==3) {          
            }else if ($key->tipo==4) {
                $criterios=null;
                $criterios_detalle=null;
                $notas=null;
                $tablas=null;
                $preguntas=null;
                $alternativas=null;
                $respuestas=null;
                $aponderacion=$this->clsaponderacion->aponderacion();
                foreach ($aponderacion as $cuad) {
                    $preguntas=$this->clspreguntas->preguntas($cuad->id_cuadro);
                    $i=0;
                    foreach ($preguntas as $keys) {
                        $respuestas[$cuad->id_cuadro][$i]=$this->clsdeetalle_llenadoMunicipal->respuestas($id_llenado,$keys->id_pregunta);
                        $i++;
                    }
                    $calificaciones[$cuad->id_cuadro]=$this->clscalificacionMunicipal->calificacion($cuad->id_cuadro);
                }
            }elseif ($key->tipo==5) {
            }
            $cuadros_impresion.=cuadrostipo(
                $key->id_cuadro,
                $key->tipo,
                $key->titulo,
                $calificacion,
                $criterios,
                $criterios_detalle,
                $notas,
                $tablas,
                $preguntas,
                $alternativas,
                $respuestas,
                $aponderacion,
                $calificaciones,
                $anexo_cuadro,
                $anexo_pregunta);
        }
        //declaramos el titulo de la pagina 
        $this->data["title"] = "Encuestas|Actualizar";
        $this->data["cuadros_preguntas"]=$cuadros_impresion;
        $this->data["contenido"]=$this->load->view('municipal/llenado', $this->data, TRUE);
        $this->load->view('template', $this->data);
    }

    function guardar(){
        //cargamos los modelos 
        $this->load->model('clsllenadoMunicipal');
        $this->load->model('clsdeetalle_llenadoMunicipal');
        $this->load->model('clslog_municipal');

        //vaerificamos si es un nuevo o una edicion por el id_llenado si viene vacio es nuevo caso contratio es una edicion
        if (isset($_POST["id_llenado"])) {$id_llenado=$this->input->post('id_llenado',TRUE);}else{$id_llenado="";}
        //si es una edicion borramos la encuesta antigua
        if ($id_llenado!="") {
            $this->clsllenadoMunicipal->delete($id_llenado);
            $this->clsdeetalle_llenadoMunicipal->delete($id_llenado);
        }
        $id_usuario=$this->input->post('id_usuario',TRUE);
        $id_distrito=$this->input->post('id_distrito',TRUE);
        $fecha=$this->input->post('fecha',TRUE);
        $fecha_destu=strtotime($fecha);
        $fecha=date(date("Y",$fecha_destu)."-".date("m",$fecha_destu)."-".date("d",$fecha_destu));
        $preguntas=$this->input->post('id_respuestas', TRUE);
        $respuestas=$this->input->post('respuestas',TRUE);
        $observacion=$this->input->post('observacion',TRUE);

        $data_usuario= array(
            'id_llenado'=>$id_llenado,
            'id_distrito' => $id_distrito,
            'id_usuario' => $id_usuario,
            'fecha' => $fecha
        );
        if ($id_llenado=="") {
            $id_llenado=$this->clsllenadoMunicipal->insertar($data_usuario);

            $data_log=array(
                'fecha' => date("Y-m-d"),
                'hora' =>  date("H:i:s"),
                'accion' =>  "Creacion",
                'id_municipal' =>  $id_llenado,
                'id_distrito' => $id_distrito,
                'id_usuario' =>  $_SESSION["usuario"]["id_usuario"],
                'usuario' => $_SESSION["usuario"]["user"]
                );
            $this->clslog_municipal->insertar($data_log);
        }else{
            $this->clsllenadoMunicipal->insertar($data_usuario);
            $data_log=array(
                'fecha' => date("Y-m-d"),
                'hora' =>  date("H:i:s"),
                'accion' =>  "Actualizacion",
                'id_municipal' =>  $id_llenado,
                'id_distrito' => $id_distrito,
                'id_usuario' =>  $_SESSION["usuario"]["id_usuario"],
                'usuario' => $_SESSION["usuario"]["user"]
                );
            $this->clslog_municipal->insertar($data_log);
        }
        foreach ($preguntas as $key) {
            if (array_key_exists($key,$respuestas)) {
                if (is_array($respuestas[$key])) {
                   $i=0;
                   $cadena="";
                   foreach ($respuestas[$key] as $ke) {
                       if ($i==0) {
                           $cadena.=$ke;
                       }else{$cadena.="/".$ke;}
                       $i++;
                   }
                    $respuestas[$key]=$cadena;
                }
                $observa="";
                if (isset($observacion[$key])) {
                    $observa=$observacion[$key];
                }
                $data_llenado= array(
                    'id_llenado' => $id_llenado,
                    'id_pregunta' => $key,
                    'respuesta' => $respuestas[$key],
                    'observacion' => $observacion[$key]
                );
                $this->clsdeetalle_llenadoMunicipal->insertar($data_llenado);
            }
        }
        header("Location:" .$this->data["base_url"]."index.php?c=municipal");
    }
    function delete(){
        $id_llenado=$this->input->get('id', TRUE);
        $this->load->model('clsllenadoMunicipal');
        $this->load->model('clsdeetalle_llenadoMunicipal');
        $this->load->model('clslog_municipal');
        $this->clsllenadoMunicipal->delete($id_llenado);
        $this->clsdeetalle_llenadoMunicipal->delete($id_llenado);
        header('Location:' .$this->data["base_url"].'index.php?c=municipal');
    }
    function ajaxLlenadoDistrito(){
        if (!isset($_POST["ajax"]) and $_POST["ajax"]=="verificardistrito"){header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        $id_distrito=$_POST["id_distrito"];
        $this->load->model("clsllenadoMunicipal");
        echo $this->clsllenadoMunicipal->verificarIdDistrito($id_distrito);
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Distritos extends CI_Controller {

    public $usuario = [];
    public $controlador;
    public $css;
    public $js;

    public function __construct() {
        parent::__construct();
        session_start();
        $this->base_url = $this->config->config['base_url'];
        $this->css = $this->config->config['css'];
        $this->js = $this->config->config['js'];
        $this->load->helper('menus');
        $this->load->helper('cuadros');
        $this->load->helper('paginador');

        $this->usuario['user'] = "saba";
        $this->usuario['tipo'] = "master";
        $this->usuario['nombre'] = "saba";
        $this->usuario['apellido_p'] = "saba";
        $this->usuario['foto'] = "team1.jpg";
        $this->usuario['region'] = "";
        $this->usuario['distrito'] = "";
        $this->usuario['group_departamento'] = "";
        $this->usuario['id_distrito'] = "";
        $this->controlador = "distritos";
        /* Modelos */
        $this->load->model('clsdistritos');
    }

     public function index() {
        $data["title"] = "Distrito";
        $data["base_url"] = $this->base_url;
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data["menus"] = menus($this->base_url, $this->controlador, $this->usuario['tipo']);
        $data["nav"]=nav($data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);
      
        
        $cuadros[0]["color"] = "blue";
        $cuadros[0]["icon"] = "users";
        $cuadros[0]["titulo"] = "Distritos";
        $cuadros[0]["cantidad"] = $this->clsdistritos->count('ubdistrito');
        $cuadros[0]["link"] = base_url()."index.php?c=Iniciodist";
        $cuadros[1]["color"] = "blue";
        $cuadros[1]["icon"] = "users";
        $cuadros[1]["titulo"] = "Provincias";
        $cuadros[1]["cantidad"] =$this->clsdistritos->count('ubprovincia');
        $cuadros[1]["link"] = $this->base_url . 'index.php?c=provincia';
        $data['cuadros']=  cuadros_link($cuadros);
        $data["contenido"] = $this->load->view('inicio', $data, TRUE);
        $this->load->view('template', $data);
    }
    public function departamento() {
        if (!isset($_POST['id_departamento'])) {
            $id_departamento = null;
        } else {
            $id_departamento = $this->input->post('id_departamento', TRUE);
        }
        $departamentos = $this->clsdistritos->regiones();
        if ($_SESSION["usuario"]["grupo_departamento"]!="") {
            echo "<option value=''>TODOS</option>";
            foreach ($departamentos as $key) {
                if (in_array($key->idDepa,$_SESSION["usuario"]["grupo_departamento"], true)) {
                    if ($id_departamento == $key->idDepa) {
                        echo "<option selected value='" . $key->idDepa . "'>" . $key->departamento . "</option>";
                    } else {
                        echo "<option value='".$key->idDepa."'>".$key->departamento . "</option>";
                    }
                }
            }
        }else{
            echo "<option value=''>TODOS</option>";
            foreach ($departamentos as $key) {
                if ($id_departamento == $key->idDepa) {
                    echo "<option selected value='" . $key->idDepa . "'>" . $key->departamento . "</option>";
                } else {
                    echo "<option value='" . $key->idDepa . "'>" . $key->departamento . "</option>";
                }
            }
        }
    }

    public function provincia() {
        if (!isset($_POST['id_departamento'])) {
            $id_departamento = null;
        } else {
            $id_departamento = $this->input->post('id_departamento', TRUE);
        }
        if (!isset($_POST['id_provincia'])) {
            $id_provincia = null;
        } else {
            $id_provincia = $this->input->post('id_provincia', TRUE);
        }

        $this->load->model('clsdistritos');
        $provincias = $this->clsdistritos->provincias($id_departamento);
        echo "<option value=''>TODOS</option>";
        foreach ($provincias->result() as $key) {
            if ($id_provincia == $key->idProv) {
                echo "<option selected value='" . $key->idProv . "'>" . $key->provincia . "</option>";
            } else {
                echo "<option value='" . $key->idProv . "'>" . $key->provincia . "</option>";
            }
        }
    }

    public function Distrito() {
        if (!isset($_POST['id_provincia'])) {
            $id_provincia = null;
        } else {
            $id_provincia = $this->input->post('id_provincia', TRUE);
        }
        if (!isset($_POST['id_distrito'])) {
            $id_distrito = null;
        } else {
            $id_distrito = $this->input->post('id_distrito', TRUE);
        }

        $this->load->model('clsdistritos');
        $distritos = $this->clsdistritos->distritos($id_provincia);
        echo "<option value=''>TODOS</option>";
        foreach ($distritos->result() as $key) {
            if ($id_distrito == $key->idDist) {
                echo "<option selected value='" . $key->idDist . "'>" . $key->distrito . "</option>";
            } else {
                echo "<option value='" . $key->idDist . "'>" . $key->distrito . "</option>";
            }
        }
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Iniciodist extends CI_Controller {

    public $usuario = [];
    public $controlador;
    public $css;
    public $js;

    public function __construct() {
        parent::__construct();
        session_start();
        $this->base_url = $this->config->config['base_url'];
        $this->css = $this->config->config['css'];
        $this->js = $this->config->config['js'];
        $this->load->helper('menus');
        $this->load->helper('cuadros');
        $this->load->helper('paginador');

        $this->usuario['user'] = "saba";
        $this->usuario['tipo'] = "master";
        $this->usuario['nombre'] = "saba";
        $this->usuario['apellido_p'] = "saba";
        $this->usuario['foto'] = "team1.jpg";
        $this->usuario['region'] = "";
        $this->usuario['distrito'] = "";
        $this->usuario['group_departamento'] = "";
        $this->usuario['id_distrito'] = "";
        $this->controlador = "distritos";
        /* Modelos */
        $this->load->model('clsdistritos');
    }

    public function index() {

        if (!isset($_POST['limite'])) {
            $limite = 5;
        } else {
            $limite = $this->input->post('limite', TRUE);
        }

        if (!isset($_POST['distrito'])) {
            $distrito = "";
        } else {
            $distrito = $this->input->post('distrito', TRUE);
        }
        if (!isset($_POST['pagina'])) {
            $pagina = 1;
        } else {
            $pagina = $this->input->post('pagina', TRUE);
        }
        if (!isset($_POST['id_departamentodis'])) {
            $id_departamentodis = "";
        } else {
            $id_departamentodis = $this->input->post('id_departamentodis', TRUE);
        }
        if (!isset($_POST['id_departamento'])) {
            $id_departamento = "";
        } else {
            $id_departamento = $this->input->post('id_departamento', TRUE);
        }
        if (!isset($_POST['id_provinciadis'])) {
            $id_provinciadis = "";
        } else {
            $id_provinciadis = $this->input->post('id_provinciadis', TRUE);
        }
        if (!isset($_POST['id_provincia'])) {
            $id_provincia = "";
        } else {
            $id_provincia = $this->input->post('id_provincia', TRUE);
        }
        $url = $this->base_url . 'index.php?c=Iniciodist&m=panel&limite=' . $limite;


        if ($distrito != "") {
            $url = $url . '&distrito=' . $distrito;
        }
        if ($id_departamento != "") {
            $url = $url . '&id_departamento=' . $id_departamento;
        }
        if ($id_departamentodis != "") {
            $url = $url . '&id_departamentodis=' . $id_departamentodis;
        }
        if ($id_provincia != "") {
            $url = $url . '&id_provincia=' . $id_provincia;
        }
        if ($id_provinciadis != "") {
            $url = $url . '&id_provinciadis=' . $id_provinciadis;
        }
        $url = $url . '&pagina=' . $pagina;
        header('Location:' . $url);
    }

    public function panel() {
        $data["title"] = "Distrito";
        $data["base_url"] = $this->base_url;
        $data["css"] = $this->css;
        $data["js"] = $this->js;
        $data["titulo2"] = "Ingreso de Datos de Distrito";
        $data["menus"] = menus($this->base_url, $this->controlador, $this->usuario['tipo']);
       $data["nav"]=nav($data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

        $data["maxdistrito"] = $this->clsdistritos->max('ubdistrito', 'idDist');


        /* recibimos todas las variables */
        $limite = $this->input->get('limite', TRUE);
        $pagina = $this->input->get('pagina', TRUE);
        $distrito = $this->input->get('distrito', TRUE);
        if ($distrito == NULL) {
            $distrito = $this->input->get('busq_usuario', TRUE);
        }
        $id_provincia = $this->input->get('id_provincia', TRUE);
        if ($id_provincia == NULL) {
            $id_provinciadis = $this->input->get('id_provinciadis', TRUE);
            $id_provincia = $id_provinciadis;
        } else {
            $id_provinciadis = $id_provincia;
        }
        $id_departamento = $this->input->get('id_departamento', TRUE);
        if ($id_departamento===NULL) {
            $id_departamentodis = $this->input->get('id_departamentodis', TRUE);
        } else {
            $id_departamentodis=$id_departamento;
        }
        /* consultamos si una de las 3 variables es nula no redericcionara a modo default ya que esos datos son prioritarios */
//        if ($limite === null or $pagina === null or $estado === null) {
//            header('Location:' . $this->base . 'index.php?c=busq_distritos');
//        }
        /* declaramos la variable comienzo que sera dedonde es que empezara a llamar los datos */
        $comienzo = ($pagina - 1) * $limite;


        /* Distrito */
        $distritos = $this->clsdistritos->listado_distritos($limite, $comienzo, $distrito, $id_provincia, $id_departamentodis);
        $data["listado"] = $distritos;

        $total = $this->clsdistritos->total_distritos($distrito, $id_provincia, $id_departamentodis);

        $url_paginador = $this->base_url . "index.php?c=Iniciodist&m=panel&limite=" . $limite;
        $data["paginador"] = paginador($url_paginador, $total, $limite, $pagina, $distritos, $distrito, "", $id_departamentodis, $id_provincia, "");

        $data["limite"] = $limite;
        $data["distrito"] = $distrito;
        $data["id_provincia"] = $id_provincia;
        $data["id_provinciadis"] = $id_provinciadis;
        $data["id_departamento"] = $id_departamento;
        $data["id_departamentodis"] = $id_departamentodis;
        $data["contenido"] = $this->load->view('distritos', $data, TRUE);
        $this->load->view('template', $data);
    }

    public function insertar_distrito() {
        $id_distrito = $this->input->post('txtId_distrito');
        $distrito = $this->input->post('txtDistrito');
        $id_provincia = $this->input->post('id_provincias');
        $dist = $this->clsdistritos->get_distrito($id_distrito);
        if ($dist) {
            $updateProvincia = $this->clsdistritos->update('ubdistrito', array('distrito' => $distrito, 'idProv' => $id_provincia), array('idDist' => $id_distrito));
        } else {
            $insetDistrito = $this->clsdistritos->insert('ubdistrito', array('idDist' => $id_distrito, 'distrito' => $distrito, 'idProv' => $id_provincia));
        }


        redirect('?c=Iniciodist');
    }

    public function edit_distrito() {
        $id_distrito = $this->input->post('id_distrito', TRUE);
        $distrito = $this->clsdistritos->get_distrito($id_distrito);

        if ($distrito) {
            foreach ($distrito as $dis) {
                $idDist = $dis->idDist;
                $distrito = $dis->distrito;
                $idDepa = $dis->idDepa;
                $idProv = $dis->idProv;
            }
            $js = array(
                'estado' => TRUE,
                'idDist' => $idDist,
                'distrito' => $distrito,
                'idDepa' => $idDepa,
                'idProv' => $idProv,
                'mensaje' => "Tiene Datos"
            );
        } else {
            $js = array(
                'estado' => FALSE,
                'mensaje' => "No se encontro registros"
            );
        }
        echo json_encode($js);
    }

    public function delete_distrito() {
        $id_distrito = $this->input->post('id_distrito', TRUE);
        $objDist = $this->clsdistritos->delete('ubdistrito', array('idDist' => $id_distrito));

        if ($objDist) {
            $json = array(
                'estado' => TRUE,
                'mensaje' => "Se elimino correctamentte"
            );
        } else {
            $json = array(
                'estado' => FALSE,
                'mensaje' => "Ocurrio un Error"
            );
        }
        echo json_encode($json);
    }

}

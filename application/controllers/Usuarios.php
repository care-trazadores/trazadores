<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuarios extends CI_Controller {
	public $controlador;
	public  function __construct() {
		parent::__construct();
		session_start();
		$this->data["base_url"]=$this->config->config['base_url'];
		if (!isset($_SESSION["usuario"])) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
		}
		if ($_SESSION["usuario"]["active"]!=TRUE) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
		if ($_SESSION["usuario"]["tipo"]=="usuario") {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
		$this->data["css"]=$this->config->config['css'];
		$this->data["js"]=$this->config->config['js'];
		$this->load->helper('menus');
		$this->load->helper('cuadros');
		$this->load->model('clsusuarios');
		$this->controlador="usuarios";
   	}
	public function index(){
		if ( ! isset($_POST['estado'])){$estado = 2;}
		else{$estado=$this->input->post('estado', TRUE);}
		if ( ! isset($_POST['limite'])){$limite = 5;}
		else{$limite=$this->input->post('limite', TRUE);}
		if ( ! isset($_POST['busq_usuario'])){$busq_usuario ="";}
		else{$busq_usuario=$this->input->post('busq_usuario', TRUE);}
		if ( ! isset($_POST['busq_correo'])){$busq_correo ="";}
		else{$busq_correo=$this->input->post('busq_correo', TRUE);}
		if ( ! isset($_POST['id_departamento'])){$id_departamento = "";}
		else{$id_departamento=$this->input->post('id_departamento', TRUE);}
		if ( ! isset($_POST['id_provincia'])){$id_provincia = "";}
		else{$id_provincia=$this->input->post('id_provincia', TRUE);}
		if ( ! isset($_POST['id_distrito'])){$id_distrito = "";}
		else{$id_distrito=$this->input->post('id_distrito', TRUE);}
		if ( ! isset($_POST['pagina'])){$pagina = 1;}
		else{$pagina=$this->input->post('pagina', TRUE);}
		$url=$this->data["base_url"].'index.php?c=usuarios&m=panel&estado='.$estado.'&limite='.$limite;
		if ($busq_usuario!="") { $url=$url.'&busq_usuario='.$busq_usuario;}
		if ($busq_correo!="") { $url=$url.'&busq_correo='.$busq_correo;}
		if ($id_departamento!="") { $url=$url.'&id_departamento='.$id_departamento;}
		if ($id_provincia!="") { $url=$url.'&id_provincia='.$id_provincia;}
		if ($id_distrito!="") { $url=$url.'&id_distrito='.$id_distrito;}
		$url=$url.'&pagina='.$pagina;
		header('Location:'.$url);
	}
	public function panel()
	{
		/*recibimos todas las variables*/
		$estado=$this->input->get('estado', TRUE);
		$limite=$this->input->get('limite', TRUE);
		$pagina=$this->input->get('pagina', TRUE);
		$busq_usuario=$this->input->get('busq_usuario', TRUE);
		$busq_correo=$this->input->get('busq_correo', TRUE);
		$id_departamento=$this->input->get('id_departamento', TRUE);
		if ($_SESSION["usuario"]['grupo_departamento']!="") {
			
		}
		
		$id_provincia=$this->input->get('id_provincia', TRUE);
		$id_distrito=$this->input->get('id_distrito', TRUE);
		/*consultamos si una de las 3 variables es nula no redericcionara a modo default ya que esos datos son prioritarios*/
		if ($limite===null or $pagina===null or $estado===null){ header('Location:'.$this->data["base_url"].'index.php?c=usuarios'); }

		$url_retorno=$this->data["base_url"].'index.php?c=usuarios&m=panel&estado='.$estado.'&limite='.$limite;
		if ($busq_usuario!="") { $url_retorno=$url_retorno.'&busq_usuario='.$busq_usuario;}
		if ($busq_correo!="") { $url_retorno=$url_retorno.'&busq_correo='.$busq_correo;}
		if ($id_departamento!="") { $url_retorno=$url_retorno.'&id_departamento='.$id_departamento;}
		if ($id_provincia!="") { $url_retorno=$url_retorno.'&id_provincia='.$id_provincia;}
		if ($id_distrito!="") { $url_retorno=$url_retorno.'&id_distrito='.$id_distrito;}
		$url_retorno=$url_retorno.'&pagina='.$pagina;

		/*declaramos la variable comienzo que sera dedonde es que empezara a llamar los datos*/
		$comienzo=($pagina-1)*$limite;
		$this->load->helper('paginador');
		$this->data["title"]="usuarios|Listado";
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
		$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);
		$cuadros[0]["color"]="blue";
		$cuadros[0]["icon"]="users";
		$cuadros[0]["titulo"]="Total usuarios";
		$cuadros[0]["cantidad"]=$this->clsusuarios->count_usuarios(2,$_SESSION["usuario"]['grupo_departamento']);
		$cuadros[1]["color"]="green";
		$cuadros[1]["icon"]="users";
		$cuadros[1]["titulo"]="Usuarios Habilitados";
		$cuadros[1]["cantidad"]=$this->clsusuarios->count_usuarios(1,$_SESSION["usuario"]['grupo_departamento']);
		$cuadros[2]["color"]="red";
		$cuadros[2]["icon"]="users";
		$cuadros[2]["titulo"]="Usuarios Deshabilitados";
		$cuadros[2]["cantidad"]=$this->clsusuarios->count_usuarios(0,$_SESSION["usuario"]['grupo_departamento']);
		$this->data["cuadros"]=cuadros($cuadros);

		$usuarios=$this->clsusuarios->listado_total($estado,$limite,$comienzo,$busq_usuario,
			$busq_correo,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["grupo_departamento"]);
		$this->data["listado"]=$usuarios;

		$total=$this->clsusuarios->busq_total_usuarios($estado,$busq_usuario,$busq_correo,
			$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["grupo_departamento"]);
		
		$url_paginador=$this->data["base_url"]."index.php?c=usuarios&m=panel&estado=".$estado."&limite=".$limite;
    	$this->data["paginador"]=paginador($url_paginador,$total,$limite,$pagina,$usuarios,$busq_usuario,$busq_correo,$id_departamento,$id_provincia,$id_distrito);

    	$this->data["estado"]=$estado;
    	$this->data["limite"]=$limite;
    	$this->data["busq_usuario"]=$busq_usuario;
    	$this->data["busq_correo"]=$busq_correo;
    	$this->data["id_departamento"]=$id_departamento;
    	$this->data["id_provincia"]=$id_provincia;
    	$this->data["id_distrito"]=$id_distrito;

    	$this->data["url_retorno"]=$url_retorno;

		$this->data["contenido"]=$this->load->view('usuarios/listado_usuarios',$this->data,TRUE);
		$this->load->view('template',$this->data);
	}
	function update_estado(){
		if ( ! isset($_POST['id_usuario'])){header('Location:'.$this->data["base_url"].'index.php?c=usuarios');}
		else{$id_usuario=$this->input->post('id_usuario');}
		if ( ! isset($_POST['estado'])){header('Location:'.$this->data["base_url"].'index.php?c=usuarios');}
		else{$estado=$this->input->post('estado');}
		$data = array(
            'estado' =>$estado
        );
		$actualizar = $this->clsusuarios->actualizar_usuario($id_usuario,$data);
        //si la actualización ha sido correcta creamos una sesión flashdata para decirlo
        if($actualizar){}
        else{echo "error al guardar";}
	}
	function delete_user(){
		if ( ! isset($_POST['tipo'])){$tipo = "";}
		else{$tipo=$this->input->post('tipo', TRUE);}
		if ( ! isset($_POST['id_usuario'])){$id_usuario = "";}
		else{$id_usuario=$this->input->post('id_usuario');}

		if ($tipo=="" or $id_usuario=="") {
			header('Location:'.$this->data["base_url"].'index.php?c=usuarios'); 
		}
		$this->load->model('clsdetalleusuario');
        $this->clsdetalleusuario->delete_user($id_usuario);
		$this->clsusuarios->usuario_delete($tipo,$id_usuario);
		$confirma=$this->clsusuarios->busqueda_user($id_usuario,null);
		if (count($confirma)==0) {
			echo "si";
		}else{echo "no";}
		
	}
	function user_config(){
		if ( ! isset($_GET['id_usuario'])){$id_usuario = "";}
		else{$id_usuario=$this->input->get('id_usuario');}
		if ($id_usuario=="") {
			header('Location:'.$this->data["base_url"].'index.php?c=usuarios');
		}
		if ( ! isset($_GET['tab'])){$tab = "";}
		else{$tab=$this->input->get('tab');}

		$this->data["title"]="Usuario Actualizar";
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
		$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

		
		$datos=$this->clsusuarios->busqueda_user($id_usuario,"usuario");
		foreach ($datos as $key) {
			$this->data["datos_usuario"]=array(
				'id'=>$key->id_usuario,
				'user'=>$key->user,
				'estado'=>$key->estado,
				'id_departamento'=>$key->idDepa,
				'departamento'=>$key->departamento,
				'id_provincia'=>$key->idProv,
				'provincia'=>$key->provincia,
				'id_distrito'=>$key->id_distrito,
				'distrito'=>$key->distrito,
				'nombre' =>$key->nombre,
				'apellido_p'=>$key->apellido_p,
				'apellido_m'=>$key->apellido_m,
				'tipo'=>$key->tipo,
				'correo'=>$key->correo,
				'dni'=>$key->dni,
				'direccion'=>$key->direccion,
				'telefono'=>$key->telefono,
				'celular'=>$key->celular,
				'foto'=>$key->foto
				);
		}
		$this->data["tab_posicion"]=$tab;
		$this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
		$this->data["contenido"]=$this->load->view('usuarios/user_config',$this->data,TRUE);
		$this->load->view('template',$this->data);
	}
	function cuenta_update(){
		$error="";
		if (!isset($_POST['user_id'])){header('Location:'.$this->data["base_url"].'index.php?c=usuarios');}
		else{$user_id=$this->input->post('user_id');}

		if ($this->input->post('user_usuario')=="") { $error="El campo usuario no puede ser vacio";}
		if ($this->input->post('id_distrito')=="") { $error="Debe seleccionar un distrito";}
		if ($error=='') {
			$data = array(
	            'user' => $this->input->post('user_usuario'),
	            'estado' => $this->input->post('user_estado'),
	            'id_distrito' =>$this->input->post('id_distrito')
	        );
	        $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
	        if($actualizar){}
	        else{echo "error al guardar";}
		}else{
			echo $error;
		}
	}
	function info_update(){
		$this->load->model('clsdetalleusuario');
		if ( ! isset($_POST['user_id'])){header('Location:'.$this->data["base_url"].'index.php?c=usuarios');}
		else{$user_id=$this->input->post('user_id');}
		$data = array(
            'nombre' => $this->input->post('user_nombre'),
            'apellido_p' => $this->input->post('user_apellido_p'),
            'apellido_m' => $this->input->post('user_apellido_m'),
            'dni' => $this->input->post('user_dni'),
            'correo' => $this->input->post('user_correo'),
            'direccion' => $this->input->post('user_direccion'),
            'telefono' => $this->input->post('user_telefono'),
            'celular' => $this->input->post('user_celular')
        );
		$actualizar = $this->clsdetalleusuario->actualizar_usuario_info($user_id,$data);
        //si la actualización ha sido correcta creamos una sesión flashdata para decirlo
        if($actualizar){}
        else{echo "error al guardar";}
	}
	function foto_update(){
		$id_usuario=$this->input->post('user_id');
		$error="";
        $file = $_FILES["file-foto"];
		$nombre = $file["name"];
		if ($nombre=="") {
			echo "Lo sentimos, debe seleccionar una imagen";
		}else{
			$tipo=$file["type"];
			$ruta_provisional=$file["tmp_name"];
			$size=$file["size"];
			$dimensiones=getimagesize($ruta_provisional);
			$width=$dimensiones[0];
			$height=$dimensiones[1];
			$carpeta="img/usuarios/";
			if ($tipo!='image/jpg' && $tipo!='image/jpeg' && $tipo!='image/png' && $tipo!='image/gif') {
				$error="Error, el archivo no es una imagen";
			}elseif($size>1024*1024){
				$error="Error, el tamaño maximo permitido es 1mb";
			}elseif ($width>500 && $width<60 && $height>500 && $height<60) {
				$error="Error, la anchura y la altura de la imagen debe de ser inferior 500px y mayor a 60 px";
			}
			if ($error=="") {
				if ($tipo=='image/jpg' || $tipo=='image/jpeg') {$extension=".jpg";}
			
				if ($tipo=='image/png') {$extension=".png";}
				if ($tipo=='image/gif') {$extension=".gif";}
				$nombre_foto='usuario'.$id_usuario.$extension;
				$src=$carpeta.$nombre_foto;
				move_uploaded_file($ruta_provisional, $src);
				$data = array(
		            'foto' => $nombre_foto
		        );
				
		        $this->load->model('clsdetalleusuario');
				$actualizar = $this->clsdetalleusuario->actualizar_usuario_info($id_usuario,$data);
		        //si la actualización ha sido correcta creamos una sesión flashdata para decirlo
		        if($actualizar){}
		        else{echo "error al guardar";}
		    	//$this->redimensionar_jpeg($nombre_foto);
			}
		}

		
	}	
	function password_update(){
		$error="";
		if (!isset($_POST['user_id'])){header('Location:'.$this->data["base_url"].'index.php?c=usuarios');}
		else{$user_id=$this->input->post('user_id');}

		if ($this->input->post('password_new')=="") { $password_new='';$error="Ingrese nueva clave";}
		else{$password_new=md5($this->input->post('password_new'));}

		if ($this->input->post('password_rep')=="") { $password_rep='';$error="Debe repetir la clave";}
		else{$password_rep=md5($this->input->post('password_rep'));}

		if ($_SESSION["usuario"]["tipo"]!="usuario") {
			if ($password_new!=$password_rep) {
				$error="Las claves nuevas no coinciden";
			}else{
				$data = array(
		            'pass' => $password_new
		        );
		        $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
		        if($actualizar){}
		        else{echo "error al guardar";}
			}
		}else{
			if ($this->input->post('password_ant')=="") {$password_ant='';$error="Necesita colocar la clave anterior";}
			else{$password_ant=md5($this->input->post('password_ant'));}

			$datos=$this->clsusuarios->busqueda_user($user_id,"usuario");
			foreach ($datos as $key) {
				$pass_anterior=$key->pass;
			}
			if ($password_new!=$password_rep) {
				$error="Las claves nuevas no coinciden";
			}elseif ($pass_anterior!=$password_ant) {
				$error="Las clave anterior no coinciden";
			}

			if ($error!="") {
				echo $error;
			}else{
				$data = array(
		            'pass' => $password_new
		        );
		        $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
		        if($actualizar){}
		        else{echo "error al guardar";}
			}
		}
		echo $error;
	}
	function nuevo(){
		$error=0;
		$errores="";
		$this->data["usuario"]="";
		$this->data["password"]="";

		$this->data["id_distrito"]="";
		$this->data["nombres"]="";
		$this->data["apellido_p"]="";
		$this->data["apellido_m"]="";
		$this->data["dni"]="";
		$this->data["direccion"]="";
		$this->data["correo"]="";
		$this->data["telefono"]="";
		$this->data["celular"]="";
		$this->data["success"]="";
		$nombre_foto="";

		if (isset($_POST["enviar"])) {
			$errores="Por favor verificar los siguientes campos:<br>";
			if ($this->input->post('usuario')=="") {
				$usuario='';$error++;$errores.="- Necesita Ingresar un nombre de usuario<br>";}
			else{
				$usuario=$this->input->post('usuario');
				$this->data["usuario"]=$usuario;}

			if ($this->input->post('password')=="") {
				$password='';$error++;$errores.="- Necesita Ingresar una contraseña<br>";}
			else{
				$password=$this->input->post('password');
				$this->data["password"]=$password;}

			if ($this->input->post('id_departamento')!="") {$this->data["datos_usuario"]["id_departamento"]=$this->input->post('id_departamento');}
			if ($this->input->post('id_provincia')!="") {$this->data["datos_usuario"]["id_provincia"]=$this->input->post('id_provincia');}

			if ($this->input->post('id_distrito')=="") {$id_distrito='';$error++;$errores.="- Necesita seleccionar un distrito<br>";}
			else{$id_distrito=$this->input->post('id_distrito');$this->data["datos_usuario"]["id_distrito"]=$this->input->post('id_distrito');}
	
			if ($this->input->post('nombres')=="") {$nombres='';$error++;$errores.="- El campo nombre es obligatorio<br>";}
			else{$nombres=$this->input->post('nombres');$this->data["nombres"]=$nombres;}

			if ($this->input->post('apellido_p')=="") {$apellido_p='';$error++;$errores.="- El campo apellido paterno es obligatorio<br>";}
			else{$apellido_p=$this->input->post('apellido_p');$this->data["apellido_p"]=$apellido_p;}

			$apellido_m=$this->input->post('apellido_m');
			$dni=$this->input->post('dni');
			$correo=$this->input->post('correo');
			$direccion=$this->input->post('direccion');
			$telefono=$this->input->post('telefono');
			$celular=$this->input->post('celular');

			if ($error==0) {
				if ($this->clsusuarios->verificar_user($usuario)==0) {
					$data = array(
			            'user' => $usuario,
			            'pass' => md5($password),
			            'id_distrito' =>$id_distrito,
			            'tipo' =>"usuario",
			            'estado' =>1
			        );
			        $id_usuario=$this->clsusuarios->insertar($data);
			       	$file=$_FILES['file-foto'];
					if ($file!="") {
						$nombre=$file["name"];
						if ($nombre!="") {
							$tipo=$file["type"];
							$ruta_provisional=$file["tmp_name"];
							$size=$file["size"];
							$dimensiones=getimagesize($ruta_provisional);
							$width=$dimensiones[0];
							$height=$dimensiones[1];
							$carpeta="img/usuarios/";
							$error_foto=0;
							if ($tipo!='image/jpg' && $tipo!='image/jpeg' && $tipo!='image/png' && $tipo!='image/gif') {
								$error++;
								$error_foto++;
								$errores.="- Error, el archivo no es una imagen<br>";
							}elseif($size>1024*1024){
								$error++;
								$error_foto++;
								$errores.="- Error, el tamaño maximo permitido es 1mb";
							}elseif ($width>500 && $width<60 && $height>500 && $height<60) {
								$error++;
								$error_foto++;
								$errores.="- Error, la anchura y la altura de la imagen debe de ser inferior 500px y mayor a 60 px";
							}
							if ($error_foto==0) {
								if ($tipo=='image/jpg' || $tipo=='image/jpeg') {$extension=".jpg";}
								if ($tipo=='image/png') {$extension=".png";}
								if ($tipo=='image/gif') {$extension=".gif";}
								$nombre_foto='usuario'.$id_usuario.$extension;
								$src=$carpeta.$nombre_foto;
								move_uploaded_file($ruta_provisional, $src);
							}
						}
					}
					$this->load->model('clsdetalleusuario');
			        $datos = array(
			            'id_usuario' => $id_usuario,
			            'nombre' => $nombres,
			            'apellido_p' =>$apellido_p,
			            'apellido_m' =>$apellido_m,
			            'dni' =>$dni,
			            'correo' =>$correo,
			            'direccion' =>$direccion,
			            'telefono' =>$telefono,
			            'celular' =>$celular,
			            'foto' =>$nombre_foto
			        );
			       	$this->clsdetalleusuario->insertar($datos);
			       	$this->data["success"]="<p class='alert alert-success'>El usuario fue creado correctamente.</p>";
			       	$this->data["usuario"]="";
					$this->data["password"]="";
					$this->data["datos_usuario"]["id_departamento"]="";
					$this->data["datos_usuario"]["id_provincia"]="";
					$this->data["datos_usuario"]["id_distrito"]="";
					$this->data["nombres"]="";
					$this->data["apellido_p"]="";
					$this->data["apellido_m"]="";
					$this->data["dni"]="";
					$this->data["direccion"]="";
					$this->data["correo"]="";
					$this->data["telefono"]="";
					$this->data["celular"]="";
				}else{
					$errores.="- lo sentimos el usuario ya existe";
				}
			}			
		}
		$this->data["errores"]=$errores;
		$this->data["title"]="usuarios|Nuevo";
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
		$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],
			$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto']);
		$this->data["contenido"]=$this->load->view('usuarios/new_usuario',$this->data,TRUE);
		$this->load->view('template',$this->data);
	}
	function vista(){
		if (!isset($_GET["id_usuario"])){header('Location:'.$this->data["base_url"].'index.php?c=usuarios');}
		else{$id_usuario=$this->input->get('id_usuario');}
		if ( ! isset($_GET['tab'])){$tab = "";}
		else{$tab=$this->input->get('tab');}

		$datos=$this->clsusuarios->busqueda_user($id_usuario,"usuario");
		foreach ($datos as $key) {
			$this->data["datos_usuario"]=array(
				'id'=>$key->id_usuario,
				'user'=>$key->user,
				'estado'=>$key->estado,
				'id_departamento'=>$key->idDepa,
				'departamento'=>$key->departamento,
				'id_provincia'=>$key->idProv,
				'provincia'=>$key->provincia,
				'id_distrito'=>$key->id_distrito,
				'distrito'=>$key->distrito,
				'nombre' =>$key->nombre,
				'apellido_p'=>$key->apellido_p,
				'apellido_m'=>$key->apellido_m,
				'tipo'=>$key->tipo,
				'correo'=>$key->correo,
				'dni'=>$key->dni,
				'direccion'=>$key->direccion,
				'telefono'=>$key->telefono,
				'celular'=>$key->celular,
				'foto'=>$key->foto
				);
		}

		$this->load->model('clsllenado');
		$encuestas=$this->clsllenado->reportes(null,null,null,null,
                        null,null,"usuario",$id_usuario,
                        null);

		$this->load->helper('encuestas');
		$this->data["graficos"]="";
		$this->data["graficos"].=encuestas_graficos($encuestas);
		$this->data["graficos"].=encuestas_tabla_comunidad($encuestas,$this->data["base_url"]);

		$this->data["title"]="usuarios|Nuevo";
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
		$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],
			$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto']);
		$this->data["contenido"]=$this->load->view('usuarios/vista',$this->data,TRUE);
		$this->load->view('template',$this->data);
	}
}

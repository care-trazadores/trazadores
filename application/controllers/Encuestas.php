<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class encuestas extends CI_Controller {
    public function __construct() {
        parent::__construct();
        session_start();
        $this->data["base_url"]=$this->config->config['base_url'];
        if (!isset($_SESSION["usuario"])) {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        if ($_SESSION["usuario"]["active"]!=TRUE) {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        $this->data["css"]=$this->config->config['css'];
        $this->data["js"]=$this->config->config['js'];

        $this->load->helper('menus');
        $this->controlador="encuestas";
        $this->data["base_url"] = $this->data["base_url"];
        $this->data["menus"] = menus($this->data["base_url"], $this->controlador, $_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

    }

    public function index() {
        $busq_correo="";

        $page=($this->input->post('page', TRUE))?$this->input->post('page', TRUE):1;
        $limite=($this->input->post('limite', TRUE))?$this->input->post('limite', TRUE):5;
        $busq_id_llenado=($this->input->post('busq_id_llenado', TRUE))?$this->input->post('busq_id_llenado', TRUE):"";
        $busq_usuario=($this->input->post('busq_usuario', TRUE))?$this->input->post('busq_usuario', TRUE):"";
        $busq_localidad=($this->input->post('busq_localidad', TRUE))?$this->input->post('busq_localidad', TRUE):"";
        
        $id_departamento=($this->input->post('id_departamento', TRUE))?$this->input->post('id_departamento', TRUE):"";
        $id_provincia=($this->input->post('id_provincia', TRUE))?$this->input->post('id_provincia', TRUE):"";
        $id_distrito=($this->input->post('id_distrito', TRUE))?$this->input->post('id_distrito', TRUE):"";
        $url = $this->data["base_url"] . 'index.php?c=encuestas&m=panel&limite=' . $limite;
        if ($busq_id_llenado != "") { $url = $url . '&busq_id_llenado=' . $busq_id_llenado;}
        if ($busq_usuario != "") { $url = $url . '&busq_usuario=' . $busq_usuario;}
        if ($busq_correo != "") {$url = $url . '&busq_correo=' . $busq_correo;}
        if ($busq_localidad != "") {$url = $url . '&busq_localidad=' . $busq_localidad;}
        if ($id_departamento != "") {$url = $url . '&id_departamento=' . $id_departamento;}
        if ($id_provincia != "") {$url = $url . '&id_provincia=' . $id_provincia;}
        if ($id_distrito != "") { $url = $url . '&id_distrito=' . $id_distrito;}
        $url = $url . '&page=' . $page;
        header('Location:' . $url);
    }

    public function panel() {
        /* recibimos todas las variables */
        $estado = $this->input->get('estado', TRUE);
        $limite = $this->input->get('limite', TRUE);
        $page = $this->input->get('page', TRUE);
        $busq_id_llenado = $this->input->get('busq_id_llenado', TRUE);
        $busq_usuario = $this->input->get('busq_usuario', TRUE);
        $busq_localidad = $this->input->get('busq_localidad', TRUE);
        $id_departamento = $this->input->get('id_departamento', TRUE);
        $id_provincia = $this->input->get('id_provincia', TRUE);
        $id_distrito = $this->input->get('id_distrito', TRUE);
        /* consultamos si una de las 3 variables es nula no redericcionara a modo default ya que esos datos son prioritarios */
        if ($limite === null or $page === null) {
           header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
        }
        /* declaramos la variable comienzo que sera dedonde es que empezara a llamar los datos */
        $comienzo = ($page - 1) * $limite;
        /* incluimos elmodelo */
        $this->load->model('clsllenado');
        /* incluimos helper */
        $this->load->helper('cuadros');
        $this->load->helper('paginador');
        $this->load->helper('pagination');


        $this->data["title"] = "encuetas|Listado";
        $this->data["menus"] = menus($this->data["base_url"], $this->controlador, $_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);
        //creamos el cuadro de las encuestas
        $cuadros[0]["color"] = "blue";
        $cuadros[0]["icon"] = "users";
        $cuadros[0]["titulo"] = "Total encuestas";
        $cuadros[0]["cantidad"] = $this->clsllenado->total_encuestas(null, null, null, null,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
        $this->data["cuadros"] = cuadros($cuadros);

        $encuestas = $this->clsllenado->reportes($limite, $comienzo, $busq_usuario, $id_departamento, $id_provincia, $id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"],$busq_localidad,$busq_id_llenado);

        $this->data["listado"] = $encuestas;

        $total = $this->clsllenado->total_encuestas($busq_usuario, $id_departamento, $id_provincia, $id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"],$busq_localidad,$busq_id_llenado);
        
        $reload=$this->data["base_url"] ."index.php?c=encuestas&m=panel";
        $items="&limite=".$limite;
        $items.=($busq_id_llenado!="")?"&busq_id_llenado=".$busq_id_llenado:"";
        $items.=($busq_usuario!="")?"&busq_usuario=".$busq_usuario:"";
        $items.=($busq_localidad!="")?"&busq_localidad=".$busq_localidad:"";
        $items.=($id_departamento!="")?"&id_departamento=".$id_departamento:"";
        $items.=($id_provincia!="")?"&id_provincia=".$id_provincia:"";
        $items.=($id_distrito!="")?"&id_distrito=".$id_distrito:"";
        $total_paginas=ceil($total/$limite);
        $this->data["paginador"]=paginate($reload,$items,$page,$total_paginas,3);

        if ($_SESSION["usuario"]["tipo"]=="usuario") {$this->data["id_usuario"]=$_SESSION["usuario"]["id_usuario"];}
        else{$this->data["id_usuario"]=null;}
        
        $this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
        $this->data["limite"] = $limite;
        $this->data["busq_id_llenado"] = $busq_id_llenado;
        $this->data["busq_usuario"] = $busq_usuario;
        $this->data["busq_localidad"] = $busq_localidad;
        $this->data["id_departamento"] = $id_departamento;
        $this->data["id_provincia"] = $id_provincia;
        $this->data["id_distrito"] = $id_distrito;
        $this->data['titulo'] = "Listado de Encuestas";
        $this->data["contenido"] = $this->load->view('encuestas/listado_encuestas', $this->data, TRUE);
        $this->load->view('template', $this->data);
    }
    function llenado(){
        $this->data["datos_llenado"]["id_llenado"]="";
        $this->data["datos_llenado"]["comunidad"]="";
        $this->data["datos_llenado"]["usuario"]=$_SESSION["usuario"]["user"];
        $this->data["datos_llenado"]["id_usuario"]=$_SESSION["usuario"]["id_usuario"];
        
        if ($_SESSION["usuario"]["tipo"]!="usuario") {
            $this->data["footer_js"][]="filtro_departamento.js";
            $this->data["datos_llenado"]["departamento"]="";
            $this->data["datos_llenado"]["provincia"]="";
            $this->data["datos_llenado"]["distrito"]="";
            $this->data["datos_llenado"]["id_distrito"]="";
        }else{

            $this->data["datos_llenado"]["departamento"]=$_SESSION["usuario"]["departamento"];
            $this->data["datos_llenado"]["provincia"]=$_SESSION["usuario"]["provincia"];
            $this->data["datos_llenado"]["distrito"]=$_SESSION["usuario"]["distrito"];
            $this->data["datos_llenado"]["id_distrito"]=$_SESSION["usuario"]["id_distrito"];
        }

        $this->data["datos_llenado"]["fecha"]=date("d-m-Y");


        $this->load->helper('cuadros_preguntas');
        $this->load->helper('cuadro_tipo0');
        $this->load->helper('cuadro_tipo1');
        $this->load->helper('cuadro_tipo2');
        $this->load->helper('cuadro_tipo3');
        $this->load->helper('cuadro_tipo4');
        $this->load->helper('cuadro_tipo5');

        $this->data["title"] = "Encuestas|Actualizar";
        //luego cargamos los modelos que se usaran
        $this->load->model('clsdesarrollo');
        $this->load->model('clsaponderacion');
        $this->load->model('clsllenado');
        $this->load->model('clscuadros');
        $this->load->model('clstablas');
        $this->load->model('clspreguntas');
        $this->load->model('clscalificacion');
        $this->load->model('clscriterios');
        $this->load->model('clsnotas');
        $this->load->model('clsanexo');

        $cuadros_impresion='';
        $cuadros=$this->clsdesarrollo->cuadros();
        $this->data["tabs"]=tabs($cuadros);
        $anexo=null;
        $anexo_cuadro=null;
        $anexo_pregunta=null;
        $respuestas=null;
        $aponderacion=null;
        $calificaciones=null;
        foreach ($cuadros as $key) {
            $calificacion=$this->clsdesarrollo->calificacion($key->id_cuadro);
            $criterios=$this->clsdesarrollo->criterios($key->id_cuadro);
            $criterios_detalle=$this->clsdesarrollo->criterios_detalle($criterios);
            $notas=$this->clsdesarrollo->notas($key->id_cuadro);
            $tablas=$this->clsdesarrollo->tablas($key->id_cuadro);
            $preguntas=$this->clsdesarrollo->preguntas($key->id_cuadro);
            $alternativas=$this->clsdesarrollo->alternativas($preguntas);
            $anexo_cuadro=$this->clsanexo->anexo_cuadro($key->id_cuadro);
            foreach ($preguntas as $k) {
                $anexo=$this->clsanexo->anexo_pregunta($k->id_pregunta);
                if ($anexo!=null) {
                    foreach ($anexo as $ke) {
                       $anexo_pregunta[$k->id_pregunta]=$ke->id_cuadro;
                    }
                }
            }
            //$repuestas=$this->clsdesarrollo->respuestas();
            if ($key->tipo==0) {
                
            }elseif ($key->tipo==3) {
                
            }else if ($key->tipo==4) {
                $aponderacion=$this->clsaponderacion->aponderacion();
            }elseif ($key->tipo==5) {
                # code...
            }
            $cuadros_impresion.=cuadrostipo(
                $key->id_cuadro,
                $key->tipo,
                $key->titulo,
                $calificacion,
                $criterios,
                $criterios_detalle,
                $notas,
                $tablas,
                $preguntas,
                $alternativas,
                $respuestas,
                $aponderacion,
                $calificaciones,
                $anexo_cuadro,
                $anexo_pregunta);
            
        }
        $this->data["cuadros_preguntas"]=$cuadros_impresion;
        $this->data["contenido"]=$this->load->view('encuestas/llenado', $this->data, TRUE);
        $this->load->view('template', $this->data);
    }
    function edit(){        
        //primero verificamos si se envio la variable de id sino reedireccionamos
        if (!isset($_GET["id"])) {header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
        $this->data["footer_js"][]="filtro_departamento.js";
        $id_llenado=$this->input->get('id', TRUE);
        $this->data["datos_llenado"]["fecha"]=date("d-m-Y"); 

        $this->load->helper('cuadro_tipo0');
        $this->load->helper('cuadro_tipo1');
        $this->load->helper('cuadro_tipo2');
        $this->load->helper('cuadro_tipo3');
        $this->load->helper('cuadro_tipo4');
        $this->load->helper('cuadro_tipo5');
          
        //luego cargamos los modelos que se usaran
        $this->load->model('clsdesarrollo');
        $this->load->model('clsaponderacion');
        $this->load->model('clsllenado');
        $this->load->model('clsdeetalle_llenado');
        $this->load->model('clscuadros');
        $this->load->model('clstablas');
        $this->load->model('clspreguntas');
        $this->load->model('clscalificacion');
        $this->load->model('clscriterios');
        $this->load->model('clsnotas');
        $this->load->model('clsanexo');
        //declaramos los datos de la encuesta
        foreach ($this->clsllenado->llenado_por_id($id_llenado) as $key) {
            $this->data["datos_llenado"]["id_llenado"]=$id_llenado;
            $this->data["datos_llenado"]["comunidad"]=$key->comunidad;
            $this->data["datos_llenado"]["id_usuario"]=$key->id_usuario;
            $this->data["datos_llenado"]["usuario"]=$key->user;
            $this->data["datos_llenado"]["departamento"]=$key->departamento;
            $this->data["datos_llenado"]["provincia"]=$key->provincia;
            $this->data["datos_llenado"]["distrito"]=$key->distrito;
            $this->data["datos_llenado"]["id_distrito"]=$key->idDist;
        }
        if ($_SESSION["usuario"]["tipo"]=="usuario") {
            if ($this->data["datos_llenado"]["id_distrito"]!=$_SESSION["usuario"]["id_distrito"]) {
            header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
            }
        }
        //cargamos el helper que realiza los cuadros de llenado o edicion
        $this->load->helper('cuadros_preguntas');
        //declaramos la variable cuadros_impresion que guardara todo una cadena de texto de los cuadros
        $cuadros_impresion='';
        //rescatamos todos los cuadros
        $cuadros=$this->clscuadros->cuadros();
        //declaramos la variable tabs con la funcion tabs y enviamos los cuadros esto devolvera una cadena de texto de los tabs
        //que se usaran para cambvios modificar el helper cuadros_preguntas function tabs
        $this->data["tabs"]=tabs($cuadros);
        //declaramos la variable respuestas como un array vacio;
        $respuestas=[];
        $calificacion="";       
        $criterios="";
        $criterios_detalle="";
        $notas="";
        $tablas="";
        $preguntas="";
        $alternativas="";
        $anexo=null;
        $anexo_cuadro=null;
        $anexo_pregunta=null;
        $jass=[];
        $jass_alternativa=null;
        //luego recorremos la variable cuadro uno por uno con foreach
        foreach ($cuadros as $key) {
            //rescatamos todas la calificaciones y las guardamos en la variable calificacion
            $calificacion=$this->clsdesarrollo->calificacion($key->id_cuadro);
            //rescatamos todos los cuadros de criterios y los guardamos en las variables
            $criterios=$this->clsdesarrollo->criterios($key->id_cuadro);
            $criterios_detalle=$this->clsdesarrollo->criterios_detalle($criterios);
            $notas=$this->clsdesarrollo->notas($key->id_cuadro);
            $tablas=$this->clsdesarrollo->tablas($key->id_cuadro);
            $preguntas=$this->clsdesarrollo->preguntas($key->id_cuadro);
            $alternativas=$this->clsdesarrollo->alternativas($preguntas);
            $anexo_cuadro=$this->clsanexo->anexo_cuadro($key->id_cuadro);
           // var_dump($anexo_cuadro);
            foreach ($preguntas as $k) {
                $anexo=$this->clsanexo->anexo_pregunta($k->id_pregunta);
                if ($anexo!=null) {
                    foreach ($anexo as $ke) {
                       $anexo_pregunta[$k->id_pregunta]=$ke->id_cuadro;
                    }
                }
            }
            $aponderacion=null;
            $calificaciones=null;
            //$respuestas=$this->clsdesarrollo->respuestas($id_llenado,$preguntas);
            foreach ($preguntas as $keys) {
                $respuestas[$keys->id_pregunta]=$this->clsdeetalle_llenado->respuestas($id_llenado,$keys->id_pregunta,null);
            }
           
            if ($key->tipo==0) {
                
            }elseif ($key->tipo==3) {
                
            }else if ($key->tipo==4) {
                $criterios=null;
                $criterios_detalle=null;
                $notas=null;
                $tablas=null;
                $preguntas=null;
                $alternativas=null;
                $respuestas=null;
                $aponderacion=$this->clsaponderacion->aponderacion();
                foreach ($aponderacion as $cuad) {
                    $preguntas=$this->clspreguntas->preguntas($cuad->id_cuadro);
                    $i=0;
                    foreach ($preguntas as $keys) {
                        $respuestas[$cuad->id_cuadro][$i]=$this->clsdeetalle_llenado->respuestas($id_llenado,$keys->id_pregunta);
                        $i++;
                    }
                    $calificaciones[$cuad->id_cuadro]=$this->clscalificacion->calificacion($cuad->id_cuadro);
                }
            }elseif ($key->tipo==5) {
              
            }
            $cuadros_impresion.=cuadrostipo(
                $key->id_cuadro,
                $key->tipo,
                $key->titulo,
                $calificacion,
                $criterios,
                $criterios_detalle,
                $notas,
                $tablas,
                $preguntas,
                $alternativas,
                $respuestas,
                $aponderacion,
                $calificaciones,
                $anexo_cuadro,
                $anexo_pregunta);
            $respuestas=[];
        }
        //declaramos el titulo de la pagina 
        $this->data["title"] = "Encuestas|Actualizar";
        $this->data["cuadros_preguntas"]=$cuadros_impresion;
        $this->data["contenido"]=$this->load->view('encuestas/llenado', $this->data, TRUE);
        $this->load->view('template', $this->data);
    }
    function guardar(){
        $this->load->model('clsllenado');
        $this->load->model('clsdeetalle_llenado');
        if (isset($_POST["id_llenado"])) {$id_llenado=$this->input->post('id_llenado',TRUE);}else{$id_llenado="";}
        if ($id_llenado!="") {
            $this->clsllenado->delete($id_llenado);
            $this->clsdeetalle_llenado->delete($id_llenado);
        }
        $comunidad=$this->input->post('comunidad',TRUE);
        $id_usuario=$_SESSION["usuario"]["id_usuario"];
        $id_distrito=$this->input->post('id_distrito',TRUE);
        $fecha=$this->input->post('fecha',TRUE);
        $fecha_destu=strtotime($fecha);
        $fecha=date(date("Y",$fecha_destu)."-".date("m",$fecha_destu)."-".date("d",$fecha_destu));
        $preguntas=$this->input->post('id_respuestas', TRUE);
        $respuestas=$this->input->post('respuestas',TRUE);
        $observacion=$this->input->post('observacion',TRUE);

        $data_usuario= array(
            'id_llenado'=>$id_llenado,
            'id_distrito' => $id_distrito,
            'id_usuario' => $id_usuario,
            'comunidad' => $comunidad,
            'fecha' => $fecha
        );
        $this->load->model("clslog_encuesta");
        if ($id_llenado=="") {
            $id_llenado=$this->clsllenado->insertar($data_usuario);

            $data_log=array(
                'fecha' => date("Y-m-d"),
                'hora' =>  date("H:i:s"),
                'accion' =>  "Creacion",
                'id_encuesta' =>  $id_llenado,
                'id_distrito' => $id_distrito,
                'localidad' =>  $comunidad,
                'id_usuario' =>  $_SESSION["usuario"]["id_usuario"],
                'usuario' => $_SESSION["usuario"]["user"]
                );
            $this->clslog_encuesta->insertar($data_log);
        }else{
            $this->clsllenado->insertar($data_usuario);
            $data_log=array(
                'fecha' => date("Y-m-d"),
                'hora' =>  date("H:i:s"),
                'accion' =>  "Actualizacion",
                'id_encuesta' =>  $id_llenado,
                'id_distrito' => $id_distrito,
                'localidad' =>  $comunidad,
                'id_usuario' =>  $_SESSION["usuario"]["id_usuario"],
                'usuario' => $_SESSION["usuario"]["user"]
                );
            $this->clslog_encuesta->insertar($data_log);
        }
        foreach ($preguntas as $key) {
            if (array_key_exists($key,$respuestas)) {
                if (is_array($respuestas[$key])) {
                   $i=0;
                   $cadena="";
                   foreach ($respuestas[$key] as $ke) {
                       if ($i===0) {
                           $cadena.=$ke;
                       }else{$cadena.="/".$ke;}
                       $i++;
                   }
                    $respuestas[$key]=$cadena;
                }
                $observa="";
                if (isset($observacion[$key])) {
                    $observa=$observacion[$key];
                }
                $data_llenado= array(
                    'id_llenado' => $id_llenado,
                    'id_pregunta' => $key,
                    'respuesta' => $respuestas[$key],
                    'observacion' => $observa
                );
                $this->clsdeetalle_llenado->insertar($data_llenado);
            }
        }
        header('Location:' .$this->data["base_url"].'index.php?c=encuestas');
    }
    function delete(){
        $id_llenado=$this->input->get('id', TRUE);
        $this->load->model('clsllenado');
        $this->load->model('clsdeetalle_llenado');
        $this->load->model("clslog_encuesta");
        
        foreach ($this->clsllenado->llenado_por_id($id_llenado) as $key) {
            $data_log=array(
                'fecha' => date("Y-m-d"),
                'hora' =>  date("H:i:s"),
                'accion' =>  "Eliminacion",
                'id_encuesta' =>  $id_llenado,
                'id_distrito' => $key->idDist,
                'localidad' =>  $key->comunidad,
                'id_usuario' =>  $_SESSION["usuario"]["id_usuario"],
                'usuario' => $_SESSION["usuario"]["user"]
                );
            $this->clslog_encuesta->insertar($data_log);
        }

        $this->clsllenado->delete($id_llenado);
        $this->clsdeetalle_llenado->delete($id_llenado);

        header('Location:' .$this->data["base_url"].'index.php?c=encuestas');
    }
}

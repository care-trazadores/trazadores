<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cuenta extends CI_Controller {
	public $controlador;
	public  function __construct() {
		parent::__construct();
		session_start();
		$this->data["base_url"]=$this->config->config['base_url'];
		if (!isset($_SESSION["usuario"])) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
		}
		if ($_SESSION["usuario"]["active"]!=TRUE) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
		

		$this->data["css"]=$this->config->config['css'];
		$this->data["js"]=$this->config->config['js'];
		$this->load->helper('menus');
		$this->load->helper('cuadros');
		$this->load->model('clsusuarios');
		$this->controlador="cuenta";
   	}
	public function index(){
		
	}

	function user_config(){
		if ( ! isset($_GET['id_usuario'])){$id_usuario = "";}
		else{$id_usuario=$this->input->get('id_usuario');}

		if ($id_usuario=="") {
			header('Location:'.$this->data["base_url"].'index.php?c=usuarios');
		}
		if ($_SESSION["usuario"]["id_usuario"]!=$id_usuario) {
			header('Location:'.$this->data["base_url"].'index.php?c=inicio');
		}
		if ( ! isset($_GET['tab'])){$tab = "";}
			else{$tab=$this->input->get('tab');}
		$this->data["title"]=" Cuenta Actualizar";
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
		$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

		if ($_SESSION["usuario"]["tipo"]=="usuario") {
			$datos=$this->clsusuarios->busqueda_user($id_usuario,"usuario");
			foreach ($datos as $key) {
				$this->data["datos_usuario"]=array(
					'id'=>$key->id_usuario,
					'user'=>$key->user,
					'estado'=>$key->estado,
					'id_departamento'=>$key->idDepa,
					'departamento'=>$key->departamento,
					'id_provincia'=>$key->idProv,
					'provincia'=>$key->provincia,
					'id_distrito'=>$key->id_distrito,
					'distrito'=>$key->distrito,
					'nombre' =>$key->nombre,
					'apellido_p'=>$key->apellido_p,
					'apellido_m'=>$key->apellido_m,
					'tipo'=>$key->tipo,
					'correo'=>$key->correo,
					'dni'=>$key->dni,
					'direccion'=>$key->direccion,
					'telefono'=>$key->telefono,
					'celular'=>$key->celular,
					'foto'=>$key->foto
					);
			}
			$this->data["tab_posicion"]=$tab;
			$this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
			$this->data["contenido"]=$this->load->view('cuenta/user_config',$this->data,TRUE);
			$this->load->view('template',$this->data);
		}elseif($_SESSION["usuario"]["tipo"]=="administrador"){
	        $datos=$this->clsusuarios->busqueda_user($id_usuario,"administrador");
	        foreach ($datos as $key) {
	            $this->data["datos_usuario"]=array(
	                'id'=>$key->id_usuario,
	                'user'=>$key->user,
	                'estado'=>$key->estado,
	                'nombre' =>$key->nombre,
	                'apellido_p'=>$key->apellido_p,
	                'apellido_m'=>$key->apellido_m,
	                'tipo'=>$key->tipo,
	                'correo'=>$key->correo,
	                'dni'=>$key->dni,
	                'direccion'=>$key->direccion,
	                'telefono'=>$key->telefono,
	                'celular'=>$key->celular,
	                'foto'=>$key->foto
	                );
	            $this->data["usuario_id_departamentos"][]=$key->id_departamento;
	            $this->data["usuario_departamentos"][]=$key->departamento;
	        }
	        $this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
	        $this->load->model("clsdepartamentos");
	        $this->data["departamentos"]=$this->clsdepartamentos->departamentos();
	        $this->data["tab_posicion"]=$tab;

	        $this->data["contenido"]=$this->load->view('cuenta/admin_config',$this->data,TRUE);
	        $this->load->view('template',$this->data);
		}else{
			$datos=$this->clsusuarios->busqueda_user($id_usuario,null);
	        foreach ($datos as $key) {
	            $this->data["datos_usuario"]=array(
	                'id'=>$key->id_usuario,
	                'user'=>$key->user,
	                'estado'=>$key->estado,
	                'nombre' =>$key->nombre,
	                'apellido_p'=>$key->apellido_p,
	                'apellido_m'=>$key->apellido_m,
	                'tipo'=>$key->tipo,
	                'correo'=>$key->correo,
	                'dni'=>$key->dni,
	                'direccion'=>$key->direccion,
	                'telefono'=>$key->telefono,
	                'celular'=>$key->celular,
	                'foto'=>$key->foto
	                );
	        }
	        $this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
	        $this->data["tab_posicion"]=$tab;

	        $this->data["contenido"]=$this->load->view('cuenta/admin_config',$this->data,TRUE);
	        $this->load->view('template',$this->data);
		}
		
	}
	function cuenta_update(){
		if ($_SESSION["usuario"]["tipo"]=="usuario") {
			$error="";
			if (!isset($_POST['user_id'])){header('Location:'.$this->data["base_url"].'index.php?c=usuarios');}
			else{$user_id=$this->input->post('user_id');}

			if ($this->input->post('user_usuario')=="") { $error="El campo usuario no puede ser vacio";}
			if ($this->input->post('id_distrito')=="") { $error="Debe seleccionar un distrito";}
			if ($error=='') {
				$data = array(
		            'user' => $this->input->post('user_usuario'),
		            'estado' => $this->input->post('user_estado'),
		            'id_distrito' =>$this->input->post('id_distrito')
		        );
		        $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
		        if($actualizar){}
		        else{echo "error al guardar";}
			}else{
				echo $error;
			}
		}else{
			$error="";
	        if (!isset($_POST['user_id'])){header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");}
	        else{$user_id=$this->input->post('user_id');}

	        if ($this->input->post('user_usuario')=="") { $error="El campo usuario no puede ser vacio";}
	        if ($error=='') {
	            $data = array(
	                'user' => $this->input->post('user_usuario'),
	                'estado' => $this->input->post('user_estado')
	            );
		        if ($_SESSION["usuario"]["tipo"]!="master") {
		        	$this->load->model('clsadmin_regiones');
			        $this->clsadmin_regiones->delete_user($user_id);
			        foreach ($this->input->post('id_departamentos') as $key ) {
			            $data_depa=array('id_usuario' =>$user_id,'id_departamento'=>$key);
			            $this->clsadmin_regiones->inserte($data_depa);
			        }

		        }
	       
	        $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
	            if($actualizar){}
	            else{echo "error al guardar";}
	        }else{
	            echo $error;
	        }
		}
		
	}
	function info_update(){
		$this->load->model('clsdetalleusuario');
		if ( ! isset($_POST['user_id'])){header('Location:'.$this->data["base_url"].'index.php?c=usuarios');}
		else{$user_id=$this->input->post('user_id');}
		$data = array(
            'nombre' => $this->input->post('user_nombre'),
            'apellido_p' => $this->input->post('user_apellido_p'),
            'apellido_m' => $this->input->post('user_apellido_m'),
            'dni' => $this->input->post('user_dni'),
            'correo' => $this->input->post('user_correo'),
            'direccion' => $this->input->post('user_direccion'),
            'telefono' => $this->input->post('user_telefono'),
            'celular' => $this->input->post('user_celular')
        );
		$actualizar = $this->clsdetalleusuario->actualizar_usuario_info($user_id,$data);
        //si la actualización ha sido correcta creamos una sesión flashdata para decirlo
        if($actualizar){}
        else{echo "error al guardar";}
	}
	function foto_update(){
		$id_usuario=$this->input->post('user_id');
		$error="";
        $file = $_FILES["file-foto"];
		$nombre = $file["name"];
		if ($nombre=="") {
			echo "Lo sentimos, debe seleccionar una imagen";
		}else{
			$tipo=$file["type"];
			$ruta_provisional=$file["tmp_name"];
			$size=$file["size"];
			$dimensiones=getimagesize($ruta_provisional);
			$width=$dimensiones[0];
			$height=$dimensiones[1];
			$carpeta="img/usuarios/";
			if ($tipo!='image/jpg' && $tipo!='image/jpeg' && $tipo!='image/png' && $tipo!='image/gif') {
				$error="Error, el archivo no es una imagen";
			}elseif($size>1024*1024){
				$error="Error, el tamaño maximo permitido es 1mb";
			}elseif ($width>500 && $width<60 && $height>500 && $height<60) {
				$error="Error, la anchura y la altura de la imagen debe de ser inferior 500px y mayor a 60 px";
			}
			if ($error=="") {
				if ($tipo=='image/jpg' || $tipo=='image/jpeg') {$extension=".jpg";}
			
				if ($tipo=='image/png') {$extension=".png";}
				if ($tipo=='image/gif') {$extension=".gif";}
				$nombre_foto='usuario'.$id_usuario.$extension;
				$src=$carpeta.$nombre_foto;
				move_uploaded_file($ruta_provisional, $src);
				$data = array(
		            'foto' => $nombre_foto
		        );
				
		        $this->load->model('clsdetalleusuario');
				$actualizar = $this->clsdetalleusuario->actualizar_usuario_info($id_usuario,$data);
		        //si la actualización ha sido correcta creamos una sesión flashdata para decirlo
		        if($actualizar){}
		        else{echo "error al guardar";}
		    	//$this->redimensionar_jpeg($nombre_foto);
			}
		}

		
	}	
	function password_update(){
		$error="";
		if (!isset($_POST['user_id'])){header('Location:'.$this->data["base_url"].'index.php?c=usuarios');}
		else{$user_id=$this->input->post('user_id');}

		if ($this->input->post('password_new')=="") { $password_new='';$error="Ingrese nueva clave";}
		else{$password_new=md5($this->input->post('password_new'));}

		if ($this->input->post('password_rep')=="") { $password_rep='';$error="Debe repetir la clave";}
		else{$password_rep=md5($this->input->post('password_rep'));}

		if ($_SESSION["usuario"]["tipo"]!="usuario") {
			if ($password_new!=$password_rep) {
				$error="Las claves nuevas no coinciden";
			}else{
				$data = array(
		            'pass' => $password_new
		        );
		        $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
		        if($actualizar){}
		        else{echo "error al guardar";}
			}
		}else{
			if ($this->input->post('password_ant')=="") {$password_ant='';$error="Necesita colocar la clave anterior";}
			else{$password_ant=md5($this->input->post('password_ant'));}

			$datos=$this->clsusuarios->busqueda_user($user_id,"usuario");
			foreach ($datos as $key) {
				$pass_anterior=$key->pass;
			}
			if ($password_new!=$password_rep) {
				$error="Las claves nuevas no coinciden";
			}elseif ($pass_anterior!=$password_ant) {
				$error="Las clave anterior no coinciden";
			}

			if ($error!="") {
				echo $error;
			}else{
				$data = array(
		            'pass' => $password_new
		        );
		        $actualizar = $this->clsusuarios->actualizar_usuario($user_id,$data);
		        if($actualizar){}
		        else{echo "error al guardar";}
			}
		}
		echo $error;
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class reportes extends CI_Controller {
	public $controlador;
	public  function __construct() {
		parent::__construct();
		session_start();
		$this->data["base_url"]=$this->config->config['base_url'];
		if (!isset($_SESSION["usuario"])) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
		}
		if ($_SESSION["usuario"]["active"]!=TRUE) {
			header("Location: ".$this->data["base_url"]."index.php?c=login&m=close");
		}
		$this->data["css"]=$this->config->config['css'];
		$this->data["js"]=$this->config->config['js'];
		$this->load->helper('menus');
		$this->load->helper('cuadros');

		$this->controlador="reportes";	
   	}
	public function index(){
		$this->data["title"] = "Administrador|Listado";
        $this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
       $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

        $i=0;
        $cuadros[$i]["color"]="green";
		$cuadros[$i]["icon"]="clipboard";
		$cuadros[$i]["titulo"]="Reporte Informacion general";
		$cuadros[$i]["cantidad"]="";
		$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=reportes&m=general";
		$i++;

        $cuadros[$i]["color"]="green";
		$cuadros[$i]["icon"]="clipboard";
		$cuadros[$i]["titulo"]="Reporte detallado (cuadros)";
		$cuadros[$i]["cantidad"]="";
		$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=reportes&m=cuadros&tipo=detallado";
		$i++;

		$cuadros[$i]["color"]="green";
		$cuadros[$i]["icon"]="clipboard";
		$cuadros[$i]["titulo"]="Reporte Resumen (cuadros)";
		$cuadros[$i]["cantidad"]="";
		$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=reportes&m=cuadros&tipo=resumen";
		$i++;

		$cuadros[$i]["color"]="green";
		$cuadros[$i]["icon"]="clipboard";
		$cuadros[$i]["titulo"]="Reporte Grafico (cuadros)";
		$cuadros[$i]["cantidad"]="";
		//$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=reportes&m=cuadros&tipo=grafico";
		$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=reportes&m=grafico";
		$i++;

		$cuadros[$i]["color"]="green";
		$cuadros[$i]["icon"]="clipboard";
		$cuadros[$i]["titulo"]="Reporte General Jass";
		$cuadros[$i]["cantidad"]="";
		$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=reportes&m=jazz";
		$i++;

		if ($_SESSION["usuario"]['tipo']!="usuario") {
			$cuadros[$i]["color"]="blue";
			$cuadros[$i]["icon"]="clipboard";
			$cuadros[$i]["titulo"]="Reporte ATM";
			$cuadros[$i]["cantidad"]="";
			$cuadros[$i]["link"]=$this->data["base_url"]."index.php?c=reportes&m=atm";
			$i++;
		}
		
		$this->data["cuadros_link"] = cuadros_link($cuadros);
        $this->data["contenido"] = $this->load->view('reportes/panel', $this->data, TRUE);
        $this->load->view('template', $this->data);
	}
	public function cuadros()
	{
		
       $this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
       $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

		$this->load->helper('paginador');
		$this->load->helper('operaciones');
		
		$this->load->model('clsaponderacion');
		$this->load->model('clscalificacion');
		$this->load->model('clspreguntas');
		$this->load->model('clsllenado');
		$this->load->model('clsdeetalle_llenado');

		$limite=5;
		$id_departamento = "";
		$id_provincia = "";
		$id_distrito = "";
		$pagina = 1;
		$tipo="detallado";

		if (isset($_POST['limite'])){$limite=$this->input->post('limite', TRUE);}
		if (isset($_GET['limite'])){$limite=$this->input->get('limite', TRUE);}
		if (isset($_POST['id_departamento'])){$id_departamento=$this->input->post('id_departamento', TRUE);}
		if (isset($_GET['id_departamento'])){$id_departamento=$this->input->get('id_departamento', TRUE);}
		if (isset($_POST['id_provincia'])){$id_provincia=$this->input->post('id_provincia', TRUE);}
		if (isset($_GET['id_provincia'])){$id_provincia=$this->input->get('id_provincia', TRUE);}
		if (isset($_POST['id_distrito'])){$id_distrito=$this->input->post('id_distrito', TRUE);}
		if (isset($_GET['id_distrito'])){$id_distrito=$this->input->get('id_distrito', TRUE);}
		if (isset($_POST['pagina'])){$pagina=$this->input->post('pagina', TRUE);}
		if (isset($_GET['pagina'])){$pagina=$this->input->get('pagina', TRUE);}
		if (isset($_POST['tipo'])){$tipo=$this->input->post('tipo', TRUE);}
		if (isset($_GET['tipo'])){$tipo=$this->input->get('tipo', TRUE);}
		$comienzo=($pagina-1)*$limite;
		$encuestas="";
		$respuestas="";
		$calificacion="";
		$calificaciones=[];

		if ($tipo=="grafico") {
			$limite="";
			$pagina = "";
			$comienzo="";
		}
		$encuestas=$this->clsllenado->reportes($limite,$comienzo,null,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
		if ($tipo=="resumen" or $tipo=="detallado") {
			$aponderacion=$this->clsaponderacion->aponderacion();
			$i=0;
			foreach ($aponderacion as $cuadros) {
				foreach ($this->clspreguntas->preguntas($cuadros->id_cuadro) as $key) {
					$preguntas[$cuadros->id_cuadro][$i]=$key->id_pregunta;
					$i++;
				}
			}
			if (count($encuestas)!=0) {
				foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
					$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
				}
			}
			
			foreach ($aponderacion as $key) {
				$calificaciones[$key->id_cuadro]=$this->clscalificacion->calificacion($key->id_cuadro);
			}
			$calificacion=$this->clscalificacion->calificacion(11);
		}elseif ($tipo=="grafico") {
			$aponderacion=$this->clsaponderacion->aponderacion();
			$i=0;
			foreach ($aponderacion as $cuadros) {
				foreach ($this->clspreguntas->preguntas($cuadros->id_cuadro) as $key) {
					$preguntas[$cuadros->id_cuadro][$i]=$key->id_pregunta;
					$i++;
				}
			}
			if ($_SESSION["usuario"]["tipo"]!="usuario") {
				if ($id_departamento!="" || $id_provincia!="" || $id_distrito!="") {
					if (count($encuestas)!=0) {
						foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
							$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
						}
					}
					
				}else{
					$encuestas="";
					/*
					foreach ($this->clsdeetalle_llenado->respuestas() as $key) {
						$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
					}*/
				}
			}else{
				if (count($encuestas)!=0) {
						foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
							$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
						}
					}
			}
			
			foreach ($aponderacion as $key) {
				$calificaciones[$key->id_cuadro]=$this->clscalificacion->calificacion($key->id_cuadro);
			}
			$calificacion=$this->clscalificacion->calificacion(11);
		}




		/*
		$aponderacion=$this->clsaponderacion->aponderacion();
		$i=0;
		foreach ($aponderacion as $cuadros) {
			foreach ($this->clspreguntas->preguntas($cuadros->id_cuadro) as $key) {
				$preguntas[$cuadros->id_cuadro][$i]=$key->id_pregunta;
				$i++;
			}
		}
		if ($id_departamento!="" || $id_provincia!="" || $id_distrito!="") {
			foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
				$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
			}
		}else{
			foreach ($this->clsdeetalle_llenado->respuestas() as $key) {
				$respuestas[$key->id_llenado][$key->id_pregunta]=$key->respuesta;
			}
		}
		foreach ($aponderacion as $key) {
			$calificaciones[$key->id_cuadro]=$this->clscalificacion->calificacion($key->id_cuadro);
		}
		$calificacion=$this->clscalificacion->calificacion(11);*/

		$total=$this->clsllenado->total_encuestas(null,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
		$url_paginador=$this->data["base_url"]."index.php?c=reportes&m=cuadros";
		if ($tipo!="grafico") {
    		$this->data["paginador"]=paginador($url_paginador,$total,$limite,$pagina,$encuestas,null,null,$id_departamento,$id_provincia,$id_distrito,$tipo);
    	}
    	

    	$this->data["datos_exportacion"]="";
    	if ($pagina!="") {
    		$this->data["datos_exportacion"].="&pagina=".$pagina;
    	}
    	if ($id_departamento!="") {
    		$this->data["datos_exportacion"].="&id_departamento=".$id_departamento;
    	}
    	if ($id_provincia!="") {
    		$this->data["datos_exportacion"].="&id_provincia=".$id_provincia;
    	}
    	if ($id_distrito!="") {
    		$this->data["datos_exportacion"].="&id_distrito=".$id_distrito;
    	}
    	if ($limite!="") {
    		$this->data["datos_exportacion"].="&limite=".$limite;
    	}
		if ($tipo=="detallado") {
			$this->data["title"] = "Reportes|Detallado";
			$this->data["titulo"]="<h3 class='page-title'>Reportes | Detallado<small> reportes en modo detallado(aponderacion)</small></h3>";
			$this->load->helper('reporte_detallado');
			$this->data["tipo_reporte"]="detallado";
			$this->data["datos_exportacion"].="&tipo=detallado";
			$this->data["action"]=$this->data["base_url"]."index.php?c=reportes&m=cuadros&tipo=detallado";
			$this->data["tablas"]=reporte_detallado($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion);
		}elseif ($tipo=="resumen") {
			$this->data["title"] = "Reportes|Resumen";
			$this->data["titulo"]="<h3 class='page-title'>Reportes | Resumen<small> reportes en modo resumido(aponderacion)</small></h3>";
			$this->load->helper('reporte_resumen');
			$this->data["tipo_reporte"]="resumen";
			$this->data["datos_exportacion"].="&tipo=resumen";
			$this->data["action"]=$this->data["base_url"]."index.php?c=reportes&m=cuadros&tipo=resumen";
			$this->data["tablas"]=reporte_resumen($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion);
		}elseif ($tipo=="grafico") {
			$this->data["title"] = "Reportes|graficos";
			$this->data["titulo"]="<h3 class='page-title'>Reportes | Graficos<small> reportes en modo grafico(aponderacion)</small></h3>";

			$this->data["tipo_reporte"]="grafico";
			$this->data["datos_exportacion"].="&tipo=grafico";
			$this->data["action"]=$this->data["base_url"]."index.php?c=reportes&m=cuadros&tipo=grafico";
			$this->data["paginador"]="";

			if ($encuestas!="") {
				$this->load->helper('reporte_grafico');
				$this->data["tablas"]=reporte_grafico($encuestas,$aponderacion,$preguntas,$respuestas,$calificaciones,$calificacion);	
			}else {
				$this->data["tablas"]="";
			}

		}
    	$this->data["limite"]=$limite;
    	$this->data["id_departamento"]=$id_departamento;
    	$this->data["id_provincia"]=$id_provincia;
    	$this->data["id_distrito"]=$id_distrito;
    	$this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];

		$this->data["contenido"] = $this->load->view('reportes/detallado', $this->data, TRUE);
        $this->load->view('template', $this->data);
	}
	public function grafico(){
		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
       	$this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);
       	$id_departamento=($this->input->get_post('id_departamento', TRUE))?$this->input->get_post('id_departamento', TRUE):"";
       	$id_provincia=($this->input->get_post('id_provincia', TRUE))?$this->input->get_post('id_provincia', TRUE):"";
       	$id_distrito=($this->input->get_post('id_distrito', TRUE))?$this->input->get_post('id_distrito', TRUE):"";
		$this->data["datos_exportacion"]="";
    	if ($id_departamento!="") { $this->data["datos_exportacion"].="&id_departamento=".$id_departamento;}
    	if ($id_provincia!="") {$this->data["datos_exportacion"].="&id_provincia=".$id_provincia;}
    	if ($id_distrito!="") {$this->data["datos_exportacion"].="&id_distrito=".$id_distrito;}
    	$this->data["title"] = "Reportes|graficos";
		$this->data["titulo"]="<h3 class='page-title'>Reportes | Graficos<small> reportes en modo grafico(aponderacion)</small></h3>";
		$this->data["tipo_reporte"]="grafico";
		$this->data["action"]=$this->data["base_url"]."index.php?c=reportes&m=grafico";
		$this->data["paginador"]="";
		$this->data["tablas"]="";
		if ($this->input->post()) {	
			$this->load->model("clsreporte_calificacion");
			$this->load->helper('reporte_grafico');
			foreach ($this->clsreporte_calificacion->cuadros() as $key) {
				$array[$key->id_cuadro][$key->calificacion]["id_cuadro"]=$key->id_cuadro;
				$array[$key->id_cuadro][$key->calificacion]["titulo"]=$key->titulo;
				$array[$key->id_cuadro][$key->calificacion]["total"]=0;
				$array[$key->id_cuadro][$key->calificacion]["calificacion"]=$key->calificacion;
				$array[$key->id_cuadro][$key->calificacion]["background"]=$key->background;
			}
			$datos=$this->clsreporte_calificacion->total_calificacion($id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
			if ($datos!=null) {
				foreach ($datos as $key) {
					$array[$key->id_cuadro][$key->calificacion]["total"]=$key->total;
				}
			}
			$dat=$this->clsreporte_calificacion->total_aponderacion($id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
			if ($dat!=null) {
				foreach ($dat as $key) {
					$array[11][$key->calificacion]["total"]=$key->total;
				}
			}
			$this->data["tablas"]=re_porgrafico($array);
		}		
    	$this->data["id_departamento"]=$id_departamento;
    	$this->data["id_provincia"]=$id_provincia;
    	$this->data["id_distrito"]=$id_distrito;
    	$this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
		$this->data["contenido"] = $this->load->view('reportes/grafico', $this->data, TRUE);
        $this->load->view('template', $this->data);
	}
	public function general(){
		$limite=5;
		$id_departamento = "";
		$id_provincia = "";
		$id_distrito = "";
		$tipo="";
		$pagina = 1;

		if (isset($_POST['limite'])){$limite=$this->input->post('limite', TRUE);}
		if (isset($_GET['limite'])){$limite=$this->input->get('limite', TRUE);}
		if (isset($_POST['id_departamento'])){$id_departamento=$this->input->post('id_departamento', TRUE);}
		if (isset($_GET['id_departamento'])){$id_departamento=$this->input->get('id_departamento', TRUE);}
		if (isset($_POST['id_provincia'])){$id_provincia=$this->input->post('id_provincia', TRUE);}
		if (isset($_GET['id_provincia'])){$id_provincia=$this->input->get('id_provincia', TRUE);}
		if (isset($_POST['id_distrito'])){$id_distrito=$this->input->post('id_distrito', TRUE);}
		if (isset($_GET['id_distrito'])){$id_distrito=$this->input->get('id_distrito', TRUE);}
		if (isset($_POST['pagina'])){$pagina=$this->input->post('pagina', TRUE);}
		if (isset($_GET['pagina'])){$pagina=$this->input->get('pagina', TRUE);}

		$comienzo=($pagina-1)*$limite;

		$this->load->helper('paginador');
		$this->load->helper('operaciones');
		$this->load->helper('reporte_general');


		
		$this->load->model('clsllenado');
		$this->load->model('clspreguntas');
		$this->load->model('clsalternativas');
		$this->load->model('clsdeetalle_llenado');

		$encuestas=$this->clsllenado->reportes($limite,$comienzo,null,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);

		$preguntas=$this->clspreguntas->preguntas(1);
		$respuestas="";
		foreach ($encuestas as $key) {
			foreach ($preguntas as $ke) {
				$respuestas[$key->id_llenado][$ke->id_pregunta]="---";
				foreach ($this->clsdeetalle_llenado->respuestas($key->id_llenado,$ke->id_pregunta,null) as $k) {
					if ($ke->id_pregunta==48 || $ke->id_pregunta==51 || $ke->id_pregunta==52 || $ke->id_pregunta==53 || $ke->id_pregunta==61) {
						$cadena="";
						$verificacion = strpos($k->respuesta,"/");
						if ($verificacion!==false) {
							$porciones = explode("/",$k->respuesta);
							foreach ($porciones as $val) {
								foreach ($this->clsalternativas->alternativa_idpregunta($ke->id_pregunta,$val) as $alter_res) {
									$cadena.=$alter_res->texto." --- ";
								}
							}
						}else{
							foreach ($this->clsalternativas->alternativa_idpregunta($ke->id_pregunta,$k->respuesta) as $alter_res) {
								$cadena.=$alter_res->texto;
							}
						}
						$respuestas[$key->id_llenado][$ke->id_pregunta]=$cadena;
					}else{
						$respuestas[$key->id_llenado][$ke->id_pregunta]=$k->respuesta;
					}
				}
			}
		}

		$this->data["tablas"]=reporte_general($encuestas,$preguntas,$respuestas);

		$total=$this->clsllenado->total_encuestas(null,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
		$url_paginador=$this->data["base_url"]."index.php?c=reportes&m=general";
    	$this->data["paginador"]=paginador($url_paginador,$total,$limite,$pagina,$encuestas,null,null,$id_departamento,$id_provincia,$id_distrito,$tipo);

		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
       $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

      	$this->data["tipo_reporte"]="";
      	$this->data["datos_exportacion"]="";

    	if ($pagina!="") {
    		$this->data["datos_exportacion"].="&pagina=".$pagina;
    	}
    	if ($id_departamento!="") {
    		$this->data["datos_exportacion"].="&id_departamento=".$id_departamento;
    	}
    	if ($id_provincia!="") {
    		$this->data["datos_exportacion"].="&id_provincia=".$id_provincia;
    	}
    	if ($id_distrito!="") {
    		$this->data["datos_exportacion"].="&id_distrito=".$id_distrito;
    	}
    	if ($limite!="") {
    		$this->data["datos_exportacion"].="&limite=".$limite;
    	}
      	$this->data["datos_exportacion"].="&tipo=general";
      	$this->data["action"]=$this->data["base_url"]."index.php?c=reportes&m=general";
		$this->data["limite"]=$limite;
    	$this->data["id_departamento"]=$id_departamento;
    	$this->data["id_provincia"]=$id_provincia;
    	$this->data["id_distrito"]=$id_distrito;
    	$this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
    	
    	$this->data["title"] = "Reportes|General";
    	$this->data["titulo"]="<h3 class='page-title'>Reportes | Generales<small> reportes en modo general</small></h3>";

		$this->data["contenido"] = $this->load->view('reportes/detallado', $this->data, TRUE);
        $this->load->view('template', $this->data);
	
	}
	function jazz(){
		$limite=5;
		$id_departamento = "";
		$id_provincia = "";
		$id_distrito = "";
		$tipo="";
		$pagina = 1;

		if (isset($_POST['limite'])){$limite=$this->input->post('limite', TRUE);}
		if (isset($_GET['limite'])){$limite=$this->input->get('limite', TRUE);}
		if (isset($_POST['id_departamento'])){$id_departamento=$this->input->post('id_departamento', TRUE);}
		if (isset($_GET['id_departamento'])){$id_departamento=$this->input->get('id_departamento', TRUE);}
		if (isset($_POST['id_provincia'])){$id_provincia=$this->input->post('id_provincia', TRUE);}
		if (isset($_GET['id_provincia'])){$id_provincia=$this->input->get('id_provincia', TRUE);}
		if (isset($_POST['id_distrito'])){$id_distrito=$this->input->post('id_distrito', TRUE);}
		if (isset($_GET['id_distrito'])){$id_distrito=$this->input->get('id_distrito', TRUE);}
		if (isset($_POST['pagina'])){$pagina=$this->input->post('pagina', TRUE);}
		if (isset($_GET['pagina'])){$pagina=$this->input->get('pagina', TRUE);}

		$comienzo=($pagina-1)*$limite;

		$this->load->helper('paginador');
		$this->load->helper('operaciones');
		$this->load->helper('reporte_jazz');

		
		$this->load->model('clsllenado');
		$this->load->model('clspreguntas');
		$this->load->model('clsdeetalle_llenado');
		$this->load->model('clscalificacion');

		$encuestas=$this->clsllenado->reportes($limite,$comienzo,null,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);


		$preguntas=$this->clspreguntas->preguntas(12);
		foreach ($this->clsdeetalle_llenado->respuestas(null,null,$encuestas) as $key) {
				$respuestas[$key->id_llenado]["respuesta"][$key->id_pregunta]=$key->respuesta;
				$respuestas[$key->id_llenado]["texto"][$key->id_pregunta]=$key->texto;
		}
		//var_dump($respuestas[1]["respuesta"]);
		$calificacion=$this->clscalificacion->calificacion(12);

		$this->data["tablas"]=reporte_jazz($encuestas,$preguntas,$respuestas,$calificacion);

		$total=$this->clsllenado->total_encuestas(null,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
		$url_paginador=$this->data["base_url"]."index.php?c=reportes&m=jazz";
    	$this->data["paginador"]=paginador($url_paginador,$total,$limite,$pagina,$encuestas,null,null,$id_departamento,$id_provincia,$id_distrito,$tipo);


		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

      	$this->data["tipo_reporte"]="";
      	$this->data["datos_exportacion"]="";

    	if ($pagina!="") {
    		$this->data["datos_exportacion"].="&pagina=".$pagina;
    	}
    	if ($id_departamento!="") {
    		$this->data["datos_exportacion"].="&id_departamento=".$id_departamento;
    	}
    	if ($id_provincia!="") {
    		$this->data["datos_exportacion"].="&id_provincia=".$id_provincia;
    	}
    	if ($id_distrito!="") {
    		$this->data["datos_exportacion"].="&id_distrito=".$id_distrito;
    	}
    	if ($limite!="") {
    		$this->data["datos_exportacion"].="&limite=".$limite;
    	}
      	$this->data["datos_exportacion"].="&tipo=jazz";
      	$this->data["action"]=$this->data["base_url"]."index.php?c=reportes&m=jazz";
		$this->data["limite"]=$limite;
    	$this->data["id_departamento"]=$id_departamento;
    	$this->data["id_provincia"]=$id_provincia;
    	$this->data["id_distrito"]=$id_distrito;
    	$this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
    	
    	$this->data["title"] = "Reportes|Jass";
    	$this->data["titulo"]="<h3 class='page-title'>Reportes | Jass<small> reportes en modo jass</small></h3>";

		$this->data["contenido"] = $this->load->view('reportes/detallado', $this->data, TRUE);
        $this->load->view('template', $this->data);
	}
	function atm(){
		$limite=5;
		$id_departamento = "";
		$id_provincia = "";
		$id_distrito = "";
		$tipo="";
		$pagina = 1;
		if (isset($_POST['limite'])){$limite=$this->input->post('limite', TRUE);}
		if (isset($_GET['limite'])){$limite=$this->input->get('limite', TRUE);}
		if (isset($_POST['id_departamento'])){$id_departamento=$this->input->post('id_departamento', TRUE);}
		if (isset($_GET['id_departamento'])){$id_departamento=$this->input->get('id_departamento', TRUE);}
		if (isset($_POST['id_provincia'])){$id_provincia=$this->input->post('id_provincia', TRUE);}
		if (isset($_GET['id_provincia'])){$id_provincia=$this->input->get('id_provincia', TRUE);}
		if (isset($_POST['id_distrito'])){$id_distrito=$this->input->post('id_distrito', TRUE);}
		if (isset($_GET['id_distrito'])){$id_distrito=$this->input->get('id_distrito', TRUE);}
		if (isset($_POST['pagina'])){$pagina=$this->input->post('pagina', TRUE);}
		if (isset($_GET['pagina'])){$pagina=$this->input->get('pagina', TRUE);}

		$comienzo=($pagina-1)*$limite;
		$this->load->helper('paginador');
		$this->load->helper('operaciones');
		$this->load->helper('reporte_atm');
		$this->load->model('clsllenadoMunicipal');
		$this->load->model('clspreguntasMunicipal');
		$this->load->model('clsdeetalle_llenadoMunicipal');
		$this->load->model('clscalificacionMunicipal');

		$encuestas=$this->clsllenadoMunicipal->reportes($limite,$comienzo,null,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
		$preguntas=$this->clspreguntasMunicipal->preguntas(1);
		foreach ($this->clsdeetalle_llenadoMunicipal->respuestas(null,null,$encuestas) as $key) {
				$respuestas[$key->id_llenado]["respuesta"][$key->id_pregunta]=$key->respuesta;
				$respuestas[$key->id_llenado]["texto"][$key->id_pregunta]=$key->texto;
		}
		//var_dump($respuestas[1]["respuesta"]);
		$calificacion=$this->clscalificacionMunicipal->calificacion(1);

		$this->data["tablas"]=reporte_atm($encuestas,$preguntas,$respuestas,$calificacion);

		$total=$this->clsllenadoMunicipal->total_encuestas(null,$id_departamento,$id_provincia,$id_distrito,$_SESSION["usuario"]["tipo"],$_SESSION["usuario"]["id_usuario"],$_SESSION["usuario"]["grupo_departamento"]);
		$url_paginador=$this->data["base_url"]."index.php?c=reportes&m=atm";
    	$this->data["paginador"]=paginador($url_paginador,$total,$limite,$pagina,$encuestas,null,null,$id_departamento,$id_provincia,$id_distrito,$tipo);


		$this->data["menus"]=menus($this->data["base_url"],$this->controlador,$_SESSION["usuario"]['tipo']);
        $this->data["nav"]=nav($this->data["base_url"],$_SESSION["usuario"]['nombres'],$_SESSION["usuario"]['apellido_p'],$_SESSION["usuario"]['foto'],$_SESSION["usuario"]["id_usuario"]);

      	$this->data["tipo_reporte"]="";
      	$this->data["datos_exportacion"]="";

    	if ($pagina!="") {
    		$this->data["datos_exportacion"].="&pagina=".$pagina;
    	}
    	if ($id_departamento!="") {
    		$this->data["datos_exportacion"].="&id_departamento=".$id_departamento;
    	}
    	if ($id_provincia!="") {
    		$this->data["datos_exportacion"].="&id_provincia=".$id_provincia;
    	}
    	if ($id_distrito!="") {
    		$this->data["datos_exportacion"].="&id_distrito=".$id_distrito;
    	}
    	if ($limite!="") {
    		$this->data["datos_exportacion"].="&limite=".$limite;
    	}
      	$this->data["datos_exportacion"].="&tipo=atm";
      	$this->data["action"]=$this->data["base_url"]."index.php?c=reportes&m=atm";
		$this->data["limite"]=$limite;
    	$this->data["id_departamento"]=$id_departamento;
    	$this->data["id_provincia"]=$id_provincia;
    	$this->data["id_distrito"]=$id_distrito;
    	$this->data["tipo_usuario"]=$_SESSION["usuario"]["tipo"];
    	
    	$this->data["title"] = "Reportes|ATM";
    	$this->data["titulo"]="<h3 class='page-title'>Reportes | ATM<small> reportes en modo jass</small></h3>";

		$this->data["contenido"] = $this->load->view('reportes/detallado', $this->data, TRUE);
        $this->load->view('template', $this->data);

	}
}

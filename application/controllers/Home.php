<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public $controlador;
	public  function __construct() {
		parent::__construct();
		$this->data["base_url"]=$this->config->config['base_url'];
		$this->data["css"]=array('reset.css','presentacion.css','estilos.css');
		$this->data["js"]=array('jquery-2.1.3.min.js','direc.js','higchats/highcharts.js','js.js','backstretch.js');
   	}
	public function index()
	{
		$this->data["title"]="Trazadores|Home";
		$this->data["controlador"]="home";
		$this->data["contenido"]=$this->load->view('presentacion/home',$this->data,TRUE);
		$this->load->view('template2',$this->data);
	}
}

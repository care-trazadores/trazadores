<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	public  function __construct() {
		parent::__construct();
		$this->data["base_url"]=$this->config->config['base_url'];
		$this->data["css"]=array('reset.css','entypo.css');
		$this->load->model('clsusuarios');
		session_start();
   	}
	public function index(){
		if (!isset($_REQUEST["error"])) {$error="";}else{$error=$this->input->get_post('error', true);}
		if ($error=="" or $error==1 or $error==2) {
			if ($error==1) {$this->data["error_data"]="Lo sentimos el usuario no existe";}
			elseif ($error==2) {$this->data["error_data"]="Lo sentimos la contaseña no coincide";}
			elseif ($error==3) {$this->data["error_data"]="Lo sentimos su cuenta ha sido desahabilitada";}
			$this->data["error"]=$error;
			$this->load->view('login/login',$this->data);
		}else{
			header("Location: ".$this->data["base_url"]."index.php?c=login");
		}
	}
	public function re_password(){
		$error="";
		if (!isset($_POST["email"])) {$email="";}else{$email=$this->input->get_post('email', true);}
		if ($email!="") {
			
		}

		$this->data["error"]=$error;
		$this->load->view('login/re_password',$this->data);
	}
	public function sesion(){
		if (!isset($_POST['username'])) {$user = "";} else {$user=$this->input->post('username', TRUE);}
		if (!isset($_POST['password'])) {$pass = "";} else {$pass=md5($this->input->post('password', TRUE));}
		$this->load->model('clsusuarios');
		$datos=$this->clsusuarios->verificar($user);
		if ($datos==null) {
			$error=1;
			header("Location: ".$this->data["base_url"]."index.php?c=login&error=".$error);
		}else{
			foreach ($datos as $key) {
				if ($key->estado==0) {
					$error=3;
					header("Location: ".$this->data["base_url"]."index.php?c=login&error=".$error);
				}
				$password=$key->pass;
				$_SESSION["usuario"]["id_usuario"]=$key->id_usuario;
				$_SESSION["usuario"]["user"]=$key->user;
				$_SESSION["usuario"]["tipo"]=$key->tipo;
				$_SESSION["usuario"]["nombres"]=$key->nombre;
				$_SESSION["usuario"]["apellido_p"]=$key->apellido_p;
				$_SESSION["usuario"]["apellido_m"]=$key->apellido_m;
				$_SESSION["usuario"]["foto"]=$key->foto;
				$_SESSION["usuario"]["id_distrito"]="";
				$_SESSION["usuario"]["grupo_departamento"]="";
				if ($_SESSION["usuario"]["tipo"]=="usuario") {
					$_SESSION["usuario"]["id_distrito"]=$key->id_distrito;
					$_SESSION["usuario"]["distrito"]=$key->distrito;
					$_SESSION["usuario"]["provincia"]=$key->provincia;
					$_SESSION["usuario"]["departamento"]=$key->departamento;
				}
				if ($_SESSION["usuario"]["tipo"]=="administrador") {
					$this->load->model('clsadmin_regiones');
					/* obteniendo regiones por cada administrador*/
		            $regiones= $this->clsadmin_regiones->departamento_admi($_SESSION["usuario"]["id_usuario"]);
		            foreach ($regiones as $key) {
						$departamentos[]=$key->id_departamento;
					}
					$_SESSION["usuario"]["grupo_departamento"]=$departamentos;
				}
				$_SESSION["usuario"]["active"]=true;
			}
			if ($password!=$pass){
				session_destroy();
				$error=2;
				header("Location: ".$this->data["base_url"]."index.php?c=login&error=".$error);
			}else{
				header("Location: ".$this->data["base_url"]."index.php?c=inicio");
			}
		}
	}
	public function close(){
		session_destroy();
		header("Location: ".$this->data["base_url"]);
	}
}


$(function () {
	//$("#rigth").width($(window).width()-$("#left").width());
	//$("#rigth").css({"margin-left":$("#left").width()});
});

$(function () {
    
	$('#portlet-body-rigth').highcharts({
	        chart: {
	            type: 'area',
	            spacingBottom: 30
	        },
	        title:{
			    text:''
			},
	        subtitle: {
	            text: '',
	            floating: true,
	            align: 'right',
	            verticalAlign: 'bottom',
	            y: 15
	        },
	        xAxis: {
	            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago','Sep','Oct','Nov','Dic']
	        },
	        yAxis: {
	            title: {
	                text: ''
	            },
	            labels: {
	                formatter: function () {
	                    return this.value;
	                }
	            }
	        },
	        tooltip: {
	            formatter: function () {
	                return '<b>' + this.series.name + '</b><br/>' +
	                    this.x + ': ' + this.y;
	            }
	        },
	        plotOptions: {
	            area: {
	                fillOpacity: 0.5,
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        series: [{
	            name: 'Encuestas',
	            data: [20, 100, 50, 70, 30, 5, 20, 90,15,150,20,90]
	        }]
	    });
});



function grafico_tipo_pie(div=null,data=null,titulo){
  $('#grafico'+div).highcharts({
        chart: {
            renderTo: 'chartcontainer',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            
        },
        title: {
            text: titulo
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            colorByPoint: true,
            data: data
        }]
    });
}
function grafico_line(div,categories,data){
    $('#'+div).highcharts({
            chart: {
                type: 'area',
                spacingBottom: 30
            },
            title:{
                text:''
            },
            subtitle: {
                text: '',
                floating: true,
                align: 'right',
                verticalAlign: 'bottom',
                y: 15
            },
            xAxis: categories,
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.x + ': ' + this.y;
                }
            },
            plotOptions: {
                area: {
                    fillOpacity: 0.5,
                }
            },
            credits: {
                enabled: false
            },
            series: data
        });
}
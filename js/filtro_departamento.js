
          function departamento(id_departamento){
             $.ajax({
              type: "POST",
              url: base_url+"index.php?c=distritos&m=departamento",
              data: {id_departamento:id_departamento}
            }).done(function( data ) {
              $("select[name=id_departamento]").html(data);
            });
          }
        function provincia(id_departamento,id_provincia){
          $.ajax({
              type: "POST",
              url: base_url+"index.php?c=distritos&m=provincia",
              data: {id_departamento:id_departamento,id_provincia:id_provincia}
            }).done(function( data ) {
              $("select[name=id_provincia]").html(data);
            });
        }
        function distrito(id_provincia,id_distrito){
          $.ajax({
              type: "POST",
              url: base_url+"index.php?c=distritos&m=distrito",
              data: {id_provincia:id_provincia,id_distrito:id_distrito}
            }).done(function( data ) {
              $("select[name=id_distrito]").html(data);
            });
        }
          $("select[name=id_departamento]").change(function(){
            var departamento=$(this).val();
            provincia(departamento,null);
            $("select[name=id_distrito]").val("");
          });
          $("select[name=id_provincia]").change(function(){
            var provincia=$(this).val();
            distrito(provincia,null);
          });
        